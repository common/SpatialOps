#include <spatialops/Nebo.h>
#include <spatialops/structured/FVStaggeredFieldTypes.h>
#include <spatialops/structured/MatVecFields.h>
#include <spatialops/structured/MatVecOps.h>
#include <spatialops/structured/FieldComparisons.h>
#include <spatialops/structured/FieldHelper.h>
#include <test/TestHelper.h>
#include <spatialops/util/TimeLogger.h>
#include <spatialops/structured/test/MatrixRandomizers.h>

#include <boost/program_options.hpp>
#include <iostream>
#include <numeric>
#include <stdlib.h>
#include <string>
#include <utility>

namespace po = boost::program_options;

using namespace SpatialOps;

template<typename T, size_t S>
size_t array_size(T(&)[S]) { return S; };

//==============================================================================

template<typename FieldT>
void generate_matrix_with_eigen( const typename FieldT::value_type * const eigenValues,
                                 const size_t eigenSize,
                                 FieldMatrix<FieldT>& M,
                                 FieldMatrix<FieldT>& scratchM,
                                 FieldVector<FieldT>& scratchV,
                                 SpatFldPtr<FieldT>&  scratch,
                                 const short int deviceIndex )
{
  assert( !scratch.isnull() );
  assert( eigenSize == M.elements() );
  assert( M.elements() == scratchM.elements() && scratchM.elements() == scratchV.elements() );

  random_matrix( scratchM, MatrixTypes::LINEARLY_INDEPENDENT, deviceIndex, scratch );

  const size_t elements = M.elements();

  //Multiply by transpose scaled by eigenValues
  size_t m = elements;
  size_t k = elements;
  size_t n = elements;
  size_t kn = k*n;
  size_t mk = m*k;

  while( n-- ){
    k = elements;
    while( k-- ){
      scratchV.at(k) <<= eigenValues[k] * scratchM.at(--kn);
    }
    m  = elements;
    mk = elements*elements;
    while( m-- ){
      FieldT& res = M.at(m*elements + n);
      res <<= 0;
      k = elements;
      while( k-- ){
        res <<= res + scratchM.at(--mk) * scratchV.at(k);
      }
    }
  }
}

template<typename FieldT>
void generate_matrix_with_complex_eigen( const typename FieldT::value_type * const eigenValues,
                                         const size_t eigenSize,
                                         FieldMatrix<FieldT>& M,
                                         FieldMatrix<FieldT>& scratchM,
                                         FieldVector<FieldT>& scratchV,
                                         SpatFldPtr<FieldT>&  scratch,
                                         const short int deviceIndex )
{
  assert( !scratch.isnull() );
  assert( M.elements() % 2 == 0 );  //Can only generate matrices of 2n x 2n
  assert( eigenSize == (M.elements()/2) );  //Complex comes in +- pairs
  assert( M.elements() == scratchM.elements() && scratchM.elements() == scratchV.elements() );

  const size_t elements = M.elements();

  size_t m = elements;
  size_t twiceN = elements;

  //Assign eigen values on block diagonal top right and bottom left (block size 2)
  m = elements;
  twiceN = elements;
  while( m-- ){
    twiceN = elements;
    while( twiceN-- ){
      FieldT& res = M.at(m*elements + twiceN);

      //Set top right block value to positive eigenvalue
      if( m % 2 == 0 && twiceN == m + 1 )
      {
        res <<= eigenValues[(m/2)];
      }
      else if( m % 2 == 1 && twiceN == m - 1 )
      {
        res <<= -eigenValues[(m/2)];
      }
      else
      {
        res <<= 0;
      }
    }
  }
}

//==============================================================================

template<typename FieldT>
bool matrix_eigen_tests( FieldMatrix<FieldT>& A,
                         FieldMatrix<FieldT>& B,
                         FieldVector<FieldT>& x,
                         FieldVector<FieldT>& b,
                         FieldT& compare,
                         SpatFldPtr<FieldT>& scratch,
                         const bool noisyOn,
                         const size_t length,
                         const MemoryWindow w,
                         const short int deviceIndex )
{
    TestHelper noisy( noisyOn );
    {
      std::pair<std::pair<MatrixTypes_e, bool>, char const *> validMatrices[] = {
      std::make_pair(std::make_pair(MatrixTypes::RANDOM   , true), "Random"),
      std::make_pair(std::make_pair(MatrixTypes::DIAGONAL , false),"Diagonal"),
      std::make_pair(std::make_pair(MatrixTypes::ZERO     , false),"Zero"),
      std::make_pair(std::make_pair(MatrixTypes::NILPOTENT, false),"Nilpotent"),
    };
    const size_t typeSize = array_size(validMatrices);
    for( size_t typeI = 0; typeI < typeSize; typeI++ ){
      TestHelper noisyType( noisyOn );
      MatrixTypes_e matrixType    = validMatrices[typeI].first.first;
      bool          generateEigen = validMatrices[typeI].first.second;
      char const *  description   = validMatrices[typeI].second;

      srand(10);
      const size_t matricesToTest = 10;
      for( size_t testMatrixIndex = 0; testMatrixIndex < matricesToTest; testMatrixIndex++ ){
        TestHelper noisyDeep( noisyOn );

        //Generate Matrix A and Eigen Values in b
        if( generateEigen ){
          typename FieldT::value_type eigenVals[length];
          for( size_t eIndex = 0; eIndex < length; eIndex++){
            eigenVals[eIndex] = (static_cast<typename FieldT::value_type>(rand())/RAND_MAX)*MatrixRandomizer<FieldT>::get_magnitude();
#           ifndef NDEBUG
            for( size_t checkIndex = 0; checkIndex < eIndex; checkIndex++ ){
              assert(eigenVals[eIndex] != eigenVals[checkIndex]);
            }
#           endif /* NDEBUG */
          }

          generate_matrix_with_eigen(&eigenVals[0], length, A, B, b, scratch, deviceIndex);
          for( size_t eIndex = 0; eIndex < length; eIndex++ ){
            b.at(eIndex) <<= eigenVals[eIndex];
          }
        }
        else{
          random_matrix(A, matrixType, deviceIndex, scratch);
          for( size_t eIndex = 0; eIndex < length; eIndex++ ){
            b.at(eIndex) = A.at(eIndex*length+eIndex);
          }
        }

        x = A.real_eigenvalues();

        //sort result
        typename FieldT::value_type buffer[length];
        x.set_device_as_active(CPU_INDEX);
        for( size_t i = 0; i < w.glob_npts(); i++ ){
          for( size_t j = 0; j < length; j++ ){
            buffer[j] = x.at(j)[i];
          }
          std::sort(buffer, buffer+length);
          for( size_t j = 0; j < length; j++){
            x.at(j)[i] = buffer[j];
          }
        }
        x.set_device_as_active(deviceIndex);
        //sort eigen compare
        b.set_device_as_active(CPU_INDEX);
        for( size_t i = 0; i < w.glob_npts(); i++ ){
          for( size_t j = 0; j < length; j++ ){
            buffer[j] = b.at(j)[i];
          }
          std::sort(buffer, buffer+length);
          for( size_t j = 0; j < length; j++ ){
            b.at(j)[i] = buffer[j];
          }
        }
        b.set_device_as_active(deviceIndex);

        //verify result
        for( size_t i = 0; i < length; i++ ){
          std::ostringstream msg;
          msg << "correct sorted eigen val #" << i;
          noisyDeep(field_equal(x(i), b(i), 1e-4), msg.str());
        }

        std::ostringstream msg;
        msg << "eigen values of matrix #" << testMatrixIndex;
        noisyType( noisyDeep.ok(), msg.str() );
      }

      std::ostringstream msg;
      msg << "Eigen matrix of type " << description;
      noisy( noisyType.ok(), msg.str() );
    }
  }


  return noisy.ok();
}

template<typename FieldT>
bool matrix_complex_eigen_tests( FieldMatrix<FieldT>& A,
                                 FieldMatrix<FieldT>& B,
                                 FieldVector<FieldT>& x,
                                 FieldVector<FieldT>& b,
                                 FieldVector<FieldT>& c,
                                 FieldT& compare,
                                 SpatFldPtr<FieldT>& scratch,
                                 const bool noisyOn,
                                 const size_t length,
                                 const MemoryWindow w,
                                 const short int deviceIndex )
{
    TestHelper noisy( noisyOn );
    {
      std::pair<std::pair<MatrixTypes_e, bool>, char const *> validMatrices[] = {
        std::make_pair(std::make_pair(MatrixTypes::RANDOM   , true), "Random"),
      };
      const size_t typeSize = array_size(validMatrices);
      for( size_t typeI = 0; typeI < typeSize; typeI++ ){
        TestHelper noisyType( noisyOn );
        MatrixTypes_e matrixType    = validMatrices[typeI].first.first;
        bool          generateEigen = validMatrices[typeI].first.second;
        char const *  description   = validMatrices[typeI].second;

        srand(10);
        const size_t matricesToTest = 10;
        for( size_t testMatrixIndex = 0; testMatrixIndex < matricesToTest; testMatrixIndex++ ){
          TestHelper noisyDeep( noisyOn );

          //Generate Matrix A and Eigen Values in b
          if( generateEigen ){
            const size_t eigenSize = length/2;
            typename FieldT::value_type eigenVals[eigenSize];
            for( size_t eIndex = 0; eIndex < eigenSize; eIndex++){
              eigenVals[eIndex] = (static_cast<typename FieldT::value_type>(rand())/RAND_MAX)*MatrixRandomizer<FieldT>::get_magnitude();
#             ifndef NDEBUG
              for( size_t checkIndex = 0; checkIndex < eIndex; checkIndex++ ){
                assert(eigenVals[eIndex] != eigenVals[checkIndex]);
              }
#             endif /* NDEBUG */
            }

            generate_matrix_with_complex_eigen(&eigenVals[0], eigenSize, A, B, b, scratch, deviceIndex);
            for( size_t eIndex = 0; eIndex < eigenSize; eIndex++ ){
              b.at(eIndex) <<= eigenVals[eIndex];
            }
            for( size_t eIndex = 0; eIndex < eigenSize; eIndex++ ){
              b.at(eigenSize + eIndex) <<= -eigenVals[eIndex];
            }
          }
          else{
            //DEPRECIATED: only a diagonal matrix of complex values would have
            //complex eigen values on the diagonal, which we cannot do using
            //only a real matrix
            random_matrix(A, matrixType, deviceIndex, scratch);
            for( size_t eIndex = 0; eIndex < length; eIndex++ ){
              b.at(eIndex) = A.at(eIndex*length+eIndex);
            }
          }

          A.eigenvalues(x, c);

          //sort real result
          typename FieldT::value_type buffer[length];
          x.set_device_as_active(CPU_INDEX);
          for( size_t i = 0; i < w.glob_npts(); i++ ){
            for( size_t j = 0; j < length; j++ ){
              buffer[j] = x.at(j)[i];
            }
            std::sort(buffer, buffer+length);
            for( size_t j = 0; j < length; j++){
              x.at(j)[i] = buffer[j];
            }
          }
          x.set_device_as_active(deviceIndex);
          //sort complex result
          c.set_device_as_active(CPU_INDEX);
          for( size_t i = 0; i < w.glob_npts(); i++ ){
            for( size_t j = 0; j < length; j++ ){
              buffer[j] = c.at(j)[i];
            }
            std::sort(buffer, buffer+length);
            for( size_t j = 0; j < length; j++){
              c.at(j)[i] = buffer[j];
            }
          }
          c.set_device_as_active(deviceIndex);
          //sort eigen compare
          b.set_device_as_active(CPU_INDEX);
          for( size_t i = 0; i < w.glob_npts(); i++ ){
            for( size_t j = 0; j < length; j++ ){
              buffer[j] = b.at(j)[i];
            }
            std::sort(buffer, buffer+length);
            for( size_t j = 0; j < length; j++ ){
              b.at(j)[i] = buffer[j];
            }
          }
          b.set_device_as_active(deviceIndex);

          //verify result
          for( size_t i = 0; i < length; i++ ){
            std::ostringstream msg;
            msg << "correct sorted eigen val #" << i;
            noisyDeep(field_equal(c(i), b(i), 1e-4), msg.str());
          }
          for( size_t i = 0; i < length; i++ ){
            std::ostringstream msg;
            msg << "correct real eigen val #" << i;
            noisyDeep(field_equal(0, x(i), 1e-4), msg.str());
          }

          std::ostringstream msg;
          msg << "eigen values of matrix #" << testMatrixIndex;
          noisyType( noisyDeep.ok(), msg.str() );
        }

        std::ostringstream msg;
        msg << "Eigen matrix of type " << description;
        noisy( noisyType.ok(), msg.str() );
      }
  }

  return noisy.ok();
}

//==============================================================================

template<typename FieldT>
bool matrix_solve_test_helper( MatrixTypes_e type,
                               FieldMatrix<FieldT>& A,
                               FieldVector<FieldT>& x,
                               FieldVector<FieldT>& b,
                               FieldT& compare,
                               SpatFldPtr<FieldT>& scratch,
                               const bool noisyOn,
                               const size_t length,
                               const short int deviceIndex,
                               const bool synthesizeb )
{
  TestHelper noisyType( noisyOn );
  std::vector<double> xvals(length); // make up a solution and compare later
  int j=length;
  while( j-- ){
    xvals[j] = j + 1.0 ;
    x(j) <<= xvals[j];
  }
  srand(10);
  random_matrix(A, type, deviceIndex, scratch);
  if( synthesizeb ){
    b = A * x; // synthesize "b"
  }
  x = 0.0; // "forget" our solution
  j=length;
  while( j-- ){
    assert( field_equal(0.0, x(j), 1e-4) );
  }
  x = A.solve(b);
  j=length;
  while( j-- ){
    compare <<= xvals[j];
    noisyType(field_equal(x(j), compare, 1e-4), "correct x value");
  }

  return noisyType.ok();
}

//==============================================================================

template<typename FieldT>
bool matrix_solve_tests( FieldMatrix<FieldT>& A,
                         FieldVector<FieldT>& x,
                         FieldVector<FieldT>& b,
                         FieldT& compare,
                         SpatFldPtr<FieldT>& scratch,
                         const bool noisyOn,
                         const size_t length,
                         const short int deviceIndex )
{
  TestHelper noisy( noisyOn );
  {
    MatrixTypes_e validMatrices[] = {
      MatrixTypes::RANDOM,
      MatrixTypes::SYMMETRIC,
      MatrixTypes::POSITIVE_DEFINITE,
      MatrixTypes::DIAGONAL,
    };
    const size_t typeSize = array_size(validMatrices);
    for( size_t typeI = 0; typeI < typeSize; typeI++ ){
      const bool ret = matrix_solve_test_helper( validMatrices[typeI],
                                                 A,
                                                 x,
                                                 b,
                                                 compare,
                                                 scratch,
                                                 noisyOn,
                                                 length,
                                                 deviceIndex,
                                                 true );
      {
        std::ostringstream msg;
        msg << "Random matrix of type "
            << MatrixTypes::matrix_type_to_string(validMatrices[typeI]);
        noisy( ret, msg.str() );
      }
    }
  }
# ifndef NDEBUG
  //Technically the solver doesn't support these kinds of matrices but we test
  //them anyway. If these stop working it is okay to remove them.
  {
    MatrixTypes_e multipleSolutionMatrices[] = {
      MatrixTypes::ZERO,
      MatrixTypes::ZERO_LAST_ROW,
    };
    const size_t typeSize = array_size(multipleSolutionMatrices);
    for( size_t typeI = 0; typeI < typeSize; typeI++ ){
      bool pass = false;
      try{
        matrix_solve_test_helper( multipleSolutionMatrices[typeI],
                                  A,
                                  x,
                                  b,
                                  compare,
                                  scratch,
                                  noisyOn,
                                  length,
                                  deviceIndex,
                                  true );
      }
      catch(...){
        pass = true;
      }

      {
        std::ostringstream msg;
        msg << "Expected failure of random matrix of type "
            << MatrixTypes::matrix_type_to_string(multipleSolutionMatrices[typeI]);
        noisy( pass, msg.str() );
      }
    }
  }
  //Technically the solver doesn't support these kinds of matrices but we test
  //them anyway. If these stop working it is okay to remove them.
  {
    MatrixTypes_e noSolutionMatrices[] = {
      MatrixTypes::ZERO,
      MatrixTypes::ZERO_LAST_ROW,
    };
    const size_t typeSize = array_size(noSolutionMatrices);
    for( size_t typeI = 0; typeI < typeSize; typeI++ ){
      //Instantiate b such that a row of 0's cannot equal it
      int i = length;
      while( i-- ){
        b(i) <<= 1+(rand()%100);  // put pseudo-random numbers in b
      }
      bool pass = false;
      try{
        matrix_solve_test_helper( noSolutionMatrices[typeI],
                                  A,
                                  x,
                                  b,
                                  compare,
                                  scratch,
                                  noisyOn,
                                  length,
                                  deviceIndex,
                                  false );
      }
      catch(...){
        pass = true;
      }

      {
        std::ostringstream msg;
        msg << "Expected failure of random matrix of type "
            << MatrixTypes::matrix_type_to_string(noSolutionMatrices[typeI])
            << " with no possible solution";
        noisy( pass, msg.str() );
      }
    }
  }
# endif // NDEBUG

  return noisy.ok();
}

//==============================================================================

bool drive_test( const IntVec dim, const int length, const bool verbose )
{
  typedef SVolField FieldT;
  TestHelper quiet( true );
  const bool noisyOn = verbose;
  const GhostData ghost(1);
  // Boundary present if direction is active
  const IntVec hasBC(dim[0]>1,dim[1]>1,dim[2]>1);
  const BoundaryCellInfo bc = BoundaryCellInfo::build<FieldT>(hasBC,hasBC);
  const MemoryWindow w( get_window_with_ghost(dim,ghost,bc) );

  // Get number of device types to check
  const size_t deviceTypeCount = 1  //CPU count
#                              ifdef __CUDACC__
                               + 1  //GPU count
#                              endif /* __CUDACC__ */
    ;

  std::pair<int, const char *> deviceTypes[deviceTypeCount];
  {
    size_t deviceCount = 0;
    deviceTypes[deviceCount]   = std::make_pair(CPU_INDEX, "CPU");
#   ifdef __CUDACC__
    deviceTypes[++deviceCount] = std::make_pair(GPU_INDEX, "GPU");
#   endif /* __CUDACC__ */
  }

  short int deviceIndex;
  for( size_t i = 0; i < deviceTypeCount; i++ ){
    TestHelper quietDevice( true );
    deviceIndex = deviceTypes[i].first;
    SpatFldPtr<FieldT> scratch( new FieldT(w,bc,ghost, NULL, InternalStorage, CPU_INDEX) );
    FieldMatrix<FieldT> A(length,w,bc,ghost, deviceIndex ),
                        B(length,w,bc,ghost, deviceIndex ),
                        C(length,w,bc,ghost, deviceIndex );
    FieldVector<FieldT> b(length,w,bc,ghost, deviceIndex ),
                        x(length,w,bc,ghost, deviceIndex ),
                        y(length,w,bc,ghost, deviceIndex );
    FieldT offset ( w, bc, ghost, NULL, InternalStorage, deviceIndex  ),
           compare( w, bc, ghost, NULL, InternalStorage, deviceIndex  );
    int dx = 1;
    offset <<= sqrt(dx);

    scratch->add_device(deviceIndex);

    A.add_device(CPU_INDEX);
    B.add_device(CPU_INDEX);
    C.add_device(CPU_INDEX);
    b.add_device(CPU_INDEX);
    x.add_device(CPU_INDEX);
    y.add_device(CPU_INDEX);
    offset.add_device(CPU_INDEX);
    compare.add_device(CPU_INDEX);

    // test assigning to an element directly
    {
      b(0) <<= 5.0;
      compare <<= 5.0;
      quietDevice(field_equal(b(0), compare, 1e-4), "vector(0) <<= scalar assignment"); // vector
      A(0) <<= 1.0;
      compare <<= 1.0;
      quietDevice(field_equal(A(0), compare, 1e-4), "matrix(0) <<= scalar assignment"); // matrix
    }

    // test the vector = scalar operator
    const double c = 2.1;
    {
      TestHelper noisy(noisyOn);
      b = c; // this assigns double c to all fields in vector b
      int j = length;
      while( j-- ){
        compare <<= c;
        noisy( field_equal(b(j), compare, 1e-4), "vector = scalar assignment" );
      }
      quietDevice( noisy.ok(), "vector = scalar assignment" );
    }

    // iterate through vector and assign elements separately
    {
      TestHelper noisy(noisyOn);
      int j = length;
      while( j-- ){
        x(j) <<= c;
        compare <<= c;
        noisy( field_equal(x(j), compare, 1e-4), "iterative vector assignment" );
      }
      quietDevice( noisy.ok(), "iterative vector assignment" );
    }

    // iterate through matrix
    {
      TestHelper noisy(noisyOn);
      int ij = length * length;
      while( ij-- ){
        A(ij) <<= c + 1;
        compare <<= c + 1;
        noisy( field_equal(A(ij), compare, 1e-4), "iterative matrix assignment" );
      }
      quietDevice( noisy.ok(), "iterative matrix assignment" );
    }

    // test dot product of b `dot` x
    {
      dot_product_proto(b,x).eval(&A(0)); // store in A(0) for convenience
      int j=length;
      compare <<= 0;
      while( j-- ){
        compare <<= compare + b(j) * x(j);
      }
      quietDevice( field_equal(A(0), compare, 1e-4), "dot product" );
    }

    // test the ability to multiply a vector by a scalar
    {
      TestHelper noisy( noisyOn );
      srand(10);
      const double c = 2.1;
      int i = length;
      while( i-- ){
        x(i) <<= offset*(rand()%100);   // put pseudo-random numbers in x
      }
      b = c*x; // use b as storage for scalar mat multiply as convenience
      i = length;
      while( i-- ){
        compare <<= c*x(i);
        noisy(field_equal(b(i), compare, 1e-4), "correct b value");
      }
      quietDevice( noisy.ok(), "vector scalar multiply" );
    }

    // test the ability to multiply a matrix by a scalar
    {
      TestHelper noisy( noisyOn );
      srand(10);
      const double c = 2.1;
      int ij = length*length;
      while( ij-- ){
        A(ij) <<= offset*(rand()%100);   // put pseudo-random numbers in A
      }
      B = c * A; // use B as storage for mat scalar multiply as convenience
      ij = length*length;
      while( ij-- ){
        compare <<= c*A(ij);
        noisy(field_equal(B(ij), compare, 1e-4), "correct B value");
      }
      quietDevice( noisy.ok(), "matrix scalar multiply" );
    }

    // test the ability to add a vector to a vector
    {
      TestHelper noisy( noisyOn );
      srand(10);
      int i = length;
      while( i-- ){
        x(i) <<= offset*(rand()%100);   // put pseudo-random numbers in x
      }
      i = length;
      while( i-- ){
        y(i) <<= offset*(rand()%100);   // put pseudo-random numbers in y
      }
      b = x + y; // use b as storage for vec vec addition as convenience
      i = length;
      while( i-- ){
        compare <<= x(i)+y(i);
        noisy(field_equal(b(i), compare, 1e-4), "correct b value");
      }
      quietDevice( noisy.ok(), "vector vector addition" );
    }

    // test the ability to add a matrix to a matrix
    {
      TestHelper noisy( noisyOn );
      srand(10);
      int ij = length*length;
      while( ij-- ){
        A(ij) <<= offset*(rand()%100);   // put pseudo-random numbers in A
      }
      ij=length*length;
      while( ij-- ){
        B(ij) <<= offset*(rand()%100);   // put pseudo-random numbers in B
      }
      C = A + B; // use C as storage for mat mat addition as convenience
      ij = length*length;
      while( ij-- ){
        compare <<= A(ij)+B(ij);
        noisy(field_equal(C(ij), compare, 1e-4), "correct C value");
      }
      quietDevice( noisy.ok(), "matrix matrix addition" );
    }

    // test the ability to subtract a vector to a vector
    {
      TestHelper noisy( noisyOn );
      srand(10);
      int i = length;
      while( i-- ){
        x(i) <<= offset*(rand()%100);   // put pseudo-random numbers in x
      }
      i = length;
      while( i-- ){
        y(i) <<= offset*(rand()%100);   // put pseudo-random numbers in y
      }
      b = x - y; // use b as storage for vec vec subtraction as convenience
      i = length;
      while( i-- ){
        compare <<= x(i)-y(i);
        noisy(field_equal(b(i), compare, 1e-4), "correct b value");
      }
      quietDevice( noisy.ok(), "vector vector subtraction" );
    }

    // test the ability to subtract a matrix to a matrix
    {
      TestHelper noisy( noisyOn );
      srand(10);
      int ij = length*length;
      while( ij-- ){
        A(ij) <<= offset*(rand()%100);   // put pseudo-random numbers in A
      }
      ij=length*length;
      while( ij-- ){
        B(ij) <<= offset*(rand()%100);   // put pseudo-random numbers in B
      }
      C = A - B; // use C as storage for mat mat subtraction as convenience
      ij = length*length;
      while( ij-- ){
        compare <<= A(ij)-B(ij);
        noisy(field_equal(C(ij), compare, 1e-4), "correct C value");
      }
      quietDevice( noisy.ok(), "matrix matrix subtraction" );
    }


    // test the ability to multiply a matrix by a vector
    {
      TestHelper noisy( noisyOn );
      srand(10);
      int ij = length*length;
      while( ij-- ){
        A(ij) <<= offset*(rand()%100);   // put pseudo-random numbers in A
      }
      int j=length;
      while( j-- ){
        x(j) <<= offset*(rand()%100);   // put pseudo-random numbers in x
      }
      b = A * x; // use b as storage for mat vec multiply as convenience
      ij = length*length;
      while( ij ){
        j=length;
        compare <<= 0;
        while( j ){
          compare <<= compare+A(--ij)*x(--j);
        }
        noisy(field_equal(b(ij/length), compare, 1e-4), "correct b value");
      }
      quietDevice( noisy.ok(), "matrix vector multiply" );
    }

    //// test the ability to multiply a matrix by a matrix
    {
      TestHelper noisy( noisyOn );
      srand(10);
      int ij = length*length;
      while( ij-- ){
        A(ij) <<= offset*(rand()%100);   // put pseudo-random numbers in A
      }
      ij = length*length;
      while( ij-- ){
        B(ij) <<= offset*(rand()%100);   // put pseudo-random numbers in B
      }
      C = A * B; // use C as storage for mat mat multiply as convenience

      for(size_t columns = 0; columns < length; columns++)
      {
        size_t ij = 0;
        for(size_t CRow = 0; CRow < length; CRow++)
        {
          compare <<= 0;
          for(size_t BRow = 0; BRow < length; BRow++)
          {
            compare <<= compare+A(ij++)*B(columns + BRow*length);
          }
          noisy(field_equal(C(columns + CRow*length), compare, 1e-4), "correct C value");
        }
      }
      quietDevice( noisy.ok(), "matrix matrix multiply" );
    }

    // test the ability to offset a matrix's diagonal by c
    {
      TestHelper noisy( noisyOn );
      srand(10);
      const double c = 2.1;
      int ij = length*length;
      while( ij-- ){
        A(ij) <<= offset*(rand()%100);   // put pseudo-random numbers in A
        B(ij) <<= A(ij);                 // create a copy of A
      }
      B.add_to_diagonal(c); // use B as storage for diagonal offset as convenience
      ij = length*length;
      while( ij-- ){
        if(ij/length == ij%length)
          compare <<= c + A(ij);
        else
          compare <<= A(ij);
        noisy(field_equal(B(ij), compare, 1e-4), "correct B value");
      }
      quietDevice( noisy.ok(), "matrix scalar diagonal offset" );
    }

    // test the ability to solve x = inv( A ) * b
    {
      const bool result = matrix_solve_tests( A,
                                              x,
                                              b,
                                              compare,
                                              scratch,
                                              noisyOn,
                                              length,
                                              deviceIndex );
      quietDevice( result, "pointwise matrix solve" );
    }

    // test only real eigenvalues randomly
    {
      const bool result = matrix_eigen_tests( A,
                                              B,
                                              x,
                                              b,
                                              compare,
                                              scratch,
                                              noisyOn,
                                              length,
                                              w,
                                              deviceIndex );
      quietDevice( result, "only real eigen values of matrix"  );
    }

    // test only complex eigenvalues randomly
    if( length % 2 == 0 ) {
      const bool result = matrix_complex_eigen_tests( A,
                                                      B,
                                                      x,
                                                      b,
                                                      y,
                                                      compare,
                                                      scratch,
                                                      noisyOn,
                                                      length,
                                                      w,
                                                      deviceIndex );
      quietDevice( result, "only complex eigen values of matrix"  );
    }

    //print results of device run
    quiet(quietDevice.ok(), deviceTypes[i].second);
  }

  return quiet.ok();
}

//==============================================================================

double drive_perf( const IntVec listSize,
                   const size_t matrixDim,
                   const size_t iterationsToAverage,
                   const size_t burnInIterations,
                   const short int deviceIndex )
{
  typedef SVolField FieldT;
  TestHelper quiet( true );
  const GhostData ghost(1);
  // Boundary present if direction is active
  const IntVec hasBC(listSize[0]>1,listSize[1]>1,listSize[2]>1);
  const BoundaryCellInfo bc = BoundaryCellInfo::build<FieldT>(hasBC,hasBC);
  const MemoryWindow w( get_window_with_ghost(listSize,ghost,bc) );

  SpatFldPtr<FieldT> scratch( new FieldT(w,bc,ghost, NULL, InternalStorage, CPU_INDEX) );
  FieldMatrix<FieldT> A( matrixDim,w,bc,ghost, deviceIndex ),
                      B( matrixDim,w,bc,ghost, deviceIndex );
  FieldVector<FieldT> b( matrixDim,w,bc,ghost, deviceIndex ),
                      x( matrixDim,w,bc,ghost, deviceIndex );

  scratch->add_device(deviceIndex);

  double totalTime = 0.0;
  size_t currentIteration;
  double tmpIterationTime;
  std::cout << "Performance Test of matrices of dimension "
            << matrixDim << "x" << matrixDim
            << " and list size " << listSize
            << std::endl;

  // test dot product of b `dot` x
  currentIteration = iterationsToAverage + burnInIterations;
  tmpIterationTime = 0.0;
  while( currentIteration-- ){
    int j=matrixDim;
    while( j-- ){
      b(j) <<= (rand()%100);  // put pseudo-random numbers in b
    }
    j=matrixDim;
    while( j-- ){
      x(j) <<= (rand()%100);  // put pseudo-random numbers in x
    }

    Timer timer;
#ifdef __CUDACC__
    cudaDeviceSynchronize();
#endif
    timer.reset();
    dot_product_proto(b,x).eval(&A(0)); // store in A(0) for convenience
#ifdef __CUDACC__
    cudaDeviceSynchronize();
#endif
    const double time = timer.stop();
    tmpIterationTime += currentIteration < iterationsToAverage ? time : 0;
  }
  totalTime += tmpIterationTime/iterationsToAverage;
  std::cout << '\t' << "Dot Product average time: " << tmpIterationTime/iterationsToAverage << std::endl;

  // test the ability to multiply a matrix by a vector
  currentIteration = iterationsToAverage + burnInIterations;
  tmpIterationTime = 0.0;
  while( currentIteration-- ){
    srand(10);
    random_matrix( A, MatrixTypes::RANDOM, deviceIndex, scratch );
    int j=matrixDim;
    while( j-- ){
      x(j) <<= (rand()%100);  // put pseudo-random numbers in x
    }

    Timer timer;
#ifdef __CUDACC__
    cudaDeviceSynchronize();
#endif
    timer.reset();
    b = A * x; // use b as storage for mat vec multiply as convenience
#ifdef __CUDACC__
    cudaDeviceSynchronize();
#endif
    const double time = timer.stop();
    tmpIterationTime += currentIteration < iterationsToAverage ? time : 0;
  }
  totalTime += tmpIterationTime/iterationsToAverage;
  std::cout << '\t' << "Matrix Multiply By Vector average time: " << tmpIterationTime/iterationsToAverage << std::endl;

  // test the ability to solve x = inv( A ) * b
  srand(10);
  currentIteration = iterationsToAverage + burnInIterations;
  tmpIterationTime = 0.0;
  while(currentIteration--)
  {
    int j=matrixDim;
    while( j-- ){
      x(j) <<= (rand()%100);   // put pseudo-random numbers in x
    }
    random_matrix( A, MatrixTypes::RANDOM, deviceIndex, scratch );
    b = A * x; // synthesize "b"
    x = 0.0; // "forget" our solution

    Timer timer;
#ifdef __CUDACC__
    cudaDeviceSynchronize();
#endif
    timer.reset();
    x = A.solve(b);
#ifdef __CUDACC__
    cudaDeviceSynchronize();
#endif
    const double time = timer.stop();
    tmpIterationTime += currentIteration < iterationsToAverage ? time : 0;
  }
  totalTime += tmpIterationTime/iterationsToAverage;
  std::cout << '\t' << "Linear Solve average time: " << tmpIterationTime/iterationsToAverage << std::endl;

  // test eigenvalues randomly
  srand(10);
  currentIteration = iterationsToAverage + burnInIterations;
  tmpIterationTime = 0.0;
  while( currentIteration-- ){
    typename FieldT::value_type eigenVals[matrixDim];
    for( size_t eIndex = 0; eIndex < matrixDim; eIndex++ ){
      eigenVals[eIndex] = (static_cast<typename FieldT::value_type>(rand())/RAND_MAX)*MatrixRandomizer<FieldT>::get_magnitude();
#     ifndef NDEBUG
      for(size_t checkIndex = 0; checkIndex < eIndex; checkIndex++){
        assert(eigenVals[eIndex] != eigenVals[checkIndex]);
      }
#     endif /* NDEBUG */
    }

    generate_matrix_with_eigen( &eigenVals[0], matrixDim, A, B, b, scratch, deviceIndex );
    Timer timer;
#ifdef __CUDACC__
    cudaDeviceSynchronize();
#endif
    timer.reset();
    x = A.real_eigenvalues();
#ifdef __CUDACC__
    cudaDeviceSynchronize();
#endif
    const double time = timer.stop();
    tmpIterationTime += currentIteration < iterationsToAverage ? time : 0;
  }
  totalTime += tmpIterationTime/iterationsToAverage;
  std::cout << '\t' << "Eigen Decomposition average time: " << tmpIterationTime/iterationsToAverage << std::endl;

  return totalTime;
}

//==============================================================================

int main(int argc, const char *argv[])
{
  po::options_description desc("Supported Options");
  desc.add_options()
    ( "help",    "print help message\n" )
    ( "timing",  "run timing tests instead of correctness tests" )
    ( "verbose", "enable verbose output" );

  po::variables_map args;
  po::store( po::parse_command_line(argc,argv,desc), args );
  po::notify(args);
  if( args.count("help") ){
    std::cout << desc << std::endl
      << "Examples:" << std::endl
      << " matrix_field --verbose" << std::endl
      << " matrix_field --timing" << std::endl
      << std::endl;
    return -1;
  }

  const bool toTime  = ( args.count("timing") > 0 );
  const bool verbose = ( args.count("verbose") > 0 );

  if( !toTime ){
    TestHelper status( true );

    const int lengths[] = { 1, 2, 3, 5, 10, 13 };

    for( int lengthIndex = 0; lengthIndex < array_size(lengths); lengthIndex++ ){
      std::ostringstream msg;
      const int length = lengths[lengthIndex];
      msg << length;
      const std::string l = msg.str();

      status( drive_test( IntVec(2,1,1),   length, verbose ), "Dimension: ( 2, 1, 1) tests. Length: " +l );
      status( drive_test( IntVec(1,2,1),   length, verbose ), "Dimension: ( 1, 2, 1) tests. Length: " +l );
      status( drive_test( IntVec(1,1,2),   length, verbose ), "Dimension: ( 1, 1, 2) tests. Length: " +l );
      status( drive_test( IntVec(2,2,2),   length, verbose ), "Dimension: ( 2, 2, 2) tests. Length: " +l );
      status( drive_test( IntVec(10,10,1), length, verbose ), "Dimension: (10,10, 1) tests. Length: " +l );
    }

    if( status.ok() ){
      return 0;
    }
    return -1;
  }
  else{
    const size_t dimsLow[] = { 8, 16, 32 };
    const size_t dimsLowSize = array_size(dimsLow);
    const size_t listSizeRootsHigh[] = { 16, 32, 64 };
    const size_t listSizeRootsHighSize = array_size(listSizeRootsHigh);

    const size_t dimsHigh[] = { 50, 100, 125 };
    const size_t dimsHighSize = array_size(dimsHigh);
    const size_t listSizeRootsLow[] = { 16 };
    const size_t listSizeRootsLowSize = array_size(listSizeRootsLow);

    const std::pair<std::pair<const size_t*, const size_t>, std::pair<const size_t*, const size_t> > testRunInfo[] = { 
      std::make_pair(std::make_pair(dimsLow,  dimsLowSize),  std::make_pair(listSizeRootsHigh, listSizeRootsHighSize)),
      std::make_pair(std::make_pair(dimsHigh, dimsHighSize), std::make_pair(listSizeRootsLow,  listSizeRootsLowSize)),
    };

    const size_t iterationsToAverage = 50;

    const size_t burnIn = 2;

    //Get number of device types to check
    const size_t deviceTypeCount = 1  //CPU count
#                                ifdef __CUDACC__
                                 + 1  //GPU count
#                                endif /* __CUDACC__ */
    ;

    std::pair<int, const char *> deviceTypes[deviceTypeCount];
    {
      size_t deviceCount = 0;
      deviceTypes[deviceCount]   = std::make_pair(CPU_INDEX, "CPU");
#     ifdef __CUDACC__
      deviceTypes[++deviceCount] = std::make_pair(GPU_INDEX, "GPU");
#     endif /* __CUDACC__ */
    }

    for( size_t testIndex = 0; testIndex < array_size(testRunInfo); testIndex++ ){
      for( size_t dimIndex = 0; dimIndex < testRunInfo[testIndex].first.second; dimIndex++ ){
        for( size_t listIndex = 0; listIndex < testRunInfo[testIndex].second.second; listIndex++ ){
          for( size_t deviceIndex = 0; deviceIndex < deviceTypeCount; deviceIndex++ ){
            const size_t dim = testRunInfo[testIndex].first.first[dimIndex];
            const size_t listSizeRoot = testRunInfo[testIndex].second.first[listIndex];
            const IntVec listSize(listSizeRoot,listSizeRoot,listSizeRoot);

            //Explicitly ignore this combination due to memory constraints
            if( !(dim == 32 && listSizeRoot == 64) ){
              std::cout << deviceTypes[deviceIndex].second << ": ";

              std::cout << "List Dimension: " << listSize << " tests."
                        << " Dim: " << dim << "x" << dim << "."
                        << " " << deviceTypes[deviceIndex].second << "."
                        << " Total Time on Average: " << drive_perf( listSize,
                                                                      dim,
                                                                      iterationsToAverage,
                                                                      burnIn,
                                                                      deviceTypes[deviceIndex].first )
                        << std::endl;
            }
          }
        }
      }
    }
  }
}
