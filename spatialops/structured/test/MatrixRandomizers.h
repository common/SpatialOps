#ifndef SpatialOps_MatrixRandomizers_h
#define SpatialOps_MatrixRandomizers_h

#include <spatialops/Nebo.h>
#include <spatialops/structured/MatVecFields.h>
#include <spatialops/structured/SpatialFieldStore.h>

#include <cassert>
#include <cstdlib>
#ifdef USE_EIGEN
#include <Eigen/Dense>  // Eigen library
#endif /* USE_EIGEN */

namespace SpatialOps
{
  namespace MatrixTypes
  {
    enum MatrixTypes_e{
      ZERO,
      ZERO_LAST_ROW,
      RANDOM,
      SYMMETRIC,
      POSITIVE_DEFINITE,
      UPPER_TRIANGULAR,
      LOWER_TRIANGULAR,
      REDUNDANT,
      DIAGONAL,
      LINEARLY_INDEPENDENT,
      NILPOTENT,
      ROTATION,
    };

    char const * matrix_type_to_string( const MatrixTypes_e type )
    {
      switch( type ){
      case ZERO:                 return "Zeroes";
      case ZERO_LAST_ROW:        return "Zeroes in the last row";
      case RANDOM:               return "Generic Random";
      case SYMMETRIC:            return "Symmetric";
      case POSITIVE_DEFINITE:    return "Positive Definite";
      case UPPER_TRIANGULAR:     return "Upper Triangular";
      case LOWER_TRIANGULAR:     return "Lower Triangular";
      case REDUNDANT:            return "Redundant Rows";
      case DIAGONAL:             return "Diagonal";
      case LINEARLY_INDEPENDENT: return "Linearly Independent";
      case NILPOTENT:            return "Nilpotent";
      case ROTATION:             return "Rotation";
      default:
        throw std::runtime_error( "Unrecognized matrix type\n" );
      }
    }
  }
  typedef MatrixTypes::MatrixTypes_e MatrixTypes_e;

  template<typename ValT>
  class MatrixList
  {
    public:
    MatrixList( size_t const m,
                size_t const n,
                size_t const length )
      : matrices_(length),
        m(m),
        n(n)
    {
      const size_t size = m*n;
      for(size_t i = 0; i < length; i++)
      { matrices_[i] = new ValT[size]; }
    }

    ~MatrixList( void )
    {
      const size_t length = matrices_.size();
      for(size_t i = 0; i < length; i++)
      { delete [] matrices_[i]; }
    }

    size_t M( void ) const { return m; }
    size_t N( void ) const { return n; }
    size_t length( void ) const { return matrices_.size(); }

    ValT*& operator[]( size_t const i )
    {
      assert( i < matrices_.size() );
      return matrices_[i];
    }
    ValT const * operator[]( size_t const i ) const
    {
      assert( i < matrices_.size() );
      return matrices_[i];
    }

    private:
    std::vector<ValT*> matrices_;
    size_t const m;
    size_t const n;
  };

  template<typename FieldT>
  class IMatrixRandomizer
  {
    public:
      virtual void operator()( FieldT& scratch,
                               size_t const ij,
                               size_t const dim ) = 0;
      virtual ~IMatrixRandomizer(){};
  };
  template<typename FieldT>
  class MatrixRandomizer : public IMatrixRandomizer<FieldT>
  {
  public:
    static double get_magnitude(){ return 100; }

    virtual ~MatrixRandomizer(){}

    virtual void operator()( FieldT& scratch,
                             size_t const ij,
                             size_t const dim )
    {
      assert( (is_same_type<typename FieldT::value_type, double>()) );
      size_t npts = scratch.window_with_ghost().glob_npts();
      while( npts-- ){
        scratch[npts] = (static_cast<double>(rand())/RAND_MAX)*get_magnitude();
      }
    }
  };
  template<typename FieldT>
  class MatrixZero : public MatrixRandomizer<FieldT>
  {
    public:
    MatrixZero( size_t const dim, size_t const firstRowToZero )
      : firstIndexToZero_(firstRowToZero*dim)
    {
      assert(firstRowToZero < dim);
    }

    virtual void operator()( FieldT& scratch,
                             size_t const ij,
                             size_t const dim )
    {
      if( firstIndexToZero_ <= ij ){
        size_t npts = scratch.window_with_ghost().glob_npts();
        while( npts-- ){
          scratch[npts] = 0;
        }
      }
      else{
        MatrixRandomizer<FieldT>::operator()(scratch, ij, dim);
      }
    }

    virtual ~MatrixZero() {};

    private:
    const size_t firstIndexToZero_;
  };

  template<typename FieldT>
  class MatrixSymmetricR : public MatrixRandomizer<FieldT>
  {
    public:
    MatrixSymmetricR( const FieldT& scratch )
      : MatrixRandomizer<FieldT>(),
        lower_(true),
        lastValue_(SpatialFieldStore::get<FieldT>(scratch, CPU_INDEX))
#       ifndef NDEBUG
        , onDiagonal_(false)
#       endif

    {}

    virtual void operator()( FieldT& scratch,
                             size_t const ij,
                             size_t const dim )
    {
      if( lower_ ){
        MatrixRandomizer<FieldT>::operator()(scratch, ij, dim);
        *lastValue_ <<= scratch;
      }
      else{
        //Ensure iteration is element then elements transpose
        assert( ij == ((lastIndex_%dim)*dim + lastIndex_/dim) );
        scratch <<= *lastValue_;
      }

      assert( !onDiagonal_ ? true : (ij/dim == ij%dim) );

      //If we are on a diagonal randomize the next value (also a diagonal)
      lower_ = (ij/dim == ij%dim) || !lower_;
#     ifndef NDEBUG
      lastIndex_ = ij;
      onDiagonal_ = (ij/dim == ij%dim);
#     endif
    }

    private:
    bool lower_;
    SpatFldPtr<FieldT> lastValue_;
#   ifndef NDEBUG
    size_t lastIndex_;
    bool onDiagonal_;
#   endif
  };

  template<typename FieldT>
  class MatrixPositiveDefiniteR : public MatrixSymmetricR<FieldT>
  {
    public:
    MatrixPositiveDefiniteR( const FieldT& scratch )
      : MatrixSymmetricR<FieldT>(scratch)
    {}

    virtual void operator()( FieldT& scratch,
                             size_t const ij,
                             size_t const dim )
    {
      MatrixSymmetricR<FieldT>::operator()(scratch, ij, dim);
      //A diagonally dominant symmetric matrix is positive definite
      if( ij/dim == ij%dim ){
        const typename FieldT::value_type diagonallyDominantMagnitude(MatrixRandomizer<FieldT>::get_magnitude()*(dim-1));
        size_t npts = scratch.window_with_ghost().glob_npts();
        while( npts-- ){
          scratch[npts] += diagonallyDominantMagnitude;
        }
      }
    }
  };

  template<typename FieldT>
  class MatrixTriangularR : public MatrixRandomizer<FieldT>
  {
    public:
    MatrixTriangularR( const bool upper )
      : MatrixRandomizer<FieldT>(), upper_(upper)
    {}

    virtual void operator()( FieldT& scratch,
                             size_t const ij,
                             size_t const dim )
    {
      const size_t row = ij/dim;
      const size_t col = ij%dim;
      if( (upper_ && row <= col) || (!upper_ && col <= row) ){
        MatrixRandomizer<FieldT>::operator()(scratch, ij, dim);
      }
      else{
        size_t npts = scratch.window_with_ghost().glob_npts();
        while( npts-- ){
          scratch[npts] = 0;
        }
      }
    }

    private:
    const bool upper_;
  };

  template<typename FieldT>
  class MatrixRedundantR : public MatrixRandomizer<FieldT>
  {
    public:
    MatrixRedundantR( const FieldT& scratch, size_t const dim, size_t const rowToCopy )
      : MatrixRandomizer<FieldT>(),
        redundantRow_(new SpatFldPtr<FieldT>[dim]),
        redundantRowInit_(new bool[dim]),
        lastRandomRowFirstIndex_(dim*rowToCopy)
    {
      assert(rowToCopy < dim);
      for( size_t i = 0; i < dim; i++ ){
        redundantRow_[i] = SpatialFieldStore::get<FieldT>(scratch, CPU_INDEX);
        redundantRowInit_[i] = false;
      }
    }

    virtual void operator()( FieldT& scratch,
                             size_t const ij,
                             size_t const dim )
    {
      bool randomize = true;
      bool store = false;
      const size_t indexInRow = ij%dim;
      if( lastRandomRowFirstIndex_ <= ij ){
        if( redundantRowInit_[indexInRow] == false ){
          store = true;
        }
        else{
          randomize = false;
          scratch <<= *redundantRow_[indexInRow];
        }
      }

      if( randomize ){
        MatrixRandomizer<FieldT>::operator()(scratch, ij, dim);
      }

      if(store){
        redundantRowInit_[indexInRow] = true;
        *redundantRow_[indexInRow] <<= scratch;
      }
    }

    virtual ~MatrixRedundantR()
    {
      delete[] redundantRow_;
      delete[] redundantRowInit_;
    }

    private:
    SpatFldPtr<FieldT>* redundantRow_;
    bool*               redundantRowInit_;
    size_t const lastRandomRowFirstIndex_;
  };

  template<typename FieldT>
  class MatrixDiagonalR : public MatrixRandomizer<FieldT>
  {
    public:
    //0 extraBands means just the main diagonal, 1 means main diagonal and first
    //diagonal above and below main, 2 means main diagonal, and 2 diagonals
    //above and below the main diagonal.
    MatrixDiagonalR( size_t const dim, size_t const extraBands )
      : MatrixRandomizer<FieldT>(), extraBands_(extraBands)
    {
      assert(extraBands_ <= dim/2);
    }

    virtual void operator()( FieldT& scratch,
                             size_t const ij,
                             size_t const dim )
    {
      const size_t rowIndex = ij / dim;
      const size_t colIndex = ij % dim;
      const size_t leftEnd = extraBands_ - ((rowIndex < extraBands_) ? (extraBands_-rowIndex) : 0);
      const size_t rightEnd = extraBands_ - ((rowIndex >= (dim-extraBands_)) ? (extraBands_-(dim-rowIndex)) : 0);
      if( rowIndex - leftEnd <= colIndex && colIndex <= rowIndex + rightEnd ){
        MatrixRandomizer<FieldT>::operator()(scratch, ij, dim);
      }
      else{
        size_t npts = scratch.window_with_ghost().glob_npts();
        while( npts-- ){
          scratch[npts] = 0;
        }
      }
    }

    private:
    const size_t extraBands_;
  };

  template<typename FieldT>
  class MatrixLinearlyIndependentR : public IMatrixRandomizer<FieldT>
  {
    public:
    MatrixLinearlyIndependentR( FieldMatrix<FieldT> const & m, size_t const dim )
      : nMatrices_(dim, dim, m.at(0).window_with_ghost().glob_npts())
    {
      MatrixRandomizer<FieldT> randomizer;
      FieldT single(MemoryWindow(IntVec(1, 1, 1)),
                    BoundaryCellInfo::build<FieldT>(),
                    GhostData(0),
                    NULL,
                    InternalStorage,
                    CPU_INDEX);
      const size_t length = nMatrices_.length();
      for( size_t l = 0; l < length; l++ ){
#       ifdef USE_EIGEN
          Eigen::MatrixXd mPt = Eigen::MatrixXd ( dim, dim );
          for( size_t i = 0; i < dim; i++ ){
            for( size_t j = 0; j < dim; j++ ){
              randomizer(single, i*dim + j, dim);
              mPt(i, j) = single[0];
            }
          }
          Eigen::HouseholderQR<Eigen::MatrixXd> qr(mPt);
          const Eigen::MatrixXd qPt(qr.householderQ());
          for( size_t i = 0; i < dim; i++ ){
            for( size_t j = 0; j < dim; j++ ){
              nMatrices_[l][i*dim+j] = qPt(i, j);
            }
          }
#       else
#       error //The QR decomposition to get an orthogonal matrix is only implemented in Eigen
#       endif /* USE_EIGEN */
      }

    }
    virtual void operator()( FieldT& scratch,
                             size_t const ij,
                             size_t const dim )
    {
      assert( scratch.window_with_ghost().glob_npts() == nMatrices_.length() );
      const size_t length = nMatrices_.length();
      for( size_t i = 0; i < length; i++ ){
        scratch[i] = nMatrices_[i][ij];
      }
    }
    private:
    MatrixList<typename FieldT::value_type> nMatrices_;
  };

  template<typename FieldT>
  class MatrixNilpotentR : public MatrixTriangularR<FieldT>
  {
    public:
    MatrixNilpotentR()
      : MatrixTriangularR<FieldT>(true)
    {}

    virtual void operator()( FieldT& scratch,
                             size_t const ij,
                             size_t const dim )
    {
      const size_t rowIndex = ij / dim;
      const size_t colIndex = ij % dim;
      if( rowIndex == colIndex ){
        size_t npts = scratch.window_with_ghost().glob_npts();
        while( npts-- ){
          scratch[npts] = 0;
        }
      }
      else{
        MatrixTriangularR<FieldT>::operator()(scratch, ij, dim);
      }
    }
  };

  template<typename FieldT>
  class MatrixRotationR : public IMatrixRandomizer<FieldT>
  {
    public:
    MatrixRotationR( FieldMatrix<FieldT> const & m, size_t const dim )
      : nMatrices_(dim, dim, m.at(0).window_with_ghost().glob_npts())
    {
      MatrixRandomizer<FieldT> randomizer;
      FieldT single(MemoryWindow(IntVec(1, 1, 1)),
                    BoundaryCellInfo::build<FieldT>(),
                    GhostData(0),
                    NULL,
                    InternalStorage,
                    CPU_INDEX);
      const size_t length = nMatrices_.length();
      for( size_t l = 0; l < length; l++ ){
#       ifdef USE_EIGEN
          Eigen::MatrixXd mPt = Eigen::MatrixXd ( dim, dim );
          for( size_t i = 0; i < dim; i++ ){
            for( size_t j = 0; j < dim; j++ ){
              randomizer(single, i*dim + j, dim);
              mPt(i, j) = single[0];
            }
          }
          Eigen::HouseholderQR<Eigen::MatrixXd> qr(mPt);
          //Generate random rotation matrix (may have det -1 instead of +1)
          const Eigen::MatrixXd qPt(Eigen::MatrixXd(qr.householderQ())*qr.matrixQR().diagonal().unaryExpr(std::ptr_fun(sgn)).asDiagonal());
          for( size_t i = 0; i < dim; i++ ){
            for( size_t j = 0; j < dim; j++ ){
              nMatrices_[l][i*dim+j] = qPt(i, j);
            }
          }
#       else
#       error //The QR decomposition is only implemented in Eigen
#       endif /* USE_EIGEN */
      }

    }
    virtual void operator()( FieldT& scratch,
                             size_t const ij,
                             size_t const dim )
    {
      assert( scratch.window_with_ghost().glob_npts() == nMatrices_.length() );
      const size_t length = nMatrices_.length();
      for( size_t i = 0; i < length; i++ ){
        scratch[i] = nMatrices_[i][ij];
      }
    }

    static typename FieldT::value_type sgn(typename FieldT::value_type val)
    {
      return (typename FieldT::value_type(0) < val) - (val < typename FieldT::value_type(0));
    }

    private:
    MatrixList<typename FieldT::value_type> nMatrices_;
  };

  template<typename FieldT>
  void random_matrix( FieldMatrix<FieldT>& m,
                      const MatrixTypes_e type,
                      const short int deviceIndex,
                      SpatFldPtr<FieldT> sP = SpatFldPtr<FieldT>() )
  {
    if( sP.isnull() ){
      sP = SpatialFieldStore::get<FieldT>(m.at(0), CPU_INDEX);
    }

    const int dim = m.elements();

    IMatrixRandomizer<FieldT>* randomizer = NULL;
    switch( type ){
      case MatrixTypes::ZERO:
        randomizer = new MatrixZero<FieldT>(dim, 0);
        break;
      case MatrixTypes::ZERO_LAST_ROW:
        randomizer = new MatrixZero<FieldT>(dim, dim-1);
        break;
      case MatrixTypes::RANDOM:
        randomizer = new MatrixRandomizer<FieldT>();
        break;
      case MatrixTypes::SYMMETRIC:
        randomizer = new MatrixSymmetricR<FieldT>(*sP);
        break;
      case MatrixTypes::POSITIVE_DEFINITE:
        randomizer = new MatrixPositiveDefiniteR<FieldT>(*sP);
        break;
      case MatrixTypes::UPPER_TRIANGULAR:
        randomizer = new MatrixTriangularR<FieldT>(true);
        break;
      case MatrixTypes::LOWER_TRIANGULAR:
        randomizer = new MatrixTriangularR<FieldT>(false);
        break;
      case MatrixTypes::REDUNDANT:
        randomizer = new MatrixRedundantR<FieldT>(*sP, dim, dim-2);
        break;
      case MatrixTypes::DIAGONAL:
        randomizer = new MatrixDiagonalR<FieldT>(dim, 0);
        break;
      case MatrixTypes::LINEARLY_INDEPENDENT:
        randomizer = new MatrixLinearlyIndependentR<FieldT>(m, dim);
        break;
      case MatrixTypes::NILPOTENT:
        randomizer = new MatrixNilpotentR<FieldT>();
        break;
      case MatrixTypes::ROTATION:
        randomizer = new MatrixRotationR<FieldT>(m, dim);
        break;
      default:
        throw std::runtime_error("Unrecognized Matrix Type!\n");
    }

    //iterate over upper triangle and their symmetric index
    size_t i = dim;
    while( i-- ){
      size_t j = i;
      while( j-- ){
        //Iterate over index and symmetric index
        for( size_t k = 0, ij = i*dim + j; k < 2; k++, ij = j*dim + i ){
          randomizer->operator()(*sP, ij, dim);
          sP->validate_device(deviceIndex);
          m(ij) <<= *sP; // put pseudo-random numbers in M
        }
      }
    }
    //iterate over diagonals
    i = dim;
    while( i-- ){
      const size_t ij = i*dim + i;
      randomizer->operator()(*sP, ij, dim);
      sP->validate_device(deviceIndex);
      m(ij) <<= *sP; // put pseudo-random numbers in M
    }

    delete randomizer;
  }
}

#endif /* SpatialOps_MatrixRandomizers_h */
