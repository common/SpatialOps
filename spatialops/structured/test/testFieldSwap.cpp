/*
 * The MIT License
 *
 * Copyright (c) 2015-2017 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

/**
 *  \file   testFieldSwap.cpp
 *  \date   Jul 29, 2015
 *  \author "James C. Sutherland"
 */

#include <spatialops/structured/FVStaggered.h>
#include <spatialops/structured/FieldComparisons.h>
#include <test/TestHelper.h>

using namespace SpatialOps;
typedef SVolField FieldT;

int main()
{
  const IntVec npts1(4,5,6);
  const IntVec npts2(7,8,9);
  const IntVec bc(true,true,true);
  const IntVec nobc(false,false,false);

  const BoundaryCellInfo b1 = BoundaryCellInfo::build<FieldT>(bc,bc);
  const BoundaryCellInfo b2 = BoundaryCellInfo::build<FieldT>(nobc,nobc);

  const GhostData g1(0), g2(1);

  const MemoryWindow w1( get_window_with_ghost(npts1,g1,b1) );
  const MemoryWindow w2( get_window_with_ghost(npts2,g2,b2) );

  FieldT f1( w1, b1, g1, NULL );
  FieldT f2( w2, b2, g2, NULL );

  f1 <<= 1.0;
  f2 <<= 2.0;

  const double* const d1 = f1.field_values();
  const double* const d2 = f2.field_values();

  const FieldT f3(f1); // full copy of 1.

  try{
    TestHelper status( true );

    status( field_equal(f1,f3), "f1 == f3 pre-swap" );
    try{
      field_equal(f1,f2); // should throw
      status( false, "f1 != f2 pre-swap" ); // shouldn't get here
    }
    catch( std::exception& ){
      status( true, "f1 != f2 pre-swap" );
    }

    f1.swap( f2 ); // now f3 should equal f2, not f1.

    status( f1.get_ghost_data()    == g2 && f2.get_ghost_data()    == g1,  "GhostData"       );
    status( f1.window_with_ghost() == w2 && f2.window_with_ghost() == w1,  "MemoryWindow"    );
    status( f1.boundary_info()     == b2 && f2.boundary_info()     == b1, "BoundaryCellInfo" );
    status( f1.field_values()      == d2 && f2.field_values()      == d1, "Field values"     );

    status( field_equal(f2,f3), "f2 == f3 post-swap" );

    try{
      field_equal(f1,f3); // should throw
      status( false, "f1 != f3 post-swap" ); // shouldn't get here
    }
    catch( std::exception& ){
      status( true, "f1 != f3 post-swap" );
    }

    if( status.ok() ){
      std::cout << "\nPASS\n";
      return 0;
    }
  }
  catch( std::exception& err ){
    std::cout << err.what() << std::endl;
  }
  std::cout << "\nFAIL\n";
  return -1;
}
