/*
 * Copyright (c) 2014-2021 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 * ----------------------------------------------------------------------------
*/

#ifndef SpatialOps_CudaMatVecUtility_h
#define SpatialOps_CudaMatVecUtility_h
#include <cfloat>
#include <cuda_runtime.h>
#include <cublas_v2.h>
#include <iostream>
#include <math.h>

#define SPATIALOPS_GPU_FLT_RADIX FLT_RADIX

namespace SpatialOps
{
  namespace CudaMatVecUtility
  {
    namespace MatVecStatus
    {
      enum MatVecStatus_e{
        SUCCESS=0,
        MAX_ITERATION,
        IMAGINARY_NUMBER_FOUND,
      };
    }
    typedef MatVecStatus::MatVecStatus_e MatVecStatus_e;

    namespace MatVecOptions
    {
      enum MatVecOptions_e{
        NONE=0,
        DISREGARD_IMAGINARY,
        PROHIBIT_IMAGINARY,
      };
    }
    typedef MatVecOptions::MatVecOptions_e MatVecOptions_e;

    inline void _cublas_err_chk(cublasStatus_t err, const char* file, const int line)
    {
#ifndef NDEBUG
      if( err != CUBLAS_STATUS_SUCCESS ){
        std::ostringstream msg;
        msg << "Nebo error in CUBLAS execution.\n";
        msg <<  "CUBLAS error in " << file << ":" << line << ".\n";
        msg << "\tError: " << err << ".\n";
        throw std::runtime_error(msg.str());
      }
#endif /* NDEBUG */
    }
    extern "C" void cublas_err_chk(cublasStatus_t err) { _cublas_err_chk(err, __FILE__, __LINE__); }

    template<typename ValT>
    __global__ void mat_vec_copy_column_major( ValT       * const * const destination,
                                               ValT const * const * const source,
                                               int const matrixWidth,
                                               int const elementsTotal,
                                               int const xyz )
    {
      int x = blockIdx.x * blockDim.x + threadIdx.x;

      for ( ; x < xyz ; x += blockDim.x * gridDim.x ){
        mat_vec_copy_inner_column_major( destination[x], source, matrixWidth, elementsTotal, x);
      }
    }
    template<typename ValT>
    __device__ __inline__ void mat_vec_copy_inner_column_major( ValT       * const         destination,
                                                                ValT const * const * const source,
                                                                int  const matrixWidth,
                                                                int  const elementsTotal,
                                                                int  const xyz )
    {
      int y = blockIdx.y * blockDim.y + threadIdx.y;
      for ( ; y < elementsTotal; y += blockDim.y * gridDim.y ){
        destination[y] = source[(y/matrixWidth)+((y%matrixWidth)*matrixWidth)][xyz];
      }
    }

    template<typename ValT>
    __global__ void mat_vec_copy_row_major( ValT       * const * const destination,
                                            ValT const * const * const source,
                                            int const matrixWidth,
                                            int const elementsTotal,
                                            int const xyz )
    {
      int x = blockIdx.x * blockDim.x + threadIdx.x;

      for ( ; x < xyz ; x += blockDim.x * gridDim.x ){
        mat_vec_copy_inner_row_major( destination[x], source, matrixWidth, elementsTotal, x);
      }
    }
    template<typename ValT>
    __device__ __inline__ void mat_vec_copy_inner_row_major( ValT       * const         destination,
                                                             ValT const * const * const source,
                                                             int  const matrixWidth,
                                                             int  const elementsTotal,
                                                             int  const xyz )
    {
      int y = blockIdx.y * blockDim.y + threadIdx.y;
      for ( ; y < elementsTotal; y += blockDim.y * gridDim.y ){
        destination[y] = source[(y%matrixWidth)+((y/matrixWidth)*matrixWidth)][xyz];
      }
    }

    template<typename FieldT, typename MatT>
    void sync_field_streams(FieldT field, MatT other, size_t otherLength, const bool usingFirstField)
    {
      cudaEventRecord(field.get_last_event(), field.get_stream());

      const size_t endValue = usingFirstField ? 1 : 0;
      while( endValue < otherLength-- ){
        cudaStreamWaitEvent(other.at(otherLength).get_stream(),field.get_last_event(), 0);
      }
    }

    template<typename FieldT, typename MatT>
    void sync_field_streams(MatT field, size_t otherLength, FieldT other, const bool usingFirstField)
    {
      const size_t endValue = usingFirstField ? 1 : 0;
      while( endValue < otherLength-- ){
        cudaEventRecord(field.at(otherLength).get_last_event(), field.at(otherLength).get_stream());
        cudaStreamWaitEvent(other.get_stream(),field.at(otherLength).get_last_event(), 0);
      }

    }

    template<typename ValT>
    void print_device_values(ValT* d_values, size_t const size, short int deviceIndex, std::string s)
    {
      ValT h_local[size];
      ema::cuda::CUDADeviceInterface& CDI = ema::cuda::CUDADeviceInterface::self();
      CDI.memcpy_from(h_local, d_values, size*sizeof(ValT), deviceIndex);
      std::cout << s << std::endl;
      for(size_t i = 0; i < size; i++)
      {
        std::cout << h_local[i] << std::endl;
      }
      std::cout << std::endl;
    }

    template<typename T>
    __device__ __inline__ const T flat_index_one_based(T const i, T const j, T const dim)
    {
      return (i-1)*dim + (j-1);
    }

    template<typename ValT>
    __device__ __inline__ void swap(ValT& g, ValT& h, ValT& y)
    {
      y = g; g = h; h = y;
    }

    template<typename ValT>
    __global__ void balanc(ValT * const * const mats, int const n, size_t const numberofMatrices)
    {
      int matNumber = blockIdx.x * blockDim.x + threadIdx.x;
      for ( ; matNumber < numberofMatrices ; matNumber += blockDim.x * gridDim.x ){
        int last,j,i;
        float s,r,g,f,c,sqrdx;

        sqrdx=SPATIALOPS_GPU_FLT_RADIX*SPATIALOPS_GPU_FLT_RADIX;
        last=0;
        while (last == 0) {
          last = 1;
          for (i=1;i<=n;i++) {
            r=c=0.0;
            for (j=1;j<=n;j++)
              if (j != i) {
                c += ::fabs(mats[flat_index_one_based(j,i,n)][matNumber]);
                r += ::fabs(mats[flat_index_one_based(i,j,n)][matNumber]);
              }
            if (c && r) {
              g=r/SPATIALOPS_GPU_FLT_RADIX;
              f=1.0;
              s=c+r;
              while (c<g) {
                f *= SPATIALOPS_GPU_FLT_RADIX;
                c *= sqrdx;
              }
              g=r*SPATIALOPS_GPU_FLT_RADIX;
              while (c>g) {
                f /= SPATIALOPS_GPU_FLT_RADIX;
                c /= sqrdx;
              }
              if ((c+r)/f < 0.95*s) {
                last=0;
                g=1.0/f;
                for (j=1;j<=n;j++) mats[flat_index_one_based(i,j,n)][matNumber] *= g;
                for (j=1;j<=n;j++) mats[flat_index_one_based(j,i,n)][matNumber] *= f;
              }
            }
          }
        }
      }
    }

    template<typename ValT>
    __global__ void elmhes(ValT * const * const mats, int const n, size_t const numberofMatrices)
    {

      int matNumber = blockIdx.x * blockDim.x + threadIdx.x;
      for ( ; matNumber < numberofMatrices ; matNumber += blockDim.x * gridDim.x ){
        int m,j,i;
        ValT y,x;

        for(m = 2; m < n; m++) {
          x = 0.0;
          i=m;
          for(j = m; j <= n; j++) {
            if(::fabs(mats[flat_index_one_based(j, m-1, n)][matNumber]) > ::fabs(x)) {
              x = mats[flat_index_one_based(j, m-1, n)][matNumber];
              i = j;
            }
          }
          if(i != m) {
            for(j = m-1; j<= n; j++) swap(mats[flat_index_one_based(i,j,n)][matNumber], mats[flat_index_one_based(m,j,n)][matNumber],y);
            for(j = 1;   j<= n; j++) swap(mats[flat_index_one_based(j,i,n)][matNumber], mats[flat_index_one_based(j,m,n)][matNumber],y);
          }
          if(x) {
            for(i = m+1; i <= n; i++) {
              if((y=mats[flat_index_one_based(i, m-1, n)][matNumber]) != 0.0) {
                y /= x;
                mats[flat_index_one_based(i, m-1, n)][matNumber] = y;
                for(j = m; j <= n; j++)
                  mats[flat_index_one_based(i, j, n)][matNumber] -= y*mats[flat_index_one_based(m, j, n)][matNumber];
                for(j = 1; j <= n; j++)
                  mats[flat_index_one_based(j, m, n)][matNumber] += y*mats[flat_index_one_based(j, i, n)][matNumber];
              }
            }
          }
        }
      }
    }

    template<typename ValT>
    __device__ __inline__ MatVecStatus_e hqr_helper(ValT * const * const mats, int const matNumber, int const n, ValT* const * const wr, ValT* const * const wi, MatVecOptions_e const option)
    {
      int nn, m, l, k, j, its, i, mmin;
      ValT z, y, x, w, v, u, t, s, r, q, p, anorm, maxTmp;

      anorm = 0.0;
      for(i=1; i<=n; i++) {
        maxTmp = i-1;
        for(j=(maxTmp<1?1:maxTmp); j<=n; j++) {
          anorm += ::fabs(mats[flat_index_one_based(i, j, n)][matNumber]);
        }
      }

      nn=n;
      t=0.0;
      while(nn >= 1) {
        its = 0;
        do {
          for (l=nn;l>=2;l--) {
            s=::fabs(mats[flat_index_one_based(l-1, l-1, n)][matNumber])+::fabs(mats[flat_index_one_based(l,l,n)][matNumber]);
            if(s==0.0) s=anorm;
            if((ValT)(::fabs(mats[flat_index_one_based(l,l-1,n)][matNumber]) + s) == s) break;
          }
          x=mats[flat_index_one_based(nn,nn,n)][matNumber];
          if(l==nn) {
            wr[nn][matNumber]=x+t;
            if(!(option == MatVecOptions::DISREGARD_IMAGINARY || option == MatVecOptions::PROHIBIT_IMAGINARY))
              wi[nn][matNumber]=0.0;
            nn--;
          } else {
            y=mats[flat_index_one_based(nn-1,nn-1,n)][matNumber];
            w=mats[flat_index_one_based(nn, nn-1,n)][matNumber]*mats[flat_index_one_based(nn-1,nn,n)][matNumber];
            if(l == (nn-1)) {
              p=0.5*(y-x);
              q=p*p+w;
              z=::sqrt(::fabs(q));
              x += t;
              if(q >= 0.0) {
                z=p+(p>=0.0?::fabs(z):-::fabs(z));
                wr[nn-1][matNumber]=wr[nn][matNumber]=x+z;
                if (z) wr[nn][matNumber]=x-w/z;
                if(!(option == MatVecOptions::DISREGARD_IMAGINARY || option == MatVecOptions::PROHIBIT_IMAGINARY))
                  wi[nn-1][matNumber]=wi[nn][matNumber]=0.0;
              } else {
                if(option != MatVecOptions::PROHIBIT_IMAGINARY)
                {
                  wr[nn-1][matNumber]=wr[nn][matNumber]=x+p;
                  if(option != MatVecOptions::DISREGARD_IMAGINARY)
                    wi[nn-1][matNumber]= -(wi[nn][matNumber]=z);
                }
                else
                {
                  return MatVecStatus::IMAGINARY_NUMBER_FOUND;
                }
              }
              nn -= 2;
            } else {
              if (its == 30) return MatVecStatus::MAX_ITERATION;
              if (its == 10 || its == 20) {
                t += x;
                for (i=1; i<=nn;i++) mats[flat_index_one_based(i,i,n)][matNumber] -= x;
                s=::fabs(mats[flat_index_one_based(nn,nn-1,n)][matNumber])+::fabs(mats[flat_index_one_based(nn-1,nn-2,n)][matNumber]);
                y=x=0.75*s;
                w = -0.4375*s*s;
              }
              ++its;
              for (m=(nn-2);m>=l;m--) {
                z=mats[flat_index_one_based(m,m,n)][matNumber];
                r=x-z;
                s=y-z;
                p=(r*s-w)/mats[flat_index_one_based(m+1,m,n)][matNumber]+mats[flat_index_one_based(m,m+1,n)][matNumber];
                q=mats[flat_index_one_based(m+1,m+1,n)][matNumber]-z-r-s;
                r=mats[flat_index_one_based(m+2,m+1,n)][matNumber];
                s=::fabs(p)+::fabs(q)+::fabs(r);
                p /= s;
                q /= s;
                r /= s;
                if (m == l) break;
                u=::fabs(mats[flat_index_one_based(m,m-1,n)][matNumber])*(::fabs(q)+::fabs(r));
                v=::fabs(p)*(::fabs(mats[flat_index_one_based(m-1,m-1,n)][matNumber])+::fabs(z)+::fabs(mats[flat_index_one_based(m+1,m+1,n)][matNumber]));
                if ((ValT)(u+v) == v) break;
              }
              for (i=m+2;i<=nn;i++) {
                mats[flat_index_one_based(i, i-2,n)][matNumber]=0.0;
                if (i != (m+2)) mats[flat_index_one_based(i, i-3,n)][matNumber]=0.0;
              }
              for (k=m;k<=nn-1;k++) {
                if (k != m) {
                  p=mats[flat_index_one_based(k,k-1,n)][matNumber];
                  q=mats[flat_index_one_based(k+1,k-1,n)][matNumber];
                  r=0.0;
                  if (k != (nn-1)) r=mats[flat_index_one_based(k+2,k-1,n)][matNumber];
                  if ((x=::fabs(p)+::fabs(q)+::fabs(r)) != 0.0) {
                    p /= x;
                    q /= x;
                    r /= x;
                  }
                }
                maxTmp = ::sqrt(p*p+q*q+r*r);
                if ((s=(p >= 0.0?::fabs(maxTmp):-::fabs(maxTmp))) != 0.0) {
                  if (k == m) {
                    if (l != m)
                      mats[flat_index_one_based(k, k-1,n)][matNumber] = -mats[flat_index_one_based(k,k-1,n)][matNumber];
                  } else
                    mats[flat_index_one_based(k,k-1,n)][matNumber] = -s*x;
                  p += s;
                  x=p/s;
                  y=q/s;
                  z=r/s;
                  q /= p;
                  r /= p;
                  for (j=k;j<=nn;j++) {
                    p=mats[flat_index_one_based(k,j,n)][matNumber]+q*mats[flat_index_one_based(k+1,j,n)][matNumber];
                    if (k != (nn-1)) {
                      p += r*mats[flat_index_one_based(k+2,j,n)][matNumber];
                      mats[flat_index_one_based(k+2,j,n)][matNumber] -= p*z;
                    }
                    mats[flat_index_one_based(k+1,j,n)][matNumber] -= p*y;
                    mats[flat_index_one_based(k,j,n)][matNumber] -= p*x;
                  }
                  mmin = nn<k+3 ? nn : k+3;
                  for (i=l;i<=mmin;i++) {
                    p=x*mats[flat_index_one_based(i,k,n)][matNumber]+y*mats[flat_index_one_based(i,k+1,n)][matNumber];
                    if (k != (nn-1)) {
                      p += z*mats[flat_index_one_based(i, k+2,n)][matNumber];
                      mats[flat_index_one_based(i,k+2,n)][matNumber] -= p*r;
                    }
                    mats[flat_index_one_based(i,k+1,n)][matNumber] -= p*q;
                    mats[flat_index_one_based(i,k,n)][matNumber] -= p;
                  }
                }
              }
            }
          }
        } while (l < nn-1);
      }

      return MatVecStatus::SUCCESS;
    }

    template<typename ValT>
    __global__ void hqr(ValT * const * const mats, int const n, int const batchSize, ValT** wr, ValT** wi, int status[], MatVecOptions_e const option)
    {
      int matNumber = blockIdx.x * blockDim.x + threadIdx.x;
      const bool useImaginary = !(option == MatVecOptions::DISREGARD_IMAGINARY || option == MatVecOptions::PROHIBIT_IMAGINARY);
      for ( ; matNumber < batchSize ; matNumber += blockDim.x * gridDim.x ){
        status[matNumber] = hqr_helper(mats, matNumber, n, wr-1, useImaginary ? wi-1 : NULL, option);  //Call with 1-based indexing
      }
    }

    template<typename ValT>
    __global__ void eigen_decomposition(ValT * const * const mArray, int const n, int const batchSize, ValT ** result, ValT ** resultComplex, int* status, MatVecOptions_e const option)
    {
      balanc(mArray, n, batchSize);
      elmhes(mArray, n, batchSize);
      hqr(mArray, n, batchSize, result, resultComplex, status, option);
    }

    template<typename ValT>
    class CublasTypeDispatcher;

    template<>
    class CublasTypeDispatcher<double>
    {
      public:
        
        static cublasStatus_t cublasgetrsBatched( cublasHandle_t handle,
                                                  cublasOperation_t trans,
                                                  int n,
                                                  int nrhs,
                                                  const double *Aarray[],
                                                  int lda,
                                                  const int *devIpiv,
                                                  double *Barray[],
                                                  int ldb,
                                                  int *info,
                                                  int batchSize )
        {
          return cublasDgetrsBatched(handle,
                                     trans,
                                     n,
                                     nrhs,
                                     Aarray,
                                     lda,
                                     devIpiv,
                                     Barray,
                                     ldb,
                                     info,
                                     batchSize);
        }
        static cublasStatus_t cublasgetrfBatched( cublasHandle_t handle,
                                                  int n,
                                                  double *Aarray[],
                                                  int lda,
                                                  int *PivotArray,
                                                  int *infoArray,
                                                  int batchSize )
        {
          return cublasDgetrfBatched(handle, n, Aarray, lda, PivotArray, infoArray, batchSize);
        }
        static cublasStatus_t cublasgetriBatched( cublasHandle_t handle,
                                                  int n,
                                                  const double *Aarray[],
                                                  int lda,
                                                  int *PivotArray,
                                                  double *Carray[],
                                                  int ldc,
                                                  int *infoArray,
                                                  int batchSize )
        {
          return cublasDgetriBatched(handle,
                                     n,
                                     Aarray,
                                     lda,
                                     PivotArray,
                                     Carray,
                                     ldc,
                                     infoArray,
                                     batchSize);
        }

        static cublasStatus_t cublasgemmBatched( cublasHandle_t handle,
                                                 cublasOperation_t transa,
                                                 cublasOperation_t transb,
                                                 int m,
                                                 int n,
                                                 int k,
                                                 const double *alpha,
                                                 const double *Aarray[],
                                                 int lda,
                                                 const double *Barray[],
                                                 int ldb,
                                                 const double *beta,
                                                 double *Carray[],
                                                 int ldc,
                                                 int batchSize )
        {
          return cublasDgemmBatched(handle,
                                    transa, transb,
                                    m, n, k,
                                    alpha,
                                    Aarray, lda,
                                    Barray, ldb,
                                    beta,
                                    Carray, ldc,
                                    batchSize);
        }

      private:
        CublasTypeDispatcher();
    };
  } // namespace CudaMatVecUtility
} // namespace SpatialOps

#endif // SpatialOps_CudaMatVecUtility_h
