#include <spatialops/SpatialOpsTools.h>
#include <spatialops/OperatorDatabase.h>

#include <spatialops/structured/stencil/OneSidedOperatorTypes.h>
#include <spatialops/structured/stencil/StencilBuilder.h>
#include <spatialops/structured/FieldComparisons.h>
#include <spatialops/structured/Grid.h>
#include <spatialops/Nebo.h>

#include <test/TestHelper.h>
#include <spatialops/structured/FieldHelper.h>


#include <boost/program_options.hpp>
namespace po = boost::program_options;

using namespace SpatialOps;

#include <stdexcept>
using std::cout;
using std::endl;

//--------------------------------------------------------------------

#define TOLERANCE 1.0e-3

//--------------------------------------------------------------------

template<typename FieldT, typename OpT> struct Result;

template<typename FieldT> struct Result<FieldT,Interpolant>{
  Result(){}
  void operator()( const FieldT& x, FieldT& result ) const{
    result <<= sin(x);
  }
};
template<typename FieldT> struct Result<FieldT,Gradient>{
  Result(){}
  void operator()( const FieldT& x, FieldT& result ) const{
    result <<= cos(x);
  }
};

//--------------------------------------------------------------------

template<typename Op, typename StencilT, typename FieldType>
bool test_stencil( const IntVec npts,
                   const OperatorDatabase & opdb,
                   const Grid& grid,
                   const double tolerance )
{
  typedef typename StencilT::DirT              OpDir; // the orientation of the stencil, e.g., <1,0,0>
  typedef typename GetNonzeroDir<OpDir>::DirT  DirT;  // the direction the stencil acts in, e.g., XDIR

  //Two ghost cells are required for 3 point one sided stencils
  const GhostData ghost(2);
  const IntVec bc(true,true,true);
  const BoundaryCellInfo bcinfo = BoundaryCellInfo::build<FieldType>(bc,bc);
  const MemoryWindow mw = get_window_with_ghost(npts, ghost, bcinfo);

  FieldType x     ( mw, bcinfo, ghost, NULL );
  FieldType y     ( mw, bcinfo, ghost, NULL );
  FieldType z     ( mw, bcinfo, ghost, NULL );
  FieldType indep ( mw, bcinfo, ghost, NULL );
  FieldType result( mw, bcinfo, ghost, NULL );
  FieldType refVal( mw, bcinfo, ghost, NULL );
  FieldType arg   ( mw, bcinfo, ghost, NULL );

  // set field values
  grid.set_coord<XDIR>(x);
  grid.set_coord<YDIR>(y);
  grid.set_coord<ZDIR>(z);

  indep <<= x + y;
  indep <<= indep + z;
//  indep <<= x+y+z;  // jcs for some strange reason, this can cause problem on serial OSX builds.

  arg    <<= sin( indep );
  const Result<FieldType,Op> calc_result;
  calc_result(indep,refVal);

  //get operator:
  typedef typename SpatialOps::OneSidedOpTypeBuilder<Op,StencilT,FieldType>::type FullOpT;
  FullOpT* const op = opdb.retrieve_operator<FullOpT>();

  result <<= 0;
  result <<= (*op)(arg);

//  write_matlab( result, "src", false );
//  write_matlab( refVal, "ref", false );
//  write_matlab( coord, "coord", false );
  const double l2Norm = nebo_norm_interior( result-refVal );

  if( l2Norm>=tolerance ) std::cout << "norm: " << l2Norm << std::endl;
  return l2Norm<tolerance;
};

//-------------------------------------------------------------------

template<typename Op, typename StencilT>
bool test_field_variants( const IntVec npts,
                          const OperatorDatabase & opdb,
                          const Grid& grid,
                          const double tolerance )
{
  TestHelper status(false);
  status( test_stencil<Op,StencilT,SVolField>(npts, opdb, grid, tolerance), "  -> SVolField" );
  if( npts[0]>1 ) status( test_stencil<Op,StencilT,XVolField>(npts, opdb, grid, tolerance), "  -> XVolField" );
  if( npts[1]>1 ) status( test_stencil<Op,StencilT,YVolField>(npts, opdb, grid, tolerance), "  -> YVolField" );
  if( npts[2]>1 ) status( test_stencil<Op,StencilT,ZVolField>(npts, opdb, grid, tolerance), "  -> ZVolField" );
  return status.ok();
}

//--------------------------------------------------------------------

int main( int iarg, char* carg[] )
{
  int nx, ny, nz;
  double length, tol2, tol3;
  {
    po::options_description desc("Supported Options");
    desc.add_options()
          ( "help", "print help message\n" )
          ( "nx", po::value<int>   ( &nx     )->default_value( 21   ), "number of points in x-dir for base mesh" )
          ( "ny", po::value<int>   ( &ny     )->default_value( 21   ), "number of points in y-dir for base mesh" )
          ( "nz", po::value<int>   ( &nz     )->default_value( 21   ), "number of points in z-dir for base mesh" )
          ( "l",  po::value<double>( &length )->default_value( 0.01 ), "length of the domain"                    )
          ( "tol2", po::value<double>( &tol2 )->default_value( 0.01 ), "error tolerance for 2-pt stencil test"   )
          ( "tol3", po::value<double>( &tol3 )->default_value( 0.01 ), "error tol2 for 3-pt stencil test"        );

    po::variables_map args;
    po::store( po::parse_command_line(iarg,carg,desc), args );
    po::notify(args);

    if( args.count("help") ){
      cout << desc << endl
          << "Example:" << endl
          << "  test_stencil --nx 5 --ny 10 --nz 3 --l 0.1 " << endl
          << "Note: Less points require a smaller length to limit approximation error" << endl
          << endl;
      return -1;
    }
  }

  TestHelper status( true );
  const IntVec npts(nx,ny,nz);

  {
    cout << "  domain : " << npts << endl
        << endl;
  }

  OperatorDatabase opdb;
  build_stencils( npts[0], npts[1], npts[2], length, length, length, opdb );

  const Grid grid( npts, DoubleVec(length, length, length) );

  typedef UnitTriplet<XDIR>::type  XPlus;
  typedef UnitTriplet<YDIR>::type  YPlus;
  typedef UnitTriplet<ZDIR>::type  ZPlus;
  typedef XPlus::Negate            XMinus;
  typedef YPlus::Negate            YMinus;
  typedef ZPlus::Negate            ZMinus;

  try{

    if( npts[0]>1 ){
      status( test_field_variants<Gradient, OneSidedStencil2<XMinus> >(npts, opdb, grid, tol2), "Two   Point, Minus Sided, X Direction, Divergence Operator" );
      status( test_field_variants<Gradient, OneSidedStencil2<XPlus > >(npts, opdb, grid, tol2), "Two   Point, Plus  Sided, X Direction, Divergence Operator" );
      status( test_field_variants<Gradient, OneSidedStencil3<XMinus> >(npts, opdb, grid, tol3), "Three Point, Minus Sided, X Direction, Divergence Operator" );
      status( test_field_variants<Gradient, OneSidedStencil3<XPlus > >(npts, opdb, grid, tol3), "Three Point, Plus  Sided, X Direction, Divergence Operator" );
    }
    if( npts[1]>1 ){
      status( test_field_variants<Gradient, OneSidedStencil2<YMinus> >(npts, opdb, grid, tol2), "Two   Point, Minus Sided, Y Direction, Divergence Operator" );
      status( test_field_variants<Gradient, OneSidedStencil2<YPlus > >(npts, opdb, grid, tol2), "Two   Point, Plus  Sided, Y Direction, Divergence Operator" );
      status( test_field_variants<Gradient, OneSidedStencil3<YMinus> >(npts, opdb, grid, tol3), "Three Point, Minus Sided, Y Direction, Divergence Operator" );
      status( test_field_variants<Gradient, OneSidedStencil3<YPlus > >(npts, opdb, grid, tol3), "Three Point, Plus  Sided, Y Direction, Divergence Operator" );
    }
    if( npts[2]>1 ){
      status( test_field_variants<Gradient, OneSidedStencil2<ZMinus> >(npts, opdb, grid, tol2), "Two   Point, Minus Sided, Z Direction, Divergence Operator" );
      status( test_field_variants<Gradient, OneSidedStencil2<ZPlus > >(npts, opdb, grid, tol2), "Two   Point, Plus  Sided, Z Direction, Divergence Operator" );
      status( test_field_variants<Gradient, OneSidedStencil3<ZMinus> >(npts, opdb, grid, tol3), "Three Point, Minus Sided, Z Direction, Divergence Operator" );
      status( test_field_variants<Gradient, OneSidedStencil3<ZPlus > >(npts, opdb, grid, tol3), "Three Point, Plus  Sided, Z Direction, Divergence Operator" );
    }

    if( status.ok() ){
      cout << "Tests passed" << endl;
      return 0;
    }
  }
  catch( std::exception& e ){
    cout << e.what() << endl;
  }

  cout << "******************************" << endl
      << "At least one test did not pass" << endl
      << "******************************" << endl;
  return -1;
}

//--------------------------------------------------------------------

