/*
 * Copyright (c) 2014-2021 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#ifndef SpatialOps_structured_OneSidedOpTypes_h
#define SpatialOps_structured_OneSidedOpTypes_h

#include <spatialops/SpatialOpsDefs.h>
#include <spatialops/Nebo.h>

namespace SpatialOps{

  /**
   * \struct OneSidedStencil2
   * \brief Support for one-sided stencils.
   * \tparam OpDir the unit vector (a IndexTriplet) that indicates the direction of offset for the stencil.
   * \tparam Offset (optional) the offset for the stencil.  <0,0,0> (default) results in a
   *         one-sided stencil that computes into the first stencil point.
   *         <1,0,0> would offset the stencil so that it effectively computes
   *         into the <-1,0,0> point.
   *
   * \par Example:
   * This code describes a 2-point stencil oriented in the (+x) direction.
   * \code{.cpp}
   * OneSidedStencil2< IndexTriplet<1,0,0> >
   * \endcode
   * It would look something like this:
   * \verbatim
             1 2 3
     Read :  o o o
     Write:  o
     \endverbatim
   *
   * \par Example:
   * This code describes a 2-point stencil oriented in the (-x) direction that
   * is offset in the (-x) direction.
   * \code{.cpp}
   * OneSidedStencil2< IndexTriplet<-1,0,0>, IndexTriplet<-1,0,0> >
   * \endcode
   * It would look something like this:
   * \verbatim
            n-2 n-1  n
     Read :  o   o
     Write:          o
     \endverbatim
   */
  template<typename OpDir, typename Offset=IndexTriplet<0,0,0> >
  struct OneSidedStencil2{
    typedef OpDir  DirT; ///< The orientation of the stencil (IndexTriplet)
    typedef typename Add<Offset,OpDir>::result Point2;
    typedef NEBO_FIRST_POINT(Offset)::NEBO_ADD_POINT(Point2)  StPtCollection;
  };

  /**
   * \struct OneSidedStencil3
   * \brief Support for one-sided stencils.
   * \tparam OpDir the unit vector (a IndexTriplet) that indicates the direction of offset for the stencil.
   * \tparam Offset (optional) the offset for the stencil.  <0,0,0> (default) results in a
   *         one-sided stencil that computes into the first stencil point.
   *         <1,0,0> would offset the stencil so that it effectively computes
   *         into the <-1,0,0> point.
   */
  template<typename OpDir, typename Offset=IndexTriplet<0,0,0> >
  struct OneSidedStencil3{
    typedef OpDir  DirT; ///< The orientation of the stencil (IndexTriplet)
    typedef typename Add<OpDir,Offset>::result  Point2;
    typedef typename Add<OpDir,Point2>::result  Point3;
    typedef NEBO_FIRST_POINT(Offset)::NEBO_ADD_POINT(Point2)::NEBO_ADD_POINT(Point3) StPtCollection;
  };

  /**
   *  \struct OneSidedOpTypeBuilder
   *  \author Derek Cline, James C. Sutherland
   *  \ingroup optypes
   *  \brief Builds OneSidedDiv operator type from field type.
   *
   *  \tparam Op the basic operator type (e.g., Gradient, Interpolant)
   *  \tparam StencilT the stencil structure (e.g., OneSidedStencil3<IndexTriplet<0,-1,0>)
   *  \tparam FieldT the field that the operator applies to (e.g., SVolField)
   *  \tparam Offset the offset for the stencil.  <0,0,0> (default) results in a
   *          one-sided stencil that computes into the first stencil point.
   *          <1,0,0> would offset the stencil so that it effectively computes
   *          into the <-1,0,0> point.
   *
   *  \par Example Usage
   *  The following obtains the full type for a two-point, one-sided stencil
   *  shifted in the (-z) direction:
   *  \code
   *  typedef UnitTriplet<ZDIR>::type::Negate ZMinus;
   *  typedef OneSidedOpTypeBuilder<Gradient,OneSidedStencil2<ZMinus>,SVolField>::type OneSidedDiv2Z;
   *  \endcode
   *
   *  Note that we only provide fully specialized versions of this template
   *  so that unsupported operator types cannot be inadvertently formed.
   */
  template<typename Op, typename StencilT, typename FieldT, typename Offset=IndexTriplet<0,0,0> >
  struct OneSidedOpTypeBuilder;

#define BUILD_ONE_SIDED_OP_ACROSS_DIRS( Op, I1, I2, I3, StencilT, FieldT )                                  \
  template<> struct OneSidedOpTypeBuilder<Op,StencilT<IndexTriplet<I1,I2,I3> >,FieldT>{                     \
    typedef NeboStencilBuilder<Op,StencilT<IndexTriplet<I1,I2,I3> >::StPtCollection, FieldT, FieldT>  type; \
  };

#define ONE_SIDED_OP_BUILDERS( Op, I1, I2, I3, FieldT )                      \
  BUILD_ONE_SIDED_OP_ACROSS_DIRS( Op, I1, I2, I3, OneSidedStencil2, FieldT ) \
  BUILD_ONE_SIDED_OP_ACROSS_DIRS( Op, I1, I2, I3, OneSidedStencil3, FieldT )

#define BUILD_ONE_SIDED_STENCILS( FieldT )              \
  ONE_SIDED_OP_BUILDERS( Gradient,  1, 0, 0, FieldT )   \
  ONE_SIDED_OP_BUILDERS( Gradient, -1, 0, 0, FieldT )   \
  ONE_SIDED_OP_BUILDERS( Gradient,  0, 1, 0, FieldT )   \
  ONE_SIDED_OP_BUILDERS( Gradient,  0,-1, 0, FieldT )   \
  ONE_SIDED_OP_BUILDERS( Gradient,  0, 0, 1, FieldT )   \
  ONE_SIDED_OP_BUILDERS( Gradient,  0, 0,-1, FieldT )

BUILD_ONE_SIDED_STENCILS(   SVolField )
//BUILD_ONE_SIDED_STENCILS( SSurfXField )
//BUILD_ONE_SIDED_STENCILS( SSurfYField )
//BUILD_ONE_SIDED_STENCILS( SSurfZField )
//
BUILD_ONE_SIDED_STENCILS(   XVolField )
//BUILD_ONE_SIDED_STENCILS( XSurfXField )
//BUILD_ONE_SIDED_STENCILS( XSurfYField )
//BUILD_ONE_SIDED_STENCILS( XSurfZField )
//
BUILD_ONE_SIDED_STENCILS(   YVolField )
//BUILD_ONE_SIDED_STENCILS( YSurfXField )
//BUILD_ONE_SIDED_STENCILS( YSurfYField )
//BUILD_ONE_SIDED_STENCILS( YSurfZField )
//
BUILD_ONE_SIDED_STENCILS(   ZVolField )
//BUILD_ONE_SIDED_STENCILS( ZSurfXField )
//BUILD_ONE_SIDED_STENCILS( ZSurfYField )
//BUILD_ONE_SIDED_STENCILS( ZSurfZField )

} // namespace SpatialOps

#endif // SpatialOps_structured_OneSidedOpTypes_h
