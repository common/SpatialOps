/*
 * The MIT License
 *
 * Copyright (c) 2016-2017 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include "StencilBuilder.h"
#include "FVStaggeredOperatorTypes.h"
#include "OneSidedOperatorTypes.h"
#include "FVStaggeredBCOp.h"
#include <spatialops/structured/FVStaggeredFieldTypes.h>
#include <spatialops/structured/Grid.h>
#include <spatialops/OperatorDatabase.h>

namespace SpatialOps{

#define REGISTER_BC_STENCILS( VOL )                                                   \
  opdb.register_new_bc_operator< BasicOpTypes<VOL>::InterpC2FX >();                   \
  opdb.register_new_bc_operator< BasicOpTypes<VOL>::InterpC2FY >();                   \
  opdb.register_new_bc_operator< BasicOpTypes<VOL>::InterpC2FZ >();                   \
  opdb.register_new_bc_operator< BasicOpTypes<VOL>::GradX      >();                   \
  opdb.register_new_bc_operator< BasicOpTypes<VOL>::GradY      >();                   \
  opdb.register_new_bc_operator< BasicOpTypes<VOL>::GradZ      >();                   \
  opdb.register_new_bc_operator< BasicOpTypes<VOL>::DivX       >();                   \
  opdb.register_new_bc_operator< BasicOpTypes<VOL>::DivY       >();                   \
  opdb.register_new_bc_operator< BasicOpTypes<VOL>::DivZ       >();                   \
  opdb.register_new_bc_operator< OperatorTypeBuilder<InterpolantX,VOL,VOL>::type >(); \
  opdb.register_new_bc_operator< OperatorTypeBuilder<InterpolantY,VOL,VOL>::type >(); \
  opdb.register_new_bc_operator< OperatorTypeBuilder<InterpolantZ,VOL,VOL>::type >(); \
  opdb.register_new_bc_operator< OperatorTypeBuilder<GradientX,   VOL,VOL>::type >(); \
  opdb.register_new_bc_operator< OperatorTypeBuilder<GradientY,   VOL,VOL>::type >(); \
  opdb.register_new_bc_operator< OperatorTypeBuilder<GradientZ,   VOL,VOL>::type >();

  //------------------------------------------------------------------

  template< typename FieldT, typename StencilT, typename CoefCollection >
  void build_one_sided_stencils( OperatorDatabase& opdb,
                                 const CoefCollection& coefs )
  {
    typedef typename OneSidedOpTypeBuilder<Gradient,StencilT,FieldT>::type  GradOp;
    opdb.register_new_operator( new GradOp( coefs ) );
  }

  //------------------------------------------------------------------

  template< typename StencilT, typename CoefCollection >
  void build_one_sided_stencils( OperatorDatabase& opdb,
                                 const CoefCollection& coefs )
  {
    build_one_sided_stencils<SVolField,StencilT,CoefCollection>( opdb, coefs );
    build_one_sided_stencils<XVolField,StencilT,CoefCollection>( opdb, coefs );
    build_one_sided_stencils<YVolField,StencilT,CoefCollection>( opdb, coefs );
    build_one_sided_stencils<ZVolField,StencilT,CoefCollection>( opdb, coefs );
  }

  //------------------------------------------------------------------

  void build_custom_stencils( const unsigned int nx,
                              const unsigned int ny,
                              const unsigned int nz,
                              const double Lx,
                              const double Ly,
                              const double Lz,
                              OperatorDatabase& opdb )
  {
    const double dx = Lx/nx;
    const double dy = Ly/ny;
    const double dz = Lz/nz;

    //Coefficients:
    NeboStencilCoefCollection<2> coefHalf    = build_two_point_coef_collection( 0.5, 0.5 );
    NeboStencilCoefCollection<2> coefDx      = build_two_point_coef_collection( -1.0/dx, 1.0/dx );
    NeboStencilCoefCollection<2> coefDy      = build_two_point_coef_collection( -1.0/dy, 1.0/dy );
    NeboStencilCoefCollection<2> coefDz      = build_two_point_coef_collection( -1.0/dz, 1.0/dz );
    NeboStencilCoefCollection<4> coefQuarter = build_four_point_coef_collection( 0.25, 0.25, 0.25, 0.25 );
    NeboStencilCoefCollection<2> coefHalfDx  = build_two_point_coef_collection( -0.5/dx, 0.5/dx );
    NeboStencilCoefCollection<2> coefHalfDy  = build_two_point_coef_collection( -0.5/dy, 0.5/dy );
    NeboStencilCoefCollection<2> coefHalfDz  = build_two_point_coef_collection( -0.5/dz, 0.5/dz );

    //___________________________________________________________________
    // Finite Difference stencils:
    //
    opdb.register_new_operator( new OperatorTypeBuilder<InterpolantX,SVolField,SVolField>::type( coefHalf ) );
    opdb.register_new_operator( new OperatorTypeBuilder<InterpolantY,SVolField,SVolField>::type( coefHalf ) );
    opdb.register_new_operator( new OperatorTypeBuilder<InterpolantZ,SVolField,SVolField>::type( coefHalf ) );
    opdb.register_new_operator( new OperatorTypeBuilder<GradientX,   SVolField,SVolField>::type( coefHalfDx ) );
    opdb.register_new_operator( new OperatorTypeBuilder<GradientY,   SVolField,SVolField>::type( coefHalfDy ) );
    opdb.register_new_operator( new OperatorTypeBuilder<GradientZ,   SVolField,SVolField>::type( coefHalfDz ) );

    opdb.register_new_operator( new OperatorTypeBuilder<InterpolantX,XVolField,XVolField>::type( coefHalf ) );
    opdb.register_new_operator( new OperatorTypeBuilder<InterpolantY,XVolField,XVolField>::type( coefHalf ) );
    opdb.register_new_operator( new OperatorTypeBuilder<InterpolantZ,XVolField,XVolField>::type( coefHalf ) );
    opdb.register_new_operator( new OperatorTypeBuilder<GradientX,   XVolField,XVolField>::type( coefHalfDx ) );
    opdb.register_new_operator( new OperatorTypeBuilder<GradientY,   XVolField,XVolField>::type( coefHalfDy ) );
    opdb.register_new_operator( new OperatorTypeBuilder<GradientZ,   XVolField,XVolField>::type( coefHalfDz ) );

    opdb.register_new_operator( new OperatorTypeBuilder<InterpolantX,YVolField,YVolField>::type( coefHalf ) );
    opdb.register_new_operator( new OperatorTypeBuilder<InterpolantY,YVolField,YVolField>::type( coefHalf ) );
    opdb.register_new_operator( new OperatorTypeBuilder<InterpolantZ,YVolField,YVolField>::type( coefHalf ) );
    opdb.register_new_operator( new OperatorTypeBuilder<GradientX,   YVolField,YVolField>::type( coefHalfDx ) );
    opdb.register_new_operator( new OperatorTypeBuilder<GradientY,   YVolField,YVolField>::type( coefHalfDy ) );
    opdb.register_new_operator( new OperatorTypeBuilder<GradientZ,   YVolField,YVolField>::type( coefHalfDz ) );

    opdb.register_new_operator( new OperatorTypeBuilder<InterpolantX,ZVolField,ZVolField>::type( coefHalf ) );
    opdb.register_new_operator( new OperatorTypeBuilder<InterpolantY,ZVolField,ZVolField>::type( coefHalf ) );
    opdb.register_new_operator( new OperatorTypeBuilder<InterpolantZ,ZVolField,ZVolField>::type( coefHalf ) );
    opdb.register_new_operator( new OperatorTypeBuilder<GradientX,   ZVolField,ZVolField>::type( coefHalfDx ) );
    opdb.register_new_operator( new OperatorTypeBuilder<GradientY,   ZVolField,ZVolField>::type( coefHalfDy ) );
    opdb.register_new_operator( new OperatorTypeBuilder<GradientZ,   ZVolField,ZVolField>::type( coefHalfDz ) );

    opdb.register_new_operator( new OperatorTypeBuilder<InterpolantX,VertexField,VertexField>::type( coefHalf ) );
    opdb.register_new_operator( new OperatorTypeBuilder<InterpolantY,VertexField,VertexField>::type( coefHalf ) );
    opdb.register_new_operator( new OperatorTypeBuilder<InterpolantZ,VertexField,VertexField>::type( coefHalf ) );
    opdb.register_new_operator( new OperatorTypeBuilder<GradientX,   VertexField,VertexField>::type( coefHalfDx ) );
    opdb.register_new_operator( new OperatorTypeBuilder<GradientY,   VertexField,VertexField>::type( coefHalfDy ) );
    opdb.register_new_operator( new OperatorTypeBuilder<GradientZ,   VertexField,VertexField>::type( coefHalfDz ) );

    //___________________________________________________________________
    // One sided stencils:
    {
      //One sided, 2 pt coefficients:
      NeboStencilCoefCollection<2> coefHalfDx2Plus   = build_two_point_coef_collection( -1.0/dx,  1.0/dx );
      NeboStencilCoefCollection<2> coefHalfDx2Minus  = build_two_point_coef_collection(  1.0/dx, -1.0/dx );
      NeboStencilCoefCollection<2> coefHalfDy2Plus   = build_two_point_coef_collection( -1.0/dy,  1.0/dy );
      NeboStencilCoefCollection<2> coefHalfDy2Minus  = build_two_point_coef_collection(  1.0/dy, -1.0/dy );
      NeboStencilCoefCollection<2> coefHalfDz2Plus   = build_two_point_coef_collection( -1.0/dz,  1.0/dz );
      NeboStencilCoefCollection<2> coefHalfDz2Minus  = build_two_point_coef_collection(  1.0/dz, -1.0/dz );

      //One sided, 3 pt coefficients:
      NeboStencilCoefCollection<3> coefHalfDx3Plus   = build_three_point_coef_collection( -1.5/dx,  2.0/dx, -0.5/dx );
      NeboStencilCoefCollection<3> coefHalfDx3Minus  = build_three_point_coef_collection(  1.5/dx, -2.0/dx,  0.5/dx );
      NeboStencilCoefCollection<3> coefHalfDy3Plus   = build_three_point_coef_collection( -1.5/dy,  2.0/dy, -0.5/dy );
      NeboStencilCoefCollection<3> coefHalfDy3Minus  = build_three_point_coef_collection(  1.5/dy, -2.0/dy,  0.5/dy );
      NeboStencilCoefCollection<3> coefHalfDz3Plus   = build_three_point_coef_collection( -1.5/dz,  2.0/dz, -0.5/dz );
      NeboStencilCoefCollection<3> coefHalfDz3Minus  = build_three_point_coef_collection(  1.5/dz, -2.0/dz,  0.5/dz );

      typedef UnitTriplet<XDIR>::type  XPlus;
      typedef UnitTriplet<YDIR>::type  YPlus;
      typedef UnitTriplet<ZDIR>::type  ZPlus;
      typedef XPlus::Negate            XMinus;
      typedef YPlus::Negate            YMinus;
      typedef ZPlus::Negate            ZMinus;

      build_one_sided_stencils<OneSidedStencil2<XPlus > >( opdb, coefHalfDx2Plus  );
      build_one_sided_stencils<OneSidedStencil2<XMinus> >( opdb, coefHalfDx2Minus );
      build_one_sided_stencils<OneSidedStencil2<YPlus > >( opdb, coefHalfDy2Plus  );
      build_one_sided_stencils<OneSidedStencil2<YMinus> >( opdb, coefHalfDy2Minus );
      build_one_sided_stencils<OneSidedStencil2<ZPlus > >( opdb, coefHalfDz2Plus  );
      build_one_sided_stencils<OneSidedStencil2<ZMinus> >( opdb, coefHalfDz2Minus );

      build_one_sided_stencils<OneSidedStencil3<XPlus > >( opdb, coefHalfDx3Plus  );
      build_one_sided_stencils<OneSidedStencil3<XMinus> >( opdb, coefHalfDx3Minus );
      build_one_sided_stencils<OneSidedStencil3<YPlus > >( opdb, coefHalfDy3Plus  );
      build_one_sided_stencils<OneSidedStencil3<YMinus> >( opdb, coefHalfDy3Minus );
      build_one_sided_stencils<OneSidedStencil3<ZPlus > >( opdb, coefHalfDz3Plus  );
      build_one_sided_stencils<OneSidedStencil3<ZMinus> >( opdb, coefHalfDz3Minus );
    }

    //---------------------------------------------------------------
    // BC Stencils
    REGISTER_BC_STENCILS( SVolField )
    REGISTER_BC_STENCILS( XVolField )
    REGISTER_BC_STENCILS( YVolField )
    REGISTER_BC_STENCILS( ZVolField )
  }

} // namespace SpatialOps
