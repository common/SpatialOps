/*
 * Copyright (c) 2014-2021 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 * ----------------------------------------------------------------------------
*/

#ifndef SpatialOps_MatVecOps_h
#define SpatialOps_MatVecOps_h
#define USE_EIGEN
#include <stdexcept>
#include <sstream>
#include <spatialops/structured/MatVecFields.h>
#include <spatialops/structured/SpatialField.h>
#include <spatialops/SpatialOpsConfigure.h>
#ifdef USE_EIGEN
#include <Eigen/Dense>  // Eigen library
#else // !USE_EIGEN
// Boost::ublas includes //
#include <boost/numeric/ublas/vector.hpp>
#include <boost/numeric/ublas/vector_proxy.hpp>
#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/triangular.hpp>
#include <boost/numeric/ublas/lu.hpp>
#include <boost/numeric/ublas/io.hpp>
#endif // USE_EIGEN
#ifdef SpatialOps_ENABLE_THREADS
#include <spatialops/Semaphore.h>
#include <spatialops/ThreadPool.h>
#endif // SpatialOps_ENABLE_THREADS
#ifdef __CUDACC__
#include <cuda_runtime.h>
#include <cublas_v2.h>
#include "spatialops/structured/CudaMatVecUtility.h"
#endif

/** @file MatVecOps.h
 * @brief Includes objects for operating on matrices of fields
 *
 * IMPORTANT: While reading, the brace indentations are organized such that every
 * open brace and close brace pair are at the exact same indentation. One line blocks
 * are an exception, the close brace is placed at the end of the single line.
 *
 * Two classes in this file provide the interface for linear operations,
 * MatVecOp and MatUnaryOp.  Their functionality is identical except the former
 * is constructed with two objects vs. one with the latter.  The basic
 * procedure for any operation proceeds in 4 steps: first a Mat*Op object is
 * constructed from the rhs; second the ::eval method is called on the lhs;
 * third, the lhs and rhs pointers are given to the operator in the ::launch
 * method; fourth, the problem is divided on available cores and the operation
 * is performed or it is sent to the GPU device and performed.
*/

namespace SpatialOps
{
template< typename FieldT >
struct DotProduct;  ///< fwd declaration as this is a specialized T parameter
template< typename FieldT >
struct MatVecMult;  ///< fwd declaration as this is a specialized T parameter

/** @brief Wraps RHS of a binary operator
 * @author Michael Brown
 * @date April 2016
 *
 * @tparam OpT The operation that this object represents
 *
 * This object is built from the RHS information and uses the template
 * parameter to do the assignment(s). The purpose is to overload result.operator =(RHS)
 * for an intuitive interface, e.g. x = mat.solve(rhs), while allowing generic
 * operations defined by the template parameter. It has a lifespan of one line,
 * it is destroyed once the operator completes its work.
*/
template< typename OpT >
class BinaryMatOp
{
public:
  typedef typename OpT::LeftT LeftT; ///< the left operand on the right hand side
  typedef typename OpT::RightT RightT; ///< the right operand on the right hand side
  typedef typename OpT::ResultT ResultT; ///< the result on the left hand side

  /** @brief Construct an object from a rhs
   *
   * @tparam OpT the operation that will be performed
   * @param left Left operand, the type is defined by the OpT parameter
   * @param right Right operand, the type is defined by the OpT parameter
   *
   * Here we grab pointers to the rhs elements for use by the operator.
  */
  BinaryMatOp ( const LeftT& left, const RightT& right )
    : op1_ ( &left ), op2_ ( &right )
  {}

  /** @brief obtain a pointer to the lhs and call the operator::launch method
   *
   * @param result ResultT* const
   * @return void
   *
   * This is the interface for the lhs of the assignment operation.
   * The pointers to the lhs and rhs operands are given to the operator
   * using the OpT::launch method
  */
  void eval ( ResultT* const result ) const;
private:
  const LeftT* const op1_; ///< left operand on the rhs
  const RightT* const op2_; ///< right operand on the rhs
};

/** @brief Wraps RHS of a binary operator with a scalar
 * @author Michael Brown
 * @date April 2016
 *
 * @tparam OpT The operation that this object represents
 *
 * This object is built from the RHS information and uses the template
 * parameter to do the assignment(s). The purpose is to overload result.operator =(RHS)
 * for an intuitive interface, e.g. x = mat.solve(rhs), while allowing generic
 * operations defined by the template parameter. It has a lifespan of one line,
 * it is destroyed once the operator completes its work.
*/
template< typename OpT >
class BinaryMatScalarOp
{
public:
  typedef typename OpT::LeftT LeftT; ///< the left operand on the right hand side
  typedef typename OpT::RightT RightT; ///< the right operand on the right hand side
  typedef typename OpT::ResultT ResultT; ///< the result on the left hand side

  /** @brief Construct an object from a rhs
   *
   * @tparam OpT the operation that will be performed
   * @param left Left operand, the type is defined by the OpT parameter
   * @param right Right operand, the type is defined by the OpT parameter
   *
   * Here we grab pointers to the rhs elements for use by the operator.
  */
  BinaryMatScalarOp ( const LeftT& left, const RightT& right )
    : op1_ ( left ), op2_ ( &right )
  {}

  /** @brief obtain a pointer to the lhs and call the operator::launch method
   *
   * @param result ResultT* const
   * @return void
   *
   * This is the interface for the lhs of the assignment operation.
   * The pointers to the lhs and rhs operands are given to the operator
   * using the OpT::launch method
  */
  void eval ( ResultT* const result ) const;
private:
  const LeftT op1_; ///< left operand on the rhs
  const RightT* const op2_; ///< right operand on the rhs
};

/** @brief Wraps RHS of a Vector - Scalar or Scalar - Vector operation
 * @author Michael Brown
 * @date April 2016
 *
 * @tparam OpT The operation that this object represents
 *
 * This object is built from the RHS information and uses the template
 * parameter to do the assignment(s). The purpose is to overload result.operator =(RHS)
 * for an intuitive interface, e.g. x = mat.solve(rhs), while allowing generic
 * operations defined by the template parameter. It has a lifespan of one line,
 * it is destroyed once the operator completes its work.
*/
template< typename OpT >
class VecScalarOp : public BinaryMatScalarOp<OpT>
{
public:
  /** @brief Construct an object from a rhs
   *
   * @tparam OpT the operation that will be performed
   * @param left Left operand, the type is defined by the OpT parameter
   * @param right Right operand, the type is defined by the OpT parameter
   *
   * Here we grab pointers to the rhs elements for use by the operator.
  */
  VecScalarOp ( const typename BinaryMatScalarOp<OpT>::LeftT& left, const typename BinaryMatScalarOp<OpT>::RightT& right )
    : BinaryMatScalarOp<OpT>( left, right )
  {}
};

/** @brief Wraps RHS of a Matrix - Scalar or Scalar - Matrix operation
 * @author Michael Brown
 * @date April 2016
 *
 * @tparam OpT The operation that this object represents
 *
 * This object is built from the RHS information and uses the template
 * parameter to do the assignment(s). The purpose is to overload result.operator =(RHS)
 * for an intuitive interface, e.g. x = mat.solve(rhs), while allowing generic
 * operations defined by the template parameter. It has a lifespan of one line,
 * it is destroyed once the operator completes its work.
*/
template< typename OpT >
class MatScalarOp : public BinaryMatScalarOp<OpT>
{
public:
  /** @brief Construct an object from a rhs
   *
   * @tparam OpT the operation that will be performed
   * @param left Left operand, the type is defined by the OpT parameter
   * @param right Right operand, the type is defined by the OpT parameter
   *
   * Here we grab pointers to the rhs elements for use by the operator.
  */
  MatScalarOp ( const typename BinaryMatScalarOp<OpT>::LeftT& left, const typename BinaryMatScalarOp<OpT>::RightT& right )
    : BinaryMatScalarOp<OpT>( left, right )
  {}
};

/** @brief Wraps RHS of a Vector - Vector operation
 * @author Michael Brown
 * @date April 2016
 *
 * @tparam OpT The operation that this object represents
 *
 * This object is built from the RHS information and uses the template
 * parameter to do the assignment(s). The purpose is to overload result.operator =(RHS)
 * for an intuitive interface, e.g. x = mat.solve(rhs), while allowing generic
 * operations defined by the template parameter. It has a lifespan of one line,
 * it is destroyed once the operator completes its work.
*/
template< typename OpT >
class VecVecOp : public BinaryMatOp<OpT>
{
public:
  /** @brief Construct an object from a rhs
   *
   * @tparam OpT the operation that will be performed
   * @param left Left operand, the type is defined by the OpT parameter
   * @param right Right operand, the type is defined by the OpT parameter
   *
   * Here we grab pointers to the rhs elements for use by the operator.
  */
  VecVecOp ( const typename BinaryMatOp<OpT>::LeftT& left, const typename BinaryMatOp<OpT>::RightT& right )
    : BinaryMatOp<OpT>( left, right )
  {}
};

/** @brief Wraps RHS of a Matrix - Vector operation
 * @author Nathan Yonkee
 * @date July 2015
 *
 * @tparam OpT The operation that this object represents
 *
 * This object is built from the RHS information and uses the template
 * parameter to do the assignment(s). The purpose is to overload result.operator =(RHS)
 * for an intuitive interface, e.g. x = mat.solve(rhs), while allowing generic
 * operations defined by the template parameter. It has a lifespan of one line,
 * it is destroyed once the operator completes its work.
*/
template< typename OpT >
class MatVecOp : public BinaryMatOp<OpT>
{
public:

  /** @brief Construct an object from a rhs
   *
   * @tparam OpT the operation that will be performed
   * @param left Left operand, the type is defined by the OpT parameter
   * @param right Right operand, the type is defined by the OpT parameter
   *
   * Here we grab pointers to the rhs elements for use by the operator.
  */
  MatVecOp ( const typename BinaryMatOp<OpT>::LeftT& left, const typename BinaryMatOp<OpT>::RightT& right )
    : BinaryMatOp<OpT>( left, right )
  {}
};

/** @brief Wraps RHS of a Matrix - Matrix operation
 * @author Michael Brown
 * @date April 2016
 *
 * @tparam OpT The operation that this object represents
 *
 * This object is built from the RHS information and uses the template
 * parameter to do the assignment(s). The purpose is to overload result.operator =(RHS)
 * for an intuitive interface, e.g. x = mat.solve(rhs), while allowing generic
 * operations defined by the template parameter. It has a lifespan of one line,
 * it is destroyed once the operator completes its work.
*/
template< typename OpT >
class MatMatOp : public BinaryMatOp<OpT>
{
public:
  /** @brief Construct an object from a rhs
   *
   * @tparam OpT the operation that will be performed
   * @param left Left operand, the type is defined by the OpT parameter
   * @param right Right operand, the type is defined by the OpT parameter
   *
   * Here we grab pointers to the rhs elements for use by the operator.
  */
  MatMatOp ( const typename BinaryMatOp<OpT>::LeftT& left, const typename BinaryMatOp<OpT>::RightT& right )
    : BinaryMatOp<OpT>( left, right )
  {}
};

/** @brief Wraps RHS of a unary matrix operation
 * @author Nathan Yonkee
 * @date July 2015
 *
 * @tparam OpT The operation that this object represents
 *
 * The behavior of this class mirrors that of MatVecOp, refer there for a more complete
 * description. This object grabs pointers to the lhs and rhs and sends them to the operator
 * where the actual calculation gets performed. This object accepts a single object on
 * the rhs.
*/
template< typename OpT >
class MatUnaryOp
{
public:
  typedef typename OpT::RhsT RhsT; ///< type of the rhs
  typedef typename OpT::ResultT ResultT; ///< type of the lhs

  /** @brief Construct an object from a rhs consisting of a single matrix
   *
   * @tparam OpT the operation that will be performed
   * @param mat The operand to the unary operation defined by OpT
   *
   * Here we grab the pointer to the rhs element for use by the operator.
  */
  MatUnaryOp ( const RhsT& mat )
    : mat_ ( &mat )
  {}

  /** @brief obtain a pointer to the lhs and call the operator::launch method
   *
   * @param result ResultT* const
   * @return void
   *
   * This is the interface for the lhs of the assignment operation.
   * The pointers to the lhs and rhs operand are given to the operator
   * using the OpT::launch method
  */
  void eval ( ResultT* const result ) const;
private:
  const RhsT* const mat_;
};

/** @brief Wraps RHS of a unary matrix operation
 * @author Michael Brown
 * @date December 2016
 *
 * @tparam OpT The operation that this object represents
 *
 * The behavior of this class mirrors that of MatVecOp, refer there for a more complete
 * description. This object grabs pointers to the lhs and rhs and sends them to the operator
 * where the actual calculation gets performed. This object accepts a single object on
 * the rhs. This object accepts two objects on the lhs.
*/
template< typename OpT >
class BinaryRetMatUnaryOp
{
public:
  typedef typename OpT::RhsT RhsT; ///< type of the rhs
  typedef typename OpT::ResultT1 ResultT1; ///< type of the first lhs
  typedef typename OpT::ResultT2 ResultT2; ///< type of the second lhs

  /** @brief Construct an object from a rhs consisting of a single matrix
   *
   * @tparam OpT the operation that will be performed
   * @param mat The operand to the unary operation defined by OpT
   *
   * Here we grab the pointer to the rhs element for use by the operator.
  */
  BinaryRetMatUnaryOp ( const RhsT& mat )
    : mat_ ( &mat )
  {}

  /** @brief obtain a pointer to the multiple lhs and call the operator::launch method
   *
   * @param result one ResultT1* const
   * @param result two ResultT2* const
   * @return void
   *
   * This is the interface for the lhs of the assignment operation.
   * The pointers to the lhs and rhs operand are given to the operator
   * using the OpT::launch method
  */
  void eval ( ResultT1* const result1, ResultT2* const result2) const;
private:
  const RhsT* const mat_;
};

/** @brief Specialization which doesn't use FieldT::operator =( ... )
 *
 * For the other operators, we overload the = operator to grab a pointer to the lhs.
 * This specialization grabs the pointer without altering the existing SpatialField.h code.
 * While prototyping this prevents injection of bugs into existing code.
*/
template< typename FieldT >
class MatVecOp< DotProduct< FieldT > >
{
public:
  typedef FieldVector< FieldT > RightT;
  typedef FieldVector< FieldT > LeftT;
  typedef FieldT ResultT;
  MatVecOp ( const LeftT& left, const RightT& right ) ///< grabs pointers for rhs
    : op1_ ( &left ), op2_ ( &right )
  {}
  void eval ( FieldT* const result ) const; ///< grabs lhs pointer and launches operator
private:
  const LeftT*  const op1_;
  const RightT* const op2_;
};

/** @struct  DotProduct
 *
 * @brief  Performs the dot product on two vectors
 *
 * This performs the standard dot product operation.
 * The operations are performed on five elements from each vector at a time.
 * This was an optimal average for us, but a different stride may be better for others.
*/
template< typename FieldT >
struct DotProduct
{
  typedef FieldVector<FieldT> RightT; ///< right operand is a vector
  typedef FieldVector<FieldT> LeftT; ///< left operand is a vector
  typedef FieldT ResultT; ///< result is a field

  /** @brief immediately call operate, Nebo handles differing backends
   *
   * @param result FieldT* const where to put the result
   * @param vec1 const LeftT& the left vector
   * @param vec2 const RightT& right vector
   * @return void
  */
  static void launch( FieldT* const result, const LeftT& vec1, const RightT& vec2 ){
    DotProduct<FieldT>::operate ( result,  vec1, vec2 );
  }

  /** @brief perform the operations using Nebo
   *
   * @param result FieldT* const where to store the result
   * @param vec1 const LeftT& left vector
   * @param vec2 const RightT& right vector
   * @return void
  */
  static void operate( FieldT* const result, const LeftT& vec1, const RightT& vec2 );
};

/** @brief Operation base class defined on matrices and/or vectors
 *
 * Represents a basic binaray operation on matrices or vectors
*/
template< typename OpT, typename RightT, typename LeftT, typename ResultT >
struct MatOperation
{
  /** @brief Called to initiate evaluation
   *
   * @param result ResultT* const pointer to the destination
   * @param mat const LeftT& matrix or vector on the left side
   * @param vec const RightT& matrix or vector on the right side
   * @return void
   *
   * Immediately calls the operate method because Nebo manages multi-thread execution
  */
  static void cpu_launch( ResultT* const result, const LeftT& left, const RightT& right ){
    OpT::operate ( result, left, right );
  }
#ifdef __CUDACC__
  /** @brief Called by initiate evaluation
   *
   * @param result ResultT* const pointer to the destination
   * @param mat const LeftT& matrix or vector on the left side
   * @param vec const RightT& matrix or vector on the right side
   * @return void
   *
   * Immediately calls the operate method because Nebo manages GPU execution
  */
  static void gpu_launch( ResultT* const result, const LeftT& left, const RightT& right ){
    OpT::operate ( result, left, right );
  }
#endif /* __CUDACC__ */
};

/** @brief Multiply a scalar by a vector
 *
 * Performs an operation of the form result = vec * scalar or scalar * vec.
 * This operator uses the same stride ( 5 elements by default ) as the dot
 * product. You may get better performance with varying stride lengths.
*/
template< typename FieldT >
struct VecScalarMult : public MatOperation<VecScalarMult<FieldT>, FieldVector<FieldT>, typename FieldT::value_type, FieldVector<FieldT> >
{ typedef FieldVector<FieldT> RightT; ///< right operand is a vector
  typedef typename FieldT::value_type LeftT; ///< left operand is scalar
  typedef FieldVector<FieldT> ResultT; ///< result is a vector

  /** @brief Evaluate this operation using Nebo as the backend
   *
   * @param result ResultT* const destination vector for solution
   * @param scal   const LeftT&   scalar on the left of *
   * @param vec    const RightT&  vector with which to multiply the scalar
   *
   */
  static void operate( ResultT* const result, const LeftT scal, const RightT& vec );
};

/** @brief Multiply a matrix by a scalar
 *
 * Performs an operation of the form result = vec * scalar or scalar * vec.
 * This operator uses the same stride ( 5 elements by default ) as the dot
 * product. You may get better performance with varying stride lengths.
*/
template< typename FieldT >
struct MatScalarMult : public MatOperation<MatScalarMult<FieldT>, FieldMatrix<FieldT>, typename FieldT::value_type, FieldMatrix<FieldT> >
{ typedef FieldMatrix<FieldT> RightT; ///< right operand is a matrix
  typedef typename FieldT::value_type LeftT; ///< left operand is scalar
  typedef FieldMatrix<FieldT> ResultT; ///< result is a matrix

  /** @brief Evaluate this operation using Nebo as the backend
   *
   * @param result ResultT* const destination matrix for solution
   * @param scal   const LeftT&   scalar on the left of *
   * @param mat    const RightT&  matrix with which to multiply the scalar
   *
   */
  static void operate( ResultT* const result, const LeftT scal, const RightT& mat );
};

/** @brief Multiply a matix by a vector on the right
 *
 * Performs an operation of the form result = mat * vec. This operator uses the
 * same stride ( 5 elements by default ) as the dot product. You may get better performance
 * with varying stride lengths.
*/
template< typename FieldT >
struct MatVecMult : public MatOperation<MatVecMult<FieldT>, FieldVector<FieldT>, FieldMatrix<FieldT>, FieldVector<FieldT> >
{ typedef FieldVector<FieldT> RightT; ///< right operand is a vector
  typedef FieldMatrix<FieldT> LeftT; ///< left operand is matrix
  typedef FieldVector<FieldT> ResultT; ///< result is a vector

  /** @brief Evaluate this operation using Nebo as the backend
   *
   * @param result ResultT* const destination vector for solution
   * @param mat    const LeftT&   matrix on the left of *
   * @param vec    const RightT&  vector with which to multiply the matrix from the right
   *
   */
  static void operate( ResultT* const result, const LeftT& mat, const RightT& vec );
};

/** @brief Multiply a matix by a matrix on the right
 *
 * Performs an operation of the form result = mat1 * mat2. This operator uses the
 * same stride ( 5 elements by default ) as the dot product. You may get better performance
 * with varying stride lengths.
*/
template< typename FieldT >
struct MatMatMult : public MatOperation<MatMatMult<FieldT>, FieldMatrix<FieldT>, FieldMatrix<FieldT>, FieldMatrix<FieldT> >
{ typedef FieldMatrix<FieldT> RightT; ///< right operand is a matrix
  typedef FieldMatrix<FieldT> LeftT; ///< left operand is matrix
  typedef FieldMatrix<FieldT> ResultT; ///< result is a matrix

  /** @brief Evaluate this operation using Nebo as the backend
   *
   * @param result ResultT* const destination vector for solution
   * @param mat1    const LeftT&   matrix on the left of *
   * @param mat2    const RightT&  matrix with which to multiply the matrix from the right
   *
   */
  static void operate( ResultT* const result, const LeftT& mat1, const RightT& mat2 );
};

/** @brief Add a vector to a vector on the right
 *
 * Performs an operation of the form result = vec1 + vec2. This operator uses the
 * same stride ( 5 elements by default ) as the dot product. You may get better performance
 * with varying stride lengths.
*/
template< typename FieldT >
struct VecVecAdd : public MatOperation<VecVecAdd<FieldT>, FieldVector<FieldT>, FieldVector<FieldT>, FieldVector<FieldT> >
{ typedef FieldVector<FieldT> RightT; ///< right operand is a matrix
  typedef FieldVector<FieldT> LeftT; ///< left operand is matrix
  typedef FieldVector<FieldT> ResultT; ///< result is a matrix

  /** @brief Evaluate this operation using Nebo as the backend
   *
   * @param result ResultT* const destination vector for solution
   * @param vec1    const LeftT&   vector on the left
   * @param vec2    const RightT&  vector with which to add the vector from the right
   *
   */
  static void operate( ResultT* const result, const LeftT& vec1, const RightT& vec2 );
};

/** @brief Add a matix to a matrix on the right
 *
 * Performs an operation of the form result = mat1 + mat2. This operator uses the
 * same stride ( 5 elements by default ) as the dot product. You may get better performance
 * with varying stride lengths.
*/
template< typename FieldT >
struct MatMatAdd : public MatOperation<MatMatAdd<FieldT>, FieldMatrix<FieldT>, FieldMatrix<FieldT>, FieldMatrix<FieldT> >
{ typedef FieldMatrix<FieldT> RightT; ///< right operand is a matrix
  typedef FieldMatrix<FieldT> LeftT; ///< left operand is matrix
  typedef FieldMatrix<FieldT> ResultT; ///< result is a matrix

  /** @brief Evaluate this operation using Nebo as the backend
   *
   * @param result ResultT* const destination vector for solution
   * @param mat1    const LeftT&   matrix on the left of
   * @param mat2    const RightT&  matrix with which to add the matrix from the right
   *
   */
  static void operate( ResultT* const result, const LeftT& mat1, const RightT& mat2 );
};

/** @brief Sub a vector to a vector on the right
 *
 * Performs an operation of the form result = vec1 - vec2. This operator uses the
 * same stride ( 5 elements by default ) as the dot product. You may get better performance
 * with varying stride lengths.
*/
template< typename FieldT >
struct VecVecSub : public MatOperation<VecVecSub<FieldT>, FieldVector<FieldT>, FieldVector<FieldT>, FieldVector<FieldT> >
{ typedef FieldVector<FieldT> RightT; ///< right operand is a matrix
  typedef FieldVector<FieldT> LeftT; ///< left operand is matrix
  typedef FieldVector<FieldT> ResultT; ///< result is a matrix

  /** @brief Evaluate this operation using Nebo as the backend
   *
   * @param result ResultT* const destination vector for solution
   * @param vec1    const LeftT&   vector on the left
   * @param vec2    const RightT&  vector with which to subtract the vector from the right
   *
   */
  static void operate( ResultT* const result, const LeftT& vec1, const RightT& vec2 );
};

/** @brief Sub a matix to a matrix on the right
 *
 * Performs an operation of the form result = mat1 - mat2. This operator uses the
 * same stride ( 5 elements by default ) as the dot product. You may get better performance
 * with varying stride lengths.
*/
template< typename FieldT >
struct MatMatSub : public MatOperation<MatMatSub<FieldT>, FieldMatrix<FieldT>, FieldMatrix<FieldT>, FieldMatrix<FieldT> >
{ typedef FieldMatrix<FieldT> RightT; ///< right operand is a matrix
  typedef FieldMatrix<FieldT> LeftT; ///< left operand is matrix
  typedef FieldMatrix<FieldT> ResultT; ///< result is a matrix

  /** @brief Evaluate this operation using Nebo as the backend
   *
   * @param result ResultT* const destination vector for solution
   * @param mat1    const LeftT&   matrix on the left
   * @param mat2    const RightT&  matrix with which to subtract the matrix from the right
   *
   */
  static void operate( ResultT* const result, const LeftT& mat1, const RightT& mat2 );
};

/** @brief Add a scalar value to the diagonal elements of a matrix
 *
 * Performs an operation of the form mat = mat + c*I. This operation 
 * iterates over the diagonal elemenets of mat and adds the scalar value
 * to each of them.
*/
template< typename FieldT >
struct MatScalDiagOffset : public MatOperation<MatScalDiagOffset<FieldT>, FieldMatrix<FieldT>, typename FieldT::value_type, FieldMatrix<FieldT> >
{ typedef FieldMatrix<FieldT> RightT; ///< right operand is a matrix
  typedef typename FieldT::value_type LeftT; ///< left operand is scalar
  typedef FieldMatrix<FieldT> ResultT; ///< result is a matrix

  /** @brief Evaluate this operation using Nebo as the backend
   *
   * @param result  ResultT* const destination matrix for solution- should be the same as RightT mat
   * @param scal    const LeftT&   scalar to offset the diagonal by
   * @param mat     const RightT&  matrix with which to add the scalar to the diagonal of
   *
   */
  static void operate( ResultT* const result, const LeftT scal, const RightT& mat );
};

/** @brief Find the solution to result = inv(mat) * rhsVec with LU decomposition
 *
 * This operator finds the solution vector, x, in the linear equation A; stated
 * differently, we find the solution of x = inv(A) *b with LU decomposition, we
 * never actually compute inv(A) explicitly (except on GPU). This operation is
 * performed using an external library, currently cublas, boost::ublas or
 * Eigen, which is an option in the makefile.  More info included at the
 * operate method.
*/
template< typename FieldT >
class LinearSolve
{
public:
  typedef FieldVector<FieldT> RightT;  ///< right operand is a vector
  typedef FieldMatrix<FieldT> LeftT;   ///< left operand is a matrix
  typedef FieldVector<FieldT> ResultT; ///< result is a vector

  /** @brief this method is the interface to receive needed pointers
   *
   * @param result ResultT* const the vector "x" in Ax=b
   * @param mat const LeftT& the  "A" matrix in Ax=b
   * @param vec const RightT& the vector "b" in Ax=b
   * @return void
   *
   * This method provides the operator with the pointers to the elements in both the
   * right and left hand side. Because we are using an external library, we schedule
   * multicore execution using our thread pool directly. The operate method is called on each
   * available thread and the actual calculation is performed there.
  */
  static void cpu_launch( ResultT* const result, const LeftT& mat, const RightT& vec );
#ifdef __CUDACC__
  /** @brief this method is the interface to receive needed pointers
   *
   * @param result ResultT* const the vector "x" in Ax=b
   * @param mat const LeftT& the  "A" matrix in Ax=b
   * @param vec const RightT& the vector "b" in Ax=b
   * @return void
   *
   * This method provides the operator with the pointers to the elements in both the
   * right and left hand side.
  */
  static void gpu_launch( ResultT* const result, const LeftT& mat, const RightT& vec );
#endif /* __CUDACC__ */
  typedef typename FieldT::value_type ValT; ///< underlying data type, double by default
private:

  /** @brief Use LU decomposition to solve the system "Ax=b"
   *
   * @param result ResultT* const the vector "x" in Ax=b
   * @param vec const RightT& the vector "b" in Ax=b
   * @param mat const LeftT& "A" matrix in Ax=b
   * @param thread const int the number of the working thread to offset array indexing per thread
   * @param sem Semaphore& semaphore to synchronize on
   * @return void
   *
   * This is where the evaluation and assignment actually occurs. One of two libraries can be used,
   * the Eigen library or boost::ublas. Eigen was faster in our benchmarks, but it may be different
   * for you. The algorithm uses partial pivoting and is therefore not absolutely stable, but the
   * algorithm is suitable for general square matrices with real valued elements which are invertible.
   *
   * Our matrix and vector objects hold memory arranged by grouping together all points of each element.
   * We first need to invert this and fill arrays with every element at a single point.
   * This "point" matrix and vector is ready to be solved and the results are reverted to
   * the original organization in SpatialFields.
  */
  static void operate( ResultT* const result,
                       const RightT& vec,
                       const LeftT& mat,
                       const int thread
#                      ifdef SpatialOps_ENABLE_THREADS
                       , Semaphore & sem );
#                      else
                       );
#                      endif // SpatialOps_ENABLE_THREADS

#ifdef __CUDACC__
  /** @brief Use LU decomposition to solve the system "Ax=b"
   *
   * @param handle cublasHandle_t& the cublas handle to operate with
   * @param result ResultT* const the vector "x" in Ax=b
   * @param vec const RightT& the vector "b" in Ax=b
   * @param mat const LeftT& "A" matrix in Ax=b
   * @return void
   *
   * This is where the evaluation and assignment actually occurs. The nvidia
   * cublas library is used to solve the system of equations.  This algorithm
   * is suitable for general square matrices with real valued elements which
   * are invertible.
   *
   * Our matrix and vector objects hold memory arranged by grouping together
   * all points of each element.  We first need to invert this and fill arrays
   * with every element at a single point.  This "point" matrix and vector is
   * ready to be solved and the results are reverted to the original
   * organization in SpatialFields.
  */
  static void gpu_operate( cublasHandle_t & handle,
                           ResultT* const result,
                           const RightT& vec,
                           const LeftT& mat );
#endif /* __CUDACC__ */


};

/** @brief Operator to perform eigen decomposition
 *
 * This class calculates the eigenvalues and eigenvectors of a matrix.  The
 * operations are performed by the Eigen library. This method can take REAL
 * inputs only and returns the REAL and COMPLEX value of the eigen values
 * depending on how it is called. Eigen also provides eigen vectors, but this
 * functionality is not yet included.
*/
template< typename FieldT >
class EigenDecomposition
{
public:
  typedef FieldMatrix<FieldT> RhsT; ///< right hand side is a matrix
  typedef FieldVector<FieldT> ResultT; ///< real value result is a vector
  typedef ResultT             ResultT1;
  typedef FieldVector<FieldT> ResultT2; ///< complex value result is a vector

  /** @brief receive pointers to the operand and result and call operate
   *
   * @param result ResultT* const vector to assign real eigenvalues
   * @param mat const RhsT& the matrix we are operating on
   * @return void
   *
   * This function is called by a MatUnaryOp object or BinaryRetMatUnaryOp
   * object to pass along pointers for the left hand side and right hand side.
   * If multi-threading is on, we divide the work evenly (as possible) across
   * threads and each thread calls ::operate, where the calculation takes
   * place.
  */
  static void cpu_launch( ResultT* const result, const RhsT& mat );
  /** @brief complex value overload of cpu_launch
   *
   * @param result1 ResultT1* const vector to assign real eigenvalues
   * @param result1 ResultT1* const vector to assign complex eigenvalues
   * @param mat const RhsT& the matrix we are operating on
   * @return void
   *
  */
  static void cpu_launch( ResultT1* const result1, ResultT2* const result2, const RhsT& mat );
#ifdef __CUDACC__
  /** @brief receive pointers to the operand and result and call operate
   *
   * @param result ResultT* const vector to assign real eigenvalues
   * @param mat const RhsT& the matrix we are operating on
   * @return void
   *
   * This function is called by a MatUnaryOp object or BinaryRetMatUnaryOp
   * object to pass along pointers for the left hand side and right hand side.
   * This function calls the GPU backend to compute the eigen decomposition.
  */
  static void gpu_launch( ResultT* const result, const RhsT& mat );
  /** @brief complex value overload of gpu_launch
   *
   * @param result1 ResultT1* const vector to assign real eigenvalues
   * @param result1 ResultT1* const vector to assign complex eigenvalues
   * @param mat const RhsT& the matrix we are operating on
   * @return void
   *
  */
  static void gpu_launch( ResultT1* const result1, ResultT2* const result2, const RhsT& mat );
#endif /* __CUDACC__ */
private:

  /** @brief This is where the values are calculated and assigned
   *
   * @param result1 ResultT1* const vector to assign real eigenvalues
   * @param result2 ResultT2* const vector to assign complex eigenvalues
   * @param mat const RhsT& the matrix we are operating on
   * @param thread const int thread index to stagger looping across points
   * @param sem Semaphore& semaphore to synchronize on
   * @return void
   *
   * The memory structure of our fields needs to be inverted to conform to Eigen's
   * interface. LinearSolve explains more detail. Once the calculation is complete,
   * we pack the values back into fields.
  */
  static void operate( ResultT1* const result1,
                       ResultT2* const result2,
                       const RhsT& mat,
                       const int thread
#                      ifdef SpatialOps_ENABLE_THREADS
                       , Semaphore & sem
#                      endif // SpatialOps_ENABLE_THREADS
                       );
#ifdef __CUDACC__
  /** @brief This is where the values are calculated and assigned
   *
   * @param result1 ResultT1* const vector to assign real eigenvalues
   * @param result2 ResultT2* const vector to assign complex eigenvalues
   * @param mat const RhsT& the matrix we are operating on
   * @return void
   *
   * The memory structure of our fields needs to be inverted to conform to our
   * GPU backend interface. LinearSolve explains more detail. Once the
   * calculation is complete, we pack the values back into fields.
  */
  static void gpu_operate( ResultT1* const result1,
                           ResultT2* const result2,
                           const RhsT& mat );
#endif /* __CUDACC__ */

};

template< typename FieldT >
void
FieldMatrix<FieldT>::eigenvalues(FieldVector<FieldT>& realVector, FieldVector<FieldT>& complexVector)     ///< declared in MatVecFields.h
{ return BinaryRetMatUnaryOp< EigenDecomposition<FieldT> > ( *this ).eval(&realVector, &complexVector); }

template< typename FieldT >
MatUnaryOp< EigenDecomposition<FieldT> >
FieldMatrix<FieldT>::real_eigenvalues()     ///< declared in MatVecFields.h
{ return MatUnaryOp< EigenDecomposition<FieldT> > ( *this ); }

template< typename FieldT >
MatVecOp< LinearSolve< FieldT > >
FieldMatrix<FieldT>::solve ( const FieldVector< FieldT >& vec ) ///< declared in MatVecFields.h
{ return MatVecOp< LinearSolve< FieldT > > ( *this, vec ); }

template< typename FieldT >
void
FieldMatrix<FieldT>::add_to_diagonal ( const typename FieldT::value_type c ) ///< declared in MatVecFields.h
{ (MatScalarOp< MatScalDiagOffset< FieldT > > ( c, *this )).eval(this); }

/** @brief Multiply a vector (right) with a scalar, i.e. scal * vec
 *
 * @param left scalar on rhs
 * @param right vector on rhs
 * @return MatScalarOp collects pointers of the left and right hand sides
 *
 * This creates an operator for a simple "scalar * vector" operation
*/
template< typename FieldT >
VecScalarOp< VecScalarMult< FieldT > > operator * ( const typename FieldT::value_type& left, const FieldVector< FieldT >& right )
{ return VecScalarOp< VecScalarMult<FieldT> > ( left, right ); }
template< typename FieldT >
VecScalarOp< VecScalarMult< FieldT > > operator * ( const FieldVector< FieldT >& right, const typename FieldT::value_type& left )
{ return VecScalarOp< VecScalarMult<FieldT> > ( left, right ); }

/** @brief Multiply a matrix (right) with a scalar, i.e. scal * mat
 *
 * @param left scalar on rhs
 * @param right matrix on rhs
 * @return MatVecOp collects pointers of the left and right hand sides
 *
 * This creates an operator for a simple "scalar * matrix" operation
*/
template< typename FieldT >
MatScalarOp< MatScalarMult< FieldT > > operator * ( const typename FieldT::value_type& left, const FieldMatrix< FieldT >& right )
{ return MatScalarOp< MatScalarMult<FieldT> > ( left, right ); }
template< typename FieldT >
MatScalarOp< MatScalarMult< FieldT > > operator * ( const FieldMatrix< FieldT >& right, const typename FieldT::value_type& left )
{ return MatScalarOp< MatScalarMult<FieldT> > ( left, right ); }

/** @brief Multiply a matrix (right) with a vector on the left, i.e. mat * vec
 *
 * @param left matrix on rhs
 * @param right vector on rhs
 * @return MatVecOp collects pointers of the left and right hand sides
 *
 * This creates an operator for a simple "matrix * vector" operation
*/
template< typename FieldT >
MatVecOp< MatVecMult< FieldT > > operator * ( const FieldMatrix< FieldT >& left, const FieldVector< FieldT >& right )
{ return MatVecOp< MatVecMult<FieldT> > ( left, right ); }

/** @brief Multiply a matrix (right) with another matrix on the left, i.e. mat1 * mat2
 *
 * @param left matrix on rhs
 * @param right matrix on rhs
 * @return MatVecOp collects pointers of the left and right hand sides
 *
 * This creates an operator for a simple "matrix * matrix" operation
*/
template< typename FieldT >
MatMatOp< MatMatMult< FieldT > > operator * ( const FieldMatrix< FieldT >& left, const FieldMatrix< FieldT >& right )
{ return MatMatOp< MatMatMult<FieldT> > ( left, right ); }

/** @brief Add a vector (right) with another vector on the left, i.e. vec1 + vec2
 *
 * @param left vector on rhs
 * @param right vector on rhs
 * @return VecVecOp collects pointers of the left and right hand sides
 *
 * This creates an operator for a simple "vector + vector" operation
*/
template< typename FieldT >
VecVecOp< VecVecAdd< FieldT > > operator + ( const FieldVector< FieldT >& left, const FieldVector< FieldT >& right )
{ return VecVecOp< VecVecAdd<FieldT> > ( left, right ); }

/** @brief Add a matrix (right) with another matrix on the left, i.e. mat1 + mat2
 *
 * @param left matrix on rhs
 * @param right matrix on rhs
 * @return MatMatOp collects pointers of the left and right hand sides
 *
 * This creates an operator for a simple "matrix + matrix" operation
*/
template< typename FieldT >
MatMatOp< MatMatAdd< FieldT > > operator + ( const FieldMatrix< FieldT >& left, const FieldMatrix< FieldT >& right )
{ return MatMatOp< MatMatAdd<FieldT> > ( left, right ); }

/** @brief Sub a vector (right) with another vector on the left, i.e. vec1 - vec2
 *
 * @param left vector on rhs
 * @param right vector on rhs
 * @return VecVecOp collects pointers of the left and right hand sides
 *
 * This creates an operator for a simple "vector - vector" operation
*/
template< typename FieldT >
VecVecOp< VecVecSub< FieldT > > operator - ( const FieldVector< FieldT >& left, const FieldVector< FieldT >& right )
{ return VecVecOp< VecVecSub<FieldT> > ( left, right ); }

/** @brief Sub a matrix (right) with another matrix on the left, i.e. mat1 - mat2
 *
 * @param left matrix on rhs
 * @param right matrix on rhs
 * @return MatMatOp collects pointers of the left and right hand sides
 *
 * This creates an operator for a simple "matrix - matrix" operation
*/
template< typename FieldT >
MatMatOp< MatMatSub< FieldT > > operator - ( const FieldMatrix< FieldT >& left, const FieldMatrix< FieldT >& right )
{ return MatMatOp< MatMatSub<FieldT> > ( left, right ); }

/** @brief calculate the dot product of two vectors
 *
 * @param vec1 a vector to dot with
 * @param vec2 other vector to dot with
 * @return DotProduct object to interface with assignment operator
 *
 * Returns the operator for the dot prod. Implementation is
 * still a prototype partly because we do not want to add an extra operator
 * before ensuring the validity of this code.
*/
template< typename FieldT >
static MatVecOp< DotProduct< FieldT > > dot_product_proto ( const FieldVector< FieldT >& vec1, const FieldVector< FieldT >& vec2  )
{ return MatVecOp< DotProduct< FieldT > > ( vec1, vec2 ); }

template< typename OpT >
void BinaryMatOp< OpT >::eval ( ResultT* const result ) const
{ assert ( result->elements() == op1_->elements() && op1_->elements() == op2_->elements() ); // ensure dimensions are consistent
#ifdef __CUDACC__
  enum INCORRECT_MEMORY {NONE, GPU, CPU, NEITHER};
  INCORRECT_MEMORY incorrectMemory = NONE;
  const int deviceIndex = result->active_device_index();
  if( IS_GPU_INDEX(deviceIndex) ){
    if( op1_->is_valid(deviceIndex) && op2_->is_valid(deviceIndex) ){
      OpT::gpu_launch ( result, *op1_, *op2_ );
    }
    else{
      incorrectMemory = GPU;
    }
  }
  else if(result->is_valid(CPU_INDEX)){
    if(op1_->is_valid(CPU_INDEX) && op2_->is_valid(CPU_INDEX)){
      OpT::cpu_launch ( result, *op1_, *op2_ );
    }
    else{
      incorrectMemory = CPU;
    }
  }
  else{
      incorrectMemory = NEITHER;
  }

  if( incorrectMemory != NONE ){
     std::ostringstream msg;
     msg << "Nebo error in " << "Nebo Mat Binary Op Launch" <<
     ":\n";
     msg << "Left-hand side of assignment allocated ";
     msg << "on ";
     switch( incorrectMemory ){
       case GPU:
         msg << "GPU but right-hand side is not ";
         msg << "(completely) accessible on the same GPU";
         break;
       case CPU:
         msg << "CPU but right-hand side is not ";
         msg << "(completely) accessible on the same CPU";
         break;
       case NEITHER:
         msg << "unknown device - not on CPU or GPU";
         break;
     }
     msg << "\n";
     msg << "\t - " << __FILE__ << " : " << __LINE__;
     throw(std::runtime_error(msg.str()));
  }
#else
  OpT::cpu_launch ( result, *op1_, *op2_ );
#endif /* __CUDACC__ */
}

template< typename OpT >
void BinaryMatScalarOp< OpT >::eval ( ResultT* const result ) const
{ assert ( result->elements() ==  op2_->elements() ); // ensure dimensions are consistent
#ifdef __CUDACC__
  enum INCORRECT_MEMORY {NONE, GPU, CPU, NEITHER};
  INCORRECT_MEMORY incorrectMemory = NONE;
  const int deviceIndex = result->active_device_index();
  if( IS_GPU_INDEX(deviceIndex) ){
    if( op2_->is_valid(deviceIndex) ){
      OpT::gpu_launch ( result, op1_, *op2_ );
    }
    else{
      incorrectMemory = GPU;
    }
  }
  else if(result->is_valid(CPU_INDEX)){
    if(op2_->is_valid(CPU_INDEX)){
      OpT::cpu_launch ( result, op1_, *op2_ );
    }
    else{
      incorrectMemory = CPU;
    }
  }
  else{
      incorrectMemory = NEITHER;
  }

  if( incorrectMemory != NONE ){
     std::ostringstream msg;
     msg << "Nebo error in " << "Nebo Mat Binary Op Launch" <<
     ":\n";
     msg << "Left-hand side of assignment allocated ";
     msg << "on ";
     switch( incorrectMemory ){
       case GPU:
         msg << "GPU but right-hand side is not ";
         msg << "(completely) accessible on the same GPU";
         break;
       case CPU:
         msg << "CPU but right-hand side is not ";
         msg << "(completely) accessible on the same CPU";
         break;
       case NEITHER:
         msg << "unknown device - not on CPU or GPU";
         break;
     }
     msg << "\n";
     msg << "\t - " << __FILE__ << " : " << __LINE__;
     throw(std::runtime_error(msg.str()));
  }
#else
  OpT::cpu_launch ( result, op1_, *op2_ );
#endif /* __CUDACC__ */
}

template< typename OpT >
void MatUnaryOp< OpT >::eval ( ResultT* const result ) const
{ assert ( result->elements() == mat_->elements() ); // ensure dimensions are consistent
#ifdef __CUDACC__
  enum INCORRECT_MEMORY {NONE, GPU, CPU, NEITHER};
  INCORRECT_MEMORY incorrectMemory = NONE;
  const int deviceIndex = result->active_device_index();
  if( IS_GPU_INDEX(deviceIndex) ){
    if( mat_->is_valid(deviceIndex) ){
      OpT::gpu_launch ( result, *mat_ );
    }
    else{
      incorrectMemory = GPU;
    }
  }
  else if( result->is_valid(CPU_INDEX) ){
    if( mat_->is_valid(CPU_INDEX) ){
      OpT::cpu_launch ( result, *mat_ );
    }
    else{
      incorrectMemory = CPU;
    }
  }
  else{
    incorrectMemory = NEITHER;
  }

  if( incorrectMemory != NONE ){
     std::ostringstream msg;
     msg << "Nebo error in MatVec Launch:\n"
         << "Left-hand side of assignment allocated on ";
     switch( incorrectMemory ){
       case GPU:
         msg << "GPU but right-hand side is not (completely) accessible on the same GPU";
         break;
       case CPU:
         msg << "CPU but right-hand side is not (completely) accessible on the same CPU";
         break;
       case NEITHER:
         msg << "unknown device - not on CPU or GPU";
         break;
     }
     msg << "\n";
     msg << "\t - " << __FILE__ << " : " << __LINE__;
     throw(std::runtime_error(msg.str()));
  }
#else
  OpT::cpu_launch ( result, *mat_ );
#endif /* __CUDACC__ */
}

template< typename OpT >
void BinaryRetMatUnaryOp< OpT >::eval ( ResultT1* const result1, ResultT2* const result2 ) const
{ assert ( result1->elements() == mat_->elements() && result2->elements() == mat_->elements() ); // ensure dimensions are consistent
#ifdef __CUDACC__
  enum INCORRECT_MEMORY {NONE, GPU, CPU, NEITHER};
  INCORRECT_MEMORY incorrectMemoryResults = NONE;
  INCORRECT_MEMORY incorrectMemory = NONE;
  const int deviceIndex = result1->active_device_index();
  if( IS_GPU_INDEX(deviceIndex) ){
    if( result2->is_valid(deviceIndex) ) {
      if( mat_->is_valid(deviceIndex) ){
        OpT::gpu_launch ( result1, result2, *mat_ );
      }
      else{
        incorrectMemory = GPU;
      }
    }
    else{
      incorrectMemoryResults = GPU;
    }
  }
  else if( result1->is_valid(CPU_INDEX) ){
    if( result2->is_valid(CPU_INDEX) )
    {
      if( mat_->is_valid(CPU_INDEX) ){
        OpT::cpu_launch ( result1, result2, *mat_ );
      }
      else{
        incorrectMemory = CPU;
      }
    }
    else{
      incorrectMemoryResults = CPU;
    }
  }
  else{
    incorrectMemory = NEITHER;
  }

  if( incorrectMemoryResults != NONE ){
     std::ostringstream msg;
     msg << "Nebo error in MatVec Launch:\n"
         << "First left-hand side of assignment allocated on ";
     switch( incorrectMemoryResults ){
       case GPU:
         msg << "GPU but other left-hand side is not (completely) accessible on the same GPU";
         break;
       case CPU:
         msg << "CPU but other left-hand side is not (completely) accessible on the same CPU";
         break;
       case NEITHER:
         msg << "unknown device - not on CPU or GPU";
         break;
     }
     msg << "\n";
     msg << "\t - " << __FILE__ << " : " << __LINE__;
     throw(std::runtime_error(msg.str()));
  }
  if( incorrectMemory != NONE ){
     std::ostringstream msg;
     msg << "Nebo error in MatVec Launch:\n"
         << "Left-hand side of assignment allocated on ";
     switch( incorrectMemory ){
       case GPU:
         msg << "GPU but right-hand side is not (completely) accessible on the same GPU";
         break;
       case CPU:
         msg << "CPU but right-hand side is not (completely) accessible on the same CPU";
         break;
       case NEITHER:
         msg << "unknown device - not on CPU or GPU";
         break;
     }
     msg << "\n";
     msg << "\t - " << __FILE__ << " : " << __LINE__;
     throw(std::runtime_error(msg.str()));
  }
#else
  OpT::cpu_launch ( result1, result2, *mat_ );
#endif /* __CUDACC__ */
}

template< typename FieldT >
void MatVecOp< DotProduct< FieldT > >::eval ( FieldT* const result ) const
{
  assert ( op1_->elements() == op2_->elements() ); // ensure dimensions are consistent
  assert ( result->window_with_ghost().glob_npts() == op1_->at(0).window_with_ghost().glob_npts() ); // ensure lengths are consistent
  DotProduct<FieldT>::launch ( result, *op1_, *op2_ );
}

#ifdef __CUDACC__
template< typename FieldT >
void EigenDecomposition< FieldT >::gpu_launch ( ResultT* const result, const RhsT& mat )
{
  assert ( result->elements() == mat.elements() );  // ensure dimensions are consistent

  gpu_launch(result, NULL, mat);
}
template< typename FieldT >
void EigenDecomposition< FieldT >::gpu_launch ( ResultT1* const result1, ResultT2* const result2, const RhsT& mat )
{
  assert ( result1->elements() == mat.elements() );  // ensure dimensions are consistent
  assert ( result2 == NULL || result2->elements() == mat.elements() );  // ensure dimensions are consistent

  gpu_operate(result1, result2, mat);
}
#endif /* __CUDACC__ */
template< typename FieldT >
void EigenDecomposition< FieldT >::cpu_launch ( ResultT* const result, const RhsT& mat )
{
  assert ( result->elements() == mat.elements() );  // ensure dimensions are consistent

  cpu_launch(result, NULL, mat);
}
template< typename FieldT >
void EigenDecomposition< FieldT >::cpu_launch ( ResultT1* const result1, ResultT2* const result2, const RhsT& mat )
{
  assert ( result1->elements() == mat.elements() );  // ensure dimensions are consistent
  assert ( result2 == NULL || result2->elements() == mat.elements() );  // ensure dimensions are consistent

  int t = NTHREADS; // we use boost::bind to call ::operate on all NTHREADS
# ifdef SpatialOps_ENABLE_THREADS
  Semaphore sem;
  while ( t-- ){
    ThreadPoolFIFO::self().schedule ( boost::bind ( operate,
                                                    result1,
                                                    result2,
                                                    boost::cref(mat),
                                                    t,
                                                    boost::ref(sem)
                                                  ) );
  }
  t = NTHREADS;  // wait on all threads to finish
  while ( t-- ){ sem.wait(); }
# else // !SpatialOps_ENABLE_THREADS
  operate ( result1,
            result2,
            mat,
            --t
            );
# endif // SpatialOps_ENABLE_THREADS
}

#ifdef __CUDACC__
template< typename FieldT >
void LinearSolve<FieldT>::gpu_launch ( ResultT* const result, const LeftT& mat, const RightT& vec )
{
  assert ( result->elements() == vec.elements() && mat.elements() == vec.elements() );  // matrix is square

  cublasHandle_t handle;
  CudaMatVecUtility::cublas_err_chk(cublasCreate(&handle));

  gpu_operate(handle, result, vec, mat);

  CudaMatVecUtility::cublas_err_chk(cublasDestroy(handle));
}
#endif /* __CUDACC__ */
template< typename FieldT >
void LinearSolve<FieldT>::cpu_launch ( ResultT* const result, const LeftT& mat, const RightT& vec )
{
  int t = NTHREADS; // we use boost::bind to call ::operate on all NTHREADS
# ifdef SpatialOps_ENABLE_THREADS
  Semaphore sem;
  while ( t-- ){
    ThreadPoolFIFO::self().schedule ( boost::bind ( LinearSolve<FieldT>::operate, result, boost::cref(vec), boost::cref(mat), t, boost::ref(sem) ) );
  }
  t = NTHREADS;  // wait on all threads to finish
  while( t-- ){ sem.wait(); }
# else  // !SpatialOps_ENABLE_THREADS
  LinearSolve<FieldT>::operate ( result, vec, mat, --t );
# endif // SpatialOps_ENABLE_THREADS
# ifndef NDEBUG
  size_t i = result->elements();
  while( i-- ){
    FieldT& r = result->at(i);
    size_t xyz = r.window_with_ghost().glob_npts();
    while( xyz-- ){
      const typename FieldT::value_type val = r[xyz];
      if( std::isnan(val) || std::isinf(val) ){
         std::ostringstream msg;
         msg << "Nebo error in " << "LinAlgCPU Kernel" <<
         ":\n";
         msg << "	 - " << "Matrix " << i << " may be singular.";
         msg << "\n";
         msg << "\t - " << __FILE__ << " : " << __LINE__;
         throw(std::runtime_error(msg.str()));;
      }
    }
  }
# endif /* NDEBUG */
}

template< typename FieldT >
void DotProduct<FieldT>::operate ( FieldT* const result, const LeftT& vec1, const RightT& vec2 )
{
  const int stride = 5; ///< stride length has an appreciable effect on performance
  FieldT& res = *result; // grab the reference for readability
  int j = vec1.elements();
  if( j-- < stride ){
    res <<= vec1 ( j ) * vec2 ( j );
  } // assignment before accumulation loop
  else{  // in prev line "if( j-- )" changed the value of j
    res <<=  vec1 ( j   ) * vec2 ( j   ) + vec1 ( j-1 ) * vec2 ( j-1 ) + vec1 ( j-2 ) * vec2 ( j-2 ) \
           + vec1 ( j-3 ) * vec2 ( j-3 ) + vec1 ( j-4 ) * vec2 ( j-4 ); // assignment before accumulation loop
    for( j -= ( stride - 1 ); j >= stride; j -= stride ){ // accumulate "stride" elements at a time
      res <<= res + vec1 ( j-1 ) * vec2 ( j-1 ) + vec1 ( j-2 ) * vec2 ( j-2 ) + vec1 ( j-3 ) * vec2 ( j-3 ) \
                  + vec1 ( j-4 ) * vec2 ( j-4 ) + vec1 ( j-5 ) * vec2 ( j-5 ); }
  }
  while ( j-- ){ // remaining elements less than "stride"
   res <<= res + vec1 ( j ) * vec2 ( j );
  }
}

template< typename FieldT >
void VecScalarMult< FieldT >::operate ( ResultT* const result, const LeftT scal, const RightT& vec )
{
  assert( result->elements() == vec.elements() );
  const int stride = 5; ///< stride length has an appreciable effect on performance
  const int elements = result->elements();
  int i;
  i = elements;
  while( i - stride >= 0 )
  {
    result->at(i-1) <<= scal * vec(i-1);
    result->at(i-2) <<= scal * vec(i-2);
    result->at(i-3) <<= scal * vec(i-3);
    result->at(i-4) <<= scal * vec(i-4);
    result->at(i-5) <<= scal * vec(i-5);
    i -= stride;
  }
  while( i-- ) {
    result->at(i) <<= scal * vec(i);
  }
}

template< typename FieldT >
void MatScalarMult< FieldT >::operate ( ResultT* const result, const LeftT scal, const RightT& mat )
{
  assert( result->elements() == mat.elements() );
  const int stride = 5; ///< stride length has an appreciable effect on performance
  const int elements = result->elements();
  int ij;
  ij = elements*elements;
  while( ij - stride >= 0 )
  {
    result->at(ij-1) <<= scal * mat(ij-1);
    result->at(ij-2) <<= scal * mat(ij-2);
    result->at(ij-3) <<= scal * mat(ij-3);
    result->at(ij-4) <<= scal * mat(ij-4);
    result->at(ij-5) <<= scal * mat(ij-5);
    ij -= stride;
  }
  while( ij-- ) {
    result->at(ij) <<= scal * mat(ij);
  }
}

template< typename FieldT >
void VecVecAdd< FieldT >::operate ( ResultT* const result, const LeftT& vec1, const RightT& vec2 )
{
  assert( result->elements() == vec1.elements() && vec1.elements() == vec2.elements() );
  const int stride = 5; ///< stride length has an appreciable effect on performance
  const int elements = result->elements();
  int i;
  i = elements;
  while( i - stride >= 0 )
  {
    result->at(i-1) <<= vec1(i-1) + vec2(i-1);
    result->at(i-2) <<= vec1(i-2) + vec2(i-2);
    result->at(i-3) <<= vec1(i-3) + vec2(i-3);
    result->at(i-4) <<= vec1(i-4) + vec2(i-4);
    result->at(i-5) <<= vec1(i-5) + vec2(i-5);
    i -= stride;
  }
  while( i-- ) {
    result->at(i) <<= vec1(i) + vec2(i);
  }
}

template< typename FieldT >
void MatMatAdd< FieldT >::operate ( ResultT* const result, const LeftT& mat1, const RightT& mat2 )
{
  assert( result->elements() == mat1.elements() && mat1.elements() == mat2.elements() );
  const int stride = 5; ///< stride length has an appreciable effect on performance
  const int elements = result->elements();
  int ij;
  ij = elements * elements;
  while( ij - stride >= 0 )
  {
    result->at(ij-1) <<= mat1(ij-1) + mat2(ij-1);
    result->at(ij-2) <<= mat1(ij-2) + mat2(ij-2);
    result->at(ij-3) <<= mat1(ij-3) + mat2(ij-3);
    result->at(ij-4) <<= mat1(ij-4) + mat2(ij-4);
    result->at(ij-5) <<= mat1(ij-5) + mat2(ij-5);
    ij -= stride;
  }
  while( ij-- ) {
    result->at(ij) <<= mat1(ij) + mat2(ij);
  }
}

template< typename FieldT >
void VecVecSub< FieldT >::operate ( ResultT* const result, const LeftT& vec1, const RightT& vec2 )
{
  assert( result->elements() == vec1.elements() && vec1.elements() == vec2.elements() );
  const int stride = 5; ///< stride length has an appreciable effect on performance
  const int elements = result->elements();
  int i;
  i = elements;
  while( i - stride >= 0 )
  {
    result->at(i-1) <<= vec1(i-1) - vec2(i-1);
    result->at(i-2) <<= vec1(i-2) - vec2(i-2);
    result->at(i-3) <<= vec1(i-3) - vec2(i-3);
    result->at(i-4) <<= vec1(i-4) - vec2(i-4);
    result->at(i-5) <<= vec1(i-5) - vec2(i-5);
    i -= stride;
  }
  while( i-- ) {
    result->at(i) <<= vec1(i) - vec2(i);
  }
}

template< typename FieldT >
void MatMatSub< FieldT >::operate ( ResultT* const result, const LeftT& mat1, const RightT& mat2 )
{
  assert( result->elements() == mat1.elements() && mat1.elements() == mat2.elements() );
  const int stride = 5; ///< stride length has an appreciable effect on performance
  const int elements = result->elements();
  int ij;
  ij = elements * elements;
  while( ij - stride >= 0 )
  {
    result->at(ij-1) <<= mat1(ij-1) - mat2(ij-1);
    result->at(ij-2) <<= mat1(ij-2) - mat2(ij-2);
    result->at(ij-3) <<= mat1(ij-3) - mat2(ij-3);
    result->at(ij-4) <<= mat1(ij-4) - mat2(ij-4);
    result->at(ij-5) <<= mat1(ij-5) - mat2(ij-5);
    ij -= stride;
  }
  while( ij-- ) {
    result->at(ij) <<= mat1(ij) - mat2(ij);
  }
}

template< typename FieldT >
void MatVecMult< FieldT >::operate ( ResultT* const result, const LeftT& mat, const RightT& vec )
{
  const int stride = 5; ///< stride length has an appreciable effect on performance
  const int elements = result->elements();
  int i, j, ij;
  i = elements;
  ij = elements * elements;
  while ( i-- ){
    j = elements;
    FieldT& res = result->at(i); // grab a reference for readability
    if( j < stride ){
     res <<= mat ( --ij ) * vec ( --j );
    }
    else{
      res <<=  mat ( ij-1 ) * vec ( j-1 ) + mat ( ij-2 ) * vec ( j-2 ) + mat ( ij-3 ) * vec ( j-3 ) \
             + mat ( ij-4 ) * vec ( j-4 ) + mat ( ij-5 ) * vec ( j-5 );
      ij -= stride;
      for( j -= stride; j >= stride; j -= stride, ij -= stride ){
        res <<= res + mat ( ij-1 ) * vec ( j-1 ) + mat ( ij-2 ) * vec ( j-2 ) + mat ( ij-3 ) * vec ( j-3 ) \
                    + mat ( ij-4 ) * vec ( j-4 ) + mat ( ij-5 ) * vec ( j-5 ); }
    }
    while ( j-- ){
      res <<= res + mat ( --ij ) * vec ( j );
    }
  }
}

template< typename FieldT >
void MatMatMult< FieldT >::operate ( ResultT* const result, const LeftT& mat1, const RightT& mat2 )
{
  assert( result->elements() == mat1.elements() && mat1.elements() == mat2.elements() );
  const int stride = 5; ///< stride length has an appreciable effect on performance
  const int elements = result->elements();
  int i, j, ij, columns;
  columns = elements;
  while( columns-- ){
    i = elements;
    ij = elements * elements;
    while ( i-- ){
      j = elements;
      FieldT& res = result->at(columns + i*elements); // grab a reference for readability
      if( j < stride ){
       res <<= mat1 ( --ij ) * mat2 ( columns + (--j)*elements );
      }
      else{
        res <<=  mat1 ( ij-1 ) * mat2 ( columns+(j-1)*elements ) + mat1 ( ij-2 ) * mat2 ( columns+(j-2)*elements ) + mat1 ( ij-3 ) * mat2 ( columns+(j-3)*elements ) \
               + mat1 ( ij-4 ) * mat2 ( columns+(j-4)*elements ) + mat1 ( ij-5 ) * mat2 ( columns+(j-5)*elements );
        ij -= stride;
        for( j -= stride; j >= stride; j -= stride, ij -= stride ){
          res <<= res + mat1 ( ij-1 ) * mat2 ( columns+(j-1)*elements ) + mat1 ( ij-2 ) * mat2 ( columns+(j-2)*elements ) + mat1 ( ij-3 ) * mat2 ( columns+(j-3)*elements ) \
                      + mat1 ( ij-4 ) * mat2 ( columns+(j-4)*elements ) + mat1 ( ij-5 ) * mat2 ( columns+(j-5)*elements ); }
      }
      while ( j-- ){
        res <<= res + mat1 ( --ij ) * mat2 ( columns+j*elements );
      }
    }
  }
}

template< typename FieldT >
void MatScalDiagOffset< FieldT >::operate ( ResultT* const result, const LeftT scal, const RightT& mat )
{
  assert( result == &mat );
  const int elements = result->elements();
  int diag;
  diag = elements*elements;
  while( --diag >= 0 )
  {
    result->at(diag) <<= scal + mat(diag);
    diag -= elements;
  }
}


template< typename FieldT >
void EigenDecomposition< FieldT >::operate ( ResultT1* const result1,
                                             ResultT2* const result2,
                                             const RhsT& mat,
                                             const int thread
#                                            ifdef SpatialOps_ENABLE_THREADS
                                             , Semaphore & sem
#                                            endif // SpatialOps_ENABLE_THREADS
                                             )
{
  typedef typename FieldT::value_type ValT;
  const int dim = mat.elements();
  const ValT* mVals[dim][dim];
  int i, j;
  i = dim;
  while( i-- ){
    j = dim;
    while( j-- ){
      mVals[i][j] = mat ( i, j ).field_values();
    }
  }
# ifdef USE_EIGEN
  Eigen::MatrixXd mPt = Eigen::MatrixXd ( dim, dim );
# endif // USE_EIGEN
  int xyz = (mat ( 0 ).window_with_ghost().glob_npts()-1) - thread;
  for( ; xyz >= 0; xyz -= NTHREADS ){
    i = dim;
    while( i-- ){
      j = dim;
      while( j-- ){
        mPt ( i, j ) = mVals[i][j][xyz];
      }
    }
#   ifdef USE_EIGEN
    Eigen::EigenSolver<Eigen::MatrixXd> solver ( mPt );
    const Eigen::EigenSolver<Eigen::MatrixXd>::EigenvalueType eigs = solver.eigenvalues();
    if(result2 == NULL)
    {
      i = dim;
      while( i-- ){
        result1->at ( i ) [xyz] = eigs.coeffRef ( i ).real();
      }
    }
    else
    {
      i = dim;
      while( i-- ){
        result1->at ( i ) [xyz] = eigs.coeffRef ( i ).real();
        result2->at ( i ) [xyz] = eigs.coeffRef ( i ).imag();
      }
    }
#   endif // USE_EIGEN
  }
# ifdef SpatialOps_ENABLE_THREADS
  sem.post();
# endif // SpatialOps_ENABLE_THREADS
}
#ifdef __CUDACC__
template< typename FieldT >
void EigenDecomposition< FieldT >::gpu_operate ( ResultT1* const result1,
                                                 ResultT2* const result2,
                                                 const RhsT& mat)
{
  using namespace CudaMatVecUtility;
  assert( result1->active_device_index() == mat.active_device_index() );
  assert( result2 == NULL || result2->active_device_index() == mat.active_device_index() );

  typedef typename FieldT::value_type ValT;

  const int dim = result1->elements();
  const int activeIndex = mat.active_device_index();

  const bool returnComplex = result2 != NULL;

  ema::cuda::CUDADeviceInterface& CDI = ema::cuda::CUDADeviceInterface::self();

  const int matDim = dim*dim;

        ValT*  h_lAddrs1[dim];
        ValT** h_lAddrs2;
  const ValT*  h_mAddrs[matDim];
  int i, j;
  i = dim;
  if(returnComplex)
  {
    h_lAddrs2 = new ValT*[dim];
    while ( i-- )
    { h_lAddrs1[i] = result1->at ( i ).field_values(activeIndex);
      h_lAddrs2[i] = result2->at ( i ).field_values(activeIndex);
      j = dim;
      while ( j-- )
      { h_mAddrs[i*dim + j] = mat ( i, j ).field_values(activeIndex); }
    }
  }
  else
  {
    while ( i-- )
    { h_lAddrs1[i] = result1->at ( i ).field_values(activeIndex);
      j = dim;
      while ( j-- )
      { h_mAddrs[i*dim + j] = mat ( i, j ).field_values(activeIndex); }
    }
  }

  const int xyz = result1->at(0).window_with_ghost().glob_npts();

  //Copy addresses to GPU in native interleaved order
  PoolAutoPtr<ValT*> d_mAddrs( Pool<ValT*>::get( activeIndex, matDim ), activeIndex);
  PoolAutoPtr<ValT*> d_lAddrs1(Pool<ValT*>::get( activeIndex, dim )   , activeIndex);
  PoolAutoPtr<ValT*> d_lAddrs2(returnComplex ? Pool<ValT*>::get( activeIndex, dim ) : NULL,
                               activeIndex);
  {
    CDI.memcpy_to(d_mAddrs,  h_mAddrs,  matDim*sizeof(ValT*), activeIndex, result1->at(0).get_stream());
    CDI.memcpy_to(d_lAddrs1, h_lAddrs1, dim*sizeof(ValT*),    activeIndex, result1->at(0).get_stream());

    if(returnComplex)
    {
      //No race condition here as far as deleting memory since this behaves
      //synchronously when using pageable memory.
      CDI.memcpy_to(d_lAddrs2, h_lAddrs2, dim*sizeof(ValT*),    activeIndex, result1->at(0).get_stream());
      delete[] h_lAddrs2;
    }

    #ifndef NDEBUG
    cudaError err;

    if( cudaSuccess != (err = cudaDeviceSynchronize()) ){
      std::ostringstream msg;
      msg << "Nebo error in LinAlgCUDA Kernel - after copy:\n\t"
          << cudaGetErrorString(err)
          << "\n\t" << __FILE__ << " : " << __LINE__;
      throw(std::runtime_error(msg.str()));;
    }
    #endif
    /* NDEBUG */;
  }


  //Make call to eigen decomp in cuda mat vec utility
  {
    PoolAutoPtr<int> d_status(Pool<int>::get( activeIndex, xyz ), activeIndex);
    MatVecOptions_e option = returnComplex ? MatVecOptions::NONE : MatVecOptions::DISREGARD_IMAGINARY;

    int computeBlockDim = 128;
    int computeGridDim  = xyz / computeBlockDim + ((xyz % computeBlockDim) > 0 ? 1 : 0);

    sync_field_streams(mat, matDim, result1->at(0), false);
    sync_field_streams(*result1, dim, result1->at(0), true);
    if(returnComplex)
      sync_field_streams(*result2, dim, result1->at(0), true);

    CudaMatVecUtility::balanc<<<computeGridDim, computeBlockDim, activeIndex, result1->at(0).get_stream()>>>(d_mAddrs.get(), dim, xyz);
    CudaMatVecUtility::elmhes<<<computeGridDim, computeBlockDim, activeIndex, result1->at(0).get_stream()>>>(d_mAddrs.get(), dim, xyz);
    CudaMatVecUtility::hqr   <<<computeGridDim, computeBlockDim, activeIndex, result1->at(0).get_stream()>>>(d_mAddrs.get(), dim, xyz, d_lAddrs1.get(), d_lAddrs2.get(), d_status, option);

    #ifndef NDEBUG
      int* h_status = new int[xyz];
      CDI.memcpy_from(h_status, d_status, xyz*sizeof(int), activeIndex, result1->at(0).get_stream());
      for( int i = 0; i < xyz; i++ ){
        if( h_status[i] != MatVecStatus::SUCCESS ){
          std::ostringstream msg;
          if( h_status[i] == MatVecStatus::IMAGINARY_NUMBER_FOUND ){
             msg << "Nebo error in " << "LinAlgCPU Kernel" <<
             ":\n";
             msg << "	 - " << "Matrix " << i << " has a complex eigen value.";
             msg << "\n";
             msg << "\t - " << __FILE__ << " : " << __LINE__;
          }
          else{
            msg << "Nebo error in LinAlgCUDA Kernel - after eigen decomposition:\n\t"
                << "Matrix " << i << " did not succeed.\n\t"
                << "MatVec Status Code: " << h_status[i] << ".\n\t"
                << __FILE__ << " : " << __LINE__;
          }

          delete[] h_status;
          throw(std::runtime_error(msg.str()));;
        }
      }
      delete[] h_status;
    #endif
  }

  sync_field_streams(result1->at(0), mat, matDim, false);
  sync_field_streams(result1->at(0), *result1, dim, true);
  if(returnComplex)
    sync_field_streams(result1->at(0), *result2, dim, true);

}
#endif /* __CUDACC__ */

template< typename FieldT >
void LinearSolve<FieldT>::operate ( ResultT* const result,
                                    const RightT& vec,
                                    const LeftT& mat,
                                    const int thread
#                                   ifdef SpatialOps_ENABLE_THREADS
                                    , Semaphore & sem )
#                                   else
                                    )
#                                   endif // SpatialOps_ENABLE_THREADS
{
  const int dim = result->elements();
  ValT* lVals[dim];
  const ValT* vVals[dim];
  const ValT* mVals[dim][dim];
  int i, j;
  i = dim;
  while( i-- ){
    lVals[i] = result->at ( i ).field_values();
    vVals[i] = vec ( i ).field_values();
    j = dim;
    while( j-- ){
      mVals[i][j] = mat ( i, j ).field_values();
    }
  }
# ifdef USE_EIGEN
  Eigen::MatrixXd mPt = Eigen::MatrixXd ( dim, dim );
  Eigen::VectorXd vPt = Eigen::MatrixXd ( dim, 1 );
  Eigen::VectorXd lPt = Eigen::MatrixXd ( dim, 1 );
# else // !USE_EIGEN
  namespace ublas = boost::numeric::ublas;
  ublas::matrix<ValT> mPt ( dim, dim );
  ublas::vector<ValT> vPt ( dim );
  ublas::permutation_matrix<size_t> pm ( mPt.size1() );
# endif // USE_EIGEN
  int xyz = (result->at ( 0 ).window_with_ghost().glob_npts()-1) - thread;
  for( ; xyz >= 0; xyz -= NTHREADS ){
    i = dim;
    while( i-- ){
      vPt ( i ) = vVals[i][xyz];
      j = dim;
      while( j-- ){
        mPt ( i, j ) = mVals[i][j][xyz];
      }
    }
#   ifdef USE_EIGEN
    lPt = mPt.lu().solve ( vPt );
    i = dim;
    while( i-- ){
      lVals[i][xyz] = lPt ( i );
    }
#   else // !USE_EIGEN
    ublas::lu_factorize ( mPt, pm );
    ublas::lu_substitute ( mPt, pm, vPt );
    i = dim;
    while( i-- ){
      lVals[i][xyz] = vPt ( i );
    }
#   endif // USE_EIGEN
  }
# ifdef SpatialOps_ENABLE_THREADS
  sem.post();
# endif // SpatialOps_ENABLE_THREADS
}
#ifdef __CUDACC__
template<typename FieldT>
void LinearSolve<FieldT>::gpu_operate ( cublasHandle_t & handle,
                                        ResultT* const result,
                                        const RightT& vec,
                                        const LeftT& mat )
{
  using namespace CudaMatVecUtility;
  assert( result->active_device_index() == vec.active_device_index() && vec.active_device_index() == mat.active_device_index() );

  typedef CudaMatVecUtility::CublasTypeDispatcher<typename FieldT::value_type> blasDispatch;
  const int dim = result->elements();
  const int activeIndex = vec.active_device_index();

  cublasSetStream(handle, result->at(0).get_stream());

  ema::cuda::CUDADeviceInterface& CDI = ema::cuda::CUDADeviceInterface::self();

  const int matDim = dim*dim;

        ValT* h_lAddrs[dim];
  const ValT* h_vAddrs[dim];
  const ValT* h_mAddrs[matDim];
  int i, j;
  i = dim;
  while( i-- ){
    h_lAddrs[i] = result->at ( i ).field_values(activeIndex);
    h_vAddrs[i] =        vec ( i ).field_values(activeIndex);
    j = dim;
    while( j-- ){
      h_mAddrs[i*dim + j] = mat ( i, j ).field_values(activeIndex);
    }
  }

  const int xyz = result->at(0).window_with_ghost().glob_npts();
  PoolAutoPtr<ValT> d_vNormal(Pool<ValT>::get( activeIndex, xyz*dim    ), activeIndex);
  PoolAutoPtr<ValT> d_mNormal(Pool<ValT>::get( activeIndex, xyz*matDim ), activeIndex);

  int blockDim = 16;
  int xGDim  = xyz    / blockDim + ((xyz    % blockDim) > 0 ? 1 : 0);
  int yGDim  = dim    / blockDim + ((dim    % blockDim) > 0 ? 1 : 0);
  int yGDim2 = matDim / blockDim + ((matDim % blockDim) > 0 ? 1 : 0);
  dim3 dimBlock       (blockDim, blockDim);
  dim3 dimGrid        (xGDim,    yGDim);
  dim3 dimGridReverse (yGDim,    xGDim);
  dim3 dimGrid2       (xGDim,    yGDim2);

  //Copy matrix values to GPU in Column Major order
  PoolAutoPtr<ValT*> d_vNaddrs(Pool<ValT*>::get( activeIndex, xyz ), activeIndex);
  PoolAutoPtr<ValT*> d_mNaddrs(Pool<ValT*>::get( activeIndex, xyz ), activeIndex);
  PoolAutoPtr<ValT*> d_lAddrs (Pool<ValT*>::get( activeIndex, dim ), activeIndex);
  {
    ValT* h_Naddrs[xyz];
    //No race condition here as far as deleting memory since this behaves
    //synchronously when using pageable memory.
    PoolAutoPtr<ValT*> d_Addrs(Pool<ValT*>::get( activeIndex, matDim ), activeIndex);
    for( int i = 0; i < xyz; i++ ){
      h_Naddrs[i] = d_mNormal + i*matDim;
    }
    sync_field_streams(mat, matDim, result->at(0), false);
    CDI.memcpy_to(d_mNaddrs, h_Naddrs,  xyz*sizeof(ValT*),    activeIndex, result->at(0).get_stream());
    CDI.memcpy_to(d_Addrs,   h_mAddrs,  matDim*sizeof(ValT*), activeIndex, result->at(0).get_stream());
    mat_vec_copy_column_major<<<dimGrid2,dimBlock, activeIndex, result->at(0).get_stream()>>>(d_mNaddrs.get(), d_Addrs.get(), dim, matDim, xyz);
    #ifndef NDEBUG
    cudaError err;

    if( cudaSuccess != (err = cudaDeviceSynchronize()) ){
      std::ostringstream msg;
      msg << "Nebo error in LinAlgCUDA Kernel - after matrix copy:\n\t"
          << cudaGetErrorString(err)
          << "\n\t" << __FILE__ << " : " << __LINE__;
      cublasSetStream(handle, NULL);
      throw(std::runtime_error(msg.str()));;
    }
    #endif
    /* NDEBUG */;
  }


  {
    PoolAutoPtr<int> d_infoArray(Pool<int>::get( activeIndex, xyz ), activeIndex);
    int* h_infoArray = new int[xyz];
    int h_info = 0;

    //LU factorize
    int* d_pivotArray = NULL;
    cublas_err_chk(blasDispatch::cublasgetrfBatched(handle,
                                                    dim,
                                                    d_mNaddrs,
                                                    dim,
                                                    d_pivotArray,
                                                    d_infoArray,
                                                    xyz));

    #ifndef NDEBUG
      CDI.memcpy_from(h_infoArray, d_infoArray, xyz*sizeof(int), activeIndex, result->at(0).get_stream());
      for( int i = 0; i < xyz; i++ ){
        if( h_infoArray[i] != 0 ){
          std::ostringstream msg;
          msg << "Nebo error in LinAlgCUDA Kernel - after LU decomposition:\n\t"
              << "Matrix " << i << " may be singular.\n\t"
              << __FILE__ << " : " << __LINE__;
          delete[] h_infoArray;
          cublasSetStream(handle, NULL);
          throw(std::runtime_error(msg.str()));;
        }
      }
    #endif

    //Copy vector values to GPU in Column Major order
    {
      ValT* h_Naddrs[xyz];
      PoolAutoPtr<ValT*> d_Addrs(Pool<ValT*>::get( activeIndex, matDim ), activeIndex);
      for( int i = 0; i < xyz; i++ ){
        h_Naddrs[i] = d_vNormal + i*dim;
      }
      sync_field_streams(vec, dim, result->at(0), false);
      CDI.memcpy_to(d_vNaddrs, h_Naddrs,  xyz*sizeof(ValT*), activeIndex, result->at(0).get_stream());
      CDI.memcpy_to(d_Addrs,   h_vAddrs,  dim*sizeof(ValT*), activeIndex, result->at(0).get_stream());
      mat_vec_copy_column_major<<<dimGrid,dimBlock, activeIndex, result->at(0).get_stream()>>>(d_vNaddrs.get(), d_Addrs.get(), 1, dim, xyz);
      //Copy plain l addresses
      CDI.memcpy_to(d_lAddrs,  h_lAddrs,  dim*sizeof(ValT*), activeIndex, result->at(0).get_stream());
      #ifndef NDEBUG
      cudaError err;

      if( cudaSuccess != (err = cudaDeviceSynchronize()) ){
        std::ostringstream msg;
        msg << "Nebo error in LinAlgCUDA Kernel - after vector copy:\n\t"
            << cudaGetErrorString(err)
            << "\n\t" << __FILE__ << " : " << __LINE__;
        delete[] h_infoArray;
        cublasSetStream(handle, NULL);
        throw(std::runtime_error(msg.str()));;
      }
      #endif
      /* NDEBUG */;
    }

    cublas_err_chk(blasDispatch::cublasgetrsBatched(handle,
                                                    CUBLAS_OP_N,
                                                    dim,
                                                    1,
                                                    const_cast<const ValT**>(d_mNaddrs.get()),
                                                    dim,
                                                    d_pivotArray,
                                                    d_vNaddrs,
                                                    dim,
                                                    &h_info,
                                                    xyz));

    #ifndef NDEBUG
      if( h_info != 0 ){
        std::ostringstream msg;
        msg << "Nebo error in LinAlgCUDA Kernel - after matrix solve:\n\t"
            << "Matrix " << -h_info << " may be singular.\n\t"
            << __FILE__ << " : " << __LINE__;
        delete[] h_infoArray;
        cublasSetStream(handle, NULL);
        throw(std::runtime_error(msg.str()));;
      }
    #endif
    delete[] h_infoArray;
  }

  mat_vec_copy_column_major<<<dimGridReverse,dimBlock, activeIndex, result->at(0).get_stream()>>>(d_lAddrs.get(), d_vNaddrs.get(), 1, xyz, dim);
  sync_field_streams(result->at(0), vec, dim, false);
  sync_field_streams(result->at(0), mat, matDim, false);
  sync_field_streams(result->at(0), *result, dim, true);

  cublasSetStream(handle, NULL);

}
#endif /* __CUDACC__ */

} // namespace SpatialOps

#endif // SpatialOps_MatVecOps_h
