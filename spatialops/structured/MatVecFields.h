/*
 * Copyright (c) 2014-2021 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 * ----------------------------------------------------------------------------
*/

#ifndef SpatialOps_MatVecFields_h
#define SpatialOps_MatVecFields_h
#include <cmath>
#include <stdexcept>
#include <sstream>
#include <spatialops/SpatialOpsConfigure.h>
#include <spatialops/structured/SpatialField.h>
#include <spatialops/structured/SpatialFieldStore.h>

/** @file MatVecFields.h
 *
 *  @brief Defines objects for containing and operating on matrices of fields.
 *
 * IMPORTANT: While reading, the brace indentations are organized such that every
 * open brace and close brace pair are at the exact same indentation. One line blocks
 * are an exception, the close brace is placed at the end of the single line.
 *
 * This file defines class objects which store references to SpatialField objects.
 * The class FieldVector represents a vector of fields and FieldMatrix a matrix of fields.
 * Operators are overloaded for select operations with the MatVecOp template, see MatVecOps.h.
*/

namespace SpatialOps
{

template< typename OpT >
class VecScalarOp;  ///< forward declaration; this object contains the rhs values
template< typename OpT >
class MatScalarOp;  ///< forward declaration; this object contains the rhs values
template< typename OpT >
class VecVecOp;  ///< forward declaration; this object contains the rhs values
template< typename OpT >
class MatVecOp;  ///< forward declaration; this object contains the rhs values
template< typename OpT >
class MatMatOp;  ///< forward declaration; this object contains the rhs values
template< typename OpT >
class MatUnaryOp;  ///< same as MatVecOp but for a rhs with a single matrix
template< typename FieldT >
class EigenDecomposition;  ///< One of the operators defined in MatVecOps.h
template< typename FieldT >
class LinearSolve;  ///< One of the operators defined in MatVecOps.h
template< typename FieldT >
class MatScalDiagOffset;  ///< One of the operators defined in MatVecOps.h

/** @brief Stores references for a vector of fields.
 * @author Nathan Yonkee
 * @date July 2015
 * @tparam FieldT - Type of field. This includes position on the grid cell
 * and the underlying data type.
 *
 * This class acts primarily as a wrapper for a std::vector of fields.
 * This allows us to overload common operators, '*', '+', '=', for linear algebra operations
 * and perform the assignment through Nebo. The boost::ubas library and Eigen library
 * perform dense linear solves using LU decomposition and calculate real eigenvalues.
*/
template< typename FieldT >
class FieldVector
{
private:
  std::vector<SpatFldPtr<FieldT> > vals_; ///< std::vector containing references to fields
  typedef typename FieldT::value_type ValT; ///< underlying data type, default is double
public:

  /** @brief Builds new fields and stores them here.
   *
   * @param elements number of elements in the vector
   * @param window window for each field
   * @param bc boundary information
   * @param ghosts properties of the ghost cells
   * @param devIdx the index of the device this will be initialized on
   *
   * We have removed arguments to the constructor: fieldValues are generated uniquely per field;
  */
  FieldVector( const int elements,
               const MemoryWindow window,
               const BoundaryCellInfo bc,
               const GhostData ghosts,
               const short int devIdx = CPU_INDEX )
    : vals_( )
  {
    int i = elements;
    vals_.reserve ( i );
    while( i-- ){
      vals_.push_back ( SpatialFieldStore::get_from_window<FieldT>( window, bc, ghosts, devIdx ) );
    }
  }

  /** @brief converts a std vector of spatial field pointers into a FieldVector
   *
   * @param vec vector to do a shallow copy of
   *
   * Each field in the list is expected to point to unique memory
  */
  FieldVector( const std::vector<SpatFldPtr<FieldT> >& vec )
    : vals_( vec )
  {
  }

  /** @brief assign the result of a vector-scalar operation
   * @tparam OpT the operation that will be performed
   * @param rhsOp the result of the RHS evaluation
   *
   * This operator assigns the result of a vector - scalar to the vector, indexing is handled internally.
   * The type of the expression is determined by the right hand side and doesn't require
   * explicit treatment here. For example, multiplying a vector is written as,
   * \verbatim thisVector = scalar * vec.
   * See MatVecOps.h for documentation on the operators currently supported.
  */
  template< typename OpT >
  void operator = ( const VecScalarOp< OpT >& rhsOp );

  /** @brief assign the result of a vector-vector operation
   * @tparam OpT the operation that will be performed
   * @param rhsOp the result of the RHS evaluation
   *
   * This operator assigns the result of a vector - vector to the vector, indexing is handled internally.
   * The type of the expression is determined by the right hand side and doesn't require
   * explicit treatment here. For example, adding two vectors is written as,
   * \verbatim thisVector = vec1 + vec2.
   * See MatVecOps.h for documentation on the operators currently supported.
  */
  template< typename OpT >
  void operator = ( const VecVecOp< OpT >& rhsOp );

  /** @brief assign the result of a matrix-vector operation
   * @tparam OpT the operation that will be performed
   * @param rhsOp the result of the RHS evaluation
   *
   * This operator assigns the result of a linear operation to the vector, indexing is handled internally.
   * The type of the expression is determined by the right hand side and doesn't require
   * explicit treatment here. For example, adding two vectors is written as,
   * \verbatim thisVector = vec1 + vec2.
   * See MatVecOps.h for documentation on the operators currently supported.
  */
  template< typename OpT >
  void operator = ( const MatVecOp< OpT >& rhsOp );

  /** @brief assignment for operations on single matrix
   *
   * @tparam OpT the operation that will be performed
   * @param rhsOp the right hand side to be evaluated
   *
   * This operator serves the same purpose as MatVecOp< OpT >.
   * Use of this operator does not require explicit treatment because
   * the type parameter is defined by the right hand side.
   * This is parameterized for operations on a single matrix, e.g. finding eigenvalues is written as,
   * \verbatim thisVector = mat.eigenvalues().
  */
  template< typename OpT >
  void operator = ( const MatUnaryOp< OpT >& rhsOp );

  /** @brief assign a scalar value to each field
   *
   * @param scalar the value assigned to every field
   *
   * This simply assigns a scalar value to every field.
   * The type of the scalar must be convertible to the value type of the fields.
   * The default value type of spatial fields is
   * \verbatim double.
   * For the default field type, this covers everything convertible to double, whether implicit or explicit.
  */
  void operator = ( const ValT scalar );

  FieldT&       operator() ( const int j )   ///< access to field at index j
  { return *(vals_[j]); }
  const FieldT& operator() ( const int j ) const   ///< const access to field at index j
  { return *(vals_[j]); }
  FieldT&       at ( const int j )   ///< access to field at index j
  { return *(vals_[j]); }
  const FieldT& at ( const int j ) const   ///< const access to field at index j
  { return *(vals_[j]); }

  std::vector<SpatFldPtr<FieldT> >& raw_vector(void) ///< access to raw vector on backend
  { return vals_; }
  const std::vector<SpatFldPtr<FieldT> >& raw_vector(void) const ///< const access to raw vector on backend
  { return vals_; }

  int elements() const    ///< length of the vector
  { return vals_.size(); }

  /**
   * \brief add device memory to these fields for given device
   *  and populate it with values from current active device *SYNCHRONOUS VERSION*
   *
   * \param deviceIndex the index of the device to add
   *
   * If device (deviceIndex) is already available for this field and
   * valid, this function becomes a no op.
   *
   * If device (deviceIndex) is already available for this field but
   * not valid, this function becomes identical to validate_device().
   *
   * Thus, regardless of the status of device (deviceIndex) for this
   * field, this function does the bare minimum to make device available
   * and valid for this field.
   *
   * Note: This operation is guaranteed to be synchronous: The host thread waits
   * until the task is completed (on the GPU).
   *
   * Note: This operation is thread safe.
   */
  void add_device(short int deviceIndex)
  {
    int i = vals_.size();
    while( i-- ){
      vals_[i]->add_device(deviceIndex);
    }
  }
 /**
   * \brief populate memory on the given device (deviceIndex) with values
   *  from the active device *SYNCHRONOUS VERSION*
   *
   * This function performs data-transfers when needed and only when needed.
   *
   * \param deviceIndex index for device to synchronize
   *
   * Note: This operation is guaranteed to be synchronous: The host thread waits
   * until the task is completed (on the GPU).
   */
  void validate_device(short int deviceIndex)
  {
    int i = vals_.size();
    while( i-- ){
      vals_[i]->validate_device(deviceIndex);
    }
  }
  /**
   * \brief set given device (deviceIndex) as active *SYNCHRONOUS VERSION*
   *
   * Given device must exist and be valid, otherwise an exception is thrown.
   *
   * \param deviceIndex index to device to be made active
   *
   * Note: This operation is guaranteed to be synchronous: The host thread waits
   * until the task is completed (on the GPU).
   */
  inline void set_device_as_active( const short int deviceIndex )
  {
    int i = vals_.size();
    while( i-- ){
      vals_[i]->set_device_as_active(deviceIndex);
    }
  }

#ifdef __CUDACC__
  /**
   * \brief check if the device (deviceIndex) is available and valid
   *
   * \param deviceIndex index of device to check
   */
  inline bool is_valid(int const deviceIndex) const
  {
    int i = vals_.size();
    bool valid = true;
    while( i-- ){
      valid &= vals_[i]->is_valid(deviceIndex);
    }
    return valid;
  }
  /**
   * \brief return the index of the current active device
   */
  inline int active_device_index(void) const
  {
    /*Note: Assume the first field has the same active field as all others*/
    return vals_[0]->active_device_index();
  }

  /**
   * \brief set the CUDA stream to for Nebo assignment to this field and for async data transfer
   *
   * \param stream the stream to use for this field
   */
  inline void set_stream( const cudaStream_t& stream )
  {
    int i = vals_.size();
    while( i-- ){
      vals_[i]->set_stream(stream);
    }
  }

  /**
   * \brief set the CUDA stream to for Nebo assignment to this field and for async data transfer
   *
   * \param i the field index of the field to assign stream
   * \param stream the stream to use for this field
   */
  inline void set_stream( const size_t i, const cudaStream_t& stream )
  {
    vals_[i]->set_stream(stream);
  }

  /**
   * \brief return the CUDA stream for this field
   *
   * \param i the field index of the field to return stream from
   */
  inline cudaStream_t const & get_stream( const size_t i ) const
  {
    return vals_[i]->get_stream();
  }

  /**
   * \brief return the CUDA event for this field
   *
   * \param i the field index of the field to return event from
   */
  inline cudaEvent_t const & get_last_event( const size_t i ) const
  {
    return vals_[i]->get_last_event();
  }
#endif /* __CUDACC__ */

};

/** @brief Stores references for a vector of fields.
 * @author Nathan Yonkee
 * @date July 2015
 *
 * @tparam FieldT - Type of field. This includes position on the grid cell
 * and the underlying data type.
 *
 * This class acts primarily as a wrapper for a std::vector of fields in row major order.
 * This allows us to overload common operators, '*', '+', '=', for linear algebra operations
 * and perform the assignment through Nebo. The Eigen library
 * performs dense linear solves using LU decomposition and eigen decompositions. Alternatively,
 * the boost::ublas can be used for performing linear solves only.
*/
template< typename FieldT >
class FieldMatrix
{
private:
  std::vector<SpatFldPtr<FieldT> > vals_;  ///< Holder for field references
  typedef typename FieldT::value_type ValT; ///< Default is double
public:

  /** @brief holds a 2Dmatrix of fieldT objects,
   *
   * @param elements number of elements in the vector
   * @param window window for each field
   * @param bc boundary information
   * @param ghosts properties of the ghost cells
   *
   * We support internal memory management only because each field needs unique memory
  */
  FieldMatrix( const int elements,
               const MemoryWindow window,
               const BoundaryCellInfo bc,
               const GhostData ghosts,
               const short int devIdx = CPU_INDEX )
    : vals_(),
      elements_( elements )
  {
    int ij = elements * elements;
    vals_.reserve ( ij );
    while( ij-- ){
      vals_.push_back ( SpatialFieldStore::get_from_window<FieldT>( window, bc, ghosts, devIdx ) );
    }
  }

  /** @brief holds a 2Dmatrix of fieldT objects,
   *
   * @param vec vector to do a shallow copy of
   *
   * Each field in the list is expected to point to unique memory
  */
  FieldMatrix( const std::vector<SpatFldPtr<FieldT> >& vec )
    : vals_( vec ),
      elements_( std::sqrt( vec.size() ) )
  {
#ifndef NDEBUG
    double elements = std::sqrt( vec.size() );
    assert( elements == std::floor( elements ) ); //Ensure that the vector coming in could be square.
#endif
  }

  /** @brief Augment the matrix with the scalar value across its diagonal.
   *
   * @param c Scalar to be added to the diagonal elements of the matrix 
   *
   * This function augments the matrix along its diagonal with the offset given
   * by the value c.
   * See MatVecOps.h for implementation.
  */
  void add_to_diagonal(const typename FieldT::value_type c);

  /** @brief Calculate eigen values of this matrix
   *
   * @return MatUnaryOp< EigenDecomposition<FieldT>> temporary object to launch solver
   *
   * This operation calculates the eigenvalues of this matrix and returns the REAL value;
   * the imaginary value is discarded. Only use this for REAL eigenvalues, complex values
   * are not supported at this time. In debug mode this will throw an error if
   * a complex value is computed. See MatVecOps.h for implementation.
  */
  MatUnaryOp< EigenDecomposition<FieldT> > real_eigenvalues();
  /** @brief Calculate eigen values of this matrix
   *
   * @param realVector vector to return real values into
   * @param complexVector vector to return complex values into
   *
   * This operation calculates the eigenvalues of this matrix and returns the
   * REAL and COMPLEX value. See MatVecOps.h for implementation.
  */
  void eigenvalues(FieldVector<FieldT>& realVector,
                   FieldVector<FieldT>& complexVector);

  /** @brief Direct linear solve using LU decomposition
   *
   * @param vec rhs vector
   * @return MatVecOp< LinearSolve< FieldT > > temporary object to launch solve
   *
   * This operation solves the linear system in the form \verbatim result =
   * inv( thisMatrix ) * rhsVec \endverbatim using LU decomposition. The matrix
   * is expected to be invertible.  If the system of equations is determined to
   * be inconsistent, an error is thrown in debug mode only.  This function
   * cannot always determine when a matrix is inconsistent and may not throw an
   * error in those cases so this should not be depended on.  See MatVecOps.h
   * for implementation.
  */
  MatVecOp< LinearSolve< FieldT > > solve( const FieldVector< FieldT >& vec ); ///< Solve Ax=b using LU decomp.

  /** @brief assign the result of a matrix-scalar operation
   * @tparam OpT the operation that will be performed
   * @param rhsOp the result of the RHS evaluation
   *
   * This operator assigns the result of a matrix - scalar to the vector, indexing is handled internally.
   * The type of the expression is determined by the right hand side and doesn't require
   * explicit treatment here. For example, multiplying a matrix is written as,
   * \verbatim thisMatrix = scalar * mat.
   * See MatVecOps.h for documentation on the operators currently supported.
  */
  template< typename OpT >
  void operator = ( const MatScalarOp< OpT >& rhsOp );

  /** @brief assign the result of a matrix-matrix operation
   * @tparam OpT the operation that will be performed
   * @param rhsOp the result of the RHS evaluation
   *
   * This operator assigns the result of a linear operation to the matrix, indexing is handled internally.
   * The type of the expression is determined by the right hand side and doesn't require
   * explicit treatment here. For example, adding two matrices is written as,
   * \verbatim thisMatrix = mat1 + mat2.
   * See MatVecOps.h for documentation on the operators currently supported.
  */
  template< typename OpT >
  void operator = ( const MatMatOp< OpT >& rhsOp );

  /** @brief Nebo assignment of scalar to all fields in this matrix.
   *
   * @param scalar value to be assigned
   *
   * This assigns a single value to every field in the matrix. ValT has a default value of double.
  */
  void operator = ( const ValT scalar );

  FieldT& operator() ( const int i, const int j )   ///< access to field at row i and column j
  { return *vals_[i * elements_ + j]; }

  FieldT& operator() ( const int ij )   ///< access field ij according to index in memory
  { return *(vals_[ij]); }

  FieldT& at ( const int ij )   ///< access field ij according to index in memory
  { return *(vals_[ij]); }

  const FieldT& operator() ( const int i, const int j ) const   ///< const access to field at row i and column j
  { return *vals_[i * elements_ + j]; }

  const FieldT& operator() ( const int ij ) const   ///< const access to field ij according to index in memory
  { return *(vals_[ij]); }

  const FieldT& at ( const int ij ) const   ///< const access to field ij according to index in memory
  { return *(vals_[ij]); }

  FieldVector<FieldT> row (const int i) const	///< access to the ith row of the field matrix - returns field vector
  {
    std::vector<SpatFldPtr<FieldT> > rowVector(vals_.begin() + (i * elements_), vals_.begin() + ((i * elements_) + elements_));

    return FieldVector<FieldT>(rowVector);
  }

  std::vector<SpatFldPtr<FieldT> >& raw_vector(void) ///< access to raw vector on backend
  { return vals_; }
  const std::vector<SpatFldPtr<FieldT> >& raw_vector(void) const ///< const access to raw vector on backend
  { return vals_; }

  int elements() const   ///< length of each dimension
  { return elements_; }

  /**
   * \brief add device memory to these fields for given device
   *  and populate it with values from current active device *SYNCHRONOUS VERSION*
   *
   * \param deviceIndex the index of the device to add
   *
   * If device (deviceIndex) is already available for this field and
   * valid, this function becomes a no op.
   *
   * If device (deviceIndex) is already available for this field but
   * not valid, this function becomes identical to validate_device().
   *
   * Thus, regardless of the status of device (deviceIndex) for this
   * field, this function does the bare minimum to make device available
   * and valid for this field.
   *
   * Note: This operation is guaranteed to be synchronous: The host thread waits
   * until the task is completed (on the GPU).
   *
   * Note: This operation is thread safe.
   */
  void add_device(short int deviceIndex)
  {
    int i = vals_.size();
    while( i-- ){
      vals_[i]->add_device(deviceIndex);
    }
  }
 /**
   * \brief populate memory on the given device (deviceIndex) with values
   *  from the active device *SYNCHRONOUS VERSION*
   *
   * This function performs data-transfers when needed and only when needed.
   *
   * \param deviceIndex index for device to synchronize
   *
   * Note: This operation is guaranteed to be synchronous: The host thread waits
   * until the task is completed (on the GPU).
   */
  void validate_device(short int deviceIndex)
  {
    int i = vals_.size();
    while( i-- ){
      vals_[i]->validate_device(deviceIndex);
    }
  }
  /**
   * \brief set given device (deviceIndex) as active *SYNCHRONOUS VERSION*
   *
   * Given device must exist and be valid, otherwise an exception is thrown.
   *
   * \param deviceIndex index to device to be made active
   *
   * Note: This operation is guaranteed to be synchronous: The host thread waits
   * until the task is completed (on the GPU).
   */
  inline void set_device_as_active( const short int deviceIndex )
  {
    int i = vals_.size();
    while( i-- ){
      vals_[i]->set_device_as_active(deviceIndex);
    }
  }

#ifdef __CUDACC__
  /**
   * \brief check if the device (deviceIndex) is available and valid
   *
   * \param deviceIndex index of device to check
   */
  inline bool is_valid(int const deviceIndex) const
  {
    int i = vals_.size();
    bool valid = true;
    while( i-- ){
      valid &= vals_[i]->is_valid(deviceIndex);
    }
    return valid;
  }
  /**
   * \brief return the index of the current active device
   */
  inline int active_device_index(void) const
  {
    /*Note: Assume the first field has the same active field as all others*/
    return vals_[0]->active_device_index();
  }

  /**
   * \brief set the CUDA stream to for Nebo assignment to this field and for async data transfer
   *
   * \param stream the stream to use for this field
   */
  inline void set_stream( const cudaStream_t& stream )
  {
    int i = vals_.size();
    while( i-- ){
      vals_[i]->set_stream(stream);
    }
  }

  /**
   * \brief set the CUDA stream to for Nebo assignment to this field and for async data transfer
   *
   * \param i the field index of the field to assign stream
   * \param stream the stream to use for this field
   */
  inline void set_stream( const size_t i, const cudaStream_t& stream )
  {
    vals_[i]->set_stream(stream);
  }

  /**
   * \brief return the CUDA stream for this field
   *
   * \param i the field index of the field to return stream from
   */
  inline cudaStream_t const & get_stream( const size_t i ) const
  {
    return vals_[i]->get_stream();
  }

  /**
   * \brief return the CUDA event for this field
   *
   * \param i the field index of the field to return event from
   */
  inline cudaEvent_t const & get_last_event( const size_t i ) const
  {
    return vals_[i]->get_last_event();
  }
#endif /* __CUDACC__ */

private:
  const int elements_;
};

template< typename FieldT >
void FieldVector<FieldT>::operator = ( const ValT scalar )
{
  int i = vals_.size();
  while( i-- ){
    *(vals_[i]) <<= scalar;
  }
}

template< typename FieldT >
void FieldMatrix<FieldT>::operator = ( const ValT scalar )
{
  int i = vals_.size();
  while( i-- ){
    *(vals_[i]) <<= scalar;
  }
}

template< typename FieldT >
template< typename OpT >
void FieldVector< FieldT >::operator = ( const VecScalarOp< OpT >& rhsOp ){
  rhsOp.eval( this );
}

template< typename FieldT >
template< typename OpT >
void FieldMatrix< FieldT >::operator = ( const MatScalarOp< OpT >& rhsOp ){
  rhsOp.eval( this );
}

template< typename FieldT >
template< typename OpT >
void FieldVector< FieldT >::operator = ( const VecVecOp< OpT >& rhsOp ){
  rhsOp.eval( this );
}

template< typename FieldT >
template< typename OpT >
void FieldVector< FieldT >::operator = ( const MatVecOp< OpT >& rhsOp ){
  rhsOp.eval( this );
}

template< typename FieldT >
template< typename OpT >
void FieldMatrix< FieldT >::operator = ( const MatMatOp< OpT >& rhsOp ){
  rhsOp.eval( this );
}

template< typename FieldT >
template< typename OpT >
void FieldVector< FieldT>::operator = ( const MatUnaryOp< OpT >& rhsOp ){
  rhsOp.eval( this );
}

} // namespace SpatialOps

#endif // SpatialOps_MatVecField_h
