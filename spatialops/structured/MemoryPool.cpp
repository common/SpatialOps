/**
 *  \file   MemoryPool.cpp
 *  \date   Jul 17, 2013
 *  \author "James C. Sutherland"
 *
 *
 * The MIT License
 *
 * Copyright (c) 2014-2021 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 *
 */

#include <spatialops/structured/MemoryPool.h>
#include <spatialops/structured/SpatialField.h>

#include <spatialops/structured/ExternalAllocators.h>

#include <boost/type_traits.hpp>
#include <boost/foreach.hpp>
#ifdef SpatialOps_ENABLE_THREADS
# include <boost/thread/mutex.hpp>
#endif

#include <limits>

namespace SpatialOps{

#ifdef SpatialOps_ENABLE_THREADS
  /**
   *  Used to lock threads to prevent simultaneous access.
   */
  inline static boost::mutex& get_mutex(){ static boost::mutex m; return m; }
#endif

template< typename T >
Pool<T>::Pool() : deviceIndex_(0)
{
  destroyed_ = false;
  cpuhighWater_ = 0;
# ifdef SpatialOps_ENABLE_CUDA
  pinned_ = true;
  gpuhighWater_ = 0;
# endif
}

template< typename T >
Pool<T>::~Pool()
{
  destroyed_ = true;

  for( typename FQSizeMap::iterator i=cpufqm_.begin(); i!=cpufqm_.end(); ++i ){
    typename Pool<T>::FieldQueue& fq = i->second;
    while( !fq.queue.empty() ){
#     ifdef SpatialOps_ENABLE_CUDA
      if(pinned_) { ema::cuda::CUDADeviceInterface::self().releaseHost( fq.queue.top() ); }
      else        { delete [] fq.queue.top(); }
#     else
      delete [] fq.queue.top();
#     endif
      fq.queue.pop();
    }
  }

# ifdef SpatialOps_ENABLE_CUDA
  for( typename FQSizeMap::iterator i=gpufqm_.begin(); i!=gpufqm_.end(); ++i ){
    typename Pool<T>::FieldQueue& fq = i->second;
    while( !fq.queue.empty() ){
      ema::cuda::CUDADeviceInterface::self().release( fq.queue.top(), deviceIndex_ );
      fq.queue.pop();
    }
  }
# endif
}

template< typename T >
Pool<T>&
Pool<T>::self()
{
# ifdef SpatialOps_ENABLE_CUDA
  //ensure CUDA driver is loaded before pool is initialized
  int deviceCount = 0;
  static cudaError_t err = cudaGetDeviceCount(&deviceCount);
  if( cudaSuccess != err ){
    std::ostringstream msg;
    msg << "Error at CudaGetDeviceCount() API, at " << __FILE__ << " : " << __LINE__
        << std::endl;
    msg << "\t - " << cudaGetErrorString(err);
    throw(std::runtime_error(msg.str()));
  }
# endif
  // see Modern C++ (Alexandrescu) chapter 6 for an excellent discussion on singleton implementation
  static Pool<T> p;
  return p;
}

template< typename T >
T*
Pool<T>::get( const short int deviceLocation, const size_t _n )
{
  Pool<T>& pool = Pool<T>::self();

# ifdef SpatialOps_ENABLE_THREADS
  boost::mutex::scoped_lock lock( get_mutex() );
# endif

  // reset access counters before they go out of range
  pool.accessCounter_++;
  if( pool.accessCounter_ >= ( std::numeric_limits<size_t>::max()-1) ){
    pool.accessCounter_ = 0;
    BOOST_FOREACH( typename FQSizeMap::value_type& fq, pool.cpufqm_ ) fq.second.accessCounter = 0;
    BOOST_FOREACH( typename FQSizeMap::value_type& fq, pool.gpufqm_ ) fq.second.accessCounter = 0;
  }

  // remove (free) entries that haven't been visited in the past nskip queries to the pool
  const size_t nskip = 200;
  if( pool.accessCounter_ % nskip == 0 ) flush_unused( nskip );

  assert( !pool.destroyed_ );

  size_t nentries = 1.2*_n;  // 20% padding on field size

  if( deviceLocation == CPU_INDEX ){
    T* field = NULL;
    typename FQSizeMap::iterator ifq = pool.cpufqm_.lower_bound( nentries );
    if( ifq == pool.cpufqm_.end() ) ifq = pool.cpufqm_.insert( ifq, std::make_pair(nentries,FieldQueue()) );
    else nentries = ifq->first;

    typename Pool<T>::FieldQueue& fq = ifq->second;
    fq.accessCounter++;

    if( fq.queue.empty() ){
      ++pool.cpuhighWater_;
#     ifdef NEBO_REPORT_BACKEND
      std::cout << "Allocating CPU memory" << std::endl;
#     endif
      try{
#       ifdef SpatialOps_ENABLE_CUDA
        /* Pinned Memory Mode
         * As the Pinned memory allocation and deallocation has higher overhead
         * this operation is performed at memory pool level which is created
         * and destroyed only once.
         */
        ema::cuda::CUDADeviceInterface& CDI = ema::cuda::CUDADeviceInterface::self();
        field = (T*)CDI.get_pinned_pointer( nentries*sizeof(T) );
#       else
        // Pageable Memory mode
        field = new T[nentries];
#       endif
      }
      catch( std::runtime_error& e ){
        std::ostringstream msg;
        msg << __FILE__ << " : " << __LINE__ << std::endl
            << "Error occurred while allocating memory on CPU" << std::endl
            << e.what() << std::endl;
        throw std::runtime_error( msg.str() );
      }
      pool.fsm_[field] = nentries;
    }
    else{
      field = fq.queue.top(); fq.queue.pop();
    }
    return field;
  }
# ifdef SpatialOps_ENABLE_CUDA
  else if( IS_GPU_INDEX(deviceLocation) ){
    T* field = NULL;
    typename FQSizeMap::iterator  ifq = pool.gpufqm_.lower_bound( nentries );
    if(ifq == pool.gpufqm_.end()) ifq = pool.gpufqm_.insert( ifq, std::make_pair(nentries,typename Pool<T>::FieldQueue()) );
    else                          nentries = ifq->first;

    typename Pool<T>::FieldQueue& fq = ifq->second;
    if( fq.queue.empty() ) {
#     ifdef NEBO_REPORT_BACKEND
      std::cout << "Allocating GPU memory" << std::endl;
#     endif
      ++pool.gpuhighWater_;
      ema::cuda::CUDADeviceInterface& CDI = ema::cuda::CUDADeviceInterface::self();
      field = (T*)CDI.get_raw_pointer( nentries*sizeof(T), deviceLocation );
      pool.fsm_[field] = nentries;
    }
    else{
      field = fq.queue.top(); fq.queue.pop();
    }
    return field;
  }
# endif
  else{
    std::ostringstream msg;
      msg << "Attempt to get unsupported memory pool ( "
          << DeviceTypeTools::get_memory_type_description(deviceLocation)
          << " ) \n"
          << "\t " << __FILE__ << " : " << __LINE__;
      throw(std::runtime_error(msg.str()));
  }
}

template< typename T >
void
Pool<T>::put( const short int deviceLocation, T* t )
{
  Pool<T>& pool = Pool<T>::self();
# ifdef SpatialOps_ENABLE_THREADS
  boost::mutex::scoped_lock lock( get_mutex() );
# endif
  // in some cases (notably in the LBMS code), singleton destruction order
  // causes the pool to be prematurely deleted.  Then subsequent calls here
  // would result in undefined behavior.  If the singleton has been destroyed,
  // then we will just ignore calls to return resources to the pool.  This will
  // leak memory on shut-down in those cases.
  if( pool.destroyed_ ) return;

  if( deviceLocation == CPU_INDEX ){
    const size_t n = pool.fsm_[t];
    const typename FQSizeMap::iterator ifq = pool.cpufqm_.lower_bound( n );
    assert( ifq != pool.cpufqm_.end() );
    ifq->second.queue.push(t);
  }
# ifdef SpatialOps_ENABLE_CUDA
  else if( IS_GPU_INDEX(deviceLocation) ) {
    const size_t n = pool.fsm_[t];
    const typename FQSizeMap::iterator ifq = pool.gpufqm_.lower_bound( n );
    assert( ifq != pool.gpufqm_.end() );
    ifq->second.queue.push(t);
  }
# endif
  else {
    std::ostringstream msg;
    msg << "Error occurred while restoring memory back to memory pool ( "
        << DeviceTypeTools::get_memory_type_description(deviceLocation)
    << " ) \n";
    msg << "\t " << __FILE__ << " : " << __LINE__;
    throw(std::runtime_error(msg.str()));
  }
}

template<typename T>
size_t
Pool<T>::active()
{
  Pool<T>& pool = Pool<T>::self();
  size_t n=0;
  for( typename FQSizeMap::const_iterator ifq=pool.cpufqm_.begin(); ifq!=pool.cpufqm_.end(); ++ifq ){
    n += ifq->second.queue.size();
  }
  return pool.cpuhighWater_ - n;
}

template<typename T>
size_t
Pool<T>::total()
{
  return Pool<T>::self().cpuhighWater_;
}

template<typename T>
size_t
Pool<T>::flush_unused( const size_t n )
{
  size_t nflushed = 0;

  Pool<T>& pool = Pool<T>::self();

  for( typename FQSizeMap::iterator ifq = pool.cpufqm_.begin(); ifq!=pool.cpufqm_.end(); ++ifq ){
    FieldQueue& fq = ifq->second;
    if( fq.accessCounter < (pool.accessCounter_ - n) ){
      // Delete all existing entries in the queue but leave the FQSizeMap entry
      // alone since it is possible that there are outstanding fields that need
      // to be returned to the pool.
//      std::cout << "Flushing " << fq.queue.size() << " CPU fields of size " << ifq->first << " because they have not been recently accessed\n";
      while( !fq.queue.empty() ){
        T* t = fq.queue.top();
        fq.queue.pop();
        pool.fsm_.erase( t );
#       ifdef SpatialOps_ENABLE_CUDA
        if(pool.pinned_) { ema::cuda::CUDADeviceInterface::self().releaseHost( t ); }
        else delete [] t;
#       else
        delete [] t;
#       endif
        ++nflushed;
      }
    }
  }
# ifdef SpatialOps_ENABLE_CUDA
  for( typename FQSizeMap::iterator ifq = pool.gpufqm_.begin(); ifq!=pool.gpufqm_.end(); ++ifq ){
    FieldQueue& fq = ifq->second;
    if( fq.accessCounter < (pool.accessCounter_ - n) ){
//      std::cout << "Flushing " << fq.queue.size() << " GPU fields of size " << ifq->first << " because they have not been recently accessed\n";
      while( !fq.queue.empty() ){
        T* t = fq.queue.top();
        fq.queue.pop();
        pool.fsm_.erase( t );
        ema::cuda::CUDADeviceInterface::self().release( t, pool.deviceIndex_ );
        ++nflushed;
      }
    }
  }
# endif
//std::cout << nflushed << " fields flushed from Pool\n";
  return nflushed;
}


// explicit instantiation for supported pool types
template class Pool<double>;
template class Pool<float>;
template class Pool<unsigned int>;
template class Pool<int>;
template class Pool<double *>;

} // namespace SpatialOps
