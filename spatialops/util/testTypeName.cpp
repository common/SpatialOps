/*
 * The MIT License
 *
 * Copyright (c) 2016-2017 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

/**
 *  \file   testTypeName.cpp
 *  \date   Jan 5, 2016
 *  \author james
 */

#include "TypeName.h"
#include "MemoryUsage.h"
#include <iostream>
using namespace std;
using namespace SpatialOps;

struct A{};
template<typename T> struct B{};

int main()
{
  cout << type_name<A>() << endl
      << type_name< B<double> >() << endl;
  A a;
  B<double> bd;
  B<int> bi;
  B<string> bs;
  cout << type_name(a) << endl
      << type_name(bd) << endl
      << type_name(bi) << endl
      << type_name(bs) << endl;

  cout << "Memory usage: " << get_memory_usage()/1024 << " kB\n";

  return 0;
}


