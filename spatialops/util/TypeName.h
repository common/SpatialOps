/*
 * The MIT License
 *
 * Copyright (c) 2016-2017 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

/**
 *  \file   TypeName.h
 *  \date   January, 2016
 *  \author James C. Sutherland
 *
 *  See: http://stackoverflow.com/questions/281818/unmangling-the-result-of-stdtype-infoname
 */

#ifndef UTIL_TYPENAME_H_
#define UTIL_TYPENAME_H_

#include <typeinfo>
#include <string>

#ifdef __GNUG__
#include <cxxabi.h>
namespace SpatialOps{
  inline std::string demangle_type_name( const char* name ){
    int status;
    char* nam = abi::__cxa_demangle( name, 0, 0, &status );
    std::string result( nam );
    free(nam);
    return result;
  }
}
#else
namespace SpatialOps{
  inline std::string demangle_type_name( const char* name ){
    return name;
  }
}
#endif

namespace SpatialOps{
  /**
   * @brief Obtain the name of a given type.
   * @param info the type_info for the given type
   * @return the name of T
   */
  inline std::string type_name( const std::type_info& info ){
    return demangle_type_name( info.name() );
  }

  /**
   * @brief Obtain the name of a given type
   * @param t an object of type T
   * @return the name of T
   */
  template <class T>
  std::string type_name(const T& t){
    return type_name( typeid(t) );
  }

  /**
   * @brief Obtain the name of a given type
   * @return the name of T
   */
  template <class T>
  std::string type_name(){
    return type_name( typeid(T) );
  }
}

#endif /* UTIL_TYPENAME_H_ */
