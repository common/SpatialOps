/*
 * Copyright (c) 2014-2021 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#ifndef NEBO_STENCIL_BUILDER_H
#define NEBO_STENCIL_BUILDER_H

//To define new stencils use the following four macros.
//NEBO_FIRST_POINT and NEBO_FIRST_IJK define a new Nebo stencil point collection.
//NEBO_ADD_POINT and NEBO_ADD_IJK add a stencil point to an existing Nebo stencil point collection.
//
//NEBO_FIRST_POINT and NEBO_ADD_POINT are designed to work together.
//Likewise, NEBO_FIRST_IJK and NEBO_ADD_IJK are designed to work together.
//They can be mixed, but it is not recommended.
//
//NEBO_FIRST_POINT and NEBO_ADD_POINT are designed to work with stencil points based off of field types.
//For examples, look at Stencil2Collection, Stencil4Collection, and FDStencilCollection definitions in this file.
//
//NEBO_FIRST_IJK and NEBO_ADD_IJK are designed to work on constant stencil points, whose shape does not change depending on field type.
//For examples, look at NullStencilCollection and the seven BoxFilter*StencilCollection definitions in this file.



//Define a new stencil from an IndexTriplet
#define NEBO_FIRST_POINT_WO_TN(POINT) NeboStencilPointCollection< POINT, NeboNil >
#define NEBO_FIRST_POINT(POINT) typename NEBO_FIRST_POINT_WO_TN(POINT)

//Add a point (IndexTriplet) to an existing stencil
#define NEBO_ADD_POINT(POINT) template AddPoint< POINT >::Result

//Define a new stencil from three constant integers
#define NEBO_FIRST_IJK(X, Y, Z) NeboStencilPointCollection< IndexTriplet<X,Y,Z>, NeboNil >

//Add a point (three constant integers) to an existing stencil
#define NEBO_ADD_IJK(X, Y, Z) AddPoint< IndexTriplet<X,Y,Z> >::Result

namespace SpatialOps {

  /**
   *  \struct Subtract
   *  \brief Perform compile-time subtraction over a list of IndexTriplet types
   */
  template< typename List, typename IT >
    struct ListSubtract
    {
      typedef NeboStencilPointCollection< typename Subtract< typename List::Point, IT >::result,
                                          typename ListSubtract< typename List::Collection, IT >::result >
              result;
    };

  template< typename Point, typename IT >
    struct ListSubtract< NeboStencilPointCollection< Point, NeboNil >, IT >
    {
      typedef NeboStencilPointCollection< typename Subtract< Point, IT >::result, NeboNil >
              result;
    };

  /**
   * \struct NeboStencilBuilder
   * \brief Supports definition of new Nebo stencils.
   *
   * \tparam OperatorType the type of operator, e.g. SpatialOps::Gradient, SpatialOps::Interpolant, etc.
   * \tparam PntCltnT     defines the stencil points
   * \tparam SrcFieldT    the type of field that this operator acts on
   * \tparam DestFieldT   the type of field that this operator produces
   */
    template<typename OperatorType, typename PntCltnT, typename SrcFieldT, typename DestFieldT>
    struct NeboStencilBuilder {
    public:
        typedef OperatorType  type;                 ///< operator type (Interpolant, Gradient, Divergence)
        typedef PntCltnT      PointCollectionType;  ///< collection of stencil points
        typedef SrcFieldT     SrcFieldType;         ///< source field type
        typedef DestFieldT    DestFieldType;        ///< destination field type
        typedef NeboStencilCoefCollection<PointCollectionType::length>
                              CoefCollection;       ///< collection of coefficients

        // typedefs for when argument is a Nebo expression
        template<typename Arg>
        struct WithArg {
            typedef NeboStencil<Initial, PointCollectionType, Arg, DestFieldType> Stencil;
            typedef NeboExpression<Stencil, DestFieldType> Result;
        };

        // typedefs for when argument is a field
        typedef NeboConstField<Initial, SrcFieldType> FieldArg;
        typedef NeboStencil<Initial, PointCollectionType, FieldArg, DestFieldType> FieldStencil;
        typedef NeboExpression<FieldStencil, DestFieldType> FieldResult;

        /**
         *  \brief construct a stencil with the specified coefficients
         */
        NeboStencilBuilder(const CoefCollection & coefs)
         : coefCollection_(coefs)
        {}

        ~NeboStencilBuilder() {}

        /**
         * \brief Return coefficient collection
         */
        CoefCollection const & coefs(void) const { return coefCollection_; };

        /**
         * \brief Apply this operator to the supplied source field to produce the supplied destination field
         * \param src the field that the operator is applied to
         * \param dest the resulting field.
         */
        void apply_to_field( const SrcFieldType & src, DestFieldType & dest ) const {
            dest <<= operator()(src);
        }

        /**
         * \brief Nebo's inline operator for field values
         * \param src the field to which the operator is applied
         */
        inline FieldResult operator ()( const SrcFieldType & src ) const {
            return FieldResult(FieldStencil(FieldArg(src), coefs()));
        }

        /**
         * \brief Nebo's inline operator for Nebo expressions
         * \param src the Nebo expression to which the operator is applied
         */
        template<typename Arg>
        inline typename WithArg<Arg>::Result
        operator ()( const NeboExpression<Arg, SrcFieldType> & src ) const {
            typedef typename WithArg<Arg>::Stencil Stencil;
            typedef typename WithArg<Arg>::Result Result;
            return Result(Stencil(src.expr(), coefs()));
        }

    private:
        const CoefCollection coefCollection_;
    };

    template<typename OperatorType, typename SrcFieldType, typename DestFieldType>
    struct Stencil2Collection {
        // source field offset
        typedef typename SrcFieldType::Location::Offset                      SrcOffset;
        // destination field offset
        typedef typename DestFieldType::Location::Offset                     DestOffset;
        // low (first) stencil point location (relative to the destination point)
        typedef typename GreaterThan<SrcOffset, DestOffset>::result::Negate  LowStPt;
        // high (second) stencil point location (relative to the destination point)
        typedef typename LessThan<SrcOffset, DestOffset>::result             HighStPt;
        // collection of all stencil points in this stencil
        typedef NEBO_FIRST_POINT(LowStPt)::NEBO_ADD_POINT(HighStPt)          StPtCollection;
    };

    template<typename OperatorType, typename SrcFieldType, typename DestFieldType>
    struct Stencil4Collection {
        template<bool Boolean, typename True, typename False>
        struct TemplateIf;

        template<typename True, typename False>
        struct TemplateIf<true, True, False> { True typedef result; };

        template<typename True, typename False>
        struct TemplateIf<false, True, False> { False typedef result; };

        // source field offset
        typedef typename SrcFieldType::Location::Offset  SrcOffset;
        // destination field offset
        typedef typename DestFieldType::Location::Offset DestOffset;
        // unit vectors
        typedef IndexTriplet<1, 0, 0>   XUnit;
        typedef IndexTriplet<0, 1, 0>   YUnit;
        typedef IndexTriplet<0, 0, 1>   ZUnit;
        // first direction (unit vector)
        typedef typename TemplateIf<((int)(SrcOffset::X) != (int)(DestOffset::X)),
                                    XUnit,
                                    YUnit>::result                                  FirstDir;
        // second direction (unit vector)
        typedef typename TemplateIf<((int)(SrcOffset::Z) != (int)(DestOffset::Z)),
                                    ZUnit,
                                    YUnit>::result                                  SecondDir;
        // source offset in the first direction
        typedef typename Multiply<SrcOffset, FirstDir>::result          SrcInFirstDir;
        // source offset in the second direction
        typedef typename Multiply<SrcOffset, SecondDir>::result         SrcInSecondDir;
        // destination offset in the first direction
        typedef typename Multiply<DestOffset, FirstDir>::result         DestInFirstDir;
        // destination offset in the second direction
        typedef typename Multiply<DestOffset, SecondDir>::result        DestInSecondDir;
        // low value in the first direction
        typedef typename GreaterThan<SrcInFirstDir,
                                                 DestInFirstDir>::result::Negate    LoValInFirstDir;
        // high value in the first direction
        typedef typename LessThan<SrcInFirstDir,
                                              DestInFirstDir>::result               HiValInFirstDir;
        // low value in the second direction
        typedef typename GreaterThan<SrcInSecondDir,
                                            DestInSecondDir>::result::Negate        LoValInSecondDir;
        // high value in the second direction
        typedef typename LessThan<SrcInSecondDir,
                                              DestInSecondDir>::result              HiValInSecondDir;
        // stencil point locations (relative to the destination point)
        typedef typename Add<LoValInFirstDir, LoValInSecondDir>::result StPt1;
        typedef typename Add<HiValInFirstDir, LoValInSecondDir>::result StPt2;
        typedef typename Add<LoValInFirstDir, HiValInSecondDir>::result StPt3;
        typedef typename Add<HiValInFirstDir, HiValInSecondDir>::result StPt4;
        // collection of all stencil points in this stencil
        typedef NEBO_FIRST_POINT(StPt1)::NEBO_ADD_POINT(StPt2)
                ::NEBO_ADD_POINT(StPt3)::NEBO_ADD_POINT(StPt4)                      StPtCollection;
    };

    template<typename OperatorType, typename SrcFieldType, typename DestFieldType>
    struct FDStencilCollection {
        typedef typename OperatorType::DirT                          DirT;
        typedef typename UnitTriplet<DirT>::type                     DirVec;
        typedef typename DirVec::Negate                              LowStPt;
        typedef DirVec                                               HighStPt;
        typedef NEBO_FIRST_POINT(LowStPt)::NEBO_ADD_POINT(HighStPt)  StPtCollection;
    };

  /**
   * \struct NeboEdgelessStencilBuilder
   * \brief Supports definition of new Nebo stencils that do NOT invalidate ghost cells.
   *
   * \tparam OperatorType the type of operator, e.g. SpatialOps::Gradient, SpatialOps::Interpolant, etc.
   * \tparam PntCltnT     defines the stencil points
   * \tparam SrcFieldT    the type of field that this operator acts on
   * \tparam DestFieldT   the type of field that this operator produces
   */
    template<typename OperatorType, typename PntCltnT, typename SrcFieldT, typename DestFieldT>
    struct NeboEdgelessStencilBuilder {
    public:
        typedef OperatorType  type;                 ///< operator type (Interpolant, Gradient, Divergence)
        typedef PntCltnT      PointCollectionType;  ///< collection of stencil points
        typedef SrcFieldT     SrcFieldType;         ///< source field type
        typedef DestFieldT    DestFieldType;        ///< destination field type
        typedef NeboStencilCoefCollection<PointCollectionType::length>
                              CoefCollection;       ///< collection of coefficients

        // typedefs for when argument is a Nebo expression
        template<typename Arg>
        struct WithArg {
            typedef NeboEdgelessStencil<Initial, PointCollectionType, Arg, DestFieldType> Stencil;
            typedef NeboExpression<Stencil, DestFieldType> Result;
        };

        // typedefs for when argument is a field
        typedef NeboConstField<Initial, SrcFieldType> FieldArg;
        typedef NeboEdgelessStencil<Initial, PointCollectionType, FieldArg, DestFieldType> FieldStencil;
        typedef NeboExpression<FieldStencil, DestFieldType> FieldResult;

        /**
         *  \brief construct a stencil with the specified coefficients
         */
        NeboEdgelessStencilBuilder(const CoefCollection & coefs)
         : coefCollection_(coefs)
        {}

        ~NeboEdgelessStencilBuilder() {}

        /**
         * \brief Return coefficient collection
         */
        CoefCollection const & coefs(void) const { return coefCollection_; };

        /**
         * \brief Apply this operator to the supplied source field to produce the supplied destination field
         * \param src the field that the operator is applied to
         * \param dest the resulting field.
         */
        void apply_to_field( const SrcFieldType & src, DestFieldType & dest ) const {
            dest <<= operator()(src);
        }

        /**
         * \brief Nebo's inline operator for field values
         * \param src the field to which the operator is applied
         */
        inline FieldResult operator ()( const SrcFieldType & src ) const {
            return FieldResult(FieldStencil(FieldArg(src), coefs()));
        }

        /**
         * \brief Nebo's inline operator for Nebo expressions
         * \param src the Nebo expression to which the operator is applied
         */
        template<typename Arg>
        inline typename WithArg<Arg>::Result
        operator ()( const NeboExpression<Arg, SrcFieldType> & src ) const {
            typedef typename WithArg<Arg>::Stencil Stencil;
            typedef typename WithArg<Arg>::Result Result;
            return Result(Stencil(src.expr(), coefs()));
        }

    private:
        const CoefCollection coefCollection_;
    };

  /**
   * \struct NeboSumStencilBuilder
   * \brief Supports definition of new Nebo sum stencils, which sums given stencil points WITHOUT coefficients.
   *
   * \tparam PntCltnT     defines the stencil points
   * \tparam SrcFieldT    the type of field that this operator acts on
   * \tparam DestFieldT   the type of field that this operator produces
   */
    template<typename PntCltnT, typename SrcFieldT, typename DestFieldT>
    struct NeboSumStencilBuilder {
    public:
        typedef PntCltnT      PointCollectionType;  ///< collection of stencil points
        typedef SrcFieldT     SrcFieldType;         ///< source field type
        typedef DestFieldT    DestFieldType;        ///< destination field type

        // typedefs for when argument is a Nebo expression
        template<typename Arg>
        struct WithArg {
            typedef NeboSumStencil<Initial, PointCollectionType, Arg, DestFieldType> Stencil;
            typedef NeboExpression<Stencil, DestFieldType> Result;
        };

        // typedefs for when argument is a field
        typedef NeboConstField<Initial, SrcFieldType> FieldArg;
        typedef NeboSumStencil<Initial, PointCollectionType, FieldArg, DestFieldType> FieldStencil;
        typedef NeboExpression<FieldStencil, DestFieldType> FieldResult;

        /**
         *  \brief construct a stencil
         */
        NeboSumStencilBuilder()
        {}

        ~NeboSumStencilBuilder() {}

        /**
         * \brief Apply this operator to the supplied source field to produce the supplied destination field
         * \param src the field that the operator is applied to
         * \param dest the resulting field.
         */
        void apply_to_field( const SrcFieldType & src, DestFieldType & dest ) const {
            dest <<= operator()(src);
        }

        /**
         * \brief Nebo's inline operator for field values
         * \param src the field to which the operator is applied
         */
        inline FieldResult operator ()( const SrcFieldType & src ) const {
            return FieldResult(FieldStencil(FieldArg(src)));
        }

        /**
         * \brief Nebo's inline operator for Nebo expressions
         * \param src the Nebo expression to which the operator is applied
         */
        template<typename Arg>
        inline typename WithArg<Arg>::Result
        operator ()( const NeboExpression<Arg, SrcFieldType> & src ) const {
            typedef typename WithArg<Arg>::Stencil Stencil;
            typedef typename WithArg<Arg>::Result Result;
            return Result(Stencil(src.expr()));
        }
    };

    struct NullStencilCollection {
        typedef NEBO_FIRST_IJK(0, 0, 0) StPtCollection;
    };

  /**
   * \struct NeboAverageStencilBuilder
   * \brief Supports definition of new Nebo average stencils, which automatically averages given stencil points.
   *
   * \tparam PntCltnT     defines the stencil points
   * \tparam SrcFieldT    the type of field that this operator acts on
   * \tparam DestFieldT   the type of field that this operator produces
   */
    template<typename PntCltnT, typename SrcFieldT, typename DestFieldT>
    struct NeboAverageStencilBuilder {
    public:
        typedef PntCltnT      PointCollectionType;  ///< collection of stencil points
        typedef SrcFieldT     SrcFieldType;         ///< source field type
        typedef DestFieldT    DestFieldType;        ///< destination field type

        // typedefs for when argument is a Nebo expression
        template<typename Arg>
        struct WithArg {
            typedef NeboSumStencil<Initial, PointCollectionType, Arg, DestFieldType> Stencil;
            typedef NeboScalar<Initial, double> Scalar;
            typedef ProdOp<Initial, Stencil, Scalar> Average;
            typedef NeboExpression<Average, DestFieldType> Result;
        };

        // typedefs for when argument is a field
        typedef NeboConstField<Initial, SrcFieldType> FieldArg;
        typedef NeboScalar<Initial, double> FieldScalar;
        typedef NeboSumStencil<Initial, PointCollectionType, FieldArg, DestFieldType> FieldStencil;
        typedef ProdOp<Initial, FieldStencil, FieldScalar> FieldAverage;
        typedef NeboExpression<FieldAverage, DestFieldType> FieldResult;

        /**
         *  \brief construct a stencil
         */
        NeboAverageStencilBuilder()
        {}

        ~NeboAverageStencilBuilder() {}

        /**
         * \brief Apply this operator to the supplied source field to produce the supplied destination field
         * \param src the field that the operator is applied to
         * \param dest the resulting field.
         */
        void apply_to_field( const SrcFieldType & src, DestFieldType & dest ) const {
            dest <<= operator()(src);
        }

        /**
         * \brief Nebo's inline operator for field values
         * \param src the field to which the operator is applied
         */
        inline FieldResult operator ()( const SrcFieldType & src ) const {
            return FieldResult(FieldAverage(FieldStencil(FieldArg(src)),
                                            FieldScalar(1.0 / double(PointCollectionType::length))));
        }

        /**
         * \brief Nebo's inline operator for Nebo expressions
         * \param src the Nebo expression to which the operator is applied
         */
        template<typename Arg>
        inline typename WithArg<Arg>::Result
        operator ()( const NeboExpression<Arg, SrcFieldType> & src ) const {
            typedef typename WithArg<Arg>::Stencil Stencil;
            typedef typename WithArg<Arg>::Scalar Scalar;
            typedef typename WithArg<Arg>::Average Average;
            typedef typename WithArg<Arg>::Result Result;
            return Result(Average(Stencil(src.expr()),
                                  Scalar(1.0 / double(PointCollectionType::length))));
        }
    };

    struct BoxFilter3DStencilCollection {
      typedef NEBO_FIRST_IJK(-1,-1,-1)::NEBO_ADD_IJK( 0,-1,-1)::NEBO_ADD_IJK( 1,-1,-1)
              ::NEBO_ADD_IJK(-1, 0,-1)::NEBO_ADD_IJK( 0, 0,-1)::NEBO_ADD_IJK( 1, 0,-1)
              ::NEBO_ADD_IJK(-1, 1,-1)::NEBO_ADD_IJK( 0, 1,-1)::NEBO_ADD_IJK( 1, 1,-1)
              ::NEBO_ADD_IJK(-1,-1, 0)::NEBO_ADD_IJK( 0,-1, 0)::NEBO_ADD_IJK( 1,-1, 0)
              ::NEBO_ADD_IJK(-1, 0, 0)::NEBO_ADD_IJK( 0, 0, 0)::NEBO_ADD_IJK( 1, 0, 0)
              ::NEBO_ADD_IJK(-1, 1, 0)::NEBO_ADD_IJK( 0, 1, 0)::NEBO_ADD_IJK( 1, 1, 0)
              ::NEBO_ADD_IJK(-1,-1, 1)::NEBO_ADD_IJK( 0,-1, 1)::NEBO_ADD_IJK( 1,-1, 1)
              ::NEBO_ADD_IJK(-1, 0, 1)::NEBO_ADD_IJK( 0, 0, 1)::NEBO_ADD_IJK( 1, 0, 1)
              ::NEBO_ADD_IJK(-1, 1, 1)::NEBO_ADD_IJK( 0, 1, 1)::NEBO_ADD_IJK( 1, 1, 1)
          StPtCollection;
    };

    struct BoxFilter2DXYStencilCollection {
      typedef NEBO_FIRST_IJK(-1,-1, 0)::NEBO_ADD_IJK( 0,-1, 0)::NEBO_ADD_IJK( 1,-1, 0)
              ::NEBO_ADD_IJK(-1, 0, 0)::NEBO_ADD_IJK( 0, 0, 0)::NEBO_ADD_IJK( 1, 0, 0)
              ::NEBO_ADD_IJK(-1, 1, 0)::NEBO_ADD_IJK( 0, 1, 0)::NEBO_ADD_IJK( 1, 1, 0)
          StPtCollection;
    };

    struct BoxFilter2DXZStencilCollection {
      typedef NEBO_FIRST_IJK(-1, 0,-1)::NEBO_ADD_IJK( 0, 0,-1)::NEBO_ADD_IJK( 1, 0,-1)
              ::NEBO_ADD_IJK(-1, 0, 0)::NEBO_ADD_IJK( 0, 0, 0)::NEBO_ADD_IJK( 1, 0, 0)
              ::NEBO_ADD_IJK(-1, 0, 1)::NEBO_ADD_IJK( 0, 0, 1)::NEBO_ADD_IJK( 1, 0, 1)
          StPtCollection;
    };

    struct BoxFilter2DYZStencilCollection {
      typedef NEBO_FIRST_IJK( 0,-1,-1)::NEBO_ADD_IJK( 0, 0,-1)::NEBO_ADD_IJK( 0, 1,-1)
              ::NEBO_ADD_IJK( 0,-1, 0)::NEBO_ADD_IJK( 0, 0, 0)::NEBO_ADD_IJK( 0, 1, 0)
              ::NEBO_ADD_IJK( 0,-1, 1)::NEBO_ADD_IJK( 0, 0, 1)::NEBO_ADD_IJK( 0, 1, 1)
          StPtCollection;
    };

    struct BoxFilter1DXStencilCollection {
      typedef NEBO_FIRST_IJK(-1, 0, 0)::NEBO_ADD_IJK( 0, 0, 0)::NEBO_ADD_IJK( 1, 0, 0)
          StPtCollection;
    };

    struct BoxFilter1DYStencilCollection {
      typedef NEBO_FIRST_IJK( 0,-1, 0)::NEBO_ADD_IJK( 0, 0, 0)::NEBO_ADD_IJK( 0, 1, 0)
          StPtCollection;
    };

    struct BoxFilter1DZStencilCollection {
      typedef NEBO_FIRST_IJK( 0, 0,-1)::NEBO_ADD_IJK( 0, 0, 0)::NEBO_ADD_IJK( 0, 0, 1)
          StPtCollection;
    };

    template<typename OperatorType, typename SrcFieldType, typename DestFieldType>
    struct MaskShiftPoints {
      // source field offset
      typedef typename SrcFieldType::Location::Offset                      SrcOffset;
      // destination field offset
      typedef typename DestFieldType::Location::Offset                     DestOffset;
      // minus-side stencil point location
      typedef typename LessThan<SrcOffset, DestOffset>::result MinusPoint;
      // plus-side stencil point location
      typedef typename GreaterThan<SrcOffset,
                                               DestOffset>::result::Negate PlusPoint;
    };

    //must be a finite difference (FD) stencil, pull direction and points from operator:
    template<typename OperatorType, typename FieldType>
    struct MaskShiftPoints<OperatorType, FieldType, FieldType> {
      typedef typename OperatorType::type                    OpT;
      // FDStencilCollection:
      typedef FDStencilCollection<OpT, FieldType, FieldType> FDStencil;
      // minus-side stencil point location
      typedef typename FDStencil::DirVec                     MinusPoint;
      // plus-side stencil point location
      typedef typename FDStencil::DirVec::Negate             PlusPoint;
    };

  /**
   * \struct NeboMaskShiftBuilder
   * \brief Supports definition of new Nebo mask shift stencils, which converts a mask of one field type into another field type.
   *
   * \tparam OperatorT    the type of the operator
   * \tparam SrcFieldT    the type of field that this operator acts on
   * \tparam DestFieldT   the type of field that this operator produces
   */
    template<typename OperatorT, typename SrcFieldT, typename DestFieldT>
      struct NeboMaskShiftBuilder {
      public:
        typedef OperatorT  OperatorType;  ///< operator type
        typedef SrcFieldT  SrcFieldType;  ///< source field type
        typedef DestFieldT DestFieldType; ///< destination field type

        typedef SpatialMask<SrcFieldType>  SrcMask;  ///< source mask type
        typedef SpatialMask<DestFieldType> DestMask; ///< destination mask type

        typedef typename MaskShiftPoints<OperatorType, SrcFieldType, DestFieldType>::MinusPoint MinusPoint; ///< negative face shift for mask
        typedef typename MaskShiftPoints<OperatorType, SrcFieldType, DestFieldType>::PlusPoint  PlusPoint;  ///< positive face shift for mask

        typedef NeboMask<Initial, SrcFieldType> Mask; ///< Nebo mask type

        typedef NeboMaskShift<Initial, MinusPoint, Mask, DestFieldType> MinusShift; ///< shift type for negative shift
        typedef NeboMaskShift<Initial, PlusPoint,  Mask, DestFieldType> PlusShift;  ///< shift type for positive shift

        typedef NeboBooleanExpression<MinusShift, DestFieldType> MinusResult; ///< result type for negative shift
        typedef NeboBooleanExpression<PlusShift,  DestFieldType> PlusResult;  ///< result type for positive shift

        /**
         *  \brief construct a stencil
         */
        NeboMaskShiftBuilder()
        {}

        ~NeboMaskShiftBuilder() {}

        /**
         * \brief Compute the minus side shift
         * \param src the mask to which the operator is applied
         */
        inline MinusResult minus( const SrcMask & src ) const {
          return MinusResult(MinusShift(Mask(src)));
        }

        /**
         * \brief Compute the plus side shift
         * \param src the mask to which the operator is applied
         */
        inline PlusResult plus( const SrcMask & src ) const {
          return PlusResult(PlusShift(Mask(src)));
        }
    };
    
/**
 * \brief The Nebo Operator for switch
 **/
  template<typename CurrentMode,typename Arg0, typename Arg1,typename FieldType>
   struct NeboSwitch;
  template<typename Arg0, typename Arg1, typename FieldType>
   struct NeboSwitch<Initial, Arg0, Arg1, FieldType> {
      FieldType typedef field_type;
      typedef NeboSwitch<SeqWalk, typename Arg0::SeqWalkType, typename Arg1::SeqWalkType, FieldType>  SeqWalkType;
#ifdef SpatialOps_ENABLE_THREADS
      NeboSwitch<Resize, typename Arg0::ResizeType, typename Arg1::ResizeType, FieldType>
      typedef ResizeType;
#endif
      NeboSwitch(Arg0 const& arg0, Arg1 const& arg1, int which): arg0_(arg0), arg1_(arg1), which_(which){}
#define INVOKE_ARG_METHOD(name) (which_ == 0?arg0_.name:arg1_.name) 
      inline GhostData ghosts_with_bc(void) const {
          if(which_ < 2)
              return INVOKE_ARG_METHOD(ghosts_with_bc());
          else
            return max(arg0_.ghosts_with_bc(), arg1_.ghosts_with_bc());
      }
      inline GhostData ghosts_without_bc(void) const {
          if(which_ < 2)
              return INVOKE_ARG_METHOD(ghosts_with_bc());
          else
            return max(arg0_.ghosts_without_bc(), arg1_.ghosts_without_bc());
      }
      inline bool has_extents(void) const { return INVOKE_ARG_METHOD(has_extents()); }
      inline IntVec extents(void) const { return INVOKE_ARG_METHOD(extents()); }
      inline IntVec has_bc(void) const { return INVOKE_ARG_METHOD(has_bc());}
      inline SeqWalkType init(IntVec const & extents, GhostData const & ghosts, IntVec const & hasBC , NeboOptionalArg & optArg) const {
          return SeqWalkType(arg0_.init(extents, ghosts, hasBC, optArg),
                             arg1_.init(extents, ghosts, hasBC, optArg),
                             which_, 
                             get_range(arg0_.extents(), arg0_.ghosts_without_bc()), 
                             get_range(arg1_.extents(), arg1_.ghosts_without_bc()));
      }
#ifdef SpatialOps_ENABLE_THREADS
      inline ResizeType resize(void) const {
          return ResizeType(arg0_.resize(), 
                            arg1_.resize(), 
                            which_, 
                            get_range(arg0_.extents(), arg0_.ghosts_without_bc()), 
                            get_range(arg1_.extents(), arg1_.ghosts_without_bc()));
      }
#endif
#ifdef __CUDACC__
      typedef NeboSwitch<GPUWalk, typename Arg0::GPUWalkType, typename Arg1::GPUWalkType, FieldType>GPUWalkType;
      inline bool cpu_ready(void) const { return INVOKE_ARG_METHOD(cpu_ready()); }
      inline bool gpu_ready(int const deviceIndex) const {return INVOKE_ARG_METHOD(gpu_ready(deviceIndex));}
      inline GPUWalkType gpu_init(IntVec const & extents,GhostData const & ghosts,IntVec const & hasBC, int const deviceIndex, cudaStream_t const & lhsStream, NeboOptionalArg & optArg) const {
        return GPUWalkType(arg0_.gpu_init(extents, ghosts, hasBC, deviceIndex, lhsStream, optArg), 
                           arg1_.gpu_init(extents, ghosts, hasBC, deviceIndex, lhsStream, optArg), 
                           which_,
                           get_range(arg0_.extents(), arg0_.ghosts_without_bc()), 
                           get_range(arg1_.extents(), arg1_.ghosts_without_bc()));
      }
      inline void stream_wait_event(cudaEvent_t const & event) const {
        arg0_.stream_wait_event(event); arg1_.stream_wait_event(event);
      }
#   ifdef NEBO_GPU_TEST
      inline void gpu_prep(int const deviceIndex) const {
          arg0_.gpu_prep(deviceIndex);
          arg1_.gpu_prep(deviceIndex);
      }
#   endif
#endif
     private:
      Arg0 const arg0_;
      Arg1 const arg1_;
      int which_;
      static inline GhostData get_range(IntVec const& extents, GhostData const& ghosts)
      {
          return GhostData(-ghosts.get_minus(0),
                           extents[0] + ghosts.get_plus(0),
                           -ghosts.get_minus(1),
                           extents[1] + ghosts.get_plus(1),
                           -ghosts.get_minus(2),
                           extents[2] + ghosts.get_plus(2));
      }
  };
#ifdef SpatialOps_ENABLE_THREADS
  template<typename Arg0, typename Arg1, typename FieldType>
   struct NeboSwitch<Resize, Arg0, Arg1, FieldType> {
     FieldType typedef field_type;
     NeboSwitch<SeqWalk, typename Arg0::SeqWalkType, typename Arg1::SeqWalkType, FieldType> typedef SeqWalkType;
     NeboSwitch(Arg0 const & arg0, Arg1 const & arg1, int which, GhostData const& range0, GhostData const& range1)
         : arg0_(arg0), arg1_(arg1), range0_(range0), range1_(range1), which_(which)
     {}
     inline SeqWalkType init(IntVec const & extents,GhostData const & ghosts, IntVec const & hasBC, NeboOptionalArg & optArg) const {
          return SeqWalkType(arg0_.init(extents, ghosts, hasBC, optArg),
                             arg1_.init(extents, ghosts, hasBC, optArg),
                             which_, 
                             _overlap(range0_, get_range(extents, ghosts)), 
                             _overlap(range1_, get_range(extents, ghosts)));
     }
     private:
      Arg0 const arg0_;
      Arg1 const arg1_;
      GhostData const range0_;
      GhostData const range1_;
      int which_;
      static inline GhostData _overlap(const GhostData& g1, const GhostData& g2)
      {
          return GhostData(std::max(g1.get_minus(0), g2.get_minus(0)),
                           std::min(g1.get_plus(0), g2.get_plus(0)),
                           std::max(g1.get_minus(1), g2.get_minus(1)),
                           std::min(g1.get_plus(1), g2.get_plus(1)),
                           std::max(g1.get_minus(2), g2.get_minus(2)),
                           std::min(g1.get_plus(2), g2.get_plus(2)));
      }
      static inline GhostData get_range(IntVec const& extents, GhostData const& ghosts)
      {
          return GhostData(-ghosts.get_minus(0),
                           extents[0] + ghosts.get_plus(0),
                           -ghosts.get_minus(1),
                           extents[1] + ghosts.get_plus(1),
                           -ghosts.get_minus(2),
                           extents[2] + ghosts.get_plus(2));
      }
   };
#endif
  template<typename Arg0, typename Arg1, typename FieldType>
   struct NeboSwitch<SeqWalk, Arg0, Arg1, FieldType> {
      FieldType typedef field_type;
      NeboSwitch(Arg0 const & arg0, Arg1 const & arg1, int which, GhostData const& range0, GhostData const& range1)
         : arg0_(arg0), arg1_(arg1), range0_(range0), range1_(range1), which_(which)
      {}
      template<typename OptionalArgs>
      inline bool eval(int const x, int const y, int const z) const {
         if(which_ < 2)
         {
             return INVOKE_ARG_METHOD(template eval<OptionalArgs>(x,y,z));
         }
         else
         {
            return ((range0_.get_minus(0) <= x && x < range0_.get_plus(0) &&
                     range0_.get_minus(1) <= y && y < range0_.get_plus(1) &&
                     range0_.get_minus(2) <= z && z < range0_.get_plus(2))?arg0_.template eval<OptionalArgs>(x,y,z):0) ||
                   ((range1_.get_minus(0) <= x && x < range1_.get_plus(0) &&
                     range1_.get_minus(1) <= y && y < range1_.get_plus(1) &&
                     range1_.get_minus(2) <= z && z < range1_.get_plus(2))?arg1_.template eval<OptionalArgs>(x,y,z):0);
         }
      }
     private:
     Arg0 const arg0_;
     Arg1 const arg1_;
     GhostData const range0_;
     GhostData const range1_;
     int which_;
  };
#ifdef __CUDACC__
  template<typename Arg0, typename Arg1, typename FieldType>
   struct NeboSwitch<GPUWalk, Arg0, Arg1, FieldType> {
      FieldType typedef field_type;
         NeboSwitch(Arg0 const & arg0, Arg1 const & arg1, int which, GhostData const& range0, GhostData const& range1)
           : arg0_(arg0), arg1_(arg1), which_(which)
         {
             for(int i = 0; i < 3; i ++)
             {
                 minus0[i] = range0.get_minus(i);
                 plus0[i] = range0.get_plus(i);
                 minus1[i] = range1.get_minus(i);
                 plus1[i] = range1.get_plus(i);
             }
         }
         template<typename OptionalArgs>
         __device__ inline bool eval(int const x, int const y, int const z) const {
             if(which_ < 2)
             {
                return INVOKE_ARG_METHOD(template eval<OptionalArgs>(x,y,z));
             }
             else
             {
                return ((minus0[0] <= x && x < plus0[0] &&
                         minus0[1] <= y && y < plus0[1] &&
                         minus0[2] <= z && z < plus0[2])?arg0_.template eval<OptionalArgs>(x,y,z):0) ||
                       ((minus1[0] <= x && x < plus1[0] &&
                         minus1[1] <= y && y < plus1[1] &&
                         minus1[2] <= z && z < plus1[2])?arg1_.template eval<OptionalArgs>(x,y,z):0);
             }
         }
        private:
         Arg0 const arg0_;
         Arg1 const arg1_;
         int minus0[3];
         int plus0[3];
         int minus1[3];
         int plus1[3];
         int which_;
    };
#endif
    /**
     * \brief Type for a Field Mask after it has been converted 
     * \param SrcMaskT the type of the source mask
     * \param DestMaskT the type of destination Mask
     * \author Hao Hou
     **/
    template <typename SrcMaskType, typename DestMaskType, typename Dir = NODIR>
    class ConvertedMask{
    public:
        typedef std::vector<IntVec> Points;
        typedef SrcMaskType FromType;
        typedef DestMaskType ToType;

        struct DummyOperatorT{
            struct type{
                typedef Dir DirT;
            };
        };
        
        typedef NeboMaskShiftBuilder<DummyOperatorT, 
                                     typename FromType::field_type, 
                                     typename ToType::field_type> Shift;

        typedef typename Shift::MinusPoint MinusPointT;
        typedef typename Shift::PlusPoint  PlusPointT;
        
        typedef typename Shift::MinusShift MinusShiftT;
        typedef typename Shift::PlusShift  PlusShiftT;

        typedef NeboSwitch<Initial, MinusShiftT, PlusShiftT, typename ToType::field_type> Selector;

        typedef NeboBooleanExpression<Selector, typename ToType::field_type> Result;
        
        ConvertedMask(const SrcMaskType& src, BCSide side1, BCSide side2, BCSide side3):src_(src), face_(side1)
        {
            if(side1 == MINUS_SIDE) neg_face_ = PLUS_SIDE;
            else neg_face_ = MINUS_SIDE;
            if(side2 == NO_SIDE) side2 = PLUS_SIDE;
            if(side3 == NO_SIDE) side3 = PLUS_SIDE;
            all_ = ((side2 != -1 && side2 != side1) || (side3 != -1 && side3 != side1));
        }
        inline BCSide which_face()
        {
            return neg_face_;
        }
        inline const Points& points() const
        {
            return src_.points();
        }
        inline const SrcMaskType& src_mask() const{
            return src_;
        }
        inline Result mask() const{
            return Result(Selector(MinusShiftT(src_), PlusShiftT(src_), all_?2:face_!= MINUS_SIDE));
        }
        /* this function make a spatial mask from a converted mask */
        inline DestMaskType to_mask(const typename DestMaskType::field_type& field) const{
            std::vector<IntVec> points;
            for(std::vector<IntVec>::const_iterator it = src_.points().begin();
                it != src_.points().end();
                it ++)
            {
                if(all_) 
                {
                    _add_point(*it - MinusPointT::int_vec(), points, field.window_without_ghost(), field.get_ghost_data());
                    _add_point(*it - PlusPointT::int_vec(), points, field.window_without_ghost(), field.get_ghost_data());
                }
                else if(face_ == MINUS_SIDE)
                {
                    _add_point(*it - MinusPointT::int_vec(), points, field.window_without_ghost(), field.get_ghost_data());
                }
                else
                {
                    _add_point(*it - PlusPointT::int_vec(), points, field.window_without_ghost(), field.get_ghost_data());
                }
            }
            return DestMaskType(field, points);
        }
        inline bool all() const{ return all_ == 1; }
        inline operator Result() const { return mask(); }
        inline IntVec get_shift_vec(int idx) const
        {
            if(all_ && idx == 0) return MinusPointT::int_vec();
            else if(all_ && idx == 1) return PlusPointT::int_vec();
            else if(face_ == MINUS_SIDE) return MinusPointT::int_vec();
            else return PlusPointT::int_vec();
        }
        template <typename FieldType, typename Point>
        inline NeboBooleanExpression<NeboMaskShift<Initial, Point, Selector, FieldType>, FieldType> shift()
        {
            typedef NeboMaskShift<Initial, Point, Selector, FieldType > ShiftType;
            typedef NeboBooleanExpression<ShiftType, FieldType> ReturnType;
            return ReturnType(ShiftType(mask().expr()));
        }
    private:
        //Shift shift_;
        const SrcMaskType& src_;
        BCSide neg_face_, face_;
        bool all_;
        static inline void _add_point(const IntVec& point, std::vector<IntVec>& vec, const MemoryWindow& maskWindow_, const GhostData& ghosts_)
        {

              if(!(point[0] >= -ghosts_.get_minus(0)) ||
                 !(point[0] < (signed int)(maskWindow_.extent(0)) + ghosts_.get_plus(0)) ||
                 !(point[1] >= -ghosts_.get_minus(1)) ||
                 !(point[1] < (signed int)(maskWindow_.extent(1)) + ghosts_.get_plus(1)) ||
                 !(point[2] >= -ghosts_.get_minus(2)) ||
                 !(point[2] < (signed int)(maskWindow_.extent(2)) + ghosts_.get_plus(2))) return;
              vec.push_back(point);
        }
    };
    template<typename OpT, typename SrcFieldT, typename DestFieldT>
    struct GetOperatorDirImp{typedef NODIR result;};
    template<typename OpT, typename SrcFieldT>
    struct GetOperatorDirImp<OpT, SrcFieldT, SrcFieldT>{typedef typename OpT::type::DirT result;};
    template<typename OpT>
    struct GetOperatorDir{
        typedef typename GetOperatorDirImp<OpT, typename OpT::SrcFieldType,typename OpT::DestFieldType>::result result;
    };

    template <typename Dir, typename SrcFieldT, typename DestFieldT>
    struct GetFDOperatorDir{typedef NODIR result;};
    template <typename Dir, typename SrcFieldT>
    struct GetFDOperatorDir<Dir, SrcFieldT, SrcFieldT>{typedef Dir result;};
    
    /**
     * \struct NeboBoundaryConditionBuilder
     * \brief Supports definition of new Nebo boundary condition.
     *
     * \tparam OperatorT   Operator to invert
     *
     * Note that Gamma is assumed to be the origin, for the stencil points and mask points.
     */
    template<typename OperatorT>
    struct NeboBoundaryConditionBuilder {
    public:
      typedef OperatorT                            OperatorType;   ///< type of operator to invert
      typedef typename OperatorType::SrcFieldType  PhiFieldType;   ///< field type of phi, which this operator modifies
      typedef typename OperatorType::DestFieldType GammaFieldType; ///< type of fields in gamma, which this operator reads

      typedef typename OperatorType::PointCollectionType PointCollection; ///< stencil point collection

      typedef typename PointCollection::Last        LowPoint;      ///< stencil offset for low point in phi, assumes gamma is origin
      typedef typename PointCollection::AllButLast  NonLowPoints;  ///< stencil offsets for all but low point in phi, assumes gamma is origin
      typedef typename PointCollection::First       HighPoint;     ///< stencil offset for high point in phi, assumes gamma is origin
      typedef typename PointCollection::AllButFirst NonHighPoints; ///< stencil offsets for all but high point in phi, assumes gamma is origin

      typedef typename Subtract<IndexTriplet<0,0,0>, LowPoint>:: result LowGammaPoint;
      typedef typename Subtract<IndexTriplet<0,0,0>, HighPoint>::result HighGammaPoint;
      typedef typename ListSubtract<NonLowPoints,  LowPoint>:: result                           NonLowSrcPoints;
      typedef typename ListSubtract<NonHighPoints, HighPoint>::result                           NonHighSrcPoints;
      typedef NeboMaskShiftBuilder<OperatorType, GammaFieldType, PhiFieldType>                  Shift;

      typedef NeboStencilCoefCollection<PointCollection::length> CoefCollection; ///< collection of coefficients

      typedef NeboEdgelessStencilBuilder<NeboNil, NEBO_FIRST_POINT_WO_TN(LowGammaPoint),  GammaFieldType, PhiFieldType> MinusGammaType;
      typedef NeboEdgelessStencilBuilder<NeboNil, NEBO_FIRST_POINT_WO_TN(HighGammaPoint), GammaFieldType, PhiFieldType> PlusGammaType;
      typedef NeboEdgelessStencilBuilder<NeboNil, NonLowSrcPoints,                        PhiFieldType,   PhiFieldType> MinusPhiType;
      typedef NeboEdgelessStencilBuilder<NeboNil, NonHighSrcPoints,                       PhiFieldType,   PhiFieldType> PlusPhiType;

      typedef std::vector<IntVec> Points;
      typedef Points::const_iterator PointIterator;

      typedef ConvertedMask<const SpatialMask<PhiFieldType>,const SpatialMask<GammaFieldType>, typename GetOperatorDir<OperatorType>::result > ConvertedPhiMask;

      /**
       *  \brief construct a boundary condition
       */
      NeboBoundaryConditionBuilder(OperatorType const & op)
      : lowCoef_(op.coefs().last()),
        highCoef_(op.coefs().coef()),
        minusGamma_(1.0),
        plusGamma_(1.0),
        minusPhi_(op.coefs().all_but_last()),
        plusPhi_(op.coefs().others()),
        shift_()
      {}

      ~NeboBoundaryConditionBuilder() {}

      /**
       * \brief Apply boundary condition with gamma as an expression and a converted/non-converted mask
       * \param mask the mask of points where boundary condition applies
       * \param phi the field to modify
       * \param shift the shift vector
       * \param gamma the Nebo expression to read
       * \param minus boolean flag, true if operating on negative face
       **/
      template<typename ExprType, typename MaskType>
      inline void apply_imp(MaskType mask,
                            PhiFieldType& phi,
                            const IntVec& shift,
                            const NeboExpression<ExprType, GammaFieldType>& gamma,
                            BCSide side) const 
      {
          if(side == MINUS_SIDE)
            cpu_apply<ExprType, MinusGammaType, MinusPhiType>(mask.points(),
                                                              shift,
                                                              minusGamma_,
                                                              minusPhi_,
                                                              phi,
                                                              gamma,
                                                              lowCoef_);
          else
            cpu_apply<ExprType, PlusGammaType, PlusPhiType>(mask.points(),
                                                            shift,
                                                            plusGamma_,
                                                            plusPhi_,
                                                            phi,
                                                            gamma,
                                                            highCoef_);
          
      }
      /**
       * \brief Apply boundary condition with gamma as an expression and a converted mask as boundary mask
       * \param mask the mask of points where boundary condition applies
       * \param phi the field to modify
       * \param gamma the expression to read
       **/
      template<typename ExprType>
      inline void operator()(ConvertedPhiMask mask, 
                               PhiFieldType& phi,
                             const NeboExpression<ExprType, GammaFieldType>& gamma) const
      {
            if(mask.all())
                throw std::runtime_error("can not apply boundary condition on more than one face");
            if(phi.active_device_index() == CPU_INDEX) {
                apply_imp(mask, 
                          phi,
                          IntVec(0,0,0),
                          gamma,
                          mask.which_face());
            } else {
                if(mask.which_face() == MINUS_SIDE)
                    phi <<= cond(mask.src_mask(), (minusGamma_(gamma) - minusPhi_(phi)) / lowCoef_)
                                (phi);
                else
                    phi <<= cond(mask.src_mask(), (plusGamma_(gamma) - plusPhi_(phi)) / highCoef_)
                                (phi);
            }
      }
      template<typename MaskType, typename GammaType>
      inline void apply_indirect_conversion(MaskType& mask, PhiFieldType& phi, GammaType gamma, bool minus, int idx) const
      {
          IntVec conv_shift = mask.get_shift_vec(idx);
          if(minus)
              apply_imp(mask,
                        phi,
                        Shift::MinusPoint::int_vec() + conv_shift,
                        gamma,
                        MINUS_SIDE);
          else
              apply_imp(mask,
                        phi,
                        Shift::PlusPoint::int_vec() + conv_shift,
                        gamma,
                        PLUS_SIDE);
      }
      template<typename ExprType, typename SrcMaskType, typename Dir>
      inline void operator()(ConvertedMask<const SpatialMask<SrcMaskType>, const SpatialMask<GammaFieldType>, Dir> mask,
                             PhiFieldType& phi,
                             const NeboExpression<ExprType, GammaFieldType>& gamma,
                             bool minus) const
      {
          if(phi.active_device_index() == CPU_INDEX)
          {
              apply_indirect_conversion(mask, phi, gamma, minus, 0);
              if(mask.all()) apply_indirect_conversion(mask, phi, gamma, minus, 1);
          }
          else
          {
              if(minus)
                phi <<= cond(mask.template shift<PhiFieldType, typename Shift::MinusPoint>(), (minusGamma_(gamma) - minusPhi_(phi)) / lowCoef_)
                            (phi);
              else
                phi <<= cond(mask.template shift<PhiFieldType, typename Shift::PlusPoint>(), (plusGamma_(gamma) - plusPhi_(phi)) / highCoef_)
                            (phi);
          }

      }
      /**
       * \brief Apply boundary condition with gamma as an expression
       * \param mask the mask of points where boundary condition applies
       * \param phi the field to modify
       * \param gamma the Nebo expression to read
       * \param minus boolean flag, true if operating on negative face
       */
      template<typename ExprType>
      inline void operator()(SpatialMask<GammaFieldType> mask,
                             PhiFieldType & phi,
                             const NeboExpression<ExprType, GammaFieldType> & gamma,
                             bool minus) const {
        if(phi.active_device_index() == CPU_INDEX) {
            if(minus)
                apply_imp(mask, 
                          phi,
                          Shift::MinusPoint::int_vec(),
                          gamma,
                          MINUS_SIDE);
            else
                apply_imp(mask, 
                          phi,
                          Shift::PlusPoint::int_vec(),
                          gamma,
                          PLUS_SIDE);
        }
        else {
          if(minus)
            phi <<= cond(shift_.minus(mask), (minusGamma_(gamma) - minusPhi_(phi)) / lowCoef_)
                        (phi);
          else
            phi <<= cond(shift_.plus(mask), (plusGamma_(gamma) - plusPhi_(phi)) / highCoef_)
                        (phi);
        }
      }

      /**
       * \brief Apply boundary condition with gamma as a field
       * \param mask the mask of points where boundary condition applies
       * \param phi the field to modify
       * \param gamma the field to read
       * \param minus
       */
      inline void operator()(SpatialMask<GammaFieldType> mask,
                             PhiFieldType & phi,
                             const GammaFieldType & gamma,
                             bool minus) const
      {
        typedef NeboConstField<Initial, GammaFieldType> GammaField;
        typedef NeboExpression<GammaField, GammaFieldType> GammaExpr;
        (*this)(mask, phi, GammaExpr(GammaField(gamma)), minus);
      }
      
      /**
       * \brief Apply boundary condition with gamma as a field
       * \param mask the mask of points where boundary condition applies
       * \param phi the field to modify
       * \param gamma the field to read
       */
      inline void operator()(ConvertedPhiMask mask, 
                             PhiFieldType & phi,
                             const GammaFieldType & gamma) const
      {
        typedef NeboConstField<Initial, GammaFieldType> GammaField;
        typedef NeboExpression<GammaField, GammaFieldType> GammaExpr;
        (*this)(mask, phi, GammaExpr(GammaField(gamma)));
      }

      template<typename SrcMaskType, typename Dir>
      inline void operator()(ConvertedMask<const SpatialMask<SrcMaskType>, const SpatialMask<GammaFieldType>, Dir> mask,
                             PhiFieldType& phi,
                             const GammaFieldType& gamma,
                             bool minus) const
      {
        typedef NeboConstField<Initial, GammaFieldType> GammaField;
        typedef NeboExpression<GammaField, GammaFieldType> GammaExpr;
        (*this)(mask, phi, GammaExpr(GammaField(gamma)), minus);
      }
      /**
       * \brief Apply boundary condition with gamma as a scalar
       * \param mask the mask of points where boundary condition applies
       * \param phi the field to modify
       * \param gamma the scalar to read
       * \param minus
       */
      inline void operator()(const SpatialMask<GammaFieldType> mask,
                             PhiFieldType & phi,
                             const double gamma,
                             bool minus) const
      {
        typedef NeboScalar<Initial, double> GammaScalar;
        typedef NeboExpression<GammaScalar, GammaFieldType> GammaExpr;
        (*this)(mask, phi, GammaExpr(GammaScalar(gamma)), minus);
      }

      /**
       * \brief Apply boundary condition with gamma as a scalar
       * \param mask the mask of points where boundary condition applies
       * \param phi the field to modify
       * \param gamma the scalar to read
       * \param minus
       */
      inline void operator()(ConvertedPhiMask mask, 
                             PhiFieldType & phi,
                             const double gamma) const
      {
        typedef NeboScalar<Initial, double> GammaScalar;
        typedef NeboExpression<GammaScalar, GammaFieldType> GammaExpr;
        (*this)(mask, phi, GammaExpr(GammaScalar(gamma)));
      }
      
      template<typename SrcMaskType, typename Dir>
      inline void operator()(ConvertedMask<const SpatialMask<SrcMaskType>, const SpatialMask<GammaFieldType>, Dir> mask,
                             PhiFieldType& phi,
                             const double gamma,
                             bool minus) const
      {
        typedef NeboScalar<Initial, double> GammaScalar;
        typedef NeboExpression<GammaScalar, GammaFieldType> GammaExpr;
        (*this)(mask, phi, GammaExpr(GammaScalar(gamma)), minus);
      }

    private:

      /**
       * \brief Apply boundary condition with gamma as an expression
       * \param points the mask of points where boundary condition applies
       * \param shift
       * \param shiftGamma
       * \param shiftPhi
       * \param phi the field to modify
       * \param gamma the Nebo expression to read
       * \param coef
       */
      template<typename ExprType, typename ShiftGamma, typename ShiftPhi>
      inline void cpu_apply( const Points & points,
                             const IntVec & shift,
                             const ShiftGamma & shiftGamma,
                             const ShiftPhi & shiftPhi,
                             PhiFieldType & phi,
                             const NeboExpression<ExprType, GammaFieldType> & gamma,
                             const double coef ) const
      {
        typedef NeboField<Initial, PhiFieldType> LhsTypeInit;
        typedef typename LhsTypeInit::SeqWalkType LhsType;
        LhsTypeInit lhsInit(phi);
        LhsType lhs = lhsInit.init();

        typedef typename ShiftGamma::template WithArg<ExprType>::Stencil GammaType;
        typedef typename ShiftPhi::FieldStencil PhiType;
        typedef DiffOp<Initial, GammaType, PhiType> DiffType;
        typedef NeboScalar<Initial, double> NumType;
        typedef DivOp<Initial, DiffType, NumType> RhsTypeInit;
        typedef typename RhsTypeInit::SeqWalkType RhsType;

        PoolAutoPtr<int> curEval (Pool<int>::get (CPU_INDEX, 3), CPU_INDEX);
        double mappedValue;

        NeboOptionalArg outerIndex(curEval, &mappedValue);

        //arguments to init() call are meaningless, but they are not used at all...
        RhsType rhs = RhsTypeInit(DiffType(shiftGamma(gamma).expr(),
                                           shiftPhi(phi).expr()),
                                  NumType(coef)).init(IntVec(0,0,0),
                                                      GhostData(0),
                                                      IntVec(0,0,0),
                                                      outerIndex);

        PointIterator       ip = points.begin();
        PointIterator const ep = points.end();
        for(; ip != ep; ip++) {
          const int x = (*ip)[0] - shift[0];
          const int y = (*ip)[1] - shift[1];
          const int z = (*ip)[2] - shift[2];

          outerIndex.setOuterIndex(x,y,z);

          lhs.ref(x, y, z) = rhs.template eval</*TODO: Wrap Into Compile Time Optional Args*/void>(x, y, z);
        }
      }

      const double lowCoef_;
      const double highCoef_;
      const MinusGammaType minusGamma_;
      const PlusGammaType plusGamma_;
      const MinusPhiType minusPhi_;
      const PlusPhiType plusPhi_;
      const Shift shift_;
    };
    template <typename DestFieldLocation, typename SrcFieldLocation>
    struct MaskConversionCheck{
        template <int x, int y>
        struct Equals{
            enum {result = (x == y)?1:0};
        };
        typedef typename DestFieldLocation::Offset Dest_Offset;
        typedef typename SrcFieldLocation::Offset Src_Offset;
        enum {Shift = (Equals<Dest_Offset::X, Src_Offset::X>::result && 
                       Equals<Dest_Offset::Y, Src_Offset::Y>::result &&
                       Equals<Dest_Offset::Z, Src_Offset::Z>::result) ?0:1
        };
        MaskConversionCheck(BCSide face)
        {
            if(Shift && face == NO_SIDE)
                throw std::runtime_error("trying to make invalid mask conversion");
        }
    };
    template <typename DstFieldType, typename SrcFieldType> struct MaskConversionFieldTypeCheck;
#define ALLOW_MASK_CONVERSION(from, to) template <> struct MaskConversionFieldTypeCheck<to, from>{};
    ALLOW_MASK_CONVERSION(SVolField, XVolField)
    ALLOW_MASK_CONVERSION(SVolField, YVolField)
    ALLOW_MASK_CONVERSION(SVolField, ZVolField)

#define ALLOW_MASK_CONVERSION_GROUP(dir)\
    ALLOW_MASK_CONVERSION(dir##VolField, dir##SurfXField)\
    ALLOW_MASK_CONVERSION(dir##VolField, dir##SurfYField)\
    ALLOW_MASK_CONVERSION(dir##VolField, dir##SurfZField)\

    ALLOW_MASK_CONVERSION_GROUP(X)
    ALLOW_MASK_CONVERSION_GROUP(Y)
    ALLOW_MASK_CONVERSION_GROUP(Z)
    ALLOW_MASK_CONVERSION_GROUP(S)
    
    /**
     * @brief Convert A Mask from SrcMskType to DstMskType
     * @param DstMskType the Destination Mask Type
     * @param SrcMaskType the Srouce Mask Type
     * @param src the Srouce Mask
     * @param face the face to perform the operation on
     **/
    template <typename DstFieldType, typename Dir, typename SrcMskType>
    static inline ConvertedMask<const SrcMskType, 
                                const SpatialMask<DstFieldType>,
                                typename GetFDOperatorDir<Dir, 
                                                         DstFieldType, 
                                                         typename SrcMskType::field_type>::result
                               > convert(const SrcMskType& src, const BCSide face1, const BCSide face2 = (BCSide)-1, const BCSide face3 = (BCSide)-1)
    {
        typedef typename SrcMskType::field_type SrcFieldType;
        typedef SpatialMask<DstFieldType> DstMskType;
        typedef typename DstFieldType::Location DstLocation;
        typedef typename SrcFieldType::Location SrcLocation;
        typedef typename GetFDOperatorDir<Dir, DstFieldType, SrcFieldType>::result FDDir;
        
        MaskConversionCheck<DstLocation, SrcLocation> conversion_check(face1);
        return ConvertedMask<const SrcMskType, const DstMskType, FDDir>(src, face1, face2, face3);
    }
    
    template <typename DstFieldType, typename SrcMskType>
    static inline ConvertedMask<const SrcMskType, 
                                const SpatialMask<DstFieldType>,
                                typename GetFDOperatorDir<NODIR, 
                                                         DstFieldType, 
                                                         typename SrcMskType::field_type>::result
                               > convert(const SrcMskType& src, const BCSide face1, const BCSide face2 = (BCSide)-1, const BCSide face3 = (BCSide)-1)
    {
        return convert<DstFieldType, NODIR, SrcMskType>(src, face1, face2, face3);
    }

    //TODO: move this to other files
    template<typename SrcT, typename DestT, typename Dir>
    static inline void masked_assign(
            const ConvertedMask<const SpatialMask<SrcT>, 
                                const SpatialMask<DestT>, 
                                Dir>& mask, 
            DestT& field, 
            const typename DestT::value_type& rhs)
    {
#if __CUDACC__
        if(field.active_device_index() == CPU_INDEX)
#endif
            masked_assign_imp(mask, field, NeboScalar<Initial, typename DestT::value_type>(rhs));
#if __CUDACC__
        else
            field <<= cond(mask.mask(), rhs)(field);
#endif
    }

    template<typename SrcT, typename DestT, typename Dir>
    static inline void masked_assign(
            const ConvertedMask<const SpatialMask<SrcT>, 
                                const SpatialMask<DestT>, 
                                Dir>& mask, 
            DestT& field, 
            const DestT& rhs)
    {
#if __CUDACC__
        if(field.active_device_index() == CPU_INDEX)
#endif
            masked_assign_imp(mask, field, NeboConstField<Initial, DestT>(rhs));
#if __CUDACC__
        else
            field <<= cond(mask.mask(), rhs)(field);
#endif
    }

    template<typename SrcT, typename DestT, typename Dir, typename RSHType, typename Expr>
    static inline void masked_assign(
            const ConvertedMask<const SpatialMask<SrcT>, 
                                const SpatialMask<DestT>, 
                                Dir>& mask, 
            DestT& field, 
            const NeboExpression<RSHType, Expr> rhs)
    {
#if __CUDACC__
        if(field.active_device_index() == CPU_INDEX)
#endif
            masked_assign_imp(mask, field, rhs.expr());
#if __CUDACC__
        else
            field <<= cond(mask.mask(), rhs)(field);
#endif
        
    }

    template<typename SrcT, typename DestT, typename Dir, typename RHS>
    static inline void masked_assign_imp(
            const ConvertedMask<const SpatialMask<SrcT>, 
                                const SpatialMask<DestT>, 
                                Dir>& mask, 
            DestT& field, 
            const RHS& rhs)
    {
        typedef NeboField<Initial, DestT> LHSType;
        LHSType ilhs(field);
#if __CUDACC__
        if(IS_GPU_INDEX(field.active_device_index())) {
            std::ostringstream msg;
            msg << "Nebo error in " << "Nebo Masked Assignment" << ":\n"
            ;
            msg << "Left-hand side of masked assignment allocated on ";
            msg << "GPU and this backend does not support GPU execution"
            ;
            msg << "\n";
            msg << "\t - " << __FILE__ << " : " << __LINE__;
            throw(std::runtime_error(msg.str()));
        }
        else {
            if(IS_CPU_INDEX(field.active_device_index())) {
               if(rhs.cpu_ready()) {
#endif

                  PoolAutoPtr<int> curEval (Pool<int>::get (CPU_INDEX, 3), CPU_INDEX);
                  double mappedValue;


                  NeboOptionalArg outerIndex(curEval, &mappedValue);


                  typename LHSType::SeqWalkType lhs = ilhs.init();
                  typename RHS::SeqWalkType expr = rhs.init(IntVec(0,0,0),
                                                            GhostData(0),
                                                            IntVec(0,0,0),
                                                            outerIndex);


                  for(int i = 0; i <= mask.all(); i ++)
                  {
                      IntVec shift = mask.get_shift_vec(i);
                      
                      std::vector<IntVec>::const_iterator ip = mask.points().begin();

                      std::vector<IntVec>::const_iterator const ep = mask.points().end();

                      for(; ip != ep; ip++) {
                         int const x = (*ip)[0] - shift[0];

                         int const y = (*ip)[1] - shift[1];

                         int const z = (*ip)[2] - shift[2];

                         outerIndex.setOuterIndex(x,y,z);

                         lhs.ref(x, y, z) = expr.template eval</*TODO: Wrap Into Compile Time Optional Args*/void>(x, y, z);
                      };
                  }
#if __CUDACC__
               }
               else {
                  std::ostringstream msg;
                  msg << "Nebo error in " << "Nebo Assignment" << ":\n";
                  msg << "Left-hand side of assignment allocated ";
                  msg << "on ";
                  msg << "CPU but right-hand side is not ";
                  msg << "(completely) accessible on the same CPU";
                  msg << "\n";
                  msg << "\t - " << __FILE__ << " : " << __LINE__;
                  throw(std::runtime_error(msg.str()));
               }
            }
            else {
               std::ostringstream msg;
               msg << "Nebo error in " << "Nebo Assignment" << ":\n";
               msg << "Left-hand side of assignment allocated ";
               msg << "on ";
               msg << "unknown device - not on CPU or GPU";
               msg << "\n";
               msg << "\t - " << __FILE__ << " : " << __LINE__;
               throw(std::runtime_error(msg.str()));
            }
        }
#endif
    }

  /**
   * \struct NeboVaryingEdgeStencilBuilder
   * \brief Supports definition of new Nebo stencils that vary as they approach
   * domain edges.
   *
   * \tparam DirT                       direction of stencil and domain boundary to consider
   * \tparam SrcFieldT                  the type of field that this operator acts on
   * \tparam DestFieldT                 the type of field that this operator produces
   * \tparam StencilBuilderT            the main stencil to execute on the interior of the field
   * \tparam NegativeStencilBuildersT   list of stencil buliders to execute on the
   *                                    negative side as execution approaches a
   *                                    boundary.  List expected in the form of
   *                                    a NeboGenericTypeList and should be
   *                                    ordered outer to inner in terms of how
   *                                    close to the edge the stencil executes.
   *                                    For example the first stencil in the
   *                                    list would be executed at the outer
   *                                    edge and the stencil last in the list
   *                                    is executed on the inner most edge.
   *                                    This list is expected to be as long as
   *                                    the needed ghost to execute the main
   *                                    stencil.
   * \tparam NegativeStencilBuildersT   list of stencil buliders to execute on the
   *                                    positive side as execution approaches a
   *                                    boundary.  List expected in the form of
   *                                    a NeboGenericTypeList and should be
   *                                    ordered outer to inner in terms of how
   *                                    close to the edge the stencil executes.
   *                                    For example the first stencil in the
   *                                    list would be executed at the outer
   *                                    edge and the stencil last in the list
   *                                    is executed on the inner most edge.
   *                                    This list is expected to be as long as
   *                                    the needed ghost to execute the main
   *                                    stencil.
   */
    template<typename DirT,
             typename SrcFieldT,
             typename DestFieldT,
             typename StencilBuilderT,
             typename NegativeStencilBuildersT,
             typename PositiveStencilBuildersT>
    struct NeboVaryingEdgeStencilBuilder {
    public:

        template<typename PListT, typename NListT, typename dummy = void>
        struct StencilListCheckSameField {
          typedef StencilListCheckSameField<typename PListT::Collection,
                                            typename NListT::Collection> StencilCollection;
          /* Source Field Check */
          typedef typename std::enable_if<std::is_same<typename PListT::First::SrcFieldType,
                                                       typename StencilCollection::SrcFieldType>::value,
                                          SrcFieldT>::type SrcFieldType1; //Stencil in varying edge has different source field type
          typedef typename std::enable_if<std::is_same<typename NListT::First::SrcFieldType,
                                                       typename StencilCollection::SrcFieldType>::value,
                                          SrcFieldT>::type SrcFieldType2; //Stencil in varying edge has different source field type
          typedef typename std::enable_if<std::is_same<SrcFieldType1, SrcFieldType2>::value,
                                                       SrcFieldType1>::type SrcFieldType; //Stencil in varying edge has different source field type
          /* Destination Field Check */
          typedef typename std::enable_if<std::is_same<typename PListT::First::DestFieldType,
                                                       typename StencilCollection::DestFieldType>::value,
                                          DestFieldT>::type DestFieldType1; //Stencil in varying edge has different destination field type
          typedef typename std::enable_if<std::is_same<typename NListT::First::DestFieldType,
                                                       typename StencilCollection::DestFieldType>::value,
                                          DestFieldT>::type DestFieldType2; //Stencil in varying edge has different destination field type
          typedef typename std::enable_if<std::is_same<DestFieldType1, DestFieldType2>::value,
                                                       DestFieldType1>::type DestFieldType; //Stencil in varying edge has different destination field type
        };
        template<typename dummy>
        struct StencilListCheckSameField<NeboGenericEmptyTypeList, NeboGenericEmptyTypeList, dummy> {
          typedef SrcFieldT SrcFieldType;
          typedef DestFieldT DestFieldType;
        };
        typedef typename StencilListCheckSameField<PositiveStencilBuildersT,
                                                   NegativeStencilBuildersT>::SrcFieldType
                SrcFieldType;         ///< source field type
        typedef typename StencilListCheckSameField<PositiveStencilBuildersT,
                                                   NegativeStencilBuildersT>::DestFieldType
                DestFieldType;         ///< destination field type


        //Conversion of lists from builder to stencil
        template<typename ListT, typename Arg>
        struct StencilListWithArg {
          typedef typename ListT::First::template WithArg<Arg>::Stencil CurrentStencil;
          typedef StencilListWithArg<typename ListT::Collection, Arg> StencilWithArgCollection;
          typedef typename StencilWithArgCollection::Result Collection;
          typedef NeboGenericTypeList<CurrentStencil, Collection>
                  Result;
          static inline Result GetStencils(ListT const list, typename Arg::field_type const & field)
          {
            return StencilWithArgCollection::GetStencils(list.others(), field)((list.current()(field)).expr());
          }
          static inline Result GetStencils(ListT const list, NeboExpression<Arg, SrcFieldType> const & expr)
          {
            return StencilWithArgCollection::GetStencils(list.others(), expr)((list.current()(expr)).expr());
          }
        };
        template<typename Arg>
        struct StencilListWithArg<NeboGenericEmptyTypeList, Arg> {
          typedef NeboGenericEmptyTypeList Result;

          static inline Result GetStencils(NeboGenericEmptyTypeList const list, Arg const & field)
          {
            return list;
          }
          static inline Result GetStencils(NeboGenericEmptyTypeList const list, NeboExpression<Arg, SrcFieldType> const & expr)
          {
            return list;
          }
        };

        // typedefs for when argument is a Nebo expression
        template<typename Arg>
        struct WithArg {
            typedef typename StencilBuilderT::template WithArg<Arg>::Stencil MainStencil;
            typedef typename StencilListWithArg<NegativeStencilBuildersT, Arg>::Result NegativeStencils;
            typedef typename StencilListWithArg<PositiveStencilBuildersT, Arg>::Result PositiveStencils;
            typedef NeboVaryingEdgeStencilOneDim<Initial,
                                                 MainStencil,
                                                 NegativeStencils,
                                                 PositiveStencils,
                                                 Arg,
                                                 DirT,
                                                 DestFieldType> Stencil;
            typedef NeboExpression<Stencil, DestFieldType> Result;
        };

        // typedefs for when argument is a field
        typedef NeboConstField<Initial, SrcFieldType> FieldArg;
        typedef typename StencilBuilderT::template WithArg<FieldArg>::Stencil FieldMainStencil;
        typedef typename StencilListWithArg<NegativeStencilBuildersT, FieldArg>::Result FieldNegativeStencils;
        typedef typename StencilListWithArg<PositiveStencilBuildersT, FieldArg>::Result FieldPositiveStencils;
        typedef NeboVaryingEdgeStencilOneDim<Initial,
                                             FieldMainStencil,
                                             FieldNegativeStencils,
                                             FieldPositiveStencils,
                                             FieldArg,
                                             DirT,
                                             DestFieldType> FieldStencil;
        typedef NeboExpression<FieldStencil, DestFieldType> FieldResult;

        /**
         *  \brief construct a stencil with the specified coefficients
         */
        NeboVaryingEdgeStencilBuilder(const StencilBuilderT & mainST,
                                      const NegativeStencilBuildersT & negST,
                                      const PositiveStencilBuildersT & posST)
         : mainST_(mainST), negST_(negST), posST_(posST)
        {}

        ~NeboVaryingEdgeStencilBuilder() {}

        /**
         * \brief Apply this operator to the supplied source field to produce the supplied destination field
         * \param src the field that the operator is applied to
         * \param dest the resulting field.
         */
        void apply_to_field( const SrcFieldType & src, DestFieldType & dest ) const {
            dest <<= operator()(src);
        }

        /**
         * \brief Nebo's inline operator for field values
         * \param src the field to which the operator is applied
         */
        inline FieldResult
        operator ()( const SrcFieldType & src ) const {
            typedef FieldResult FieldResult;
            typedef FieldStencil FieldStencil;
            typedef StencilListWithArg<NegativeStencilBuildersT, FieldArg> NegativeConverter;
            typedef StencilListWithArg<PositiveStencilBuildersT, FieldArg> PositiveConverter;
            FieldArg const arg(src);
            return FieldResult(FieldStencil(arg,
                                            mainST_(src).expr(),
                                            NegativeConverter::GetStencils(negST_, src),
                                            PositiveConverter::GetStencils(posST_, src)));
        }

        /**
         * \brief Nebo's inline operator for Nebo expressions
         * \param src the Nebo expression to which the operator is applied
         */
        template<typename Arg>
        inline typename WithArg<Arg>::Result
        operator ()( const NeboExpression<Arg, SrcFieldType> & src ) const {
            typedef typename WithArg<Arg>::Stencil Stencil;
            typedef typename WithArg<Arg>::Result Result;
            typedef StencilListWithArg<NegativeStencilBuildersT, Arg> NegativeConverter;
            typedef StencilListWithArg<PositiveStencilBuildersT, Arg> PositiveConverter;
            return Result(Stencil(src.expr(),
                                  mainST_(src).expr(),
                                  NegativeConverter::GetStencils(negST_, src),
                                  PositiveConverter::GetStencils(posST_, src)));
        }

    private:
        const StencilBuilderT mainST_;
        const NegativeStencilBuildersT negST_;
        const PositiveStencilBuildersT posST_;
    };

} // namespace SpatialOps
#endif // NEBO_STENCIL_BUILDER_H

