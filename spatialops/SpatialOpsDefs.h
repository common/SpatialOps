/*
 * Copyright (c) 2014-2021 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#ifndef UT_SpatialOpsDefs_h
#define UT_SpatialOpsDefs_h

#include <spatialops/SpatialOpsConfigure.h>

/** \file SpatialOpsDefs.h */

namespace SpatialOps{

  //==================================================================

  /**
   * @defgroup DirectionDefinitions Direction Definitions
   * @brief Specification of the directions in Cartesian coordinates
   * @{
   */

  /**
   *  @struct XDIR
   *  @brief Defines a type for the x-direction.
   */
  struct XDIR{ enum{value=0}; };

  /**
   *  @struct YDIR
   *  @brief Defines a type for the y-direction.
   */
  struct YDIR{ enum{value=1}; };

  /**
   *  @struct ZDIR
   *  @brief Defines a type for the z-direction.
   */
  struct ZDIR{ enum{value=2}; };

  /**
   *  @struct NODIR
   *  @brief Defines a type to represent no direction
   */
  struct NODIR{ enum{value=-10}; };

  /** @} */  // end of Direction group.




  /**
   *  @addtogroup optypes
   *  @{
   */

  /**
   *  @struct Interpolant
   *  @brief  Defines a type for Interpolant operators.
   */
  struct Interpolant{};

  /**
   *  @struct Extrapolant
   *  @brief  Defines a type for Extrapolant operators.
   */
  struct Extrapolant{};

  /**
   *  @struct Gradient
   *  @brief  Defines a type for Gradient operators.
   */
  struct Gradient{};

  /**
   *  @struct Divergence
   *  @brief  Defines a type for Divergence operators.
   */
  struct Divergence{};

  /**
   *  @struct Filter
   *  @brief  Defines a type for Filter operators.
   */
  struct Filter{};

  /**
   *  @struct Restriction
   *  @brief  Defines a type for Restriction operators.
   */
  struct Restriction{};


  /**
   * @struct InterpolantX
   * @brief X-interpolant for use with FD operations whose src and dest fields are the same type
   */
  struct InterpolantX{ typedef XDIR DirT; };
  /**
   * @struct InterpolantY
   * @brief Y-interpolant for use with FD operations whose src and dest fields are the same type
   */
  struct InterpolantY{ typedef YDIR DirT; };

  /**
   * @struct InterpolantZ
   * @brief Z-interpolant for use with FD operations whose src and dest fields are the same type
   */
  struct InterpolantZ{ typedef ZDIR DirT; };

  /**
   * @struct GradientX
   * @brief X-interpolant for use with FD operations whose src and dest fields are the same type
   */
  struct GradientX{ typedef XDIR DirT; };
  /**
   * @struct GradientY
   * @brief Y-interpolant for use with FD operations whose src and dest fields are the same type
   */
  struct GradientY{ typedef YDIR DirT; };

  /**
   * @struct GradientZ
   * @brief Z-interpolant for use with FD operations whose src and dest fields are the same type
   */
  struct GradientZ{ typedef ZDIR DirT; };

/** @} */  // end of Operator Types group


  /**
   *  @defgroup boundaryconditions Boundary Conditions
   *  @brief Tools to aid in application of boundary conditions.
   *  @{
   */

  /**
   * \enum BCSide
   * \brief Allows identification of whether we are setting the BC
   *        on the right or left side when using an operator.
   */
  enum BCSide{
    MINUS_SIDE,  ///< Minus side
    PLUS_SIDE,   ///< Plus side
    NO_SIDE      ///< for wide stencils where we set on a point rather than a face
  };


  /**
   * \enum BCType
   */
  enum BCType{
    DIRICHLET,         ///< Dirichlet boundary
    NEUMANN,           ///< Neumann boundary
    UNSUPPORTED_BCTYPE ///< Unsupported boundary type
  };

  /** @} */  // end of boundary conditions group

  /**
   * \namespace DomainEdgeSide
   * \brief Allows identification of whether a domain edge lies on the minus,
   * positive, or both sides of a single dimension.
   */
  namespace DomainEdgeSide
  {
    /**
     *  @struct NO_SIDE
     *  @brief Defines no domain edge on a dimension
     */
    struct NO_SIDE{ enum{value=0x0}; };
    /**
     *  @struct MINUS_SIDE
     *  @brief Defines a domain edge on the minus side of a dimension
     */
    struct MINUS_SIDE{ enum{value=0x1}; };
    /**
     *  @struct PLUS_SIDE
     *  @brief Defines a domain edge on the plus side of a dimension
     */
    struct PLUS_SIDE{ enum{value=0x2}; };
    /**
     *  @struct BOTH_SIDE
     *  @brief Defines a domain edge on both sides of a dimension
     */
    struct BOTH_SIDE{ enum{value=0x3}; };
  }

  //==================================================================

}

#endif
