#include <set>
#include <iostream>
using std::cout;
using std::endl;

#include <spatialops/structured/FVStaggeredFieldTypes.h>

#include <spatialops/particles/ParticleOperators.h>
#include <spatialops/structured/FieldHelper.h>
#include <spatialops/structured/Grid.h>
#include <spatialops/util/TimeLogger.h>
#include <test/TestHelper.h>

#include <cfloat>

typedef SpatialOps::SVolField CellField;
using namespace SpatialOps;


#ifdef SpatialOps_ENABLE_CUDA
bool gpu_check(const int ncell,
               const SpatialOps::Particle::InterpOptions interMethod)
{
  typedef SpatialOps::Particle::ParticleField ParticleField;

  const size_t np = pow(ncell, 3);
  const IntVec dim(ncell, ncell, ncell);
  
  const IntVec dimx(dim[0], 1, 1);
  const IntVec dimy(1, dim[1], 1);
  const IntVec dimz(1, 1, dim[2]);
  
  const DoubleVec length( dim[0], dim[1], dim[2] );
  const Grid grid( dim, length );
  const GhostData cg(3);
  const BoundaryCellInfo cbc = BoundaryCellInfo::build<CellField>();
  const MemoryWindow mw = get_window_with_ghost( dim,  cg, BoundaryCellInfo::build<CellField>() );
  const MemoryWindow mwx= get_window_with_ghost( dimx, cg, BoundaryCellInfo::build<CellField>() );
  const MemoryWindow mwy= get_window_with_ghost( dimy, cg, BoundaryCellInfo::build<CellField>() );
  const MemoryWindow mwz= get_window_with_ghost( dimz, cg, BoundaryCellInfo::build<CellField>() );
  
  CellField xcoord  ( mwx, cbc, cg, NULL );
  CellField ycoord  ( mwy, cbc, cg, NULL );
  CellField zcoord  ( mwz, cbc, cg, NULL );
  CellField f       ( mw,  cbc, cg, NULL );
  CellField ctmpcpu ( mw,  cbc, cg, NULL );
  CellField ctmpgpu ( mw,  cbc, cg, NULL );

  const GhostData pg(0);
  const BoundaryCellInfo pbc = BoundaryCellInfo::build<SpatialOps::Particle::ParticleField>();
  const MemoryWindow pmw( IntVec(np,1,1) );

  ParticleField pxCoord( pmw, pbc, pg, NULL );
  ParticleField pyCoord( pmw, pbc, pg, NULL );
  ParticleField pzCoord( pmw, pbc, pg, NULL );
  ParticleField pSize  ( pmw, pbc, pg, NULL );
  ParticleField pfield ( pmw, pbc, pg, NULL );
  ParticleField ptmpcpu( pmw, pbc, pg, NULL );
  ParticleField ptmpgpu( pmw, pbc, pg, NULL );
  
  grid.set_coord<SpatialOps::XDIR>( xcoord ); // set X the coordinates.
  grid.set_coord<SpatialOps::YDIR>( ycoord ); // set Y the coordinates
  grid.set_coord<SpatialOps::ZDIR>( zcoord ); // set Z the coordinates.
  
  f <<= 10;                            // set the field values
  pfield <<= 1;
  pSize <<= 0.01;

  //
  // build the operators
  //
  typedef SpatialOps::Particle::CellToParticle<CellField> C2P;
  C2P c2p(xcoord[1]-xcoord[0], xcoord[cg.get_minus(0)],
          ycoord[1]-ycoord[0], ycoord[cg.get_minus(0)],
          zcoord[1]-zcoord[0], zcoord[cg.get_minus(0)],
          interMethod);
  
  typedef SpatialOps::Particle::ParticleToCell<CellField> P2C;
  P2C p2c(xcoord[1] - xcoord[0], xcoord[cg.get_minus(0)],
          ycoord[1] - ycoord[0], ycoord[cg.get_minus(0)],
          zcoord[1] - zcoord[0], zcoord[cg.get_minus(0)],
          interMethod);

  ParticleField::iterator ipx    = pxCoord.begin();
  ParticleField::iterator ipy    = pyCoord.begin();
  ParticleField::iterator ipz    = pzCoord.begin();  
  srand(10);
  for(; ipx != pxCoord.end(); ++ipx, ++ipy, ++ipz)
  {
    *ipx    = (double)(rand() % (dim[0]*100))/100;
    *ipy    = (double)(rand() % (dim[1]*100))/100;
    *ipz    = (double)(rand() % (dim[2]*100))/100;
  }

  c2p.set_coordinate_information( &pxCoord, &pyCoord, &pzCoord, &pSize );
  p2c.set_coordinate_information( &pxCoord, &pyCoord, &pzCoord, &pSize );
  
  Timer timer;
  timer.reset();
  p2c.apply_to_field( pfield, ctmpcpu );
  const double p2ctime_cpu = timer.stop();

  
  timer.reset();
  c2p.apply_to_field( f, ptmpcpu );
  const double c2ptime_cpu = timer.stop();
  
  // GPU
  
  pxCoord.set_device_as_active(GPU_INDEX);
  pyCoord.set_device_as_active(GPU_INDEX);
  pzCoord.set_device_as_active(GPU_INDEX);
  pSize.set_device_as_active  (GPU_INDEX);
  f.set_device_as_active      (GPU_INDEX);
  pfield.set_device_as_active (GPU_INDEX);
  
  
  ptmpgpu.set_device_as_active(GPU_INDEX);
  ctmpgpu.set_device_as_active(GPU_INDEX);
  
  timer.reset();
  p2c.apply_to_field( pfield, ctmpgpu );
  const double p2ctime_gpu = timer.stop();
  
  timer.reset();
  c2p.apply_to_field( f, ptmpgpu );
  const double c2ptime_gpu = timer.stop();

  std::cout << " CPU/GPU time for ncell: " << ncell << "^3 \n"
            << "     P2C : " << p2ctime_cpu/p2ctime_gpu << endl
            << "     C2P : " << c2ptime_cpu/c2ptime_gpu << endl;
  // sanity check
  
  ptmpgpu.set_device_as_active(CPU_INDEX);
  ctmpgpu.set_device_as_active(CPU_INDEX);
  
  ParticleField::iterator ipcpu = ptmpcpu.begin();
  ParticleField::iterator ipgpu = ptmpgpu.begin();
  
  bool sanitycheck = true;
  for (; ipcpu != ptmpcpu.end() ; ++ipcpu, ++ipgpu) {
    if (abs(*ipcpu - *ipgpu) > 1e-8)
    {
      cout << "GPU ERROR ON PARTICE FIELD!" << endl
           << "CPU : " << *ipcpu << ", GPU : " << *ipgpu << "\n";
      sanitycheck = false;
      break;
    }
  }
  
  CellField::iterator iccpu = ctmpcpu.begin();
  CellField::iterator icgpu = ctmpgpu.begin();
  for (; iccpu != ctmpcpu.end() ; ++iccpu, ++icgpu) {
    if (abs(*iccpu - *icgpu) > 1e-8)
    {
      cout << "GPU ERROR ON CELL FIELD!" << endl
           << "CPU : " << *iccpu << ", GPU : " << *icgpu << "\n";
      sanitycheck = false;
      break;
    }
  }
  return sanitycheck;
}

#endif

int main()
{
  const IntVec dim(10,1,1);
  const DoubleVec length( 10,1,1 );
  const Grid grid( dim, length );

  const GhostData cg(3);
  const BoundaryCellInfo cbc = BoundaryCellInfo::build<CellField>();
  const MemoryWindow mw = get_window_with_ghost( dim, cg, BoundaryCellInfo::build<CellField>() );

  //
  // build the fields
  //
  
# ifdef SpatialOps_ENABLE_CUDA
  const short int location = GPU_INDEX;
# else
  const short int location = CPU_INDEX;
# endif
  
  CellField xcoord( mw, cbc, cg, NULL, InternalStorage, location );
  CellField f     ( mw, cbc, cg, NULL, InternalStorage, location );
  CellField ctmpIn( mw, cbc, cg, NULL, InternalStorage, location );
  CellField ctmpNI( mw, cbc, cg, NULL, InternalStorage, location );

  const GhostData pg(0);
  const BoundaryCellInfo pbc = BoundaryCellInfo::build<SpatialOps::Particle::ParticleField>();
  const size_t np=1;
  const MemoryWindow pmw( IntVec(np,1,1) );

  SpatialOps::Particle::ParticleField pCoord( pmw, pbc, pg, NULL, InternalStorage, location );
  SpatialOps::Particle::ParticleField pSize ( pmw, pbc, pg, NULL, InternalStorage, location );
  SpatialOps::Particle::ParticleField pfield( pmw, pbc, pg, NULL, InternalStorage, location );
  SpatialOps::Particle::ParticleField ptmpIn( pmw, pbc, pg, NULL, InternalStorage, location );
  SpatialOps::Particle::ParticleField ptmpNI( pmw, pbc, pg, NULL, InternalStorage, location );

  grid.set_coord<SpatialOps::XDIR>( xcoord ); // set the coordinates.
  f      <<= 10*xcoord;                            // set the field values
  pCoord <<= 3.25;  // location of the particle
  pSize  <<= 5;     // size of the particle
  pfield <<= 20;    // value on the particle
  //
  // build the operators - Interpolating
  //
  
  typedef SpatialOps::Particle::CellToParticle<CellField> C2P;
  C2P c2p( 1, 0.5 );
  c2p.set_coordinate_information( &pCoord, NULL, NULL, &pSize );

  typedef SpatialOps::Particle::ParticleToCell<CellField> P2C;
  P2C p2c( 1, 0.5 );
  p2c.set_coordinate_information( &pCoord, NULL, NULL, &pSize );
  //
  // interpolate to particles
  //

  c2p.apply_to_field( f, ptmpIn );
  p2c.apply_to_field( pfield, ctmpIn );

  //
  // build the operators - Not Interpolating
  //
  typedef SpatialOps::Particle::CellToParticle<CellField> C2P;
  C2P c2pNI( 1, 0.5,
             0, 0, 0, 0,
             SpatialOps::Particle::NO_INTERPOLATE );
  c2pNI.set_coordinate_information( &pCoord, NULL, NULL, &pSize );
  
  typedef SpatialOps::Particle::ParticleToCell<CellField> P2C;
  P2C p2cNI( 1, 0.5,
             0, 0, 0, 0,
             SpatialOps::Particle::NO_INTERPOLATE );
  p2cNI.set_coordinate_information( &pCoord, NULL, NULL, &pSize );
  
  //
  // interpolate to particles
  //
  c2pNI.apply_to_field( f, ptmpNI );
  p2cNI.apply_to_field( pfield, ctmpNI );
  
# ifdef SpatialOps_ENABLE_CUDA
  // [] operator is not working on the GPU fields
  ptmpIn.set_device_as_active(CPU_INDEX);
  ctmpIn.set_device_as_active(CPU_INDEX);
  ctmpNI.set_device_as_active(CPU_INDEX);
  ptmpNI.set_device_as_active(CPU_INDEX);
# endif
  
  TestHelper status(true);
  status( ptmpIn[0] == 32.5, "c2p     - Interpolated" );

  const size_t ishift = cg.get_minus(0);
  status( ctmpIn[ishift+0] == 1, "p2c [0] - Interpolated" );
  status( ctmpIn[ishift+1] == 4, "p2c [1] - Interpolated" );
  status( ctmpIn[ishift+2] == 4, "p2c [2] - Interpolated" );
  status( ctmpIn[ishift+3] == 4, "p2c [3] - Interpolated" );
  status( ctmpIn[ishift+4] == 4, "p2c [4] - Interpolated" );
  status( ctmpIn[ishift+5] == 3, "p2c [5] - Interpolated" );
  status( ctmpIn[ishift+6] == 0, "p2c [6] - Interpolated" );
  status( ctmpIn[ishift+7] == 0, "p2c [7] - Interpolated" );
  status( ctmpIn[ishift+8] == 0, "p2c [8] - Interpolated" );
  status( ctmpIn[ishift+9] == 0, "p2c [9] - Interpolated" );

  status( ptmpNI[0] == 35,  "c2p     - Not Interpolated" );
  
  status( ctmpNI[ishift+0] == 0, "p2c [0] - Not Interpolated" );
  status( ctmpNI[ishift+1] == 0, "p2c [1] - Not Interpolated" );
  status( ctmpNI[ishift+2] == 0, "p2c [2] - Not Interpolated" );
  status( ctmpNI[ishift+3] == 20,"p2c [3] - Not Interpolated" );
  status( ctmpNI[ishift+4] == 0, "p2c [4] - Not Interpolated" );
  status( ctmpNI[ishift+5] == 0, "p2c [5] - Not Interpolated" );
  status( ctmpNI[ishift+6] == 0, "p2c [6] - Not Interpolated" );
  status( ctmpNI[ishift+7] == 0, "p2c [7] - Not Interpolated" );
  status( ctmpNI[ishift+8] == 0, "p2c [8] - Not Interpolated" );
  status( ctmpNI[ishift+9] == 0, "p2c [9] - Not Interpolated" );

# ifdef SpatialOps_ENABLE_CUDA
  status( gpu_check(16, SpatialOps::Particle::NO_INTERPOLATE),  "GPU test: ncell= 16^3, Approximate method" );
  status( gpu_check(32, SpatialOps::Particle::NO_INTERPOLATE),  "GPU test: ncell= 32^3, Approximate method" );
  status( gpu_check(16, SpatialOps::Particle::INTERPOLATE),     "GPU test: ncell= 16^3, Interpolation" );
  status( gpu_check(32, SpatialOps::Particle::INTERPOLATE),     "GPU test: ncell= 32^3, Interpolation" );
# endif

  if( status.ok() ) return 0;
  return -1;
};
