#include <set>
#include <sstream>
#include <iostream>
#include <algorithm>
using std::cout;
using std::endl;
#include <iomanip>
#include <spatialops/structured/FVStaggeredFieldTypes.h>
#include <spatialops/particles/ParticleOperators.h>
#include <spatialops/structured/FieldHelper.h>
#include <spatialops/structured/MatVecFields.h>
#include <spatialops/util/TimeLogger.h>
#include <test/TestHelper.h>
#include <stdio.h>
#include <spatialops/structured/FieldComparisons.h>

#include <spatialops/structured/ExternalAllocators.h>

#include <cfloat>
#include <stdio.h>
#include <fstream>
#include <stdlib.h>

using namespace SpatialOps;
using namespace SpatialOps::Particle;
using std::cout;
using std::endl;

#include <boost/program_options.hpp>
namespace po = boost::program_options;

int main( int iarg, char* carg[] )
{
    int nparticles, minx,miny,minz, maxx,maxy,maxz, pd;

    double h = 1.0f;

    int seed;

    bool sort = false, bf = false;

    po::options_description desc("Supported Options");
    desc.add_options()
    ( "help", "print help message\n" )
    ( "np",   po::value<int>(&nparticles)->default_value(1000), "number of particles" )
    ( "gridminx" , po::value<int>(&minx)->default_value(0), "lower bound for particle x coordinate" )
    ( "gridminy" , po::value<int>(&miny)->default_value(0), "lower bound for particle y coordinate" )
    ( "gridminz" , po::value<int>(&minz)->default_value(0), "lower bound for particle z coordinate" )
    ( "gridmaxx" , po::value<int>(&maxx)->default_value(50), "upper bound for particle x coordinate" )
    ( "gridmaxy" , po::value<int>(&maxy)->default_value(50), "upper bound for particle y coordinate" )
    ( "gridmaxz" , po::value<int>(&maxz)->default_value(50), "upper bound for particle z coordinate" )
    ( "seed" , po::value<int>(&seed)->default_value(10), "seed for particle position generation" )
    ( "h" , po::value<double>(&h)->default_value(1.0f), "smoothing radius for each particle" )
    ( "sort", "sort data?" )
    ( "bf" , "brute force?" )
    ( "pd" , po::value<int>(&pd)->default_value(-1), "particle density per cell, if specified overrides num particles" );

    po::variables_map args;
    po::store( po::parse_command_line(iarg,carg,desc), args );
    po::notify(args);

    if( args.count("sort") ) sort = true;
    if( args.count("bf") ) bf = true;

    if( args.count("help") ){
        cout << desc << endl
        << "Examples:" << endl
        << " range_search_grid --gridminx 5 --sort" << endl
        << " range_search_grid  --seed 1 --bf" << endl
        << endl;
        return -1;
    }


    IntVec gridMin (-1, -1, -1);
    IntVec gridMax (10, 10, 1);


    int fluid_particles(500);
    double height(5); double width(1.5);    // Dam Dimensions
    double dist(sqrt(height*width/fluid_particles));  // Distance (horizontally and vertically) between particles
    int xparticles(width/dist); int yparticles(height/dist); // number of particles in one row of dam
    int domain_height(8); int domain_width(8);
    h=2.5*dist;



    // initializing Dam particle positions*:
    std::vector<DoubleVec> positions;
    std::vector<IntVec> maskSet;

    double ppx,ppy,ppz;

    int i=0;
    for (int dx = 0; dx <= xparticles; dx++) // arranged such that it loops from bot left to top right (going from bottom to top)
        for (int dy = 0; dy <= yparticles; dy++)
            //      for (int z = 0; z < zparticles; z++)
        {
            ppx = dist*dx;
            ppy = dist*dy;
            ppz = 0;

            positions.push_back(DoubleVec(ppx,ppy,ppz));
            positions.push_back(DoubleVec(domain_width - ppx,ppy,ppz));

            i++;
        }
    fluid_particles=i*2; // redefine number of particles
    i=i+1;
    // initializing Boundary Dam positions*:
    int d=0;
    int xbp ((domain_width+dist/1.5)/(dist));

    for (int dx = 0; dx <= xbp ; dx++) //bottom section
        for (int dy = 0; dy <= 1; dy++)
            //      for (int z = 0; z < zparticles; z++)
        {

            ppx = -dist/3+dist*(dx)+dist/2*(dy);
            ppy = -dist*(dy)-dist;
            ppz = 0;
            positions.push_back(DoubleVec(ppx,ppy,ppz));
            i++;
            d++;
        }

    int ybp (((domain_height+4.0*dist)/(dist)));

    for (int j = 0; j <= 1 ; j++)
        for (int dx = 0; dx <= 1 ; dx++)
            for (int dy = 0; dy <= ybp; dy++)
                //      for (int z = 0; z < zparticles; z++)
            {
                if (j==0){                                       // left wall
                    ppx = -dist*dx -dist;
                    ppy = -2*dist+dist*dy+dist/2*dx;
                    ppz = 0;
                    positions.push_back(DoubleVec(ppx,ppy,ppz));
                    i++;
                }
                else{
                    ppx = domain_width + dist*dx + dist;
                    ppy = -2*dist+dist*dy+dist/2*dx;
                    ppz = 0;
                    positions.push_back(DoubleVec(ppx,ppy,ppz));
                    i++;
                }
                d++;
            }



    int boundary_particles=d; // redefine number of boundary particles
    nparticles=fluid_particles+boundary_particles; // total number of particles

    const GhostData pg(0);
    const BoundaryCellInfo pbc = BoundaryCellInfo::build<SpatialOps::Particle::ParticleField>();
    const MemoryWindow pmwRS( get_window_with_ghost( IntVec(nparticles,0,0), pg, pbc) );

    SpatialOps::Particle::ParticleField mapped_val_field( pmwRS, pbc, pg, NULL );

    FieldVector<ParticleField> properties (5, pmwRS, pbc, pg);
    FieldVector<ParticleField> out (4, pmwRS, pbc, pg);
    FieldVector<ParticleField> pCoord (3, pmwRS, pbc, pg);

    for (int i= 0; i < nparticles; i++)
    {

        pCoord.at(0)[i]    = positions[i][0];
        pCoord.at(1)[i]    = positions[i][1];
        pCoord.at(2)[i]    = positions[i][2];

        if (i>=fluid_particles){
            maskSet.push_back( IntVec(i, 0, 0) );
        }
    }

    SpatialMask<ParticleField> maskx( pCoord.at(0), maskSet );
    SpatialMask<ParticleField> masky( pCoord.at(1), maskSet );
    SpatialMask<ParticleField> maskz( pCoord.at(2), maskSet );


    // this loop initializes the vector of properties of size 4 with random values
    for (size_t i = 0; i < properties.elements(); i++)
    {
        if (i==0)
            properties.at(0) <<= 1.0;   // density is 1000
        else
            properties.at(i) <<= 0.0;
        // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   set xvelcity to be 1
    }


    // initializes the output properties in the vector of fields of size 2 with 0
    for (size_t i = 0; i < out.elements(); i++)
    {
        out.at(i) <<= 0.0;
    }



    //   SpatialOps::SpatialMask<SpatialOps::Particle::ParticleField> mask( pCoord.at(0), maskSet );

    short int location = CPU_INDEX;

# ifdef SpatialOps_ENABLE_CUDA
    pCoord.set_device_as_active(GPU_INDEX);
    properties.set_device_as_active(GPU_INDEX);
    out.set_device_as_active(GPU_INDEX);
    maskx.add_consumer(GPU_INDEX);
    masky.add_consumer(GPU_INDEX);
    maskz.add_consumer(GPU_INDEX);
    location = GPU_INDEX;
# endif

    /*
     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


     out(0)=density
     out(1)=xvelocity
     out(2)=yvelocity
     out(3)=zvelocity

     properties(0)=density
     properties(1)=xvelocity
     properties(2)=yvelocity
     properties(3)=zvelocity
     properties(4)=pressure

     rij= pow( pow(local(pCoord.at(0))-pCoord.at(0),2) + pow(local(pCoord.at(1))-pCoord.at(1),2),0.5)
     xvelocity_ij * xij = (local(properties.at(1)) - properties.at(1))*(local(pCoord.at(0))-pCoord.at(0))

     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

     */

    double rho0(1.0);
    double mass_p=(height*width*rho0/fluid_particles); // mass of each particle

    double pi(3.14);
    double c0 =10.0*sqrt(2*1.81*height); // reference speed of sound usually defined as 10*max(velocity)
    int gamma=7; // used in tait equation of state.
    double beta =rho0*c0*c0/gamma; // constant for tait equation
    double gravity(-1.81);

    double sim_time(0.0);
    double end_time(3.2);
    double delta_time(.0001); int iterations(0);
    double detla_t_io=.03; double output_time=0;
    h=1.25*dist;

    int file_count = 1;
    
    // for 2D
//    h=h/2;
    while (sim_time<=end_time){


        RangeSearch rangeSearch(pmwRS, pg, pbc, gridMin, gridMax, nparticles, h, pCoord.at(0),pCoord.at(1),!bf, sort, location);

        // Density eval

        out.at(0) <<= nebo_mapped_reduction (mass_p*
                                             ((local(properties.at(1)) - properties.at(1))*(local(pCoord.at(0))-pCoord.at(0)) // (xvelocity_ij * x_ij + ...
                                              + (local(properties.at(2)) - properties.at(2))*(local(pCoord.at(1))-pCoord.at(1))) // yvelocity_ij * y_ij) * ...
                                             *
                                             (cond( (mapped_value() > pow(10,-12)), // to avoid NaNs
                                                   cond( (mapped_value()/h <= 2),
                                                        ( 7/(4*pi*pow(h,3))) * // alpha *
                                                        (-5*mapped_value()/h    * // -5*r.ij/h *
                                                         pow(    1   -   .5  *  mapped_value()/h     ,3)) // (1- 0.5*r.ij/h)^3 /
                                                        /mapped_value() // rij
                                                        )
                                                   (0)) // else (0)
                                              (0)) // else 0                   dW/dr
                                             , rangeSearch.GetCurrentState(location));

        // xvelocity eval

        out.at(1) <<= nebo_mapped_reduction (mass_p *

                                             (local(properties.at(4))/pow(local(properties.at(0)),2) //    ( pressure_i / (rho_i) ^ 2 + ...
                                              + properties.at(4)/pow(properties.at(0),2))          //      pressure_j / (rho_j) ^ 2 )
                                             *
                                             (cond( mapped_value() > pow(10,-12), // to avoid NaNs

                                                   cond( (mapped_value()/h) <= 2,

                                                        (local(pCoord.at(0))-pCoord.at(0))  // x_ij  * only value that changes for velocity calc
                                                        *( 7/(4*pi*pow(h,3))) * //alpha *
                                                        (-5*mapped_value()/h    * //-5*r.ij/h *
                                                         pow(    1   -   (.5  *   mapped_value())/h     ,3)) // (1- (0.5*r.ij/h)^3 /
                                                        /mapped_value() //rij
                                                        )
                                                   (0)) //else (0)
                                              (0)) // else 0                   dW/dr                                  )
                                             , rangeSearch.GetCurrentState(location));

        // yvelocity eval

        out.at(2) <<= nebo_mapped_reduction (mass_p *
                                             (local(properties.at(4))/pow(local(properties.at(0)),2) //    ( pressure_i / (rho_i) ^ 2 + ...
                                              + properties.at(4)/pow(properties.at(0),2))          //      pressure_j / (rho_j) ^ 2 )
                                             * (   // dW/dr:

                                                cond( mapped_value() > pow(10,-12), // to avoid NaNs
                                                     (
                                                      cond( (mapped_value()/h <= 2), // if rij/h<=2

                                                           (
                                                            (local(pCoord.at(1))-pCoord.at(1))  // y_ij
                                                            *7/(4*pi*pow(h,3)))   * //alpha

                                                           (-5*mapped_value()/h    * //-5*r.ij/h

                                                            pow(    1   -   (.5  *   mapped_value())/h     ,3)) // (1- (0.5*r.ij/h)^3

                                                           /mapped_value() //rij

                                                           )

                                                      (0) // else 0
                                                      ))
                                                (0)
                                                )
                                             , rangeSearch.GetCurrentState(location));




        // Density update

        properties.at(0) <<=  properties.at(0) + delta_time * out.at(0);

        //Boundary particles' density is >=1000

        properties.at(0) <<= cond( maskx && properties.at(0)<rho0, rho0 )
                                                                   (properties.at(0) );

        //Pressure Eval

        properties.at(4) <<= beta*(pow(properties.at(0)/rho0 ,7.0 )-1.0); // beta * ((rho_i/rho0 )^7-1) (Tait Equation of State)

        // Pressure >0

        properties.at(4) <<= cond( properties.at(4)<0, 0 )
                                    ( properties.at(4) );

        // Velocity update

        properties.at(1) <<=  properties.at(1) + delta_time * (-out.at(1));
        properties.at(2) <<=  properties.at(2) + delta_time * (-out.at(2)+gravity);

        // boundary particles are stationary

        properties.at(1) <<= cond(maskx,0)
                                        (properties.at(1));

        properties.at(2) <<= cond(masky,0)
                                        (properties.at(2));

        // Position update

        pCoord.at(0) <<= pCoord.at(0) + properties.at(1)*delta_time;
        pCoord.at(1) <<= pCoord.at(1) + properties.at(2)*delta_time;

        sim_time=sim_time+delta_time;
        iterations+=1;

#ifndef NDEBUG
            
//
        if (output_time<= sim_time)
        {
//
            std::string filename = "out";
            std::string ext = ".dat";



            std::ostringstream oss;
            oss << filename << file_count++ << ext;
            std::ofstream outputFile(oss.str().c_str());
//            std::ofstream outputFile(filename.c_str());
            outputFile.precision(3);
            outputFile.width(20);
            outputFile.fill(0);

            outputFile.clear();

    #ifdef SpatialOps_ENABLE_CUDA
            pCoord.set_device_as_active(CPU_INDEX);
    #endif

            for (i=0; i<nparticles; i++){
                outputFile<< std::fixed << pCoord.at(0)[i]<< ' ' << pCoord.at(1)[i] << ' ' << pCoord.at(2)[i] << endl ;
            }
          
    #ifdef SpatialOps_ENABLE_CUDA
            pCoord.set_device_as_active(GPU_INDEX);
    #endif
            output_time=output_time+detla_t_io;
            outputFile<< endl;
            //system("/opt/local/bin/gnuplot -e \'plot \"out.txt\"  u 1:2\' ");

            outputFile.close();

        }
        
#endif
    } //   --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    return 0;
};


