
#ifndef ParticlesOperatorHelper_h
#define ParticlesOperatorHelper_h
namespace SpatialOps{
  namespace Particle{
    
        enum InterpOptions{
          INTERPOLATE    = 0,
          NO_INTERPOLATE = 1
        };
  }
}

#endif // ParticlesOperatorHelper_h
