/*
 * Copyright (c) 2014-2021 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */
#ifndef ParticleOperators_h
#define ParticleOperators_h

#include <spatialops/particles/ParticleFieldTypes.h>
#include <spatialops/particles/ParticlesOperatorHelper.h>
#include <spatialops/Nebo.h>
#include <spatialops/structured/SpatialMask.h>
#include <spatialops/structured/MatVecFields.h>
#include <spatialops/particles/ParticleGrid.h>

#include <stdexcept>
#include <cmath>
#include <algorithm>
#include <vector>

namespace SpatialOps{
  namespace Particle{
    
    /**
     *  @class    ParticlesPerCell
     *  @date     October 31, 2014
     *  @author   Tony Saad
     *  @brief    Computes the number of particles per cell and accounts for fractional contributions.
     *  @ingroup  optypes
     *
     */
    template< typename CellField >
    class ParticlesPerCell
    {
    public:
      typedef ParticleField SrcFieldType;
      typedef CellField     DestFieldType;
      
      /**
       * @brief construct a ParticlesPerCell operator
       * @param dx  x-direction mesh spacing
       * @param xlo x-direction low coordinate of CellField (interior - not including ghost cells)
       * @param dy  y-direction mesh spacing
       * @param ylo y-direction low coordinate of CellField (interior - not including ghost cells)
       * @param dz  z-direction mesh spacing
       * @param zlo z-direction low coordinate of CellField (interior - not including ghost cells)
       */
      ParticlesPerCell( const double dx,    const double xlo,
                        const double dy=-1, const double ylo =0,
                        const double dz=-1, const double zlo =0,
                        const InterpOptions method=INTERPOLATE);
      
      /**
       * @param pxcoord x-coordinate of each particle
       * @param pycoord y-coordinate of each particle
       * @param pzcoord z-coordinate of each particle
       * @param psize   diameter of each particle
       */
      void set_coordinate_information( const ParticleField * const pxcoord,
                                       const ParticleField * const pycoord,
                                       const ParticleField * const pzcoord,
                                       const ParticleField * const psize );
      
      /**
       *  @param dest destination field to which values are interpolated (CellField)
       */
      void apply_to_field( DestFieldType& dest ) const;

    private:
      const double dx_, dy_, dz_;
      const double xlo_, ylo_, zlo_;
      const ParticleField *px_, *py_, *pz_, *psize_;
      const InterpOptions method_;
    };

    
    /**
     *  @class  ParticleToCell
     *  @author James C. Sutherland, Babak Goshayeshi
     *  @brief Interpolates an extensive particle field onto an underlying mesh field.
     *  @ingroup optypes
     *
     *  Note that this should only be used to interpolate extensive quantities and
     *  not intensive quantities.
     */
    template< typename CellField >
    class ParticleToCell
    {
    public:
      typedef ParticleField SrcFieldType;
      typedef CellField     DestFieldType;
      
      /**
       * @brief construct a ParticleToCell operator
       * @param dx  x-direction mesh spacing
       * @param xlo x-direction low coordinate of CellField (interior - not including ghost cells)
       * @param dy  y-direction mesh spacing
       * @param ylo y-direction low coordinate of CellField (interior - not including ghost cells)
       * @param dz  z-direction mesh spacing
       * @param zlo z-direction low coordinate of CellField (interior - not including ghost cells)
       */
      ParticleToCell( const double dx,    const double xlo,
                      const double dy=-1, const double ylo =0,
                      const double dz=-1, const double zlo =0,
                      const InterpOptions method=INTERPOLATE );
      
      
      /**
       * @param pxcoord x-coordinate of each particle
       * @param pycoord y-coordinate of each particle
       * @param pzcoord z-coordinate of each particle
       * @param psize  particle diameter - only used if method==INTERPOLATE
       */
      void set_coordinate_information( const ParticleField * const pxcoord,
                                       const ParticleField * const pycoord,
                                       const ParticleField * const pzcoord,
                                       const ParticleField * const psize );
      
      /**
       *  @param src source field from which values are interpolated to particles (ParticleField)
       *  @param dest destination field to which values are interpolated (CellField)
       */
      void apply_to_field( const SrcFieldType& src,
                           DestFieldType& dest ) const;
      

    private:
      
      const double dx_, dy_, dz_;
      const double xlo_, ylo_, zlo_;
      const ParticleField *px_, *py_, *pz_, *psize_;
      const InterpOptions method_;
    };
    
    
    //==================================================================
    
    
    /**
     *  @class CellToParticle
     *  @brief Operator to interpolate a mesh field onto a particle.
     *  @author James C. Sutherland, Babak Goshayeshi
     *  @brief Operator to interpolate a mesh field onto a particle.
     *  @ingroup optypes
     *
     *  Note that this can be used for either intensive or extensive quantities.
     */
    template< typename CellField >
    class CellToParticle
    {
    public:
      typedef CellField     SrcFieldType;
      typedef ParticleField DestFieldType;
      
      /**
       * @brief Construct a CellToParticle operator
       * @param dx  x-direction mesh spacing
       * @param xlo x-direction low coordinate of CellField (interior - not including ghost cells)
       * @param dy  y-direction mesh spacing
       * @param ylo y-direction low coordinate of CellField (interior - not including ghost cells)
       * @param dz  z-direction mesh spacing
       * @param zlo z-direction low coordinate of CellField (interior - not including ghost cells)
       */
      CellToParticle( const double dx,    const double xlo,
                      const double dy=-1, const double ylo =0,
                      const double dz=-1, const double zlo =0,
                      const InterpOptions method=INTERPOLATE );
      
      /**
       * @param pxcoord x-coordinate of each particle
       * @param pycoord y-coordinate of each particle
       * @param pzcoord z-coordinate of each particle
       * @param psize   diameter of each particle - It is not going be used!
       *                However, it is kept in the declration to be consistent.
       */
      void set_coordinate_information( const ParticleField* pxcoord,
                                       const ParticleField* pycoord,
                                       const ParticleField* pzcoord,
                                       const ParticleField* psize );
      
      /**
       * @param src source field from which values are interpolated to particles (VolField)
       * @param dest destination field to which values are interpolated (ParticleField)
       */
      void apply_to_field( const SrcFieldType& src,
                           DestFieldType& dest ) const;
      
      void apply_to_field( const SrcFieldType& src,
                           DestFieldType& dest,
                           const std::string& method ) const;

    private:
      
      const double dx_, dy_, dz_;
      const double xlo_, ylo_, zlo_;
      const ParticleField *px_, *py_, *pz_, *psize_;
      const InterpOptions method_;
    };

    /**
	 *  @class    RangeSearch
	 *  @date     October , 2016
	 *  @author   Siddartha Ravichandran
	 *  @brief    Maintains particle state information, needed to perform Nebo Mapped reduction for SPH simulations.
	 *
	 */
    class RangeSearch
    {
    public:

      typedef typename ParticleGrid::ParticleIterator p_iterator;
      typedef typename ParticleGrid::GridState search_state;

      /**
       * @brief Construct a 3D RangeSearch operator 
       * @param pParticleMemoryWindow  	Memory Window used by particles to construct state information
       * @param pParticleGhostData  	Ghost Data used by particles to construct state information
       * @param pParticleBoundaryInfo  	Boundary info used by particles to construct state information
       * @param pParticleMinExtent      DoubleVec representing the lower bound for particle positions in the domain space
       * @param pParticleMaxExtent  	DoubleVec representing the upper bound for particle positions in the domain space
       * @param pNumParticles 			Number of particles in the domain space
       * @param pSmoothingRadius		Smoothing radius for performing the range search
       * @param pXcoord					Particle field storing the x-coordinates of the particles
       * @param pYcoord					Particle field storing the y-coordinates of the particles
       * @param pZcoord					Particle field storing the z-coordinates of the particles
       * @param pUseGrid				Whether to use a grid to optimize the search
       * @param pSortData				Whether to sort the particle information for optimizing search on GPUs
       * @param pLocation				Whether to store the state information on CPU/GPU
       */
      RangeSearch( const MemoryWindow& pParticleMemoryWindow,
                const GhostData& pParticleGhostData,
                const BoundaryCellInfo& pParticleBoundaryInfo,
                const DoubleVec pParticleMinExtent,
                const DoubleVec pParticleMaxExtent,
                const size_t pNumParticles,
                const double pSmoothingRadius,
                const ParticleField& pXcoord,
                const ParticleField& pYcoord,
                const ParticleField& pZcoord,
                const bool pUseGrid = false,
                const bool pSortData = false,
                const short int pLocation = CPU_INDEX);

    
      /**
       * @brief Construct a 2D RangeSearch operator
       * @param pParticleMemoryWindow   Memory Window used by particles to construct state information
       * @param pParticleGhostData      Ghost Data used by particles to construct state information
       * @param pParticleBoundaryInfo   Boundary info used by particles to construct state information
       * @param pParticleMinExtent      DoubleVec representing the lower bound for particle positions in the domain space
       * @param pParticleMaxExtent      DoubleVec representing the upper bound for particle positions in the domain space
       * @param pNumParticles           Number of particles in the domain space
       * @param pSmoothingRadius        Smoothing radius for performing the range search
       * @param pXcoord                 Particle field storing the x-coordinates of the particles
       * @param pYcoord                 Particle field storing the y-coordinates of the particles
       * @param pUseGrid                Whether to use a grid to optimize the search
       * @param pSortData               Whether to sort the particle information for optimizing search on GPUs
       * @param pLocation               Whether to store the state information on CPU/GPU
       */
      RangeSearch( const MemoryWindow& pParticleMemoryWindow,
                const GhostData& pParticleGhostData,
                const BoundaryCellInfo& pParticleBoundaryInfo,
                const DoubleVec pParticleMinExtent,
                const DoubleVec pParticleMaxExtent,
                const size_t pNumParticles,
                const double pSmoothingRadius,
                const ParticleField& pXcoord,
                const ParticleField& pYcoord,
                const bool pUseGrid = false,
                const bool pSortData = false,
                const short int pLocation = CPU_INDEX);

      /**
       * @brief Returns the state information needed to perform the nebo mapped reduction
       *
       * @param location Whether to fetch the state information for CPU/GPU operation
       *
       */
      search_state GetCurrentState (short int location = CPU_INDEX) {
          return grid_.GetCurrentState(location, useGrid_);
      }

#ifndef __CUDACC__
#ifndef NDEBUG
      /**
       * @brief Returns the number of pairs found during the range search (available only for Debug build)
       */
      long num_pairs_found () const {
          return grid_.NumPairsFound();
      }
#endif
#endif

    private:

	  /**
	   * @brief Sets up the particle grid based on the parameters
	   *
	   * @param sortData	Whether to sort data or not for optimizing search on GPUs
	   * @param location	Which device to perform the operation on
	   */
      void setup_grid(bool sortData = false, short int location = CPU_INDEX);


      ParticleGrid grid_;

      bool useGrid_;
      bool sortData_;

      const ParticleField *px_, *py_, *pz_;
    };


    
  } // namespace Particle
} // namespace SpatialOps

#endif // ParticleOperators_h

