/*
 * Copyright (c) 2014 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */
#ifndef ParticleGrid_h
#define ParticleGrid_h

#include <spatialops/particles/ParticleFieldTypes.h>
#include <spatialops/structured/DoubleVec.h>
#include <spatialops/SpatialOpsConfigure.h>
#include <spatialops/structured/SpatialField.h>
#include <spatialops/structured/SpatialFieldStore.h>
#include <spatialops/structured/ExternalAllocators.h>
#ifdef SpatialOps_ENABLE_THREADS
#include <spatialops/NeboBasic.h>
#include <atomic>
#endif

#ifdef ENABLE_CUDA
#include <spatialops/particles/ParticleGridCuda.h>
#endif

#include <stdexcept>
#include <cmath>
#include <algorithm>
#include <vector>
#include <cstring>
#include <limits>


#define GRID_UNDEF 4294967295

// helper function
inline bool isPowerOfTwo(int n) { return ((n&(n-1))==0) ; }

// helper function
inline int floorPow2(int n) {

  int exp;

  std::frexp((float)n, &exp);
  return 1 << (exp - 1);

}

namespace SpatialOps{
  namespace Particle{

      /**
       * @class     ParticleGrid
       * @brief     Stores the particle information needed to perform range search operation
       * @author    Siddartha Ravichandran
       *
       */
      class ParticleGrid {
    
      public:

          /**
           * @struct ParticleIterator
           * @brief  Iterator used to index the particles found during the range search
           */
          struct ParticleIterator {

          public:
              ParticleIterator (int x,int y, int z, int curCell)
              : data_ (x,y,z), curCell_(curCell)
              {}

              IntVec& operator* () { return data_; }

              int cell () { return curCell_; }

          private:
              IntVec data_;
              int curCell_;
          };


          /**
           * @struct GridState
           * @brief  Maintains the grid information needed to perform particle range search
           *         based on the device (CPU/GPU)
           */
          struct GridState {

              typedef int* p_iterator;

              GridState (double const * px,
                         double const * py,
                         double const * pz,
                         unsigned int * pcell,
                         unsigned int * nextp,
                         unsigned int * grid,
                         unsigned int * adjGrid,
                         unsigned int * gridOffset,
                         unsigned int * gridCount,
                         unsigned int * sortedParticleIndex,
                         const double sRad,
                         const size_t nParticles,
                         const size_t gridSize,
                         const int gridSearch,
                         const int gridResx, const int gridResy, const int gridResz,
                         const bool useGrid = true) :
                             px_(px), py_(py), pz_(pz),
                             pCell_(pcell), nextP_(nextp),
                             grid_(grid), adjGrid_(adjGrid),
                             gridOffset_(gridOffset), gridCount_(gridCount),
                             sortedParticleIndex_(sortedParticleIndex),
                             sRadius_(sRad), numParticles_(nParticles),
                             gridSize_(gridSize), gridSearch_(gridSearch),
                             gridResX_(gridResx),gridResY_(gridResy),gridResZ_(gridResz),
                             useGrid_(useGrid)
              {}

              /**
               * @brief Evaluates the range search for x,y,z and returns the initial iterator and distance
               *
               * @param x               x-index for the particle range search -from LHS
               * @param y               y-index for the particle range search -from LHS
               * @param z               z-index for the particle range search -from LHS
               * @param it              initial iterator returned after evaluation
               * @param ret_distance    distance between the first pair of particles found
               */
              void eval (const int x, const int y, const int z, p_iterator it, double * ret_distance) {

                  if (useGrid_)
                    grid_eval (x, y, z, it, ret_distance);
                  else
                    seq_eval (x, y, z, it, ret_distance);
              }

              /**
               * @brief Evaluates the range search for x,y,z based on the previous iterator and returns the distance
               *
               * @param x               x-index for the particle range search -from LHS
               * @param y               y-index for the particle range search -from LHS
               * @param z               z-index for the particle range search -from LHS
               * @param prev            previous iterator used for the search
               * @param ret_distance    distance between the current pair of particles found
               */
              void next (const int x, const int y, const int z, p_iterator prev, double * ret_distance) {

                  if (useGrid_)
                      return grid_next (x,y,z,prev,ret_distance);
                  else
                      return seq_next (x,y,z,prev,ret_distance);

              }

              /**
               * @brief Evaluates the range search to find the initial pair of particles using the grid
               *
               * @param x               x-index for the particle range search -from LHS
               * @param y               y-index for the particle range search -from LHS
               * @param z               z-index for the particle range search -from LHS
               * @param it              initial iterator returned after evaluation
               * @param ret_distance    distance between the first pair of particles found
               */
              void grid_eval (const int x, const int y, const int z, p_iterator it, double * ret_distance)
              {
                DoubleVec d;
                double distance;
                unsigned int j;
                int nadj = (gridResZ_ + 1)*gridResX_ + 1;

                int gridCell;

                //Search space : how many grid cells to check around the current cell
                int searchSpace = gridSearch_ * gridSearch_ * gridSearch_;

                // if the particle at x,y,z is contained in the grid
                if (pCell_[x] != GRID_UNDEF) {

                    // search all particles in the search space that fall within the smoothing radius
                    for (int cell=0; cell < searchSpace; cell++) {
                        gridCell = (int)(pCell_[x] - nadj + adjGrid_[cell]);

                        if (gridCell < 0 || gridCell >= ((int)gridSize_))
                            continue;

                        j = grid_[ gridCell ] ;

                        while ( j != GRID_UNDEF ) {
                            d = DoubleVec(px_[j],py_[j],pz_ ? pz_[j] : 0);
                            d -= DoubleVec(px_[x],py_[x],pz_ ? pz_[x] : 0);

                            distance = d[0] * d[0] + d[1] * d[1] + d[2] * d[2];

                            if ( distance >= 0 && distance <= (sRadius_ * sRadius_) ) {

                                *ret_distance = std::sqrt(distance);

                        #ifndef __CUDACC__
                            #ifndef NDEBUG
                                //numPairsFound_++;
                            #endif
                        #endif

                                it[0] = j;
                                it[1] = cell;

                                return;
                            }

                            j = nextP_[j];
                        }
                    }
                }

                it[0] = -1;
                it[1] = -1;
              }

              /**
               * @brief Evaluates the range search for x,y,z based on the previous iterator and returns the distance using the grid
               *
               * @param x               x-index for the particle range search -from LHS
               * @param y               y-index for the particle range search -from LHS
               * @param z               z-index for the particle range search -from LHS
               * @param previous        previous iterator used for the search
               * @param ret_distance    distance between the current pair of particles found
               */
              void grid_next (const int x, const int y, const int z, p_iterator previous, double * ret_distance)
              {

                if (previous[1] == -1)
                    return;

                DoubleVec d;
                double distance;
                unsigned int j;
                int nadj = (gridResZ_ + 1)*gridResX_ + 1;

                int gridCell;

                //Search space : how many grid cells to check around the current cell
                int searchSpace = gridSearch_ * gridSearch_ * gridSearch_;

                // if the particle at x,y,z is contained in the grid
                if (pCell_[x] != GRID_UNDEF) {

                    // search all particles in the search space that fall within the smoothing radius
                    for (int cell = previous[1]; cell < searchSpace; cell++) {
                        gridCell = (int)(pCell_[x] - nadj + adjGrid_[cell]);

                        if (gridCell < 0 || gridCell >= ((int)gridSize_))
                            continue;

                        if (previous[0] != -1) {
                            j = nextP_[previous[0]];
                            previous[0] = -1;
                        } else
                            j = grid_[ gridCell ] ;

                        while ( j != GRID_UNDEF ) {
                            d = DoubleVec(px_[j],py_[j],pz_ ? pz_[j] : 0);
                            d -= DoubleVec(px_[x],py_[x],pz_ ? pz_[x] : 0);

                            distance = d[0] * d[0] + d[1] * d[1] + d[2] * d[2];

                            if ( distance >= 0 && distance <= (sRadius_ * sRadius_) ) {

                                *ret_distance = std::sqrt(distance);

                        #ifndef __CUDACC__
                            #ifndef NDEBUG
                                //numPairsFound_++;
                            #endif
                        #endif

                                previous[0] = j;
                                previous[1] = cell;           

                                return;
                            }

                            j = nextP_[j];
                        }
                    }
                }

                previous[0] = -1;
                previous[1] = -1;
            
              }

              /**
               * @brief Evaluates the range search for x,y,z and returns the initial iterator and distance without using the grid
               *
               * @param x               x-index for the particle range search -from LHS
               * @param y               y-index for the particle range search -from LHS
               * @param z               z-index for the particle range search -from LHS
               * @param it              initial iterator returned after evaluation
               * @param ret_distance    distance between the first pair of particles found
               */
              void seq_eval (const int x, const int y, const int z, p_iterator it, double * ret_distance)
              {
                DoubleVec d;
                double distance;

                for (size_t j=0; j < numParticles_; j++) {

                    d = DoubleVec(px_[j],py_[j],pz_ ? pz_[j] : 0);
                    d -= DoubleVec(px_[x],py_[x],pz_ ? pz_[x] : 0);

                    distance = d[0] * d[0] + d[1] * d[1] + d[2] * d[2];

                    if ( distance >= 0 && distance <= (sRadius_ * sRadius_) ) {

                        *ret_distance = std::sqrt(distance);

                    #ifndef __CUDACC__
                        #ifndef NDEBUG
                        //numPairsFound_++;
                        #endif
                    #endif

                        it[0] = j;
                        it[1] = j;

                        return;
                    }
                }

                it[0] = -1;
                it[1] = -1;

              }

              /**
               * @brief Evaluates the range search for x,y,z based on the previous iterator and returns the distance without using the grid
               *
               * @param x               x-index for the particle range search -from LHS
               * @param y               y-index for the particle range search -from LHS
               * @param z               z-index for the particle range search -from LHS
               * @param prev            previous iterator used for the search
               * @param ret_distance    distance between the current pair of particles found
               */
              void seq_next (const int x, const int y, const int z, p_iterator prev, double * ret_distance)
              {
                if (prev[1] == -1)
                    return;

                DoubleVec d;
                double distance;

                for (size_t j=prev[1] + 1; j < numParticles_; j++) {

                    d = DoubleVec(px_[j],py_[j],pz_ ? pz_[j] : 0);
                    d -= DoubleVec(px_[x],py_[x],pz_ ? pz_[x] : 0);

                    distance = d[0] * d[0] + d[1] * d[1] + d[2] * d[2];

                    if ( distance >= 0 && distance <= (sRadius_ * sRadius_) ) {

                        *ret_distance = std::sqrt(distance);

                    #ifndef __CUDACC__
                        #ifndef NDEBUG
                        //numPairsFound_++;
                        #endif
                    #endif

                        prev[0] = j;
                        prev[1] = j;

                        return;
                    }
                }

                prev[0] = -1;
                prev[1] = -1;

              }

#ifdef __CUDACC__

              /**
               * @brief Evaluates the range search for x,y,z and returns the initial iterator and distance
               *
               * @param x               x-index for the particle range search -from LHS
               * @param y               y-index for the particle range search -from LHS
               * @param z               z-index for the particle range search -from LHS
               * @param it              initial iterator returned after evaluation
               * @param retDistance     distance between the first pair of particles found
               */
              __device__ inline void gpu_eval (const int x, const int y, const int z, p_iterator it, double * retDistance) {
                
                if (useGrid_)
                    gpu_grid_eval (x,y,z,it,retDistance);
                else
                    gpu_seq_eval (x,y,z,it,retDistance);
              }
            
              /**
               * @brief Evaluates the range search for x,y,z based on the previous iterator and returns the distance
               *
               * @param x               x-index for the particle range search -from LHS
               * @param y               y-index for the particle range search -from LHS
               * @param z               z-index for the particle range search -from LHS
               * @param prev            previous iterator used for the search
               * @param retDistance     distance between the current pair of particles found
               */
              __device__ inline void gpu_next (const int x, const int y, const int z, p_iterator prev, double * retDistance) {

                if (useGrid_)
                    return gpu_grid_next (x,y,z,prev,retDistance);
                else
                    return gpu_seq_next (x,y,z,prev,retDistance);
              }

              /**
               * @brief Evaluates the range search to find the initial pair of particles using the grid
               *
               * @param x               x-index for the particle range search -from LHS
               * @param y               y-index for the particle range search -from LHS
               * @param z               z-index for the particle range search -from LHS
               * @param it              initial iterator returned after evaluation
               * @param retDistance     distance between the first pair of particles found
               */
              __device__ inline void gpu_grid_eval (const int x, const int y, const int z, p_iterator it, double * retDistance) {

                double distance;
                double dx,dy,dz;
        
                int nadj = (gridResZ_ + 1)* gridResX_ + 1;
    
                const int searchSpace = gridSearch_ * gridSearch_ * gridSearch_;
        
                int gridCell;

                int startNdx,endNdx;

                if (pCell_[x] != GRID_UNDEF) {

                    for (int cell=0; cell < searchSpace; cell++) {
                        gridCell = (int)(pCell_[x]  - nadj + adjGrid_[cell]);

                        if (gridCell < 0 || gridCell >= gridSize_)
                            continue;

                        if (gridCount_[gridCell] == 0)
                            continue;

                        startNdx = gridOffset_[gridCell]; 
                        endNdx = startNdx + gridCount_[gridCell];

                        for (int index = startNdx; index < endNdx; index++) {
                
                            int particleIndex = sortedParticleIndex_[index];
    
                            dx = px_[particleIndex] - px_[x];
                            dy = py_[particleIndex] - py_[x];
                            dz = pz_ ? (pz_[particleIndex] - pz_[x]) : 0;
                    
                            distance = (dx * dx + dy * dy + dz * dz);

                            if ( distance >= 0 && distance <= (sRadius_ * sRadius_) ) {

                            #ifndef NDEBUG
                                //atomicAdd ( numPairsFound, 1 );
                            #endif
                                
                                retDistance[x] = std::sqrt(distance);
 
                                it[0] = particleIndex;
                                it[1] = cell;
                                it[2] = index;

                                return;
                            }
                    
                        }
                    }
                }

                it[0] = -1;
                it[1] = -1;
                it[2] = -1;

              }

              /**
               * @brief Evaluates the range search for x,y,z based on the previous iterator and returns the distance using the grid
               *
               * @param x               x-index for the particle range search -from LHS
               * @param y               y-index for the particle range search -from LHS
               * @param z               z-index for the particle range search -from LHS
               * @param prev            previous iterator used for the search
               * @param retDistance     distance between the current pair of particles found
               */
              __device__ inline void gpu_grid_next (const int x, const int y, const int z, p_iterator prev, double * retDistance) {

                if (prev[1] == -1)
                    return; 

                double distance;
                double dx,dy,dz;

                int nadj = (gridResZ_ + 1)* gridResX_ + 1;

                const int searchSpace = gridSearch_ * gridSearch_ * gridSearch_;

                int gridCell;

                int startNdx,endNdx;

                if (pCell_[x] != GRID_UNDEF) {

                    for (int cell= prev[1]; cell < searchSpace; cell++) {
                        gridCell = (int)(pCell_[x]  - nadj + adjGrid_[cell]);

                        if (gridCell < 0 || gridCell >= gridSize_)
                            continue;

                        if (gridCount_[gridCell] == 0)
                            continue;


                        if (prev[0] != -1) {
                            startNdx = prev[2] + 1;
                            prev[0] = -1;
                        } else  
                            startNdx = gridOffset_[gridCell];

                        endNdx = gridOffset_[gridCell] + gridCount_[gridCell];

                        for (int index = startNdx; index < endNdx; index++) {

                            int particleIndex = sortedParticleIndex_[index];

                            dx = px_[particleIndex] - px_[x];
                            dy = py_[particleIndex] - py_[x];
                            dz = pz_ ? (pz_[particleIndex] - pz_[x]) : 0;

                            distance = (dx * dx + dy * dy + dz * dz);

                            if ( distance >= 0 && distance <= (sRadius_ * sRadius_) ) {

                            #ifndef NDEBUG
                                //atomicAdd ( numPairsFound, 1 );
                            #endif

                                retDistance[x] = std::sqrt(distance);
                                      
                                prev[0] = particleIndex;
                                prev[1] = cell;
                                prev[2] = index;

                                return;
                            }

                        }
                    }
                }

                prev[0] = -1;
                prev[1] = -1;
                prev[2] = -1;

              }

              /**
               * @brief Evaluates the range search for x,y,z and returns the initial iterator and distance without using the grid
               *
               * @param x               x-index for the particle range search -from LHS
               * @param y               y-index for the particle range search -from LHS
               * @param z               z-index for the particle range search -from LHS
               * @param it              initial iterator returned after evaluation
               * @param retDistance     distance between the first pair of particles found
               */
              __device__ inline void gpu_seq_eval (const int x, const int y, const int z, p_iterator it, double * retDistance) {

                double distance;
                double dx,dy,dz;


                for (int i = 0; i < numParticles_; i++)
                {
                    dx = px_[i] - px_[x];
                    dy = py_[i] - py_[x];
                    dz = pz_ ? (pz_[i] - pz_[x]) : 0;

                    distance = (dx * dx + dy * dy + dz * dz);

                    if ( distance >= 0 && distance <= (sRadius_ * sRadius_) ) {

                #ifndef NDEBUG

                        //atomicAdd ( numPairsFound, 1 );

                #endif

                        retDistance[x] = std::sqrt(distance);
                        
                        it[0] = i;
                        it[1] = i;

                        return;

                    }

                }

                it[0] = -1;
                it[1] = -1;

              }

              /**
               * @brief Evaluates the range search for x,y,z based on the previous iterator and returns the distance without using the grid
               *
               * @param x               x-index for the particle range search -from LHS
               * @param y               y-index for the particle range search -from LHS
               * @param z               z-index for the particle range search -from LHS
               * @param prev            previous iterator used for the search
               * @param retDistance     distance between the current pair of particles found
               */
              __device__ inline void gpu_seq_next (const int x, const int y, const int z, p_iterator prev, double * retDistance) {

                if (prev[1] == -1)
                    return;

                double distance;
                double dx,dy,dz;

                for (int i = prev[0] + 1; i < numParticles_; i++)
                {
                    dx = px_[i] - px_[x];
                    dy = py_[i] - py_[x];
                    dz = pz_ ? (pz_[i] - pz_[x]) : 0;

                    distance = (dx * dx + dy * dy + dz * dz);

                    if ( distance >= 0 && distance <= (sRadius_ * sRadius_) ) {

                #ifndef NDEBUG

                        //atomicAdd ( numPairsFound, 1 );

                #endif

                        retDistance[x] = std::sqrt(distance);

                        prev[0] = i;
                        prev[1] = i;
                        
                        return;

                    }

                }

                prev[0] = -1;
                prev[1] = -1;
    
              }


#endif

          private:

              double const * px_;
              double const * py_;
              double const * pz_;
              unsigned int const * const pCell_;
              unsigned int const * const nextP_;
              unsigned int const * const grid_;
              unsigned int const * const adjGrid_;
              unsigned int const * const gridOffset_;
              unsigned int const * const gridCount_;
              unsigned int const * const sortedParticleIndex_;
              const double sRadius_;
              const size_t numParticles_;
              const size_t gridSize_;
              const int gridSearch_;
              const int gridResX_, gridResY_, gridResZ_;
              const bool useGrid_;

          };

          /**
           * @brief Constructs the ParticleGrid instance
           *
           * @param px                  x-coordinates of the particles
           * @param py                  y-coordinates of the particles
           * @param pz                  z-coordinates of the particles
           * @param pGridMax            Upper bound for particle positions
           * @param pGridMin            Lower bound for particle positions
           * @param pSmoothingRadius    Smoothing radius for the search
           * @param pMemoryWindow       Memory window of the particle fields
           * @param pGhostData          Ghost data of the particle field
           * @param pBoundaryInfo       Boundary info of the particle field
           * @param location            Device where to maintain the information
           */
          ParticleGrid(size_t pNumParticles,
                       const ParticleField * const px, const ParticleField * const py, const ParticleField * const pz,
                       const DoubleVec pGridMax, const DoubleVec pGridMin, double pSmoothingRadius,
                       const MemoryWindow& pMemoryWindow, const GhostData& pGhostData, const BoundaryCellInfo& pBoundaryInfo,
                       const short int location = CPU_INDEX) :
              numParticles_(pNumParticles),
              smoothingRadius_(pSmoothingRadius),
              xCoord_(px), yCoord_(py), zCoord_(pz),
              gridMin_(pGridMin),
              gridRes_ ( std::ceil ( (pGridMax - pGridMin)[0] / pSmoothingRadius ),
                         std::ceil ( (pGridMax - pGridMin)[1] / pSmoothingRadius ),
                         std::ceil ( (pGridMax - pGridMin)[2] / pSmoothingRadius ) ),
              ghostData_(0),
              boundaryInfo_(BoundaryCellInfo::build<ParticleGridField>()),
              memoryWindow_(IntVec(std::ceil ( (pGridMax - pGridMin)[0] / pSmoothingRadius ),
                                   std::ceil ( (pGridMax - pGridMin)[1] / pSmoothingRadius ),
                                   std::ceil ( (pGridMax - pGridMin)[2] / pSmoothingRadius ))),
              particleCell_(pMemoryWindow, pBoundaryInfo, pGhostData, NULL, InternalStorage, location),
              nextParticle_(pMemoryWindow, pBoundaryInfo, pGhostData, NULL, InternalStorage, location),
             #ifdef __CUDACC__
              sortedParticleCell_(pMemoryWindow, pBoundaryInfo, pGhostData, NULL, InternalStorage, location),
              sortedNextParticle_(pMemoryWindow, pBoundaryInfo, pGhostData, NULL, InternalStorage, location),
              sortedParticleIndex_(pMemoryWindow, pBoundaryInfo, pGhostData, NULL, InternalStorage, location),
             #else
              #ifndef NDEBUG
              numPairsFound_(0),
              #endif
             #endif
              storeNeighbors_(false) {

        #ifdef __CUDACC__
            #ifndef NDEBUG
               numPairsFound_ = SpatialFieldStore::get_from_window<ParticleGridField>(get_window_with_ghost(IntVec(1,1,1), ghostData_, BoundaryCellInfo::build<ParticleGridField>()), boundaryInfo_, ghostData_,location);

               (*numPairsFound_) <<= 0;
              #endif
        #endif
          }

        #ifndef __CUDACC__
            #ifndef NDEBUG

              /**
               * @brief Returns the number of pairs found at the end of the range search
               */
              long NumPairsFound () const{
                  return numPairsFound_;
              }

            #endif
        #endif

          /**
           * @brief Sets up the particle grid, the internal memory needed to maintain the state
           *        information used while searching
           */
          void Setup() {

            # ifdef __CUDACC__
              const short int location = GPU_INDEX;
            # else
              const short int location = CPU_INDEX;
            # endif

             //currently cell size = h, therefore we restrict search to a 3x3x3 space around
             //a given particle
             gridSearch_ =  3;

             gridAdj_ = SpatialFieldStore::get_from_window<ParticleGridField>(get_window_with_ghost(IntVec(gridSearch_, gridSearch_, gridSearch_),
                                                                                                    ghostData_,
                                                                                                    BoundaryCellInfo::build<ParticleGridField>()),
                                                                              boundaryInfo_,
                                                                              ghostData_,
                                                                              CPU_INDEX);

             gridSize_ = gridRes_[0] * gridRes_[1] * gridRes_[2];

             for (int k=0; k < gridSearch_; k++ )
                 for (int j=0; j < gridSearch_; j++ )
                     for (int i=0; i < gridSearch_; i++ )
                         (*gridAdj_)(i,j,k) = ( j*gridRes_[2] + k )*gridRes_[1] +  i ;

             # ifdef __CUDACC__
               gridCount_ = SpatialFieldStore::get_from_window<ParticleGridField>(memoryWindow_,boundaryInfo_,ghostData_,location);
               gridOffset_ = SpatialFieldStore::get_from_window<ParticleGridField>(get_window_with_ghost(IntVec(gridSize_,1,1), ghostData_, BoundaryCellInfo::build<ParticleGridField>()), boundaryInfo_, ghostData_,location);

               sortedParticlePosition_ = SpatialFieldStore::get_from_window<ParticleField>(get_window_with_ghost(IntVec(numParticles_ * 3,1,1), ghostData_, BoundaryCellInfo::build<ParticleField>()), boundaryInfo_, ghostData_,location);

               (*gridCount_) <<= 0;
               (*gridOffset_) <<= 0;
               
               gridAdj_->set_device_as_active(GPU_INDEX);

               unsigned int blockSize = BLOCK_SIZE; // max size of the thread blocks
               unsigned int numElts = gridSize_;
    
               do {       
                 int numBlocks = std::max(1, (int)std::ceil((float)numElts / (2.f * blockSize)));
                 if (numBlocks > 1) 
                   prefixSums_.push_back(SpatialFieldStore::get_from_window<ParticleGridField>(get_window_with_ghost(IntVec(numBlocks,1,1), ghostData_, BoundaryCellInfo::build<ParticleGridField>()), boundaryInfo_, ghostData_,location));
                 numElts = numBlocks;
               } while (numElts > 1);
             # else

                grid_ = SpatialFieldStore::get_from_window<ParticleGridField>(memoryWindow_,boundaryInfo_,ghostData_,location);
               
                (*grid_) <<= std::numeric_limits<unsigned int>::max();
             #endif

          }

          /**
           * @brief Returns a grid state instance using the current state of the grid.
           *
           * @param location    Which device to use for performing the range search
           * @param useGrid     Whether to use the particle grid to optimize searchs
           */
          GridState GetCurrentState(short int location = CPU_INDEX, bool useGrid = true) {

              return GridState (xCoord_->field_values(location),
                                yCoord_->field_values(location),
                                zCoord_? zCoord_->field_values(location) : NULL,
                                particleCell_.field_values(location),
                                nextParticle_.field_values(location),
                            #ifdef __CUDACC__
                                NULL,
                                gridAdj_->field_values(location),
                                gridOffset_->field_values(location),
                                gridCount_->field_values(location),
                                sortedParticleIndex_.field_values(location),
                            #else
                                grid_->field_values(location),
                                gridAdj_->field_values(location),
                                NULL,
                                NULL,
                                NULL,
                            #endif
                                smoothingRadius_,
                                numParticles_,
                                gridSize_,
                                gridSearch_,
                                gridRes_[0],gridRes_[1],gridRes_[2],
                                useGrid);

          }

          /**
           * @brief Insert particles into the particle grid, updating the internal memory,
           *        which represents its state
           */
          void InsertParticles() {

            int gs;

            IntVec gc;
            ParticleGridField::iterator pgrid =     particleCell_.begin();
            ParticleGridField::iterator pnext =     nextParticle_.begin();

            for ( size_t n=0; n < numParticles_; n++ ) {
                gs = GetGridCell ( n, gc );

                if ( gc[0] >= 0 && gc[0] <= gridRes_[0] && gc[1] >= 0 && gc[1] <= gridRes_[1] && gc[2] >= 0 && gc[2] <= gridRes_[2] ) {
                    // put current particle at head of grid cell, pointing to next in list (previous head of cell)
                    *pgrid = gs;
                    *pnext = (*grid_)[gs];

                    (*grid_)[gs] = n;

                } else {

                //particle out of grid bounds
                #ifndef NDEBUG
                    std::cout << "Particle cell location: " << gc << ":" << gs << std::endl;
                    std::cout << "Particle out of bounds" << std::endl;
                #endif

                }
                pgrid++;
                pnext++;
            }

          }


          /**
           * @brief     Returns the grid cell, which the particle p falls into
           *
           * @param p   Particle index
           * @param gc  Triply indexed grid cell index, where the particle falls into
           */
          int GetGridCell ( int p, IntVec & gc )
          {
             gc[0] = (int)( (*(xCoord_->begin() + p) - gridMin_[0]) / smoothingRadius_);            // Cell in which particle is located
             gc[1] = (int)( (*(yCoord_->begin() + p) - gridMin_[1]) / smoothingRadius_);
             gc[2] = zCoord_ ? ((int)( (*(zCoord_->begin() + p) - gridMin_[2]) / smoothingRadius_)) : 0;

             return (int)( (gc[1]*gridRes_[2] + gc[2])*gridRes_[0] + gc[0]);
          }


#ifdef __CUDACC__

        /**
         * @brief Insert particles into the particle grid, updating the internal memory,
         *      which represents its state
         */
        __host__ inline void _insert_particles_cuda()
        {
        #ifndef NDEBUG
            cudaError err;
        
            if( cudaSuccess != (err = cudaDeviceSynchronize()) ){
                std::ostringstream msg;
                msg << "error - before particle insertion:\n\t"
                    << cudaGetErrorString(err)
                    << "\n\t" << __FILE__ << " : " << __LINE__;
                throw(std::runtime_error(msg.str()));;
            }

        #endif
            

            // Calcuate the number of Blocks and Threads
            const size_t nblock = numParticles_ / INNER_BLOCK_ONE_DIM + 1;
            const size_t nthread =  (numParticles_ > INNER_BLOCK_ONE_DIM) ? INNER_BLOCK_ONE_DIM : numParticles_;

            _insert_particles<<<nblock,nthread>>>( xCoord_ ? xCoord_->field_values(GPU_INDEX) : NULL,
                                   yCoord_ ? yCoord_->field_values(GPU_INDEX) : NULL,
                                   zCoord_ ? zCoord_->field_values(GPU_INDEX) : NULL,
                                   particleCell_.field_values(GPU_INDEX),
                                   nextParticle_.field_values(GPU_INDEX),
                                   gridCount_->field_values(GPU_INDEX),
                                   smoothingRadius_,
                                   numParticles_,
                                   gridMin_[0], gridMin_[1], gridMin_[2],
                                   gridRes_[0], gridRes_[1], gridRes_[2]);

            
        #ifndef NDEBUG

            if( cudaSuccess != (err = cudaDeviceSynchronize()) ){
                std::ostringstream msg;
                msg << "error - after particle insertion:\n\t"
                    << cudaGetErrorString(err)
                    << "\n\t" << __FILE__ << " : " << __LINE__;
                throw(std::runtime_error(msg.str()));;
            }

        #endif

        }

        /**
         * @brief Perform prefix sum of the particles, initial step for counting sort
         */
        __host__ void _prescan_array_cuda (unsigned int *outArray, const unsigned int *inArray, int numElements, int level)
        {
            const unsigned int blockSize = BLOCK_SIZE; // max size of the thread blocks
            const unsigned int numBlocks = std::max(1, (int)std::ceil((float)numElements / (2.f * blockSize)));
            unsigned int numThreads;

            if (numBlocks > 1)
                numThreads = blockSize;
            else if (isPowerOfTwo(numElements))
                numThreads = numElements / 2;
            else {
                numThreads = floorPow2(numElements);
            }

            const unsigned int numEltsPerBlock = numThreads * 2;

            // if this is a non-power-of-2 array, the last block will be non-full
            // compute the smallest power of 2 able to compute its scan.
            const unsigned int numEltsLastBlock = numElements - (numBlocks-1) * numEltsPerBlock;
            unsigned int numThreadsLastBlock = std::max(1,(int) numEltsLastBlock / 2);
            unsigned int np2LastBlock = 0;
            unsigned int sharedMemLastBlock = 0;

            if (numEltsLastBlock != numEltsPerBlock) {
                np2LastBlock = 1;
                if(!isPowerOfTwo(numEltsLastBlock)) numThreadsLastBlock = floorPow2(numEltsLastBlock);
                const unsigned int extraSpace = (2 * numThreadsLastBlock) / NUM_BANKS;
                sharedMemLastBlock = sizeof(float) * (2 * numThreadsLastBlock + extraSpace);
            }

            // padding space is used to avoid shared memory bank conflicts
            const unsigned int extraSpace = numEltsPerBlock / NUM_BANKS;
            const unsigned int sharedMemSize = sizeof(float) * (numEltsPerBlock + extraSpace);

            // setup execution parameters
            // if NP2, we process the last block separately
            const dim3  grid(std::max(1,(int) (numBlocks - np2LastBlock)), 1, 1);
            const dim3  threads(numThreads, 1, 1);

            // execute the scan
            if (numBlocks > 1) {
                prescanInt <true, false><<< grid, threads, sharedMemSize >>> (outArray, inArray,  prefixSums_[level]->field_values(GPU_INDEX), numThreads * 2, 0, 0);

                if(np2LastBlock) {
                  prescanInt <true, true><<< 1, numThreadsLastBlock, sharedMemLastBlock >>> (outArray, inArray, prefixSums_[level]->field_values(GPU_INDEX), numEltsLastBlock, numBlocks - 1, numElements - numEltsLastBlock);
                }

                // After scanning all the sub-blocks, we are mostly done.  But now we
                // need to take all of the last values of the sub-blocks and scan those.
                // This will give us a new value that must be added to each block to
                // get the final results.
                // recursive (CPU) call
                _prescan_array_cuda (prefixSums_[level]->field_values(GPU_INDEX), prefixSums_[level]->field_values(GPU_INDEX), numBlocks, level+1);

                uniformAddInt <<< grid, threads >>> (outArray, prefixSums_[level]->field_values(GPU_INDEX), numElements - numEltsLastBlock, 0, 0);
                if (np2LastBlock) {
                  uniformAddInt <<< 1, numThreadsLastBlock >>>(outArray, prefixSums_[level]->field_values(GPU_INDEX), numEltsLastBlock, numBlocks - 1, numElements - numEltsLastBlock);
                }

            } else if (isPowerOfTwo(numElements)) {

                prescanInt <false, false><<< grid, threads, sharedMemSize >>> (outArray, inArray, 0, numThreads * 2, 0, 0);

            } else {

                prescanInt <false, true><<< grid, threads, sharedMemSize >>> (outArray, inArray, 0, numElements, 0, 0);

            }
        }       

        /**
         * @brief perform prefix sum: initial step prior to sorting particles based on position
         */
        __host__ inline void _prefix_sum_cuda()
        {
        #ifndef NDEBUG

            cudaError err;

            if( cudaSuccess != (err = cudaDeviceSynchronize()) ){
                std::ostringstream msg;
                msg << "error - during prefix sum:\n\t"
                    << cudaGetErrorString(err)
                    << "\n\t" << __FILE__ << " : " << __LINE__;
                throw(std::runtime_error(msg.str()));;
            }

        #endif

        _prescan_array_cuda(gridOffset_->field_values(GPU_INDEX), gridCount_->field_values(GPU_INDEX), gridSize_, 0);

        #ifndef NDEBUG

            if( cudaSuccess != (err = cudaDeviceSynchronize()) ){
                std::ostringstream msg;
                msg << "error - after prefix sum:\n\t"
                    << cudaGetErrorString(err)
                    << "\n\t" << __FILE__ << " : " << __LINE__;
                throw(std::runtime_error(msg.str()));;
            }

        #endif

        }

        /**
         * @brief Perform counting sort of particles and store an indexing of the sorted particles
         */
        __host__ inline void _counting_sort_index_cuda()
        {
        #ifndef NDEBUG

            cudaError err;

            if( cudaSuccess != (err = cudaDeviceSynchronize()) ){
                std::ostringstream msg;
                msg << "error - during counting index sort:\n\t"
                    << cudaGetErrorString(err)
                    << "\n\t" << __FILE__ << " : " << __LINE__;
                throw(std::runtime_error(msg.str()));;
            }

        #endif


            // Calcuate the number of Blocks and Threads
            const size_t nblock = numParticles_ / INNER_BLOCK_ONE_DIM + 1;
            const size_t nthread =  (numParticles_ > INNER_BLOCK_ONE_DIM) ? INNER_BLOCK_ONE_DIM : numParticles_;

            _counting_sort_index<<<nblock,nthread>>> ( particleCell_.field_values(GPU_INDEX),
                                                       nextParticle_.field_values(GPU_INDEX),
                                                       gridOffset_->field_values(GPU_INDEX),
                                                       sortedParticleIndex_.field_values(GPU_INDEX),
                                                       numParticles_ );

        #ifndef NDEBUG

            if( cudaSuccess != (err = cudaDeviceSynchronize()) ){
               std::ostringstream msg;
                msg << "error - during creation of sorted index array:\n\t"
                    << cudaGetErrorString(err)
                    << "\n\t" << __FILE__ << " : " << __LINE__;
                throw(std::runtime_error(msg.str()));;
            }

        #endif

        }
        
        /**
         * @brief Sort (deep sort) particle data based on the counting sort indexing
         */
        __host__ inline void _data_sort_cuda()
        {
        #ifndef NDEBUG
            cudaError err;

            if( cudaSuccess != (err = cudaDeviceSynchronize()) ){
                std::ostringstream msg;
                msg << "error - during data sort:\n\t"
                    << cudaGetErrorString(err)
                    << "\n\t" << __FILE__ << " : " << __LINE__;
                throw(std::runtime_error(msg.str()));;
            }

        #endif

            const size_t nblock = numParticles_ / INNER_BLOCK_ONE_DIM + 1;
            const size_t nthread =  (numParticles_ > INNER_BLOCK_ONE_DIM) ? INNER_BLOCK_ONE_DIM : numParticles_;

            _data_sort<<<nblock,nthread>>> ( xCoord_ ? xCoord_->field_values(GPU_INDEX) : NULL,
                                             yCoord_ ? yCoord_->field_values(GPU_INDEX) : NULL,
                                             zCoord_ ? zCoord_->field_values(GPU_INDEX) : NULL,
                                             sortedParticlePosition_->field_values(GPU_INDEX),         
                                             particleCell_.field_values(GPU_INDEX),
                                             nextParticle_.field_values(GPU_INDEX),
                                             gridOffset_->field_values(GPU_INDEX),
                                             sortedParticleCell_.field_values(GPU_INDEX),
                                             sortedNextParticle_.field_values(GPU_INDEX),
                                             sortedParticleIndex_.field_values(GPU_INDEX),
                                             numParticles_ );

        #ifndef NDEBUG

            if( cudaSuccess != (err = cudaDeviceSynchronize()) ){
                std::ostringstream msg;
                msg << "error - after data sort:\n\t"
                    << cudaGetErrorString(err)
                    << "\n\t" << __FILE__ << " : " << __LINE__;
                throw(std::runtime_error(msg.str()));;
            }

        #endif

        }



#endif

      private:

        const size_t numParticles_;
        const double smoothingRadius_;
        
        const ParticleField * xCoord_, * yCoord_, * zCoord_;

        const DoubleVec gridMin_;
        const IntVec gridRes_;

        const GhostData ghostData_;
        const BoundaryCellInfo boundaryInfo_;
        const MemoryWindow memoryWindow_;

        ParticleGridField particleCell_, nextParticle_;

#ifdef __CUDACC__

        ParticleGridField sortedParticleCell_, sortedNextParticle_, sortedParticleIndex_;

    #ifndef NDEBUG
        SpatFldPtr<ParticleGridField> numPairsFound_;
    #endif

        std::vector< SpatFldPtr<ParticleGridField> > prefixSums_;
        SpatFldPtr<ParticleGridField> gridOffset_;
        SpatFldPtr<ParticleGridField> gridCount_;
        SpatFldPtr<ParticleField> sortedParticlePosition_;

#elif defined SpatialOps_ENABLE_THREADS
    #ifndef NDEBUG
        std::atomic<long> numPairsFound_;       
    #endif
#else
    #ifndef NDEBUG
        long numPairsFound_;
    #endif
#endif
        int gridSearch_;

        SpatFldPtr<ParticleGridField> gridAdj_;
        SpatFldPtr<ParticleGridField> grid_;

        const bool storeNeighbors_;

        size_t gridSize_;



      };

  }
}

#endif //ParticleGrid_h


