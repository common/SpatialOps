#include <spatialops/particles/ParticleOperatorsCuda.h>
#include <spatialops/particles/ParticlesOperatorHelper.h>
#include <spatialops/structured/MemoryTypes.h>
#include <spatialops/structured/IntVec.h>

#include <math_functions.h>

#include <cuda_runtime.h>
#include <cuda.h>
#include <cuda_runtime_api.h>

#include <iostream>
#include <sstream>
#include <stdexcept>

//==================================================================

namespace SpatialOps{
  namespace Particle{
    
    #define Index3D_Mem(_nx,_ny,_i,_j,_k) ((_i)+_nx*((_j)+_ny*(_k)))

    //------------------------------------------------------------------

    // Overloading atomic_add for doubles
    inline __device__ double atomic_add(double* address, double val) {
      unsigned long long int* address_as_ull = (unsigned long long int*)address;
      unsigned long long int old = *address_as_ull, assumed;
      do {
        assumed = old;
        old = atomicCAS(address_as_ull, assumed, __double_as_longlong(val + __longlong_as_double(assumed)));
        // Note: uses integer comparison to avoid hang in case of NaN (since NaN != NaN)
      } while (assumed != old);
      return __longlong_as_double(old);
    }
    
    //==================================================================
    
    __global__ void _ppc_apply_to_field_ni( const double*  px,
                                            const double*  py,
                                            const double*  pz,
                                            double* dest,
                                            const double dx,
                                            const double dy,
                                            const double dz,
                                            const double xloface,
                                            const double yloface,
                                            const double zloface,
                                            const size_t nparticle,
                                            const int nmax1,
                                            const int nmax2,
                                            const int nmax3 )
    {
      const int iprt = (blockIdx.x * blockDim.x) + threadIdx.x;
      
      if( iprt>=nparticle ) return;
      
      const int i = px ? ( px[iprt] - xloface ) / dx : 0;
      const int j = py ? ( py[iprt] - yloface ) / dy : 0;
      const int k = pz ? ( pz[iprt] - zloface ) / dz : 0;
      atomic_add(&dest[Index3D_Mem(nmax1, nmax2, i, j, k)], 1.0);
    }
    
    //------------------------------------------------------------------
    
    __global__ void _ppc_apply_to_field_in( const double* px,
                                            const double* py,
                                            const double* pz,
                                            const double* psize,
                                            double* dest,
                                            const double dx,
                                            const double dy,
                                            const double dz,
                                            const double xloface,
                                            const double yloface,
                                            const double zloface,
                                            const size_t nparticle,
                                            const int nmax1,
                                            const int nmax2,
                                            const int nmax3 )
    {
      const int iprt = (blockIdx.x * blockDim.x) + threadIdx.x;
      
      if( iprt>=nparticle ) return;
      
      const double rp = psize[iprt] * 0.5;
      
      // Identify the location of the particle boundary (assuming that it is a cube)
      const double pxlo = px ? px[iprt] - rp : 0;
      const double pylo = py ? py[iprt] - rp : 0;
      const double pzlo = pz ? pz[iprt] - rp : 0;
      
      const double pxhi = px ? pxlo + psize[iprt] : 0;
      const double pyhi = py ? pylo + psize[iprt] : 0;
      const double pzhi = pz ? pzlo + psize[iprt] : 0;
      
      const int ixlo = ( pxlo - xloface ) / dx;
      const int iylo = ( pylo - yloface ) / dy;
      const int izlo = ( pzlo - zloface ) / dz;
      
      // hi indices are 1 past the end
      const int ixhi = px ? fmin( (double)nmax1, ( pxhi - xloface ) / dx + 1 ) : ixlo+1;
      const int iyhi = py ? fmin( (double)nmax2, ( pyhi - yloface ) / dy + 1 ) : iylo+1;
      const int izhi = pz ? fmin( (double)nmax3, ( pzhi - zloface ) / dz + 1 ) : izlo+1;
      
      const double pvol = (px ? 2*rp : 1) * (py ? 2*rp : 1) * (pz ? 2*rp : 1);
      
      for( int k=izlo; k<izhi; ++k ){
        const double zcm = zloface + k*dz;
        const double zcp = zcm + dz;
        // determine the z bounding box for the particle in this cell
        const double zcont = pz ? fmin(zcp,pzhi) - fmax(zcm,pzlo) : 1;
        for( int j=iylo; j<iyhi; ++j ){
          const double ycm = yloface + j*dy;
          const double ycp = ycm + dy;
          // determine the y bounding box for the particle in this cell
          const double ycont = py ? fmin(ycp,pyhi) - fmax(ycm,pylo) : 1;
          for( int i=ixlo; i<ixhi; ++i ){
            const double xcm = xloface + i*dx;
            const double xcp = xcm + dx;
            const double xcont = px ? fmin(xcp,pxhi) - fmax(xcm,pxlo) : 1;
            // contribution is the fraction of the particle volume in this cell.
            const double contribution = xcont*ycont*zcont / pvol;
            atomic_add(&dest[Index3D_Mem(nmax1, nmax2, i, j, k)], contribution);
          }
        }
      }      
    }
    
    
    //------------------------------------------------------------------
    
    __global__ void _p2c_apply_to_field_ni( const double*  px,
                                            const double*  py,
                                            const double*  pz,
                                            const double*  src,
                                            double* dest,
                                            const double dx,
                                            const double dy,
                                            const double dz,
                                            const double xloface,
                                            const double yloface,
                                            const double zloface,
                                            const size_t nparticle,
                                            const int nmax1,
                                            const int nmax2,
                                            const int nmax3 )
    {
      const int iprt = (blockIdx.x * blockDim.x) + threadIdx.x;

      if( iprt>=nparticle ) return;
    
      const int i = px ? ( px[iprt] - xloface ) / dx : 0;
      const int j = py ? ( py[iprt] - yloface ) / dy : 0;
      const int k = pz ? ( pz[iprt] - zloface ) / dz : 0;
      atomic_add(&dest[Index3D_Mem(nmax1, nmax2, i, j, k)], src[iprt]);
    }
    
    //------------------------------------------------------------------
    
    __global__ void _p2c_apply_to_field_in( const double* px,
                                            const double* py,
                                            const double* pz,
                                            const double* psize,
                                            const double* src,
                                            double* dest,
                                            const double dx,
                                            const double dy,
                                            const double dz,
                                            const double xloface,
                                            const double yloface,
                                            const double zloface,
                                            const size_t nparticle,
                                            const int nmax1,
                                            const int nmax2,
                                            const int nmax3 )
    {
      const int iprt = (blockIdx.x * blockDim.x) + threadIdx.x;
      
      if( iprt>=nparticle ) return;
      
      const double rp = psize[iprt] * 0.5;
      
      // Identify the location of the particle boundary (assuming that it is a cube)
      const double pxlo = px ? px[iprt] - rp : 0;
      const double pylo = py ? py[iprt] - rp : 0;
      const double pzlo = pz ? pz[iprt] - rp : 0;
      
      const double pxhi = px ? pxlo + psize[iprt] : 0;
      const double pyhi = py ? pylo + psize[iprt] : 0;
      const double pzhi = pz ? pzlo + psize[iprt] : 0;
      
      const int ixlo = ( pxlo - xloface ) / dx;
      const int iylo = ( pylo - yloface ) / dy;
      const int izlo = ( pzlo - zloface ) / dz;
      
      // hi indices are 1 past the end
      const int ixhi = px ? fmin( (double)nmax1, ( pxhi - xloface ) / dx + 1 ) : ixlo+1;
      const int iyhi = py ? fmin( (double)nmax2, ( pyhi - yloface ) / dy + 1 ) : iylo+1;
      const int izhi = pz ? fmin( (double)nmax3, ( pzhi - zloface ) / dz + 1 ) : izlo+1;
      
      const double pvol = (px ? 2*rp : 1) * (py ? 2*rp : 1) * (pz ? 2*rp : 1);
      
      for( int k=izlo; k<izhi; ++k ){
        const double zcm = zloface + k*dz;
        const double zcp = zcm + dz;
        // determine the z bounding box for the particle in this cell
        const double zcont = pz ? fmin(zcp,pzhi) - fmax(zcm,pzlo) : 1;
        for( int j=iylo; j<iyhi; ++j ){
          const double ycm = yloface + j*dy;
          const double ycp = ycm + dy;
          // determine the y bounding box for the particle in this cell
          const double ycont = py ? fmin(ycp,pyhi) - fmax(ycm,pylo) : 1;
          for( int i=ixlo; i<ixhi; ++i ){
            const double xcm = xloface + i*dx;
            const double xcp = xcm + dx;
            const double xcont = px ? fmin(xcp,pxhi) - fmax(xcm,pxlo) : 1;
            // contribution is the fraction of the particle volume in this cell.
            const double contribution = xcont*ycont*zcont / pvol;
            atomic_add(&dest[Index3D_Mem(nmax1, nmax2, i, j, k)], src[iprt] * contribution);
          }
        }
      }
    
    }
    
    //------------------------------------------------------------------
    
    __global__ void _c2p_apply_to_field_ni( const double*  px,
                                            const double*  py,
                                            const double*  pz,
                                            const double*  src,
                                            double* dest,
                                            const double dx,
                                            const double dy,
                                            const double dz,
                                            const double xlo,
                                            const double ylo,
                                            const double zlo,
                                            const size_t nparticle,
                                            const int nmax1,
                                            const int nmax2,
                                            const int nmax3 )
    {
      const int iprt = (blockIdx.x * blockDim.x) + threadIdx.x;
      
      if( iprt>=nparticle ) return;
      
      const int i = px ? ( px[iprt] - xlo ) / dx : 0;
      const int j = py ? ( py[iprt] - ylo ) / dy : 0;
      const int k = pz ? ( pz[iprt] - zlo ) / dz : 0;
      dest[iprt] += src[Index3D_Mem(nmax1, nmax2, i, j, k)];
    }

    //------------------------------------------------------------------
    
    __global__ void _c2p_apply_to_field_in( const double* px,
                                            const double* py,
                                            const double* pz,
                                            const double* psize,
                                            const double* src,
                                            double* dest,
                                            const double dx,
                                            const double dy,
                                            const double dz,
                                            const double xloface,
                                            const double yloface,
                                            const double zloface,
                                            const size_t nparticle,
                                            const int nmax1,
                                            const int nmax2,
                                            const int nmax3 )
    {
      const int iprt = (blockIdx.x * blockDim.x) + threadIdx.x;
      
      if( iprt>=nparticle ) return;

      const double rp = psize[iprt] * 0.5;
      
      // Identify the location of the particle boundary (assuming that it is a cube)
      const double pxlo = px ? px[iprt] - rp : 0;
      const double pylo = py ? py[iprt] - rp : 0;
      const double pzlo = pz ? pz[iprt] - rp : 0;
      
      const double pxhi = px ? pxlo + psize[iprt] : 0;
      const double pyhi = py ? pylo + psize[iprt] : 0;
      const double pzhi = pz ? pzlo + psize[iprt] : 0;
      
      const int ixlo = ( pxlo - xloface ) / dx;
      const int iylo = ( pylo - yloface ) / dy;
      const int izlo = ( pzlo - zloface ) / dz;
      
      // hi indices are 1 past the end
      const int ixhi = px ? fmin( (double)nmax1, ( pxhi - xloface ) / dx + 1 ) : ixlo+1;
      const int iyhi = py ? fmin( (double)nmax2, ( pyhi - yloface ) / dy + 1 ) : iylo+1;
      const int izhi = pz ? fmin( (double)nmax3, ( pzhi - zloface ) / dz + 1 ) : izlo+1;
      
      const double pvol = (px ? 2*rp : 1) * (py ? 2*rp : 1) * (pz ? 2*rp : 1);
      
      for( int k=izlo; k<izhi; ++k ){
        const double zcm = zloface + k*dz;
        const double zcp = zcm + dz;
        // determine the z bounding box for the particle in this cell
        const double zcont = pz ? fmin(zcp,pzhi) - fmax(zcm,pzlo) : 1;
        for( int j=iylo; j<iyhi; ++j ){
          const double ycm = yloface + j*dy;
          const double ycp = ycm + dy;
          // determine the y bounding box for the particle in this cell
          const double ycont = py ? fmin(ycp,pyhi) - fmax(ycm,pylo) : 1;
          for( int i=ixlo; i<ixhi; ++i ){
            const double xcm = xloface + i*dx;
            const double xcp = xcm + dx;
            const double xcont = px ? fmin(xcp,pxhi) - fmax(xcm,pxlo) : 1;
            // contribution is the fraction of the particle volume in this cell.
            const double contribution = xcont*ycont*zcont / pvol;

            atomic_add(&dest[iprt], src[Index3D_Mem(nmax1, nmax2, i, j, k)] * contribution);
          }
        }
      }
    }
    
  }
}

