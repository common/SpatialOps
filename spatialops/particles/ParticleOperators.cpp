/*
 * Copyright (c) 2014-2021 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#ifndef ParticleOperatorsImplementation_h
#define ParticleOperatorsImplementation_h

#include <spatialops/particles/ParticleOperators.h>
#include <spatialops/particles/ParticleGrid.h>
#ifdef SpatialOps_ENABLE_CUDA
#include <spatialops/particles/ParticleOperatorsCuda.h>
#endif // SpatialOps_ENABLE_CUDA

#include <spatialops/util/TimeLogger.h>

namespace SpatialOps{
  namespace Particle{

    template< typename CellField >
    ParticlesPerCell<CellField>::
    ParticlesPerCell( const double dx, const double xlo,
                      const double dy, const double ylo,
                      const double dz, const double zlo,
                      const InterpOptions method)
    : dx_ ( dx<=0 ? 1.0 : dx ),
      dy_ ( dy<=0 ? 1.0 : dy ),
      dz_ ( dz<=0 ? 1.0 : dz ),
      xlo_( dx<=0 ? 0.0 : xlo ),
      ylo_( dy<=0 ? 0.0 : ylo ),
      zlo_( dz<=0 ? 0.0 : zlo ),
      method_( method )
    {
      px_    = NULL;
      py_    = NULL;
      pz_    = NULL;
      psize_ = NULL;
    }
    
    //------------------------------------------------------------------
    
    template< typename CellField >
    void
    ParticlesPerCell<CellField>::
    set_coordinate_information( const ParticleField *pxcoord,
                                const ParticleField *pycoord,
                                const ParticleField *pzcoord,
                                const ParticleField *psize )
    {
      px_    = pxcoord;
      py_    = pycoord;
      pz_    = pzcoord;
      psize_ = psize  ;
    }
    
    //------------------------------------------------------------------

    template< typename CellField >
    void
    ParticlesPerCell<CellField>::
    apply_to_field( DestFieldType& dest ) const
    {
      if (IS_GPU_INDEX(dest.active_device_index())) {
#       ifdef __CUDACC__
        _ppc_apply_to_field<CellField, ParticleField>(px_, py_, pz_, psize_,
                                                      dest,
                                                      dx_, dy_, dz_,
                                                      xlo_, ylo_, zlo_,
                                                      method_);
        return;
#       endif
      } // IS_GPU_INDEX if
      else {
        
        switch( method_ ){
            
          case NO_INTERPOLATE:{
            
            const IntVec& ngm  = dest.get_ghost_data().get_minus();
            
            const double xloface = xlo_ - dx_*ngm[0] - dx_/2;
            const double yloface = ylo_ - dy_*ngm[1] - dy_/2;
            const double zloface = zlo_ - dz_*ngm[2] - dz_/2;
            
            dest <<= 0.0;  // set to zero and accumulate contributions from each particle below.
            
            ParticleField::const_iterator ipx  = px_ ? px_->begin() : psize_->begin();
            ParticleField::const_iterator ipy  = py_ ? py_->begin() : psize_->begin();
            ParticleField::const_iterator ipz  = pz_ ? pz_->begin() : psize_->begin();
            ParticleField::const_iterator ipsize = psize_->begin();
            const ParticleField::const_iterator ipsizeend = psize_->end();
            for( ; ipsize != ipsizeend; ++ipx, ++ipy, ++ipz, ++ipsize ){
              const int i = ( *ipx - xloface ) / dx_;
              const int j = ( *ipy - yloface ) / dy_;
              const int k = ( *ipz - zloface ) / dz_;
              dest(i,j,k) += 1;
            } // particle loop
            
            break;
          }
            
          case INTERPOLATE:{
            assert( psize_ != NULL );
            
            const IntVec& ngm = dest.get_ghost_data().get_minus();
            const IntVec& nmax = dest.window_with_ghost().extent();
            
            const double xloface = xlo_ - dx_*ngm[0] - dx_/2;
            const double yloface = ylo_ - dy_*ngm[1] - dy_/2;
            const double zloface = zlo_ - dz_*ngm[2] - dz_/2;
            
            dest <<= 0.0;  // set to zero and accumulate contributions from each particle below.
            
            // Note: here the outer loop is over particles and the inner loop is over
            // cells to sum in the particle contributions.  This should be optimal for
            // situations where there are a relatively small number of particles. In
            // cases where many particles per cell present, it may be more effective
            // to invert the loop structure.
            ParticleField::const_iterator ipx    = px_ ? px_->begin() : psize_->begin();
            ParticleField::const_iterator ipy    = py_ ? py_->begin() : psize_->begin();
            ParticleField::const_iterator ipz    = pz_ ? pz_->begin() : psize_->begin();
            ParticleField::const_iterator ipsize = psize_->begin();
            const ParticleField::const_iterator ipsizeend = psize_->end();
            for( ; ipsize != ipsizeend; ++ipx, ++ipy, ++ipz, ++ipsize ){
              
              const double rp = *ipsize * 0.5;
              
              // Identify the location of the particle boundary (assuming that it is a cube)
              const double pxlo = px_ ? *ipx - rp : 0;
              const double pylo = py_ ? *ipy - rp : 0;
              const double pzlo = pz_ ? *ipz - rp : 0;
              
              const double pxhi = px_ ? pxlo + *ipsize : 0;
              const double pyhi = py_ ? pylo + *ipsize : 0;
              const double pzhi = pz_ ? pzlo + *ipsize : 0;
              
              const int ixlo = ( pxlo - xloface ) / dx_;
              const int iylo = ( pylo - yloface ) / dy_;
              const int izlo = ( pzlo - zloface ) / dz_;
              
              // hi indices are 1 past the end
              const int ixhi = px_ ? std::min( nmax[0], int(( pxhi - xloface ) / dx_) + 1 ) : ixlo+1;
              const int iyhi = py_ ? std::min( nmax[1], int(( pyhi - yloface ) / dy_) + 1 ) : iylo+1;
              const int izhi = pz_ ? std::min( nmax[2], int(( pzhi - zloface ) / dz_) + 1 ) : izlo+1;
              
              // Distribute particle through the volume(s) it touches. Here we are
              // doing a highly approximate job at approximating how much fractional
              // particle volume is in each cell.
              const double pvol = (px_ ? 2*rp : 1)
              * (py_ ? 2*rp : 1)
              * (pz_ ? 2*rp : 1);
              for( int k=izlo; k<izhi; ++k ){
                const double zcm = zloface + k*dz_;
                const double zcp = zcm + dz_;
                // determine the z bounding box for the particle in this cell
                const double zcont = pz_ ? std::min(zcp,pzhi) - std::max(zcm,pzlo) : 1;
                for( int j=iylo; j<iyhi; ++j ){
                  const double ycm = yloface + j*dy_;
                  const double ycp = ycm + dy_;
                  // determine the y bounding box for the particle in this cell
                  const double ycont = py_ ? std::min(ycp,pyhi) - std::max(ycm,pylo) : 1;
                  for( int i=ixlo; i<ixhi; ++i ){
                    const double xcm = xloface + i*dx_;
                    const double xcp = xcm + dx_;
                    const double xcont = px_ ? std::min(xcp,pxhi) - std::max(xcm,pxlo) : 1;
                    // contribution is the fraction of the particle volume in this cell.
                    const double contribution = xcont*ycont*zcont / pvol;
                    dest(i,j,k) += contribution;
                 }
                }
              }
            } // particle loop
            
            break;
          } // case INTERPOLATE
            
        } // switch
      } // else IS_GPU

    }
    
    //==================================================================
    
    template< typename CellField >
    ParticleToCell<CellField>::
    ParticleToCell( const double dx, const double xlo,
                    const double dy, const double ylo,
                    const double dz, const double zlo,
                    const InterpOptions method )
    : dx_ ( dx<=0 ? 1.0 : dx  ),
      dy_ ( dy<=0 ? 1.0 : dy  ),
      dz_ ( dz<=0 ? 1.0 : dz  ),
      xlo_( dx<=0 ? 0.0 : xlo ),
      ylo_( dy<=0 ? 0.0 : ylo ),
      zlo_( dz<=0 ? 0.0 : zlo ),
      method_( method )
    {
      px_    = NULL;
      py_    = NULL;
      pz_    = NULL;
      psize_ = NULL;
    }
    
    //------------------------------------------------------------------
    
    template< typename CellField >
    void
    ParticleToCell<CellField>::
    set_coordinate_information( const ParticleField *pxcoord,
                                const ParticleField *pycoord,
                                const ParticleField *pzcoord,
                                const ParticleField *psize )
    {
      px_    = pxcoord;
      py_    = pycoord;
      pz_    = pzcoord;
      psize_ = psize  ;
    }

    //------------------------------------------------------------------
    
    template< typename CellField >
    void
    ParticleToCell<CellField>::
    apply_to_field( const SrcFieldType& src,
                    DestFieldType& dest ) const
    {
      if (IS_GPU_INDEX(dest.active_device_index())) {
#       ifdef __CUDACC__
        _p2c_apply_to_field<CellField, ParticleField>(px_, py_, pz_, psize_,
                                                      src, dest,
                                                      dx_, dy_, dz_,
                                                      xlo_, ylo_, zlo_,
                                                      method_);
        return;
#       endif
      } // IS_GPU_INDEX if
      else {
        
        switch( method_ ){
            
          case NO_INTERPOLATE:{
            
            const IntVec& ngm  = dest.get_ghost_data().get_minus();
            
            const double xloface = xlo_ - dx_*ngm[0] - dx_/2;
            const double yloface = ylo_ - dy_*ngm[1] - dy_/2;
            const double zloface = zlo_ - dz_*ngm[2] - dz_/2;
            
            dest <<= 0.0;  // set to zero and accumulate contributions from each particle below.
            
            ParticleField::const_iterator ipx  = px_ ? px_->begin() : src.begin();
            ParticleField::const_iterator ipy  = py_ ? py_->begin() : src.begin();
            ParticleField::const_iterator ipz  = pz_ ? pz_->begin() : src.begin();
            ParticleField::const_iterator isrc = src.begin();
            const ParticleField::const_iterator ise = src.end();
            for( ; isrc != ise; ++ipx, ++ipy, ++ipz, ++isrc ){
              const int i = ( *ipx - xloface ) / dx_;
              const int j = ( *ipy - yloface ) / dy_;
              const int k = ( *ipz - zloface ) / dz_;
              dest(i,j,k) += *isrc;
            } // particle loop
            
            break;
          }
            
          case INTERPOLATE:{
            assert( psize_ != NULL );
            
            const IntVec& ngm = dest.get_ghost_data().get_minus();
            const IntVec& nmax = dest.window_with_ghost().extent();
            
            const double xloface = xlo_ - dx_*ngm[0] - dx_/2;
            const double yloface = ylo_ - dy_*ngm[1] - dy_/2;
            const double zloface = zlo_ - dz_*ngm[2] - dz_/2;
            
            dest <<= 0.0;  // set to zero and accumulate contributions from each particle below.
            
            // Note: here the outer loop is over particles and the inner loop is over
            // cells to sum in the particle contributions.  This should be optimal for
            // situations where there are a relatively small number of particles. In
            // cases where many particles per cell present, it may be more effective
            // to invert the loop structure.
            ParticleField::const_iterator ipx    = px_ ? px_->begin() : src.begin();
            ParticleField::const_iterator ipy    = py_ ? py_->begin() : src.begin();
            ParticleField::const_iterator ipz    = pz_ ? pz_->begin() : src.begin();
            ParticleField::const_iterator ipsize = psize_->begin();
            ParticleField::const_iterator isrc   = src.begin();
            const ParticleField::const_iterator ise = src.end();
            for( ; isrc != ise; ++ipx, ++ipy, ++ipz, ++ipsize, ++isrc ){
              
              const double rp = *ipsize * 0.5;
              
              // Identify the location of the particle boundary (assuming that it is a cube)
              const double pxlo = px_ ? *ipx - rp : 0;
              const double pylo = py_ ? *ipy - rp : 0;
              const double pzlo = pz_ ? *ipz - rp : 0;
              
              const double pxhi = px_ ? pxlo + *ipsize : 0;
              const double pyhi = py_ ? pylo + *ipsize : 0;
              const double pzhi = pz_ ? pzlo + *ipsize : 0;
              
              const int ixlo = ( pxlo - xloface ) / dx_;
              const int iylo = ( pylo - yloface ) / dy_;
              const int izlo = ( pzlo - zloface ) / dz_;
              
              // hi indices are 1 past the end
              const int ixhi = px_ ? std::min( nmax[0], int(( pxhi - xloface ) / dx_) + 1 ) : ixlo+1;
              const int iyhi = py_ ? std::min( nmax[1], int(( pyhi - yloface ) / dy_) + 1 ) : iylo+1;
              const int izhi = pz_ ? std::min( nmax[2], int(( pzhi - zloface ) / dz_) + 1 ) : izlo+1;
              
              // Distribute particle through the volume(s) it touches. Here we are
              // doing a highly approximate job at approximating how much fractional
              // particle volume is in each cell.
              const double pvol = (px_ ? 2*rp : 1)
              * (py_ ? 2*rp : 1)
              * (pz_ ? 2*rp : 1);
#           ifndef NDEBUG
              double sumterm=0.0;
#           endif
              for( int k=izlo; k<izhi; ++k ){
                const double zcm = zloface + k*dz_;
                const double zcp = zcm + dz_;
                // determine the z bounding box for the particle in this cell
                const double zcont = pz_ ? std::min(zcp,pzhi) - std::max(zcm,pzlo) : 1;
                for( int j=iylo; j<iyhi; ++j ){
                  const double ycm = yloface + j*dy_;
                  const double ycp = ycm + dy_;
                  // determine the y bounding box for the particle in this cell
                  const double ycont = py_ ? std::min(ycp,pyhi) - std::max(ycm,pylo) : 1;
                  for( int i=ixlo; i<ixhi; ++i ){
                    const double xcm = xloface + i*dx_;
                    const double xcp = xcm + dx_;
                    const double xcont = px_ ? std::min(xcp,pxhi) - std::max(xcm,pxlo) : 1;
                    // contribution is the fraction of the particle volume in this cell.
                    const double contribution = xcont*ycont*zcont / pvol;
                    dest(i,j,k) += *isrc * contribution;
#                 ifndef NDEBUG
                    sumterm += contribution;
#                 endif
                  }
                }
              }
#           ifndef NDEBUG
              if( std::abs(1.0-sumterm) >= 1e-10 ) std::cout << "sum: " << sumterm << std::endl;
              assert( std::abs(1.0-sumterm) < 1e-10 );
#           endif
            } // particle loop
            
            break;
          } // case INTERPOLATE
            
        } // switch( method_ )
      } // else IS_GPU
    }

    //==================================================================
    
    template< typename CellField >
    CellToParticle<CellField>::
    CellToParticle( const double dx, const double xlo,
                    const double dy, const double ylo,
                    const double dz, const double zlo,
                    const InterpOptions method )
    : dx_ ( dx<=0 ? 1.0 : dx ),
      dy_ ( dy<=0 ? 1.0 : dy ),
      dz_ ( dz<=0 ? 1.0 : dz ),
      xlo_( dx<=0 ? 0.0 : xlo ),
      ylo_( dy<=0 ? 0.0 : ylo ),
      zlo_( dz<=0 ? 0.0 : zlo ),
      method_( method )
    {
      px_    = NULL;
      py_    = NULL;
      pz_    = NULL;
      psize_ = NULL;
    }
    
    //------------------------------------------------------------------
    
    template<typename CellField>
    void
    CellToParticle<CellField>::
    set_coordinate_information( const ParticleField *pxcoord,
                                const ParticleField *pycoord,
                                const ParticleField *pzcoord,
                                const ParticleField *psize )
    {
      px_    = pxcoord;
      py_    = pycoord;
      pz_    = pzcoord;
      psize_ = psize  ;
    }
    
    //------------------------------------------------------------------
    
    template<typename CellField>
    void
    CellToParticle<CellField>::
    apply_to_field( const SrcFieldType& src,
                    DestFieldType& dest ) const
    {
      if (IS_GPU_INDEX(dest.active_device_index())) {
#       ifdef __CUDACC__
        _c2p_apply_to_field<CellField, ParticleField>(px_, py_, pz_, psize_,
                                                      src, dest,
                                                      dx_, dy_, dz_,
                                                      xlo_, ylo_, zlo_,
                                                      method_);
        return;
#       endif
      } // IS_GPU_INDEX if
      else {
        
        switch( method_ ){
          case NO_INTERPOLATE: {
            const IntVec& ngm = src.get_ghost_data().get_minus();
            
            const double xloface = xlo_ - dx_*ngm[0] - dx_/2;
            const double yloface = ylo_ - dy_*ngm[1] - dy_/2;
            const double zloface = zlo_ - dz_*ngm[2] - dz_/2;
            
            ParticleField::const_iterator ipx    = px_ ? px_->begin() : dest.begin();
            ParticleField::const_iterator ipy    = py_ ? py_->begin() : dest.begin();
            ParticleField::const_iterator ipz    = pz_ ? pz_->begin() : dest.begin();
            ParticleField::iterator        idst  = dest.begin();
            const ParticleField::iterator  idste = dest.end();
            for( ; idst != idste; ++ipx, ++ipy, ++ipz, ++idst ){
              
              const int i = ( *ipx - xloface ) / dx_;
              const int j = ( *ipy - yloface ) / dy_;
              const int k = ( *ipz - zloface ) / dz_;
              
              *idst = src(i,j,k);
              
            } // particle loop
            
            break;
          } // case NO_INTERPOLATE
            
          case INTERPOLATE:{
            const IntVec& ngm = src.get_ghost_data().get_minus();
            const IntVec& nmax = src.window_with_ghost().extent();
            
            // Get the location of the (-) face associated with this cell field. Here we
            // shift the xlo_ value (the value associated with this field type's origin)
            // by its ghost offset and by the distance from the cell center to the face.
            const double xloface = xlo_ - dx_*ngm[0] - dx_/2;
            const double yloface = ylo_ - dy_*ngm[1] - dy_/2;
            const double zloface = zlo_ - dz_*ngm[2] - dz_/2;
            
            ParticleField::const_iterator ipx    = px_ ? px_->begin() : dest.begin();
            ParticleField::const_iterator ipy    = py_ ? py_->begin() : dest.begin();
            ParticleField::const_iterator ipz    = pz_ ? pz_->begin() : dest.begin();
            ParticleField::const_iterator ipsize = psize_->begin();
            ParticleField::iterator        idst  = dest.begin();
            const ParticleField::iterator  idste = dest.end();
            for( ; idst != idste; ++ipx, ++ipy, ++ipz, ++ipsize, ++idst ){
              
              *idst = 0.0;
              
              const double rp = *ipsize * 0.5;
              
              // Identify the location of the particle boundary
              const double pxlo = px_ ? *ipx - rp : 0;
              const double pylo = py_ ? *ipy - rp : 0;
              const double pzlo = pz_ ? *ipz - rp : 0;
              
              const double pxhi = px_ ? pxlo + *ipsize : 0;
              const double pyhi = py_ ? pylo + *ipsize : 0;
              const double pzhi = pz_ ? pzlo + *ipsize : 0;
              
              const int ixlo = ( pxlo - xloface ) / dx_;
              const int iylo = ( pylo - yloface ) / dy_;
              const int izlo = ( pzlo - zloface ) / dz_;
              
              // hi indices are 1 past the end
              const int ixhi = px_ ? std::min( nmax[0], int(( pxhi - xloface ) / dx_) + 1 ) : ixlo+1;
              const int iyhi = py_ ? std::min( nmax[1], int(( pyhi - yloface ) / dy_) + 1 ) : iylo+1;
              const int izhi = pz_ ? std::min( nmax[2], int(( pzhi - zloface ) / dz_) + 1 ) : izlo+1;
              
              // Distribute particle through the volume(s) it touches. Here we are
              // doing a highly approximate job at approximating how much fractional
              // particle volume is in each cell.
              const double pvol = (px_ ? 2*rp : 1)
              * (py_ ? 2*rp : 1)
              * (pz_ ? 2*rp : 1);
              
#           ifndef NDEBUG
              double sumterm=0.0;
#           endif
              for( int k=izlo; k<izhi; ++k ){
                const double zcm = zloface + k*dz_; // (-) face of the cell
                const double zcp = zcm + dz_;       // (+) face of the cell
                // determine the z bounding box for the particle in this cell
                const double zcont = pz_ ? std::min(zcp,pzhi) - std::max(zcm,pzlo) : 1;
                for( int j=iylo; j<iyhi; ++j ){
                  const double ycm = yloface + j*dy_;
                  const double ycp = ycm + dy_;
                  // determine the y bounding box for the particle in this cell
                  const double ycont = py_ ? std::min(ycp,pyhi) - std::max(ycm,pylo) : 1;
                  for( int i=ixlo; i<ixhi; ++i ){
                    const double xcm = xloface + i*dx_;
                    const double xcp = xcm + dx_;
                    const double xcont = px_ ? std::min(xcp,pxhi) - std::max(xcm,pxlo) : 1;
                    // contribution is the fraction of the particle volume in this cell.
                    const double contribution = xcont*ycont*zcont / pvol;
                    *idst += src(i,j,k) * contribution;
#                 ifndef NDEBUG
                    assert( contribution > 0 );
                    sumterm += contribution;
#                 endif
                  }
                }
              }
#           ifndef NDEBUG
              if( std::abs(1.0-sumterm) >= 1e-10 ) std::cout << "C2P sum: " << sumterm << std::endl;
              assert( std::abs(1.0-sumterm) < 1e-10 );
#           endif
            } // particle loop
          } // case INTERPOLATE
            break;
            
        } // switch( method_ )
      } // else - GPU vs CPU
    }

    //------------------------------------------------------------------

    RangeSearch::RangeSearch( const MemoryWindow& pParticleMemoryWindow,
                              const GhostData& pParticleGhostData,
                              const BoundaryCellInfo& pParticleBoundaryInfo,
                              const DoubleVec pParticleMinExtent,
                              const DoubleVec pParticleMaxExtent,
                              const size_t pNumParticles,
                              const double pSmoothingRadius,
                              const ParticleField& pxcoord,
                              const ParticleField& pycoord,
                              const ParticleField& pzcoord,
                              const bool pUseGrid,
                              const bool pSortData,
                              const short int location )
    							: px_(&pxcoord),
							  py_(&pycoord),
							  pz_(&pzcoord),
							  sortData_ (pSortData),
							  useGrid_(pUseGrid),
							  grid_( pNumParticles,
								 &pxcoord,
								 &pycoord,
								 &pzcoord,
								 pParticleMaxExtent,
								 pParticleMinExtent,
								 pSmoothingRadius,
								 pParticleMemoryWindow,
								 pParticleGhostData,
								 pParticleBoundaryInfo,
								 location)

    {
      assert (pxcoord.window_with_ghost().glob_npts() == pNumParticles);
      assert (pycoord.window_with_ghost().glob_npts() == pNumParticles);
      assert (pzcoord.window_with_ghost().glob_npts() == pNumParticles);
      assert (pParticleMinExtent < pParticleMaxExtent);

      if (pUseGrid)
        setup_grid(pSortData, location);
    }

    //------------------------------------------------------------------

    RangeSearch::RangeSearch( const MemoryWindow& pParticleMemoryWindow,
                              const GhostData& pParticleGhostData,
                              const BoundaryCellInfo& pParticleBoundaryInfo,
                              const DoubleVec pParticleMinExtent,
                              const DoubleVec pParticleMaxExtent,
                              const size_t pNumParticles,
                              const double pSmoothingRadius,
                              const ParticleField& pxcoord,
                              const ParticleField& pycoord,
                              const bool pUseGrid,
                              const bool pSortData,
                              const short int location )
    : px_(&pxcoord),
      py_(&pycoord),
      pz_(nullptr),
      sortData_ (pSortData),
      useGrid_(pUseGrid),
      grid_( pNumParticles,
             &pxcoord,
             &pycoord,
             nullptr,
             pParticleMaxExtent,
             pParticleMinExtent,
             pSmoothingRadius,
             pParticleMemoryWindow,
             pParticleGhostData,
             pParticleBoundaryInfo,
             location)

    {
      assert (pxcoord.window_with_ghost().glob_npts() == pNumParticles);
      assert (pycoord.window_with_ghost().glob_npts() == pNumParticles);

      assert (pParticleMinExtent < pParticleMaxExtent);

      if (pUseGrid)
        setup_grid(pSortData, location);
    }

    //------------------------------------------------------------------

    void
    RangeSearch::setup_grid(bool pSortData, short int location)
    {
      Timer timer;

      timer.reset();
      grid_.Setup();
      const double setupTime = timer.stop();

      if ( IS_GPU_INDEX(location) &&
          IS_GPU_INDEX(px_->active_device_index()) &&
          IS_GPU_INDEX(py_->active_device_index()) &&
          (!pz_ || IS_GPU_INDEX(pz_->active_device_index())) ) {
#ifdef __CUDACC__

        // Insert particles into the grid
        timer.reset();
        grid_._insert_particles_cuda();
        const double particleInsertionTime = timer.stop();

        // Perform prefix sum -initial step prior to performing counting sorting
        timer.reset();
        grid_._prefix_sum_cuda();
        const double prefixSumTime = timer.stop();

        double dataSortTime;

        if( pSortData ){
          timer.reset();
          grid_._data_sort_cuda();
          dataSortTime = timer.stop();
        }
        else{
          timer.reset();
          grid_._counting_sort_index_cuda();
          dataSortTime = timer.stop();
        }

#ifndef NDEBUG
        std::cout << "Setup Time: " << setupTime << std::endl;
        std::cout << "Particle Insertion Time: " << particleInsertionTime << std::endl;
        std::cout << "Prefix Sum Time: " << prefixSumTime << std::endl;
        std::cout << "Setup Sort Time: " << dataSortTime << std::endl;
        std::cout << "Total Amortized Time: " << (setupTime + particleInsertionTime + prefixSumTime + dataSortTime) << std::endl;
#endif

#endif
      }
      else{ // IS_GPU_INDEX if
        // Insert particles into the grid
        timer.reset();
        grid_.InsertParticles();
        const double particleInsertionTime = timer.stop();

#ifndef NDEBUG
        std::cout << "Setup Time: " << setupTime << std::endl;
        std::cout << "Particle Insertion Time: " << particleInsertionTime << std::endl;
        std::cout << "Total Amortized Time: " << (setupTime + particleInsertionTime) << std::endl;
#endif
      }
    }


    //================================================
    // Explicit template instantiation
    #include <spatialops/structured/FVStaggeredFieldTypes.h>
#define instantiate(VOL)                   \
    template class ParticlesPerCell<VOL>;  \
    template class CellToParticle  <VOL>;  \
    template class ParticleToCell  <VOL>;
    instantiate( SVolField )
    instantiate( XVolField )
    instantiate( YVolField )
    instantiate( ZVolField )

  } // namespace Particle
} // namespace SpatialOps


#endif // ParticleOperatorsImplementation_h


