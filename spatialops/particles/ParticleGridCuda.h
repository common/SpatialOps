//
//  ParticleGridCuda.h
//
//
//
#ifdef SpatialOps_ENABLE_CUDA
#ifndef ParticleGridCuda_h
#define ParticleGridCuda_h

#include <spatialops/SpatialOpsConfigure.h>
#include <cuda.h>
#include <vector_types.h>

namespace SpatialOps{
  namespace Particle{

    #define INNER_BLOCK_ONE_DIM  1024
    #define BLOCK_SIZE 256
    #define GRID_UNDEF 4294967295
    #define NUM_BANKS               16
    #define LOG_NUM_BANKS    4

	//------------------------------------------------------------------

    __global__ void _insert_particles( const double* px,
									   const double* py,
									   const double* pz,
									   unsigned int * particleCell,
								 	   unsigned int * particleIndex,
									   unsigned int * gridCount,
									   const double smoothingRadius,
									   const size_t nparticle,
									   const double gridMinx,
									   const double gridMiny,
									   const double gridMinz,
									   const int gridResx,
									   const int gridResy,
									   const int gridResz );


    
    //------------------------------------------------------------------

    __global__ void _neighbor_search (const double* px,
                                      const double* py,
                                      const double* pz,
                                      unsigned int * particleCell,
                                      unsigned int * sortedParticleIndex,
                                      unsigned int * gridCount,
                                      unsigned int * gridAdj,
                                      unsigned int * gridOffset,
                                      const double smoothingRadius,
                                      const size_t nparticle,
                                      const int gridResx,
                                      const int gridResy,
                                      const int gridResz,
                                      const int gridSearch,
                                      const int gridSize,
                                      double ** particleProperties, const int dimProps,
                                      double ** particlePropertiesOut, const int dimOut,
                                      void * kernel
                                    #ifndef NDEBUG
                                      ,unsigned int * numPairsFound
                                    #endif
                                       );
     
     __global__ void _neighbor_search (const double* px,
                                      const double* py,
                                      const double* pz,
                                      unsigned int * particleCell,
                                      unsigned int * sortedParticleIndex,
                                      unsigned int * gridCount,
                                      unsigned int * gridAdj,
                                      unsigned int * gridOffset,
                                      const double smoothingRadius,
                                      const size_t nparticle,
                                      const int gridResx,
                                      const int gridResy,
                                      const int gridResz,
                                      const int gridSearch,
                                      const int gridSize,
                                      double ** particleProperties, const int dimProps,
                                      double * particlePropertiesOut,
                                      void * kernel
                                    #ifndef NDEBUG
                                      ,unsigned int * numPairsFound
                                    #endif
                                       );

     //------------------------------------------------------------------

     __global__ void _sorted_neighbor_search (const double* sortedParticlePos,
											  unsigned int * particleCell,
											  unsigned int * sortedParticleIndex,
											  unsigned int * gridCount,
											  unsigned int * gridAdj,
											  unsigned int * gridOffset,
											  const double smoothingRadius,
											  const size_t nparticle,
											  const int gridResx,
											  const int gridResy,
											  const int gridResz,
											  const int gridSearch,
											  const int gridSize,
											  double ** sortedParticleProperties, const int dimProps,
											  double ** particlePropertiesOut, const int dimOut,
											  void * kernel
											#ifndef NDEBUG
											  ,unsigned int * numPairsFound
											#endif
											);

     __global__ void _sorted_neighbor_search (const double* sortedParticlePos,
											  unsigned int * particleCell,
											  unsigned int * sortedParticleIndex,
											  unsigned int * gridCount,
											  unsigned int * gridAdj,
											  unsigned int * gridOffset,
											  const double smoothingRadius,
											  const size_t nparticle,
											  const int gridResx,
											  const int gridResy,
											  const int gridResz,
											  const int gridSearch,
											  const int gridSize,
											  double ** sortedParticleProperties, const int dimProps,
											  double * particlePropertiesOut,
											  void * kernel
											#ifndef NDEBUG
											  ,unsigned int * numPairsFound
											#endif
											);

     //------------------------------------------------------------------


     __global__ void _neighbor_search_slow (const double* px,
                                            const double* py,
                                            const double* pz,
                                            const double smoothingRadius,
                                            double ** particleProperties,
                                            const int dimProps,
                                            double ** particlePropertiesOut,
                                            const int dimOut,
                                            void * kernel,
                                    #ifndef NDEBUG
                                            unsigned int * numPairsFound,
                                    #endif
                                            const size_t nparticle );

     //------------------------------------------------------------------

     __global__ void _neighbor_search_slow (const double* px,
                                            const double* py,
                                            const double* pz,
                                            const double smoothingRadius,
                                            double ** particleProperties,
                                            const int dimProps,
                                            double * particlePropertiesOut,
                                            void * kernel,
                                    #ifndef NDEBUG
                                            unsigned int * numPairsFound,
                                    #endif
                                            const size_t nparticle );


    //------------------------------------------------------------------

    __global__ void _counting_sort_index( const unsigned int * particleCell,
					  const unsigned int * particleIndex,
					  const unsigned int * gridOffset,
					  unsigned int * sortedParticleIndex,
					  const size_t nparticle );


    //------------------------------------------------------------------
    __global__ void _data_sort ( const double * px,
                                 const double * py,
                                 const double * pz,
                                 double * particlePosition,
                                 const unsigned int * particleCell,
                                 const unsigned int * nextParticle,
                                 const unsigned int * gridOffset,
                                 unsigned int * sortedParticleCell,
                                 unsigned int * sortedNextParticle,
                                 unsigned int * sortedParticleIndex,
				 const size_t nparticle );

    //------------------------------------------------------------------
    __global__ void _property_sort ( const unsigned int * sortedParticleIndex,
				     const double * particleProperties,
				     double * sortedParticleProperties,
				     const size_t nparticle );

    //------------------------------------------------------------------
    __global__ void _property_unsort ( const unsigned int * sortedParticleIndex,
                                       double * particleProperties,
                                       const double * sortedParticleProperties,
                                       const size_t nparticle );

    //------------------------------------------------------------------
	template <bool isNP2> __device__ void loadSharedChunkFromMemInt (unsigned int *s_data, const unsigned int *g_idata, int n, int baseIndex, int& ai, int& bi, int& mem_ai, int& mem_bi, int& bankOffsetA, int& bankOffsetB );

	//------------------------------------------------------------------
	template <bool isNP2> __device__ void storeSharedChunkToMemInt (unsigned int* g_odata, const unsigned int* s_data, int n, int ai, int bi, int mem_ai, int mem_bi,int bankOffsetA, int bankOffsetB);

	//------------------------------------------------------------------
	template <bool storeSum> __device__ void clearLastElementInt ( unsigned int* s_data, unsigned int *g_blockSums, int blockIndex);

	__device__ unsigned int buildSumInt (unsigned int *s_data);

	//------------------------------------------------------------------
	__device__ void scanRootToLeavesInt (unsigned int *s_data, unsigned int stride);

	//------------------------------------------------------------------
	template <bool storeSum> __device__ void prescanBlockInt (unsigned int *data, int blockIndex,unsigned int *blockSums);

	//------------------------------------------------------------------
	__global__ void uniformAddInt ( unsigned int *g_data, unsigned int *uniforms, int n, int blockOffset, int baseIndex);

    //------------------------------------------------------------------
    template <bool storeSum, bool isNP2> __global__ void prescanInt (unsigned int *g_odata, const unsigned int *g_idata,unsigned int *g_blockSums, int n, int blockIndex, int baseIndex);

  }
}

#endif /* ParticleGridCuda_h */
#endif /* SpatialOps_ENABLE_CUDA */
