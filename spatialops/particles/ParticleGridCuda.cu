#include <spatialops/particles/ParticleGridCuda.h>
#include <spatialops/particles/ParticlesOperatorHelper.h>
#include <spatialops/structured/MemoryTypes.h>
#include <spatialops/structured/IntVec.h>

#include <math_functions.h>

#include <cuda_runtime.h>
#include <cuda.h>
#include <cuda_runtime_api.h>

#include <stdio.h>

#include <iostream>
#include <sstream>
#include <stdexcept>

//==================================================================

namespace SpatialOps{
  namespace Particle{

#define GRID_UNDEF 4294967295
#define NUM_BANKS               16
#define LOG_NUM_BANKS    4    

#ifdef ZERO_BANK_CONFLICTS
#define CONFLICT_FREE_OFFSET(index) ((index) >> LOG_NUM_BANKS + (index) >> (2*LOG_NUM_BANKS))
#else
#define CONFLICT_FREE_OFFSET(index) ((index) >> LOG_NUM_BANKS)
#endif



    //==================================================================
    
    __global__ void _insert_particles ( const double* px,
								   	  const double* py,
								  	  const double* pz,
								 	  unsigned int * particleCell,
									  unsigned int * particleIndex,
							   		  unsigned int * gridCount,
								   	  const double smoothingRadius,
								   	  const size_t nparticle,
								   	  const double gridMinx,
								   	  const double gridMiny,
								   	  const double gridMinz,
								   	  const int gridResx,
								   	  const int gridResy,
									  const int gridResz )
	{
	
		const int iprt = (blockIdx.x * blockDim.x) + threadIdx.x;
		
		if( iprt>=nparticle ) return;
	
		int gs;

		const int gcx = (int)( (px[iprt] - gridMinx) / smoothingRadius);			
    	const int gcy = (int)( (py[iprt]- gridMiny) / smoothingRadius);
    	const int gcz = pz ? (int)( (pz[iprt]- gridMinz) / smoothingRadius) : NULL;

		gs = (int)( (gcy*gridResz + gcz)*gridResx + gcx);

		if ( gcx >= 0 && gcx <= gridResx && gcy >= 0 && gcy <= gridResy && gcz >= 0 && gcz <= gridResz ) {

			// put current particle at head of grid cell, pointing to next in list (previous head of cell)
			particleCell[iprt] = gs;

			//grid[gs] = iprt;
			particleIndex[iprt] = atomicAdd ( gridCount + gs, 1 );

		} 	
	}
	
	
     //------------------------------------------------------------------
    
     typedef void (*FVKernel) (double **, double, double, double*);

     __device__ void CUDAEquationSolver (double ** out, const unsigned int dimOut,  double distance, double smoothingRadius, double * properties, const unsigned int dimProps)
     {

    /*for (int i = 0; i < dimOut; i++)
    {
        *out[i] += ((smoothingRadius*smoothingRadius) - distance) * properties[0];
#ifndef NDEBUG 
        printf("%f", *out[i]);
#endif
    }*/

#ifndef NDEBUG
   printf("solving equation\n");
#endif
     }

     __device__ void CUDAEquationSolverIFV (double * out, double distance, double smoothingRadius, double * properties, const unsigned int dimProps)
     {

        *out += ((smoothingRadius*smoothingRadius) - distance) * properties[0];
#ifndef NDEBUG 
        printf("%f", *out);
#endif

#ifndef NDEBUG
   printf("solving equation\n");
#endif
     }

    typedef void (*CFVKernel) (double **, const unsigned int, double, double, double*, const unsigned int);
    typedef void (*IFVKernel) (double *, double, double , double*, const unsigned int);
    
     __device__ CFVKernel fptr = CUDAEquationSolver;
     __device__ IFVKernel funcptr = CUDAEquationSolverIFV;


     __global__ void _neighbor_search_slow (const double* px,
                                            const double* py,
                                            const double* pz,
                                            const double smoothingRadius,
		                                    double ** particleProperties,
                                            const int dimProps,
                                            double ** particlePropertiesOut,
                                            const int dimOut,
									        void * kernel,
									#ifndef NDEBUG
                                            unsigned int * numPairsFound,
									#endif
	    	        						const size_t nparticle )
   	{

		const int iprt = (blockIdx.x * blockDim.x) + threadIdx.x;

        if( iprt>=nparticle ) return;
	
		double distance;
        double dx,dy,dz;

        double ** out = new double *[dimOut];
        double * props = new double [dimProps];

		for (int i = 0; i < nparticle; i++)
		{
			if (i == iprt)
				continue;
			
			dx = px[i] - px[iprt];
            dy = py[i] - py[iprt];
            dz = pz[i] - pz[iprt];

            distance = (dx * dx + dy * dy + dz * dz);

            if ( distance > 0 && distance <= (smoothingRadius * smoothingRadius) ) {

		#ifndef NDEBUG

	            atomicAdd ( numPairsFound, 1 );

		#endif

                for (int j = 0; j < dimOut; ++j)
                {
                    out [j] = &((particlePropertiesOut[j])[iprt]);
                }

                for (int j = 0; j < dimProps; ++j)
                {
                    props [j] = ((particleProperties[j])[i]);
                }

                fptr (out, dimOut, distance, smoothingRadius, props, dimProps);         
            }

		}	

        delete props;
        delete [] out;

	}

    __global__ void _neighbor_search_slow (const double* px,
                                            const double* py,
                                            const double* pz,
                                            const double smoothingRadius,
                                            double ** particleProperties,
                                            const int dimProps,
                                            double * particlePropertiesOut,
                                            void * kernel,
                                    #ifndef NDEBUG
                                            unsigned int * numPairsFound,
                                    #endif
                                            const size_t nparticle )
    {

        //kernel[0] (NULL,0,0,NULL);

        const int iprt = (blockIdx.x * blockDim.x) + threadIdx.x;

        if( iprt>=nparticle ) return;

        double distance;
        double dx,dy,dz;

        double * props = new double [dimProps];

        for (int i = 0; i < nparticle; i++)
        {
            if (i == iprt)
                continue;

            dx = px[i] - px[iprt];
            dy = py[i] - py[iprt];
            dz = pz[i] - pz[iprt];

            distance = (dx * dx + dy * dy + dz * dz);

            if ( distance > 0 && distance <= (smoothingRadius * smoothingRadius) ) {

        #ifndef NDEBUG

                atomicAdd ( numPairsFound, 1 );

        #endif

                for (int j = 0; j < dimProps; ++j)
                {
                    props [j] = ((particleProperties[j])[i]);
                }

                //((FVKernel)kernel) (out, distance, smoothingRadius, props);           
                //((FVKernel)kernel)(NULL, 0, 0, NULL);
                funcptr (particlePropertiesOut + iprt, distance, smoothingRadius, props, dimProps);
            }

        }

        delete props;
    }
	
    //==================================================================
    
    __global__ void _neighbor_search (const double* px,
                                      const double* py,
                                      const double* pz,
                                      unsigned int * particleCell,
                                      unsigned int * sortedParticleIndex,
							   		  unsigned int * gridCount,
							   		  unsigned int * gridAdj,
									  unsigned int * gridOffset,
								   	  const double smoothingRadius,
								   	  const size_t nparticle,
								   	  const int gridResx,
								   	  const int gridResy,
									  const int gridResz,
									  const int gridSearch,
									  const int gridSize,
									  double ** particleProperties, const int dimProps,
                                      double ** particlePropertiesOut, const int dimOut,
									  void * kernel
									#ifndef NDEBUG
								 	  ,unsigned int * numPairsFound
									#endif 
									   )
	{
	
		const int iprt = (blockIdx.x * blockDim.x) + threadIdx.x;
		
		if( iprt>=nparticle ) return;
	
		double distance;
		double dx,dy,dz;
		
		int nadj = (gridResz + 1)*gridResx + 1;
	
        double ** out = new double *[dimOut];
        double * props = new double [dimProps];

		//nadj *= 2;
		
		const int searchSpace = gridSearch * gridSearch * gridSearch;
		
		int gridCell;

		int startNdx,endNdx;

		if (particleCell[iprt] != GRID_UNDEF) {

			for (int cell=0; cell < searchSpace; cell++) {
				gridCell = (int)(particleCell[iprt]  - nadj + gridAdj[cell]);

				if (gridCell < 0 || gridCell >= gridSize)
					continue;

				if (gridCount[gridCell] == 0)
					continue;

				startNdx = gridOffset[gridCell]; 
				endNdx = startNdx + gridCount[gridCell];

				for (int index = startNdx; index < endNdx; index++) {
				
					int particleIndex = sortedParticleIndex[index];
	
					dx = px[particleIndex] - px[iprt];
					dy = py[particleIndex] - py[iprt];
					dz = pz[particleIndex] - pz[iprt];
					
					distance = (dx * dx + dy * dy + dz * dz);

					if ( distance > 0 && distance <= (smoothingRadius * smoothingRadius) ) {

					#ifndef NDEBUG
						atomicAdd ( numPairsFound, 1 );
					#endif

                        for (int j = 0; j < dimOut; ++j)
                        {
                            out [j] = &((particlePropertiesOut[j])[iprt]);
                        }

                        for (int j = 0; j < dimProps; ++j)
                        {
                             props [j] = ((particleProperties[j])[particleIndex]);
                        }

                        fptr (out, dimOut, distance, smoothingRadius, props, dimProps);
					}
					
				}
			}
		}
		
        delete props;
        delete [] out;

		
	}

    __global__ void _neighbor_search (const double* px,
                                      const double* py,
                                      const double* pz,
                                      unsigned int * particleCell,
                                      unsigned int * sortedParticleIndex,
                                      unsigned int * gridCount,
                                      unsigned int * gridAdj,
                                      unsigned int * gridOffset,
                                      const double smoothingRadius,
                                      const size_t nparticle,
                                      const int gridResx,
                                      const int gridResy,
                                      const int gridResz,
                                      const int gridSearch,
                                      const int gridSize,
                                      double ** particleProperties, const int dimProps,
                                      double * particlePropertiesOut,
                                      void * kernel
                                    #ifndef NDEBUG
                                      ,unsigned int * numPairsFound
                                    #endif
                                       )
    {

        //printf (" search fv*f ");

        const int iprt = (blockIdx.x * blockDim.x) + threadIdx.x;

        if( iprt>=nparticle ) return;

        double distance;
        double dx,dy,dz;

        int nadj = (gridResz + 1)*gridResx + 1;

        double * props = new double [dimProps];

        //nadj *= 2;

        const int searchSpace = gridSearch * gridSearch * gridSearch;

        int gridCell;

        int startNdx,endNdx;

        if (particleCell[iprt] != GRID_UNDEF) {

            for (int cell=0; cell < searchSpace; cell++) {
                gridCell = (int)(particleCell[iprt]  - nadj + gridAdj[cell]);

                if (gridCell < 0 || gridCell >= gridSize)
                    continue;

                if (gridCount[gridCell] == 0)
                    continue;

                startNdx = gridOffset[gridCell];
                endNdx = startNdx + gridCount[gridCell];

                for (int index = startNdx; index < endNdx; index++) {

                    int particleIndex = sortedParticleIndex[index];

                    dx = px[particleIndex] - px[iprt];
                    dy = py[particleIndex] - py[iprt];
                    dz = pz[particleIndex] - pz[iprt];

                    distance = (dx * dx + dy * dy + dz * dz);
                    
                    if ( distance > 0 && distance <= (smoothingRadius * smoothingRadius) ) {

                    #ifndef NDEBUG
                        atomicAdd ( numPairsFound, 1 );
                    #endif

                        for (int j = 0; j < dimProps; ++j)
                        {
                            props [j] = ((particleProperties[j])[particleIndex]);
                        }

                        funcptr (particlePropertiesOut + iprt, distance, smoothingRadius, props, dimProps);
                    }

                }
            }
        }

        delete props;

    }

    //==================================================================

    __global__ void _sorted_neighbor_search (const double* sortedParticlePos,
                                                                          unsigned int * particleCell,
                                                                          unsigned int * sortedParticleIndex,
                                                                          unsigned int * gridCount,
                                                                          unsigned int * gridAdj,
                                                                          unsigned int * gridOffset,
                                                                          const double smoothingRadius,
                                                                          const size_t nparticle,
                                                                          const int gridResx,
                                                                          const int gridResy,
                                                                          const int gridResz,
                                                                          const int gridSearch,
                                                                          const int gridSize,
                                    									  double ** sortedParticleProperties, const int dimProps,
                                                                          double ** sortedPropertiesOut, const int dimOut,
									                                      void *  kernel
                                                                        #ifndef NDEBUG
                                                                          ,unsigned int * numPairsFound
                                                                        #endif
                                                                           )
        {

                const int iprt = (blockIdx.x * blockDim.x) + threadIdx.x;

                if( iprt>=nparticle ) return;

                double distance;
                double dx,dy,dz;

                int nadj = (gridResz + 1)*gridResx + 1;

                double ** out = new double *[dimOut];
                double * props = new double [dimProps];

		//nadj *= 2;

                const int searchSpace = gridSearch * gridSearch * gridSearch;

                int gridCell;

                int startNdx,endNdx;

                if (particleCell[iprt] != GRID_UNDEF) {

                        for (int cell=0; cell < searchSpace; cell++) {
                                gridCell = (int)(particleCell[iprt]  - nadj + gridAdj[cell]);

                                if (gridCell < 0 || gridCell >= gridSize)
                                        continue;

                                if (gridCount[gridCell] == 0)
                                        continue;

                                startNdx = gridOffset[gridCell];
                                endNdx = startNdx + gridCount[gridCell];

                                for (int index = startNdx; index < endNdx; index++) {

                                        int particleIndex = index;

                                        dx = sortedParticlePos[particleIndex * 3] - sortedParticlePos[iprt * 3];
                                        dy = sortedParticlePos[particleIndex * 3 + 1] - sortedParticlePos[iprt * 3 + 1];
                                        dz = sortedParticlePos[particleIndex * 3 + 2] - sortedParticlePos[iprt * 3 + 2];

                                        distance = (dx * dx + dy * dy + dz * dz);

                                        if ( distance > 0 && distance <= (smoothingRadius * smoothingRadius) ) {

                                        #ifndef NDEBUG
                                                atomicAdd ( numPairsFound, 1 );
                                        #endif
						
						                //sum += distance * sortedParticleProperties[particleIndex];
		                				//gpuKernels[kernelType](&sum,distance,smoothingRadius,sortedParticleProperties[particleIndex]);
                                        
                                            for (int j = 0; j < dimOut; ++j)
                                            {
                                                out [j] = &((sortedPropertiesOut[j])[iprt]);
                                            }

                                            for (int j = 0; j < dimProps; ++j)
                                            {
                                                props [j] = ((sortedParticleProperties[j])[particleIndex]);
                                            }

                                            //((FVKernel)kernel) (out, distance, smoothingRadius, props);
                                            fptr (out, dimOut, distance, smoothingRadius, props, dimProps);
                                        }
                                }
                        }
                }
        delete [] out;
        delete props;

        }

    __global__ void _sorted_neighbor_search (const double* sortedParticlePos,
                                                                          unsigned int * particleCell,
                                                                          unsigned int * sortedParticleIndex,
                                                                          unsigned int * gridCount,
                                                                          unsigned int * gridAdj,
                                                                          unsigned int * gridOffset,
                                                                          const double smoothingRadius,
                                                                          const size_t nparticle,
                                                                          const int gridResx,
                                                                          const int gridResy,
                                                                          const int gridResz,
                                                                          const int gridSearch,
                                                                          const int gridSize,
                                                                          double ** sortedParticleProperties, const int dimProps,
                                                                          double * sortedPropertiesOut,
                                                                          void *  kernel
                                                                        #ifndef NDEBUG
                                                                          ,unsigned int * numPairsFound
                                                                        #endif
                                                                           )
        {

                const int iprt = (blockIdx.x * blockDim.x) + threadIdx.x;

                if( iprt>=nparticle ) return;

                double distance;
                double dx,dy,dz;

                int nadj = (gridResz + 1)*gridResx + 1;

                double * props = new double [dimProps];

        //nadj *= 2;

                const int searchSpace = gridSearch * gridSearch * gridSearch;

                int gridCell;

                int startNdx,endNdx;

                if (particleCell[iprt] != GRID_UNDEF) {

                        for (int cell=0; cell < searchSpace; cell++) {
                                gridCell = (int)(particleCell[iprt]  - nadj + gridAdj[cell]);

                                if (gridCell < 0 || gridCell >= gridSize)
                                        continue;

                                if (gridCount[gridCell] == 0)
                                        continue;

                                startNdx = gridOffset[gridCell];
                                endNdx = startNdx + gridCount[gridCell];

                                for (int index = startNdx; index < endNdx; index++) {

                                        int particleIndex = index;

                                        dx = sortedParticlePos[particleIndex * 3] - sortedParticlePos[iprt * 3];
                                        dy = sortedParticlePos[particleIndex * 3 + 1] - sortedParticlePos[iprt * 3 + 1];
                                        dz = sortedParticlePos[particleIndex * 3 + 2] - sortedParticlePos[iprt * 3 + 2];

                                        distance = (dx * dx + dy * dy + dz * dz);

                                        if ( distance > 0 && distance <= (smoothingRadius * smoothingRadius) ) {

                                        #ifndef NDEBUG
                                                atomicAdd ( numPairsFound, 1 );
                                        #endif

                                            for (int j = 0; j < dimProps; ++j)
                                            {
                                                props [j] = ((sortedParticleProperties[j])[particleIndex]);
                                            }

                                            //((FVKernel)kernel) (out, distance, smoothingRadius, props);
                                            funcptr (sortedPropertiesOut + iprt, distance, smoothingRadius, props, dimProps);
                                        }
                                }
                        }
                }
        delete props;

        }

    //==================================================================

     __global__ void _counting_sort_index( const unsigned int * particleCell,
                                          const unsigned int * particleIndex,
                                          const unsigned int * gridOffset,
                                          unsigned int * sortedParticleIndex,
					  const size_t nparticle )
    {

	const int iprt = (blockIdx.x * blockDim.x) + threadIdx.x;

        if( iprt>=nparticle ) return;

        unsigned int pcell = particleCell[iprt];
        unsigned int pndx =  particleIndex[iprt];
        unsigned int sort_index = gridOffset[ pcell ] + pndx;
        if ( pcell != GRID_UNDEF ) {
                sortedParticleIndex[sort_index] = iprt;
        }

    }
	
    //==================================================================

    __global__ void _data_sort ( const double * px,
				 const double * py,
				 const double * pz,
				 double * particlePosition,
				 const unsigned int * particleCell,
				 const unsigned int * nextParticle,
				 const unsigned int * gridOffset,
				 unsigned int * sortedParticleCell,
				 unsigned int * sortedNextParticle,
				 unsigned int * sortedParticleIndex,
				 const size_t nparticle )
    {

	const int iprt = (blockIdx.x * blockDim.x) + threadIdx.x;

	if (iprt >= nparticle) return;
	
	unsigned int pcell = particleCell[iprt];
	unsigned int pndx = nextParticle[iprt];

	if (pcell != GRID_UNDEF) {

		int sort_index = gridOffset[pcell] + pndx;

		particlePosition[sort_index * 3] = px[iprt];
		particlePosition[sort_index * 3 + 1] = py[iprt];
		particlePosition[sort_index * 3 + 2] = pz[iprt];

		//sort properties here
		//sortedParticleProperties[sort_index] = particleProperties[iprt];
			

		sortedParticleCell[sort_index] = pcell;
		sortedNextParticle[sort_index] = pndx;		

		sortedParticleIndex[sort_index] = iprt;
	}
    }

   //==================================================================

    __global__ void _property_sort ( const unsigned int * sortedParticleIndex,
                                     const double * particleProperties,
                                     double * sortedParticleProperties,
                                     const size_t nparticle )
    {

        const int iprt = (blockIdx.x * blockDim.x) + threadIdx.x;

        if (iprt >= nparticle) return;

        unsigned int pndx = sortedParticleIndex[iprt];

        //sort properties here
        sortedParticleProperties[iprt] = particleProperties[pndx];
    }

    //==================================================================

    __global__ void _property_unsort ( const unsigned int * sortedParticleIndex,
                                       double * particleProperties,
                                       const double * sortedParticleProperties,
                                       const size_t nparticle )
    {

        const int iprt = (blockIdx.x * blockDim.x) + threadIdx.x;

        if (iprt >= nparticle) return;

	unsigned int pndx = sortedParticleIndex[iprt];
                
        //sort properties here
	particleProperties[pndx] = sortedParticleProperties[iprt];

    }

    //------------------------------------------------------------------
	template <bool isNP2> 
    __device__ void loadSharedChunkFromMemInt (unsigned int *s_data, const unsigned int *g_idata, int n, int baseIndex, int& ai, int& bi, int& mem_ai, int& mem_bi, int& bankOffsetA, int& bankOffsetB )
	{
		int thid = threadIdx.x;
		mem_ai = baseIndex + threadIdx.x;
		mem_bi = mem_ai + blockDim.x;

		ai = thid;
		bi = thid + blockDim.x;
		bankOffsetA = CONFLICT_FREE_OFFSET(ai);                 // compute spacing to avoid bank conflicts
		bankOffsetB = CONFLICT_FREE_OFFSET(bi);

			s_data[ai + bankOffsetA] = g_idata[mem_ai];             // Cache the computational window in shared memory pad values beyond n with zeros

		if (isNP2) { // compile-time decision
			s_data[bi + bankOffsetB] = (bi < n) ? g_idata[mem_bi] : 0;
		} else {
			s_data[bi + bankOffsetB] = g_idata[mem_bi];
		}
	}

	//------------------------------------------------------------------
	template <bool isNP2> 
    __device__ void storeSharedChunkToMemInt (unsigned int* g_odata, const unsigned int* s_data, int n, int ai, int bi, int mem_ai, int mem_bi,int bankOffsetA, int bankOffsetB)
	{
		__syncthreads();

		g_odata[mem_ai] = s_data[ai + bankOffsetA];                 // write results to global memory
		if (isNP2) { // compile-time decision
			if (bi < n) g_odata[mem_bi] = s_data[bi + bankOffsetB];
		} else {
			g_odata[mem_bi] = s_data[bi + bankOffsetB];
		}
	}

	//------------------------------------------------------------------
	template <bool storeSum> 
    __device__ void clearLastElementInt ( unsigned int* s_data, unsigned int *g_blockSums, int blockIndex)
	{
		if (threadIdx.x == 0) {
			int index = (blockDim.x << 1) - 1;
			index += CONFLICT_FREE_OFFSET(index);
			if (storeSum) { // compile-time decision
				// write this block's total sum to the corresponding index in the blockSums array
				g_blockSums[blockIndex] = s_data[index];
			}
			s_data[index] = 0;              // zero the last element in the scan so it will propagate back to the front
		}
	}

	__device__ unsigned int buildSumInt (unsigned int *s_data)
	{
		unsigned int thid = threadIdx.x;
		unsigned int stride = 1;

		// build the sum in place up the tree
		for (int d = blockDim.x; d > 0; d >>= 1) {
			__syncthreads();
			if (thid < d) {
				int i  = __mul24(__mul24(2, stride), thid);
				int ai = i + stride - 1;
				int bi = ai + stride;
				ai += CONFLICT_FREE_OFFSET(ai);
				bi += CONFLICT_FREE_OFFSET(bi);
				s_data[bi] += s_data[ai];
			}
			stride *= 2;
		}
		return stride;
	}

	//------------------------------------------------------------------
	__device__ void scanRootToLeavesInt (unsigned int *s_data, unsigned int stride)
	{
		 unsigned int thid = threadIdx.x;

		// traverse down the tree building the scan in place
		for (int d = 1; d <= blockDim.x; d *= 2) {
			stride >>= 1;
			__syncthreads();

			if (thid < d) {
				int i  = __mul24(__mul24(2, stride), thid);
				int ai = i + stride - 1;
				int bi = ai + stride;
				ai += CONFLICT_FREE_OFFSET(ai);
				bi += CONFLICT_FREE_OFFSET(bi);
				int t = s_data[ai];
				s_data[ai] = s_data[bi];
				s_data[bi] += t;
			}
		}
	}

	//------------------------------------------------------------------
	template <bool storeSum> 
    __device__ void prescanBlockInt (unsigned int *data, int blockIndex,unsigned int *blockSums)
	{
		int stride = buildSumInt (data);               // build the sum in place up the tree
		clearLastElementInt <storeSum>(data, blockSums, (blockIndex == 0) ? blockIdx.x : blockIndex);
		scanRootToLeavesInt (data, stride);            // traverse down tree to build the scan
	}

	//------------------------------------------------------------------
	__global__ void uniformAddInt ( unsigned int *g_data, unsigned int *uniforms, int n, int blockOffset, int baseIndex)
	{
		__shared__ int uni;
		if (threadIdx.x == 0) uni = uniforms[blockIdx.x + blockOffset];
		unsigned int address = __mul24(blockIdx.x, (blockDim.x << 1)) + baseIndex + threadIdx.x;

		__syncthreads();
		// note two adds per thread
		g_data[address]              += uni;
		g_data[address + blockDim.x] += (threadIdx.x + blockDim.x < n) * uni;
	}

    //------------------------------------------------------------------
    template <bool storeSum, bool isNP2> 
    __global__ void prescanInt (unsigned int *g_odata, const unsigned int *g_idata,unsigned int *g_blockSums, int n, int blockIndex, int baseIndex) {
			int ai, bi, mem_ai, mem_bi, bankOffsetA, bankOffsetB;
			extern __shared__ unsigned int s_dataInt [];
			loadSharedChunkFromMemInt <isNP2>(s_dataInt, g_idata, n, (baseIndex == 0) ? __mul24(blockIdx.x, (blockDim.x << 1)):baseIndex, ai, bi, mem_ai, mem_bi, bankOffsetA, bankOffsetB); 
			prescanBlockInt<storeSum>(s_dataInt, blockIndex, g_blockSums); 
			storeSharedChunkToMemInt <isNP2>(g_odata, s_dataInt, n, ai, bi, mem_ai, mem_bi, bankOffsetA, bankOffsetB); 
    }

    template
    __global__ void prescanInt<true,false> (unsigned int *g_odata, const unsigned int *g_idata,unsigned int *g_blockSums, int n, int blockIndex, int baseIndex);

     template
    __global__ void prescanInt<true,true> (unsigned int *g_odata, const unsigned int *g_idata,unsigned int *g_blockSums, int n, int blockIndex, int baseIndex);

    template
    __global__ void prescanInt<false,false> (unsigned int *g_odata, const unsigned int *g_idata,unsigned int *g_blockSums, int n, int blockIndex, int baseIndex);

    template
    __global__ void prescanInt<false,true> (unsigned int *g_odata, const unsigned int *g_idata,unsigned int *g_blockSums, int n, int blockIndex, int baseIndex);



  }
}


