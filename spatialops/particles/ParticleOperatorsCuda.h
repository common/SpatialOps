//
//  ParticleOperatorsCuda.h
//  
//
//  Created by Babak Goshayeshi on 12/28/15.
//
//
#ifdef SpatialOps_ENABLE_CUDA
#ifndef ParticleOperatorsCuda_h
#define ParticleOperatorsCuda_h

#include <spatialops/SpatialOpsConfigure.h>

namespace SpatialOps{
  namespace Particle{

#define GPU_ERROR_CHECK(err) { gpu_error( err, __FILE__, __LINE__); }

    inline void gpu_error(cudaError err, const char *file, int line)
    {
      if( cudaSuccess != err ){
        std::ostringstream msg;
        msg << "GPU failed!\n"
            << "Cuda message : " << cudaGetErrorString(err) << "\n"
            << "\t - " << file << " : " << line << std::endl;
        throw(std::runtime_error(msg.str()));
       }
     }

    // Maximum number of block in one dimension is 65535.
    #define INNER_BLOCK_ONE_DIM  1024

    //------------------------------------------------------------------

    __global__ void _ppc_apply_to_field_in( const double* px,
                                            const double* py,
                                            const double* pz,
                                            const double* psize,
                                            double* dest,
                                            const double dx,
                                            const double dy,
                                            const double dz,
                                            const double xloface,
                                            const double yloface,
                                            const double zloface,
                                            const size_t nparticle,
                                            const int nmax1,
                                            const int nmax2,
                                            const int nmax3 );

    //------------------------------------------------------------------

    __global__ void _ppc_apply_to_field_ni( const double*  px,
                                            const double*  py,
                                            const double*  pz,
                                            double* dest,
                                            const double dx,
                                            const double dy,
                                            const double dz,
                                            const double xloface,
                                            const double yloface,
                                            const double zloface,
                                            const size_t nparticle,
                                            const int nmax1,
                                            const int nmax2,
                                            const int nmax3 );

    //------------------------------------------------------------------

    __global__ void _p2c_apply_to_field_ni( const double*  px,
                                            const double*  py,
                                            const double*  pz,
                                            const double*  src,
                                            double* dest,
                                            const double dx,
                                            const double dy,
                                            const double dz,
                                            const double xloface,
                                            const double yloface,
                                            const double zloface,
                                            const size_t nparticle,
                                            const int nmax1,
                                            const int nmax2,
                                            const int nmax3 );

    __global__ void _p2c_apply_to_field_in( const double* px,
                                            const double* py,
                                            const double* pz,
                                            const double* psize,
                                            const double* src,
                                            double* dest,
                                            const double dx,
                                            const double dy,
                                            const double dz,
                                            const double xloface,
                                            const double yloface,
                                            const double zloface,
                                            const size_t nparticle,
                                            const int nmax1,
                                            const int nmax2,
                                            const int nmax3 );

    __global__ void _c2p_apply_to_field_ni( const double*  px,
                                            const double*  py,
                                            const double*  pz,
                                            const double*  src,
                                            double* dest,
                                            const double dx,
                                            const double dy,
                                            const double dz,
                                            const double xlo,
                                            const double ylo,
                                            const double zlo,
                                            const size_t nparticle,
                                            const int nmax1,
                                            const int nmax2,
                                            const int nmax3 );

    __global__ void _c2p_apply_to_field_in( const double* px,
                                            const double* py,
                                            const double* pz,
                                            const double* psize,
                                            const double* src,
                                            double* dest,
                                            const double dx,
                                            const double dy,
                                            const double dz,
                                            const double xloface,
                                            const double yloface,
                                            const double zloface,
                                            const size_t nparticle,
                                            const int nmax1,
                                            const int nmax2,
                                            const int nmax3 );

    //------------------------------------------------------------------

    template<class CellField, class ParticleField>
    __host__ inline void _ppc_apply_to_field( const ParticleField  *px,
                                              const ParticleField  *py,
                                              const ParticleField  *pz,
                                              const ParticleField  *psize,
                                              CellField&     dest,
                                              const double& dx,
                                              const double& dy,
                                              const double& dz,
                                              const double& xlo,
                                              const double& ylo,
                                              const double& zlo,
                                              const InterpOptions& method )
    {
      const IntVec& ngm  = dest.get_ghost_data().get_minus();
      const IntVec& nmax = dest.window_with_ghost().extent();

      const double xloface = xlo - dx*ngm[0] - dx/2;
      const double yloface = ylo - dy*ngm[1] - dy/2;
      const double zloface = zlo - dz*ngm[2] - dz/2;

      cudaError err;
      err = cudaSetDevice( dest.active_device_index() );

      if( cudaSuccess != err ){
        throw( std::runtime_error( cudaGetErrorString(err) ) );
      }

      // Obtaing the number of particles
      const size_t nparticle = psize->window_with_ghost().local_npts();

      // Calcuate the number of Blocks and Threads
      const size_t nblock = nparticle / INNER_BLOCK_ONE_DIM + 1 ;
      const size_t nthread =  (nparticle > INNER_BLOCK_ONE_DIM) ? INNER_BLOCK_ONE_DIM : nparticle;

      dest <<= 0.0;
      switch (method) {
        case NO_INTERPOLATE:
          _ppc_apply_to_field_ni<<<nblock,nthread>>>(px ? px->field_values(GPU_INDEX) : NULL,
                                                     py ? py->field_values(GPU_INDEX) : NULL,
                                                     pz ? pz->field_values(GPU_INDEX) : NULL,
                                                     dest.field_values(GPU_INDEX),
                                                     dx, dy, dz,
                                                     xloface, yloface, zloface,
                                                     nparticle,
                                                     nmax[0], nmax[1], nmax[2]);
          break;
        case INTERPOLATE:
          _ppc_apply_to_field_in<<<nblock,nthread>>>(px ? px->field_values(GPU_INDEX) : NULL,
                                                     py ? py->field_values(GPU_INDEX) : NULL,
                                                     pz ? pz->field_values(GPU_INDEX) : NULL,
                                                     psize->field_values(GPU_INDEX),
                                                     dest.field_values(GPU_INDEX),
                                                     dx, dy, dz,
                                                     xloface, yloface, zloface,
                                                     nparticle,
                                                     nmax[0], nmax[1], nmax[2]);


          break;
      }
      cudaDeviceSynchronize();
      GPU_ERROR_CHECK(cudaGetLastError());
    }

    //------------------------------------------------------------------

    template<class CellField, class ParticleField>
    __host__ inline void _p2c_apply_to_field( const ParticleField  *px,
                                              const ParticleField  *py,
                                              const ParticleField  *pz,
                                              const ParticleField  *psize,
                                              const ParticleField& src,
                                              CellField&     dest,
                                              const double& dx,
                                              const double& dy,
                                              const double& dz,
                                              const double& xlo,
                                              const double& ylo,
                                              const double& zlo,
                                              const InterpOptions& method )
    {
      const IntVec& ngm  = dest.get_ghost_data().get_minus();
      const IntVec& nmax = dest.window_with_ghost().extent();

      const double xloface = xlo - dx*ngm[0] - dx/2;
      const double yloface = ylo - dy*ngm[1] - dy/2;
      const double zloface = zlo - dz*ngm[2] - dz/2;

      cudaError err;
      err = cudaSetDevice( dest.active_device_index() );

      if( cudaSuccess != err ){
        throw( std::runtime_error( cudaGetErrorString(err) ) );
      }

      // Obtaing the number of particles
      const size_t nparticle = src.window_with_ghost().local_npts();

      // Calcuate the number of Blocks and Threads
      const size_t nblock = nparticle / INNER_BLOCK_ONE_DIM + 1 ;
      const size_t nthread =  (nparticle > INNER_BLOCK_ONE_DIM) ? INNER_BLOCK_ONE_DIM : nparticle;

      dest <<= 0.0;
      switch (method) {
        case NO_INTERPOLATE:
          _p2c_apply_to_field_ni<<<nblock,nthread>>>(px ? px->field_values(GPU_INDEX) : NULL,
                                                     py ? py->field_values(GPU_INDEX) : NULL,
                                                     pz ? pz->field_values(GPU_INDEX) : NULL,
                                                     src.field_values(GPU_INDEX),
                                                     dest.field_values(GPU_INDEX),
                                                     dx, dy, dz,
                                                     xloface, yloface, zloface,
                                                     nparticle,
                                                     nmax[0], nmax[1], nmax[2]);
          break;
        case INTERPOLATE:
          _p2c_apply_to_field_in<<<nblock,nthread>>>(px ? px->field_values(GPU_INDEX) : NULL,
                                                     py ? py->field_values(GPU_INDEX) : NULL,
                                                     pz ? pz->field_values(GPU_INDEX) : NULL,
                                                     psize->field_values(GPU_INDEX),
                                                     src.field_values(GPU_INDEX),
                                                     dest.field_values(GPU_INDEX),
                                                     dx, dy, dz,
                                                     xloface, yloface, zloface,
                                                     nparticle,
                                                     nmax[0], nmax[1], nmax[2]);


          break;
      }
      cudaDeviceSynchronize();
      GPU_ERROR_CHECK(cudaGetLastError());
    }

    //------------------------------------------------------------------

    template<class CellField, class ParticleField>
    __host__ inline void _c2p_apply_to_field( const ParticleField  *px,
                                              const ParticleField  *py,
                                              const ParticleField  *pz,
                                              const ParticleField  *psize,
                                              const CellField& src,
                                              ParticleField& dest,
                                              const double& dx,
                                              const double& dy,
                                              const double& dz,
                                              const double& xlo,
                                              const double& ylo,
                                              const double& zlo,
                                              const InterpOptions& method )
    {
      const IntVec& ngm = src.get_ghost_data().get_minus();
      const IntVec& nmax = src.window_with_ghost().extent();

      const double xloface = xlo - dx*ngm[0] - dx/2;
      const double yloface = ylo - dy*ngm[1] - dy/2;
      const double zloface = zlo - dz*ngm[2] - dz/2;

      cudaError err;
      err = cudaSetDevice( dest.active_device_index() );

      if( cudaSuccess != err ){
        throw( std::runtime_error( cudaGetErrorString(err) ) );
      }

      // Obtaing the number of particles
      const size_t nparticle = dest.window_with_ghost().local_npts();

      // Calcuate the number of Blocks and Threads
      const size_t nblock = nparticle / INNER_BLOCK_ONE_DIM + 1;
      const size_t nthread =  (nparticle > INNER_BLOCK_ONE_DIM) ? INNER_BLOCK_ONE_DIM : nparticle;

      dest <<= 0.0;
      switch (method) {
        case NO_INTERPOLATE:
          _c2p_apply_to_field_ni<<<nblock,nthread>>>(px ? px->field_values(GPU_INDEX) : NULL,
                                                     py ? py->field_values(GPU_INDEX) : NULL,
                                                     pz ? pz->field_values(GPU_INDEX) : NULL,
                                                     src.field_values(GPU_INDEX),
                                                     dest.field_values(GPU_INDEX),
                                                     dx, dy, dz,
                                                     xloface, yloface, zloface,
                                                     nparticle,
                                                     nmax[0], nmax[1], nmax[2]);

          break;
        case INTERPOLATE:

          _c2p_apply_to_field_in<<<nblock,nthread>>>(px ? px->field_values(GPU_INDEX) : NULL,
                                                     py ? py->field_values(GPU_INDEX) : NULL,
                                                     pz ? pz->field_values(GPU_INDEX) : NULL,
                                                     psize->field_values(GPU_INDEX),
                                                     src.field_values(GPU_INDEX),
                                                     dest.field_values(GPU_INDEX),
                                                     dx, dy, dz,
                                                     xloface, yloface, zloface,
                                                     nparticle,
                                                     nmax[0], nmax[1], nmax[2]);

          break;
      }
      cudaDeviceSynchronize();
      GPU_ERROR_CHECK(cudaGetLastError());
    }

    //-------------------------------------------------------------------------

  }
}

#endif /* ParticleOperatorsCuda_h */
#endif /* SpatialOps_ENABLE_CUDA */
