//define USE_INDEPENDENT_FIELDS to throw in fields which are not dependent on
//others to test stream concurrency using nvvp or some other method.  Not
//necessary to test synchronization is working in general.
//#define USE_INDEPENDENT_FIELDS

#include <spatialops/Nebo.h>
#include <test/TestHelper.h>
#include <spatialops/structured/FieldHelper.h>
#include <spatialops/structured/FieldComparisons.h>

#include <iostream>

#include <boost/program_options.hpp>
namespace po = boost::program_options;

using namespace SpatialOps;
using std::cout;
using std::endl;

/**
 * @brief Sets up testing framework based off of user input and runs multiple
 * GPU kernals that depend on each other to test that the final calculation is
 * not affected by a race condition.
 *
 */
int main( int iarg, char* carg[] )
{
    int nx, ny, nz;
    IntVec bcplus ( false, false, false );
    IntVec bcminus( false, false, false );

    po::options_description desc("Supported Options");
    desc.add_options()
        ( "help", "print help message\n" )
        ( "nx",   po::value<int>(&nx)->default_value(10), "number of points in x-dir for base mesh" )
        ( "ny",   po::value<int>(&ny)->default_value(10), "number of points in y-dir for base mesh" )
        ( "nz",   po::value<int>(&nz)->default_value(10), "number of points in z-dir for base mesh" )
        ( "bcx-",  "physical boundary on -x side?" )
        ( "bcx+",  "physical boundary on +x side?" )
        ( "bcy-",  "physical boundary on -y side?" )
        ( "bcy+",  "physical boundary on +y side?" )
        ( "bcz-",  "physical boundary on -z side?" )
        ( "bcz+",  "physical boundary on +z side?" );

    po::variables_map args;
    po::store( po::parse_command_line(iarg,carg,desc), args );
    po::notify(args);

    if( args.count("bcx-") ) bcminus[0] = true;
    if( args.count("bcy-") ) bcminus[1] = true;
    if( args.count("bcz-") ) bcminus[2] = true;
    if( args.count("bcx+") ) bcplus [0] = true;
    if( args.count("bcy+") ) bcplus [1] = true;
    if( args.count("bcz+") ) bcplus [2] = true;

    if( args.count("help") ){
      cout << desc << endl
           << "Examples:" << endl
           << " test_nebo --nx 5 --ny 10 --nz 3 --bcx" << endl
           << " test_nebo --bcx --bcy --bcz" << endl
           << " test_nebo --nx 50 --bcz" << endl
           << endl;
      return -1;
    }

    TestHelper status(true);

    typedef SVolField Field;

    GhostData ghost(1);
    BoundaryCellInfo bcinfo(BoundaryCellInfo::build<Field>(bcminus,bcplus));
    MemoryWindow window(get_window_with_ghost(IntVec(nx, ny, nz), ghost, bcinfo));

    size_t allFields_i = 0;
    static const size_t DEPENDENT_FIELDS = 10000;
#ifdef USE_INDEPENDENT_FIELDS
    static const size_t INDEPENDENT_FIELDS = 300;
    static const size_t ALL_FIELDS = DEPENDENT_FIELDS + INDEPENDENT_FIELDS;
#else
    static const size_t ALL_FIELDS = DEPENDENT_FIELDS;
#endif
    Field* allFields[ALL_FIELDS];

    //Set up fields
    Field* dependentFields[DEPENDENT_FIELDS];
    for(int i = 0; i < DEPENDENT_FIELDS; i++)
    {
      dependentFields[i] = new Field(window, bcinfo, ghost, NULL, InternalStorage, GPU_INDEX);
      *dependentFields[i] <<= 0;

      allFields[allFields_i++] = dependentFields[i];
    }
#ifdef USE_INDEPENDENT_FIELDS
    Field* independentFields[INDEPENDENT_FIELDS];
    for(int i = 0; i < INDEPENDENT_FIELDS; i++)
    {
      independentFields[i] = new Field(window, bcinfo, ghost, NULL, InternalStorage, GPU_INDEX);
      *independentFields[i] <<= i;

      allFields[allFields_i++] = independentFields[i];
    }
#endif

    //Assign streams
    cudaStream_t streams[ALL_FIELDS];
    for(int i = 0; i < ALL_FIELDS; i++)
    {
      cudaStreamCreate(&streams[i]);
      allFields[i]->set_stream(streams[i]);
    }

    //Run calculations
#ifdef USE_INDEPENDENT_FIELDS
    for(int i = 0; i < INDEPENDENT_FIELDS; i += 3)
    {
      *independentFields[i+2] <<= *independentFields[i+1] / *independentFields[i];
    }
#endif
    *dependentFields[0] <<= 1;
    for(int i = 0; i < DEPENDENT_FIELDS-1; i++)
    {
      *dependentFields[i+1] <<= *dependentFields[i] + 1;
    }

    //Compare answer
    status(field_equal(DEPENDENT_FIELDS, *dependentFields[DEPENDENT_FIELDS-1]));

    //Cleanup
    for(int i = 0; i < ALL_FIELDS; i++)
    {
      cudaStreamDestroy(streams[i]);
      delete allFields[i];
    }


    if( status.ok() ) {
      cout << "ALL TESTS PASSED :)" << endl;
      return 0;
    }
    else {
        cout << "******************************" << endl
             << " At least one test FAILED! :(" << endl
             << "******************************" << endl;
        return -1;
    };
}
