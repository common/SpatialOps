#include <set>
#include <sstream>
#include <iostream>
#include <algorithm>
using std::cout;
using std::endl;
#include <iomanip>
#include <spatialops/structured/FVStaggeredFieldTypes.h>
#include <spatialops/particles/ParticleOperators.h>
#include <spatialops/structured/FieldHelper.h>
#include <spatialops/structured/MatVecFields.h>
#include <spatialops/util/TimeLogger.h>
#include <test/TestHelper.h>
#include <stdio.h>
#include <limits.h>
#include <spatialops/structured/FieldComparisons.h>

#include <spatialops/structured/ExternalAllocators.h>

#include <cfloat>
#include <stdio.h>
#include <fstream>
#include <stdlib.h>

using namespace SpatialOps;
using namespace SpatialOps::Particle;
using std::cout;
using std::endl;

#include <boost/program_options.hpp>
namespace po = boost::program_options;

#define DISTANCE(i,j) (pCoord.at(0)[i] - pCoord.at(0)[j]) * (pCoord.at(0)[i] - pCoord.at(0)[j]) + (pCoord.at(1)[i] - pCoord.at(1)[j]) * (pCoord.at(1)[i] - pCoord.at(1)[j])

int main( int iarg, char* carg[] )
{
    int nparticles;

    double h = 1.0f;

    bool sort = false, bf = false;

    DoubleVec gridMin (-1, -1, -1);
    DoubleVec gridMax (10, 10, 1);

    int fluid_particles(250);
    double height(5); double width(1);    // Dam Dimensions
    double dist(sqrt(height*width/fluid_particles));  // Distance (horizontally and vertically) between particles
    int xparticles(width/dist); int yparticles(height/dist); // number of particles in one row of dam
    int domain_height(5); int domain_width(5);
    h=2.5*dist;

    // initializing Dam particle positions*:
    std::vector<DoubleVec> positions;
    std::vector<IntVec> maskSet;

    double ppx,ppy,ppz;

    int i=0;
    for (int dx = 0; dx <= xparticles; dx++) // arranged such that it loops from bot left to top right (going from bottom to top)
        for (int dy = 0; dy <= yparticles; dy++)
        {
            ppx = dist*dx;
            ppy = dist*dy;
            ppz = 0;

            positions.push_back(DoubleVec(ppx,ppy,ppz));

            i++;
        }

    fluid_particles=i; // redefine number of particles
    i=i+1;

    // initializing Boundary Dam positions*:
    int d=0;
    int xbp ((domain_width+dist/1.5)/(dist));

    for (int dx = 0; dx <= xbp ; dx++) //bottom section
        for (int dy = 0; dy <= 1; dy++)
        {

            ppx = -dist/3+dist*(dx)+dist/2*(dy);
            ppy = -dist*(dy)-dist;
            ppz = 0;
            positions.push_back(DoubleVec(ppx,ppy,ppz));
            i++;
            d++;
        }

    int ybp (((domain_height+4.0*dist)/(dist)));

    for (int j = 0; j <= 1 ; j++)
        for (int dx = 0; dx <= 1 ; dx++)
            for (int dy = 0; dy <= ybp; dy++)
            {
                if (j==0){                                       // left wall
                    ppx = -dist*dx -dist;
                    ppy = -2*dist+dist*dy+dist/2*dx;
                    ppz = 0;
                    positions.push_back(DoubleVec(ppx,ppy,ppz));
                    i++;
                }
                else{
                    ppx = domain_width + dist*dx + dist;
                    ppy = -2*dist+dist*dy+dist/2*dx;
                    ppz = 0;
                    positions.push_back(DoubleVec(ppx,ppy,ppz));
                    i++;
                }
                d++;
            }



    int boundary_particles=d; // redefine number of boundary particles
    nparticles=fluid_particles+boundary_particles; // total number of particles

    const GhostData pg(0);
    const BoundaryCellInfo pbc = BoundaryCellInfo::build<SpatialOps::Particle::ParticleField>();
    const MemoryWindow pmwRS( get_window_with_ghost( IntVec(nparticles,0,0), pg, pbc) );

    FieldVector<ParticleField> properties (3, pmwRS, pbc, pg);
    FieldVector<ParticleField> out (9, pmwRS, pbc, pg);
    FieldVector<ParticleField> pCoord (3, pmwRS, pbc, pg);

    for (int i= 0; i < nparticles; i++)
    {

        pCoord.at(0)[i]    = positions[i][0];
        pCoord.at(1)[i]    = positions[i][1];
        pCoord.at(2)[i]    = positions[i][2];

        if (i>=fluid_particles){
            maskSet.push_back( IntVec(i, 0, 0) );
        }
    }

    SpatialMask<ParticleField> maskx( pCoord.at(0), maskSet );
    SpatialMask<ParticleField> masky( pCoord.at(1), maskSet );
    SpatialMask<ParticleField> maskz( pCoord.at(2), maskSet );


    // this loop initializes the vector of properties of size 4 with random values
    for (size_t i = 0; i < properties.elements(); i++)
    {
        if (i!=0)
            initialize_field(properties.at(i), 0.0, false, i * 4.251);
        else
            properties.at(i) <<= 0.0;
    }


    // initializes the output properties in the vector of fields of size 2 with 0
    for (size_t i = 0; i < out.elements(); i++)
    {
        out.at(i) <<= 0.0;
    }

    short int location = CPU_INDEX;

# ifdef SpatialOps_ENABLE_CUDA
    pCoord.set_device_as_active(GPU_INDEX);
    properties.set_device_as_active(GPU_INDEX);
    out.set_device_as_active(GPU_INDEX);
    maskx.add_consumer(GPU_INDEX);
    masky.add_consumer(GPU_INDEX);
    maskz.add_consumer(GPU_INDEX);
    location = GPU_INDEX;
# endif

    double rho0(1000.0); double mass_p=(height*width*rho0/fluid_particles); // mass of each particle

    double pi(3.14);
    double c0 =10.0*sqrt(2*9.81*height); // reference speed of sound usually defined as 10*max(velocity)

    h=h/2;

    TestHelper status(true);

    RangeSearch rangeSearch(pmwRS, pg, pbc, gridMin, gridMax, nparticles, h, pCoord.at(0), pCoord.at(1), !bf, sort,location);


    // Simple mapped reduction
    out.at(0) <<= nebo_mapped_reduction ( properties.at(1), rangeSearch.GetCurrentState(location));

    // Mapped reduction with local() operator
    out.at(1) <<= nebo_mapped_reduction ( local(properties.at(2)), rangeSearch.GetCurrentState(location));

    // Mapped reduction with mapped_value() operator
    out.at(2) <<= nebo_mapped_reduction ( mapped_value(), rangeSearch.GetCurrentState(location));

    // Compound statement within a mapped reduction
    out.at(6) <<= nebo_mapped_reduction ( properties.at(1) * local(properties.at(2)) - mapped_value(), rangeSearch.GetCurrentState(location));

    // Complex computation
    out.at(7) <<= nebo_mapped_reduction (mass_p*
                                             ((local(properties.at(1)) - properties.at(1))*(local(pCoord.at(0))-pCoord.at(0)) // (xvelocity_ij * x_ij + ...
                                              + (local(properties.at(2)) - properties.at(2))*(local(pCoord.at(1))-pCoord.at(1))) // yvelocity_ij * y_ij) * ...
                                             *
                                             (cond( (mapped_value() > pow(10,-12)), // to avoid NaNs
                                                   cond( (mapped_value()/h <= 2),
                                                        ( 7/(4*pi*pow(h,3))) * // alpha *
                                                        (-5*mapped_value()/h    * // -5*r.ij/h *
                                                         pow(    1   -   .5  *  mapped_value()/h     ,3)) // (1- 0.5*r.ij/h)^3 /
                                                        /mapped_value() // rij
                                                        )
                                                   (0)) // else (0)
                                              (0)) // else 0                   dW/dr
                                             , rangeSearch.GetCurrentState(location));
#ifdef SpatialOps_ENABLE_CUDA
    if (location == GPU_INDEX) {
        out.set_device_as_active(CPU_INDEX);
        properties.set_device_as_active(CPU_INDEX);
        pCoord.set_device_as_active(CPU_INDEX);
    }
#endif

    for (int i=0; i < nparticles; i++)
    {
        for (int j=0; j < nparticles; j++)
        {
            double d = DISTANCE(i,j);
        
            if (d >= 0.0f && d <= h * h) {
                out.at(3)[i] += properties.at(1)[j];
                out.at(4)[i] += properties.at(2)[i];
                out.at(5)[i] += std::sqrt(d);
                out.at(8)[i] += properties.at(1)[j] * properties.at(2)[i] - std::sqrt(d);
            }
        }
    }  

    status(field_equal(out.at(0), out.at(3), 1e-10), "Simple Nebo Mapped Reduction test");
    status(field_equal(out.at(1), out.at(4), 0), "Nebo Mapped Reduction with local() operator test");
    status(field_equal(out.at(2), out.at(5), 1e-10), "Nebo mapped reduction with mapped_value() operator test");
    status(field_equal(out.at(6), out.at(8), 1e-10), "Nebo mapped reduction with a compound statement");
    
    if( status.ok() ) {
      cout << "ALL TESTS PASSED :)" << endl;
      return 0;
    }
    else {
        cout << "******************************" << endl
             << " At least one test FAILED! :(" << endl
             << "******************************" << endl;
        return -1;
    };

};

