#include <spatialops/SpatialOpsTools.h>
#include <spatialops/OperatorDatabase.h>

#include <spatialops/structured/stencil/FVStaggeredOperatorTypes.h>
#include <spatialops/structured/stencil/OneSidedOperatorTypes.h>
#include <spatialops/structured/stencil/StencilBuilder.h>
#include <spatialops/NeboStencilBuilder.h>
#include <spatialops/structured/FieldComparisons.h>
#include <spatialops/structured/Grid.h>
#include <spatialops/Nebo.h>

#include <test/TestHelper.h>
#include <spatialops/structured/FieldHelper.h>

#include <boost/program_options.hpp>
namespace po = boost::program_options;

using namespace SpatialOps;

#include <stdexcept>
using std::cout;
using std::endl;

#include <algorithm>
#include <functional>
#include <type_traits>
#include <vector>

typedef SVolField FieldT;

#define STATEMENT_COUNT 1

//#define COMPILE_PLUS_STATEMENT
//#define COMPILE_VARYING_STENCIL
//#define COMPILE_NINE_POINT_STENCIL
//#define COMPILE_FOUR_POINT_STENCIL
//#define COMPILE_TWO_POINT_STENCIL
//#define COMPILE_REDUCTION_SUM

//--------------------------------------------------------------------

#if defined(COMPILE_NINE_POINT_STENCIL) || defined(COMPILE_VARYING_STENCIL) || defined(COMPILE_FOUR_POINT_STENCIL)
#define CUSTOM_STENCIL_COMPILATION
#endif
#if defined(COMPILE_TWO_POINT_STENCIL)
#define TRADITIONAL_STENCIL_COMPILATION
#endif

//--------------------------------------------------------------------

#define MACRO_CAT(A, B) A##B

#define OP_PAREN(OP, TEXT) (TEXT)
#define OP_BETWEEN(BETWEEN, TEXT) BETWEEN TEXT

#define REPEAT_EXPRESSION_SUMMED(N, EXPRESSION, ARG) MACRO_CAT(REPEAT_EXPRESSION_,N)(+, OP_BETWEEN, EXPRESSION OP_PAREN(,ARG), EXPRESSION OP_PAREN(,ARG))
#define REPEAT_EXPRESSION_NESTED(N, EXPRESSION, ARG) MACRO_CAT(REPEAT_EXPRESSION_,N)(, OP_PAREN, EXPRESSION, ARG)
#define REPEAT_EXPRESSION_OP(N, BETWEEN, ARG) MACRO_CAT(REPEAT_EXPRESSION_,N)(BETWEEN, OP_BETWEEN, ARG, ARG)

#define REPEAT_EXPRESSION_1(BETWEEN, OP, EXPRESSION, ARG) EXPRESSION OP(BETWEEN, ARG)
#define REPEAT_EXPRESSION_2(BETWEEN, OP, EXPRESSION, ARG) EXPRESSION OP(BETWEEN, REPEAT_EXPRESSION_1(BETWEEN, OP, EXPRESSION, ARG) )
#define REPEAT_EXPRESSION_3(BETWEEN, OP, EXPRESSION, ARG) EXPRESSION OP(BETWEEN, REPEAT_EXPRESSION_2(BETWEEN, OP, EXPRESSION, ARG) )
#define REPEAT_EXPRESSION_4(BETWEEN, OP, EXPRESSION, ARG) EXPRESSION OP(BETWEEN, REPEAT_EXPRESSION_3(BETWEEN, OP, EXPRESSION, ARG) )
#define REPEAT_EXPRESSION_5(BETWEEN, OP, EXPRESSION, ARG) EXPRESSION OP(BETWEEN, REPEAT_EXPRESSION_4(BETWEEN, OP, EXPRESSION, ARG) )
#define REPEAT_EXPRESSION_6(BETWEEN, OP, EXPRESSION, ARG) EXPRESSION OP(BETWEEN, REPEAT_EXPRESSION_5(BETWEEN, OP, EXPRESSION, ARG) )
#define REPEAT_EXPRESSION_7(BETWEEN, OP, EXPRESSION, ARG) EXPRESSION OP(BETWEEN, REPEAT_EXPRESSION_6(BETWEEN, OP, EXPRESSION, ARG) )
#define REPEAT_EXPRESSION_8(BETWEEN, OP, EXPRESSION, ARG) EXPRESSION OP(BETWEEN, REPEAT_EXPRESSION_7(BETWEEN, OP, EXPRESSION, ARG) )
#define REPEAT_EXPRESSION_9(BETWEEN, OP, EXPRESSION, ARG) EXPRESSION OP(BETWEEN, REPEAT_EXPRESSION_8(BETWEEN, OP, EXPRESSION, ARG) )
#define REPEAT_EXPRESSION_10(BETWEEN, OP, EXPRESSION, ARG) EXPRESSION OP(BETWEEN, REPEAT_EXPRESSION_9(BETWEEN, OP, EXPRESSION, ARG) )
#define REPEAT_EXPRESSION_11(BETWEEN, OP, EXPRESSION, ARG) EXPRESSION OP(BETWEEN, REPEAT_EXPRESSION_10(BETWEEN, OP, EXPRESSION, ARG) )
#define REPEAT_EXPRESSION_12(BETWEEN, OP, EXPRESSION, ARG) EXPRESSION OP(BETWEEN, REPEAT_EXPRESSION_11(BETWEEN, OP, EXPRESSION, ARG) )
#define REPEAT_EXPRESSION_13(BETWEEN, OP, EXPRESSION, ARG) EXPRESSION OP(BETWEEN, REPEAT_EXPRESSION_12(BETWEEN, OP, EXPRESSION, ARG) )
#define REPEAT_EXPRESSION_14(BETWEEN, OP, EXPRESSION, ARG) EXPRESSION OP(BETWEEN, REPEAT_EXPRESSION_13(BETWEEN, OP, EXPRESSION, ARG) )
#define REPEAT_EXPRESSION_15(BETWEEN, OP, EXPRESSION, ARG) EXPRESSION OP(BETWEEN, REPEAT_EXPRESSION_14(BETWEEN, OP, EXPRESSION, ARG) )
#define REPEAT_EXPRESSION_16(BETWEEN, OP, EXPRESSION, ARG) EXPRESSION OP(BETWEEN, REPEAT_EXPRESSION_15(BETWEEN, OP, EXPRESSION, ARG) )
#define REPEAT_EXPRESSION_17(BETWEEN, OP, EXPRESSION, ARG) EXPRESSION OP(BETWEEN, REPEAT_EXPRESSION_16(BETWEEN, OP, EXPRESSION, ARG) )
#define REPEAT_EXPRESSION_18(BETWEEN, OP, EXPRESSION, ARG) EXPRESSION OP(BETWEEN, REPEAT_EXPRESSION_17(BETWEEN, OP, EXPRESSION, ARG) )
#define REPEAT_EXPRESSION_19(BETWEEN, OP, EXPRESSION, ARG) EXPRESSION OP(BETWEEN, REPEAT_EXPRESSION_18(BETWEEN, OP, EXPRESSION, ARG) )
#define REPEAT_EXPRESSION_20(BETWEEN, OP, EXPRESSION, ARG) EXPRESSION OP(BETWEEN, REPEAT_EXPRESSION_19(BETWEEN, OP, EXPRESSION, ARG) )
#define REPEAT_EXPRESSION_21(BETWEEN, OP, EXPRESSION, ARG) EXPRESSION OP(BETWEEN, REPEAT_EXPRESSION_20(BETWEEN, OP, EXPRESSION, ARG) )
#define REPEAT_EXPRESSION_22(BETWEEN, OP, EXPRESSION, ARG) EXPRESSION OP(BETWEEN, REPEAT_EXPRESSION_21(BETWEEN, OP, EXPRESSION, ARG) )
#define REPEAT_EXPRESSION_23(BETWEEN, OP, EXPRESSION, ARG) EXPRESSION OP(BETWEEN, REPEAT_EXPRESSION_22(BETWEEN, OP, EXPRESSION, ARG) )
#define REPEAT_EXPRESSION_24(BETWEEN, OP, EXPRESSION, ARG) EXPRESSION OP(BETWEEN, REPEAT_EXPRESSION_23(BETWEEN, OP, EXPRESSION, ARG) )
#define REPEAT_EXPRESSION_25(BETWEEN, OP, EXPRESSION, ARG) EXPRESSION OP(BETWEEN, REPEAT_EXPRESSION_24(BETWEEN, OP, EXPRESSION, ARG) )
#define REPEAT_EXPRESSION_26(BETWEEN, OP, EXPRESSION, ARG) EXPRESSION OP(BETWEEN, REPEAT_EXPRESSION_25(BETWEEN, OP, EXPRESSION, ARG) )
#define REPEAT_EXPRESSION_27(BETWEEN, OP, EXPRESSION, ARG) EXPRESSION OP(BETWEEN, REPEAT_EXPRESSION_26(BETWEEN, OP, EXPRESSION, ARG) )
#define REPEAT_EXPRESSION_28(BETWEEN, OP, EXPRESSION, ARG) EXPRESSION OP(BETWEEN, REPEAT_EXPRESSION_27(BETWEEN, OP, EXPRESSION, ARG) )
#define REPEAT_EXPRESSION_29(BETWEEN, OP, EXPRESSION, ARG) EXPRESSION OP(BETWEEN, REPEAT_EXPRESSION_28(BETWEEN, OP, EXPRESSION, ARG) )
#define REPEAT_EXPRESSION_30(BETWEEN, OP, EXPRESSION, ARG) EXPRESSION OP(BETWEEN, REPEAT_EXPRESSION_29(BETWEEN, OP, EXPRESSION, ARG) )
#define REPEAT_EXPRESSION_31(BETWEEN, OP, EXPRESSION, ARG) EXPRESSION OP(BETWEEN, REPEAT_EXPRESSION_30(BETWEEN, OP, EXPRESSION, ARG) )
#define REPEAT_EXPRESSION_32(BETWEEN, OP, EXPRESSION, ARG) EXPRESSION OP(BETWEEN, REPEAT_EXPRESSION_31(BETWEEN, OP, EXPRESSION, ARG) )
#define REPEAT_EXPRESSION_33(BETWEEN, OP, EXPRESSION, ARG) EXPRESSION OP(BETWEEN, REPEAT_EXPRESSION_32(BETWEEN, OP, EXPRESSION, ARG) )
#define REPEAT_EXPRESSION_34(BETWEEN, OP, EXPRESSION, ARG) EXPRESSION OP(BETWEEN, REPEAT_EXPRESSION_33(BETWEEN, OP, EXPRESSION, ARG) )
#define REPEAT_EXPRESSION_35(BETWEEN, OP, EXPRESSION, ARG) EXPRESSION OP(BETWEEN, REPEAT_EXPRESSION_34(BETWEEN, OP, EXPRESSION, ARG) )
#define REPEAT_EXPRESSION_36(BETWEEN, OP, EXPRESSION, ARG) EXPRESSION OP(BETWEEN, REPEAT_EXPRESSION_35(BETWEEN, OP, EXPRESSION, ARG) )
#define REPEAT_EXPRESSION_37(BETWEEN, OP, EXPRESSION, ARG) EXPRESSION OP(BETWEEN, REPEAT_EXPRESSION_36(BETWEEN, OP, EXPRESSION, ARG) )
#define REPEAT_EXPRESSION_38(BETWEEN, OP, EXPRESSION, ARG) EXPRESSION OP(BETWEEN, REPEAT_EXPRESSION_37(BETWEEN, OP, EXPRESSION, ARG) )
#define REPEAT_EXPRESSION_39(BETWEEN, OP, EXPRESSION, ARG) EXPRESSION OP(BETWEEN, REPEAT_EXPRESSION_38(BETWEEN, OP, EXPRESSION, ARG) )
#define REPEAT_EXPRESSION_40(BETWEEN, OP, EXPRESSION, ARG) EXPRESSION OP(BETWEEN, REPEAT_EXPRESSION_39(BETWEEN, OP, EXPRESSION, ARG) )
#define REPEAT_EXPRESSION_41(BETWEEN, OP, EXPRESSION, ARG) EXPRESSION OP(BETWEEN, REPEAT_EXPRESSION_40(BETWEEN, OP, EXPRESSION, ARG) )
#define REPEAT_EXPRESSION_42(BETWEEN, OP, EXPRESSION, ARG) EXPRESSION OP(BETWEEN, REPEAT_EXPRESSION_41(BETWEEN, OP, EXPRESSION, ARG) )
#define REPEAT_EXPRESSION_43(BETWEEN, OP, EXPRESSION, ARG) EXPRESSION OP(BETWEEN, REPEAT_EXPRESSION_42(BETWEEN, OP, EXPRESSION, ARG) )
#define REPEAT_EXPRESSION_44(BETWEEN, OP, EXPRESSION, ARG) EXPRESSION OP(BETWEEN, REPEAT_EXPRESSION_43(BETWEEN, OP, EXPRESSION, ARG) )
#define REPEAT_EXPRESSION_45(BETWEEN, OP, EXPRESSION, ARG) EXPRESSION OP(BETWEEN, REPEAT_EXPRESSION_44(BETWEEN, OP, EXPRESSION, ARG) )
#define REPEAT_EXPRESSION_46(BETWEEN, OP, EXPRESSION, ARG) EXPRESSION OP(BETWEEN, REPEAT_EXPRESSION_45(BETWEEN, OP, EXPRESSION, ARG) )
#define REPEAT_EXPRESSION_47(BETWEEN, OP, EXPRESSION, ARG) EXPRESSION OP(BETWEEN, REPEAT_EXPRESSION_46(BETWEEN, OP, EXPRESSION, ARG) )
#define REPEAT_EXPRESSION_48(BETWEEN, OP, EXPRESSION, ARG) EXPRESSION OP(BETWEEN, REPEAT_EXPRESSION_47(BETWEEN, OP, EXPRESSION, ARG) )
#define REPEAT_EXPRESSION_49(BETWEEN, OP, EXPRESSION, ARG) EXPRESSION OP(BETWEEN, REPEAT_EXPRESSION_48(BETWEEN, OP, EXPRESSION, ARG) )
#define REPEAT_EXPRESSION_50(BETWEEN, OP, EXPRESSION, ARG) EXPRESSION OP(BETWEEN, REPEAT_EXPRESSION_49(BETWEEN, OP, EXPRESSION, ARG) )

//--------------------------------------------------------------------

namespace
{
#ifdef CUSTOM_STENCIL_COMPILATION
  typedef XDIR DirT;
  typedef UnitTriplet<DirT>::type DirTripletT;
  typedef Multiply<IndexTriplet<2, 2, 2>, DirTripletT>::result TwoDirTripletT;
  typedef Multiply<IndexTriplet<3, 3, 3>, DirTripletT>::result ThreeDirTripletT;
  typedef Multiply<IndexTriplet<4, 4, 4>, DirTripletT>::result FourDirTripletT;
#endif
#ifdef CUSTOM_STENCIL_COMPILATION
  //Main Point
  namespace
  {
    typedef NEBO_FIRST_IJK( 0, 0, 0)
          ::NEBO_ADD_POINT  ( DirTripletT )
          ::NEBO_ADD_POINT  ( TwoDirTripletT )
          ::NEBO_ADD_POINT  ( ThreeDirTripletT )
          ::NEBO_ADD_POINT  ( FourDirTripletT )
          ::NEBO_ADD_POINT  ( DirTripletT::Negate )
          ::NEBO_ADD_POINT  ( TwoDirTripletT::Negate )
          ::NEBO_ADD_POINT  ( ThreeDirTripletT::Negate )
          ::NEBO_ADD_POINT  ( FourDirTripletT::Negate )
      MainPointStencilCollectionT;
    typedef NeboStencilBuilder<Gradient,
                               MainPointStencilCollectionT,
                               FieldT,
                               FieldT>
            MainPointStencilT;
    const NeboStencilCoefCollection<MainPointStencilCollectionT::length> mainCoefs
      = build_coef_collection(1.0)(1.0)(1.0)(1.0)(1.0)(1.0)(1.0)(1.0)(1.0);
  }
#endif
#ifdef COMPILE_VARYING_STENCIL
  //One Point
  namespace
  {
    //Positive
    namespace
    {
      typedef NEBO_FIRST_IJK( 0, 0, 0)
        OnePointPositiveStencilCollectionT;
        typedef NeboStencilBuilder<Gradient,
                                   OnePointPositiveStencilCollectionT,
                                   FieldT,
                                   FieldT>
                PBasicOneT;
        const NeboStencilCoefCollection<OnePointPositiveStencilCollectionT::length> pCoefOne
          = build_coef_collection(1.0);
    }
    //Negative
    namespace
    {
      typedef OnePointPositiveStencilCollectionT OnePointNegativeStencilCollectionT;
      typedef PBasicOneT NBasicOneT;
      const NeboStencilCoefCollection<OnePointPositiveStencilCollectionT::length> nCoefOne
        = pCoefOne;
    }
  }
  //Two Point One Sided
  namespace
  {
    //Two Point Positive
    namespace
    {
      typedef NEBO_FIRST_IJK( 0, 0, 0)
            ::NEBO_ADD_POINT  ( DirTripletT )
            //::NEBO_ADD_POINT  ( TwoDirTripletT )
        TwoPointPositiveStencilCollectionT;
        typedef NeboStencilBuilder<Gradient,
                                   TwoPointPositiveStencilCollectionT,
                                   FieldT,
                                   FieldT>
                PBasicTwoT;
        const NeboStencilCoefCollection<TwoPointPositiveStencilCollectionT::length> pCoefTwo
          = build_coef_collection(1.0)(1.0);
    }
    //Two Point Negative
    namespace
    {
      typedef NEBO_FIRST_IJK( 0, 0, 0)
            ::NEBO_ADD_POINT  ( DirTripletT::Negate )
            //::NEBO_ADD_POINT  ( TwoDirTripletT::Negate )
        TwoPointNegativeStencilCollectionT;
        typedef NeboStencilBuilder<Gradient,
                                   TwoPointNegativeStencilCollectionT,
                                   FieldT,
                                   FieldT>
                NBasicTwoT;
        const NeboStencilCoefCollection<TwoPointPositiveStencilCollectionT::length> nCoefTwo
          = pCoefTwo;
    }
  }
  //Three Point Centered
  namespace
  {
    //Point Positive
    namespace
    {
      typedef NEBO_FIRST_IJK( 0, 0, 0)
            ::NEBO_ADD_POINT  ( DirTripletT )
            ::NEBO_ADD_POINT  ( DirTripletT::Negate )
        ThreePointPositiveStencilCollectionT;
        typedef NeboStencilBuilder<Gradient,
                                   ThreePointPositiveStencilCollectionT,
                                   FieldT,
                                   FieldT>
                PBasicThreeT;
        const NeboStencilCoefCollection<ThreePointPositiveStencilCollectionT::length> pCoefThree
          = build_coef_collection(1.0)(1.0)(1.0);
    }
    //Negative
    namespace
    {
      typedef ThreePointPositiveStencilCollectionT ThreePointNegativeStencilCollectionT;
      typedef PBasicThreeT NBasicThreeT;
        const NeboStencilCoefCollection<ThreePointPositiveStencilCollectionT::length> nCoefThree
          = pCoefThree;
    }
  }
  //Four Point Slightly Centered
  namespace
  {
#endif /* COMPILE_VARYING_STENCIL */
#if defined(COMPILE_VARYING_STENCIL) || defined(COMPILE_FOUR_POINT_STENCIL)
    //Point Positive
    namespace
    {
      typedef NEBO_FIRST_IJK( 0, 0, 0)
            ::NEBO_ADD_POINT  ( DirTripletT )
            ::NEBO_ADD_POINT  ( TwoDirTripletT )
            ::NEBO_ADD_POINT  ( DirTripletT::Negate )
        FourPointPositiveStencilCollectionT;
        typedef NeboStencilBuilder<Gradient,
                                   FourPointPositiveStencilCollectionT,
                                   FieldT,
                                   FieldT>
                PBasicFourT;
        const NeboStencilCoefCollection<FourPointPositiveStencilCollectionT::length> pCoefFour
          = build_coef_collection(1.0)(1.0)(1.0)(1.0);
    }
#endif /* COMPILE_VARYING_STENCIL || COMPILE_FOUR_POINT_STENCIL */
#ifdef COMPILE_VARYING_STENCIL
    //Negative
    namespace
    {
      typedef NEBO_FIRST_IJK( 0, 0, 0)
            ::NEBO_ADD_POINT  ( DirTripletT::Negate )
            ::NEBO_ADD_POINT  ( TwoDirTripletT::Negate )
            ::NEBO_ADD_POINT  ( DirTripletT )
        FourPointNegativeStencilCollectionT;
        typedef NeboStencilBuilder<Gradient,
                                   FourPointNegativeStencilCollectionT,
                                   FieldT,
                                   FieldT>
                NBasicFourT;
        const NeboStencilCoefCollection<FourPointPositiveStencilCollectionT::length> nCoefFour
          = pCoefFour;
    }
  }

  typedef NeboGenericEmptyTypeList::AddType<NBasicOneT>::Result
                                  ::AddType<NBasicTwoT>::Result
                                  ::AddType<NBasicThreeT>::Result
                                  ::AddType<NBasicFourT>::Result
    NegativeStencilListT;


  typedef NeboGenericEmptyTypeList::AddType<PBasicOneT>::Result
                                  ::AddType<PBasicTwoT>::Result
                                  ::AddType<PBasicThreeT>::Result
                                  ::AddType<PBasicFourT>::Result
    PositiveStencilListT;


  typedef NeboVaryingEdgeStencilBuilder<DirT,
                                        FieldT,
                                        FieldT,
                                        MainPointStencilT,
                                        NegativeStencilListT,
                                        PositiveStencilListT>
          VaryEdgeOpT;
#endif /* COMPILE_VARYING_STENCIL */
#ifdef CUSTOM_STENCIL_COMPILATION
  void build_varying_stencils( const unsigned int nx,
                               const unsigned int ny,
                               const unsigned int nz,
                               const double Lx,
                               const double Ly,
                               const double Lz,
                               OperatorDatabase& opdb )
  {
    //Create main five point stencil
    {
      opdb.register_new_operator( new MainPointStencilT(mainCoefs) );
    }

#ifdef COMPILE_VARYING_STENCIL
    //Create one point stencils
    {
      opdb.register_new_operator( new PBasicOneT(pCoefOne) ); //Same type as negative
    }

    //Create two point stencils
    {
      opdb.register_new_operator( new PBasicTwoT(pCoefTwo) );
      opdb.register_new_operator( new NBasicTwoT(nCoefTwo) );
    }

    //Create three point stencils
    {
      opdb.register_new_operator( new PBasicThreeT(pCoefThree) ); //Same type as negative
    }

    //Create four point stencils
    {
      opdb.register_new_operator( new PBasicFourT(pCoefFour) );
      opdb.register_new_operator( new NBasicFourT(nCoefFour) );
    }

    {
      auto& mainStencil   = *opdb.retrieve_operator<MainPointStencilT>();
      auto& pOnePointStencil = *opdb.retrieve_operator<PBasicOneT>();
      auto& nOnePointStencil = *opdb.retrieve_operator<NBasicOneT>();
      auto& pTwoPointStencil = *opdb.retrieve_operator<PBasicTwoT>();
      auto& nTwoPointStencil = *opdb.retrieve_operator<NBasicTwoT>();
      auto& pThreePointStencil = *opdb.retrieve_operator<PBasicThreeT>();
      auto& nThreePointStencil = *opdb.retrieve_operator<NBasicThreeT>();
      auto& pFourPointStencil = *opdb.retrieve_operator<PBasicFourT>();
      auto& nFourPointStencil = *opdb.retrieve_operator<NBasicFourT>();
      opdb.register_new_operator( new VaryEdgeOpT (mainStencil,
                                                   NeboGenericEmptyTypeList()(nOnePointStencil)
                                                                             (nTwoPointStencil)
                                                                             (nThreePointStencil)
                                                                             (nFourPointStencil),
                                                   NeboGenericEmptyTypeList()(pOnePointStencil)
                                                                             (pTwoPointStencil)
                                                                             (pThreePointStencil)
                                                                             (pFourPointStencil)) );
    }
#endif /* COMPILE_VARYING_STENCIL */
  }

  void build_varying_stencils( const Grid& grid, OperatorDatabase& opDB )
  {
    build_stencils( grid.extent(0), grid.extent(1), grid.extent(2),
                    grid.length(0), grid.length(1), grid.length(2),
                    opDB );
  }
#endif /* CUSTOM_STENCIL_COMPILATION */
}

//--------------------------------------------------------------------

#ifdef COMPILE_PLUS_STATEMENT
bool Plus( IntVec i_npts )
{
  const GhostData ghost(0);

  const IntVec bcMinus(false, false, false);
  const IntVec bcPlus(false, false, false);
  const BoundaryCellInfo bcinfo = BoundaryCellInfo::build<FieldT>(bcMinus, bcPlus);
  const MemoryWindow mw = get_window_with_ghost(i_npts, ghost, bcinfo);


  FieldT x     ( mw, bcinfo, ghost, NULL, InternalStorage, CPU_INDEX );
  FieldT result( mw, bcinfo, ghost, NULL, InternalStorage, CPU_INDEX );

  x <<= 1;
  result <<= 0;
  //Timed code
  {
    result <<= REPEAT_EXPRESSION_OP(STATEMENT_COUNT, +, x);
  }
  {
    print_field(result, std::cout);
  }

  return true;
}
#endif /* COMPILE_PLUS_STATEMENT */

#ifdef COMPILE_VARYING_STENCIL
bool VaryingStencil( IntVec i_npts, OperatorDatabase const & i_opdb, Grid const & i_grid )
{
  const GhostData ghost(0);

  const IntVec bcMinus(false, false, false);
  const IntVec bcPlus(false, false, false);
  //const IntVec bcMinus(true, false, false);
  //const IntVec bcPlus(true, false, false);
  const BoundaryCellInfo bcinfo = BoundaryCellInfo::build<FieldT>(bcMinus, bcPlus);
  const MemoryWindow mw = get_window_with_ghost(i_npts, ghost, bcinfo);


  FieldT x     ( mw, bcinfo, ghost, NULL, InternalStorage, CPU_INDEX );
  FieldT result( mw, bcinfo, ghost, NULL, InternalStorage, CPU_INDEX );

  // set field values
  i_grid.set_coord<XDIR>(x);

  VaryEdgeOpT& op = *i_opdb.retrieve_operator<VaryEdgeOpT>();
  x <<= 1;
  result <<= 0;
  //Timed code
  {
    result <<= REPEAT_EXPRESSION_NESTED(STATEMENT_COUNT, op.operator(), x);
  }
  {
    print_field(result, std::cout);
  }

  return true;
}
#endif /* COMPILE_VARYING_STENCIL */

#ifdef COMPILE_NINE_POINT_STENCIL
bool NinePointStencil( IntVec i_npts, OperatorDatabase const & i_opdb, Grid const & i_grid )
{
  const GhostData ghost(0);

  const IntVec bcMinus(false, false, false);
  const IntVec bcPlus(false, false, false);
  const BoundaryCellInfo bcinfo = BoundaryCellInfo::build<FieldT>(bcMinus, bcPlus);
  const MemoryWindow mw = get_window_with_ghost(i_npts, ghost, bcinfo);

  FieldT x     ( mw, bcinfo, ghost, NULL, InternalStorage, CPU_INDEX );
  FieldT result( mw, bcinfo, ghost, NULL, InternalStorage, CPU_INDEX );

  // set field values
  i_grid.set_coord<XDIR>(x);

  MainPointStencilT& op = *i_opdb.retrieve_operator<MainPointStencilT>();
  x <<= 1;
  result <<= 0;
  //Timed code
  {
    result <<= REPEAT_EXPRESSION_NESTED(STATEMENT_COUNT, op.operator(), x);
  }
  {
    print_field(result, std::cout);
  }

  return true;
}
#endif /* COMPILE_NINE_POINT_STENCIL */
#ifdef COMPILE_FOUR_POINT_STENCIL
bool FourPointStencil( IntVec i_npts, OperatorDatabase const & i_opdb, Grid const & i_grid )
{
  const GhostData ghost(0);

  const IntVec bcMinus(false, false, false);
  const IntVec bcPlus(false, false, false);
  const BoundaryCellInfo bcinfo = BoundaryCellInfo::build<FieldT>(bcMinus, bcPlus);
  const MemoryWindow mw = get_window_with_ghost(i_npts, ghost, bcinfo);

  FieldT x     ( mw, bcinfo, ghost, NULL, InternalStorage, CPU_INDEX );
  FieldT result( mw, bcinfo, ghost, NULL, InternalStorage, CPU_INDEX );

  // set field values
  i_grid.set_coord<XDIR>(x);

  PBasicFourT& op = *i_opdb.retrieve_operator<PBasicFourT>();
  x <<= 1;
  result <<= 0;
  //Timed code
  {
    result <<= REPEAT_EXPRESSION_NESTED(STATEMENT_COUNT, op.operator(), x);
  }
  {
    print_field(result, std::cout);
  }

  return true;
}
#endif /* COMPILE_FOUR_POINT_STENCIL */
#ifdef COMPILE_TWO_POINT_STENCIL
bool TwoPointStencil( IntVec i_npts, OperatorDatabase const & i_opdb, Grid const & i_grid )
{
  const GhostData ghost(0);

  const IntVec bcMinus(false, false, false);
  const IntVec bcPlus(false, false, false);
  const BoundaryCellInfo bcinfo = BoundaryCellInfo::build<FieldT>(bcMinus, bcPlus);
  const MemoryWindow mw = get_window_with_ghost(i_npts, ghost, bcinfo);

  FieldT x     ( mw, bcinfo, ghost, NULL, InternalStorage, CPU_INDEX );
  FieldT result( mw, bcinfo, ghost, NULL, InternalStorage, CPU_INDEX );

  // set field values
  i_grid.set_coord<XDIR>(x);

  typedef OperatorTypeBuilder<GradientX, FieldT, FieldT>::type GradX;
  GradX& op = *i_opdb.retrieve_operator<GradX>();
  x <<= 1;
  result <<= 0;
  //Timed code
  {
    result <<= REPEAT_EXPRESSION_NESTED(STATEMENT_COUNT, op.operator(), x);
  }
  {
    print_field(result, std::cout);
  }

  return true;
}
#endif /* COMPILE_TWO_POINT_STENCIL */

#ifdef COMPILE_REDUCTION_SUM
bool ReductionSum( IntVec i_npts )
{
  const GhostData ghost(0);

  const IntVec bcMinus(false, false, false);
  const IntVec bcPlus(false, false, false);
  const BoundaryCellInfo bcinfo = BoundaryCellInfo::build<FieldT>(bcMinus, bcPlus);
  const MemoryWindow mw = get_window_with_ghost(i_npts, ghost, bcinfo);


  FieldT x     ( mw, bcinfo, ghost, NULL, InternalStorage, CPU_INDEX );
  FieldT result( mw, bcinfo, ghost, NULL, InternalStorage, CPU_INDEX );

  x <<= 1;
  result <<= 0;
  //Timed code
  {
    result <<= REPEAT_EXPRESSION_SUMMED(STATEMENT_COUNT, nebo_sum, x);
  }
  {
    print_field(result, std::cout);
  }

  return true;
}
#endif /* COMPILE_REDUCTION_SUM */

//--------------------------------------------------------------------

int main( int iarg, char* carg[] )
{
  int nx, ny, nz;
  double length;
  {
    po::options_description desc("Supported Options");
    desc.add_options()
          ( "help", "print help message\n" )
          ( "nx", po::value<int>   ( &nx     )->default_value( 32   ), "number of points in x-dir for base mesh" )
          ( "ny", po::value<int>   ( &ny     )->default_value( 32   ), "number of points in y-dir for base mesh" )
          ( "nz", po::value<int>   ( &nz     )->default_value( 32   ), "number of points in z-dir for base mesh" )
          ( "l",  po::value<double>( &length )->default_value( 0.01 ), "length of the domain"                    );

    po::variables_map args;
    po::store( po::parse_command_line(iarg,carg,desc), args );
    po::notify(args);

    if( args.count("help") ){
      cout << desc << endl
          << "Example:" << endl
          << "  test_varying_edge_stencil --nx 5 --ny 10 --nz 3 " << endl
          << endl;
      return -1;
    }

  }


  TestHelper status( true );
  const IntVec npts(nx,ny,nz);

  {
    cout << "  domain : " << npts << endl
         << endl;
  }

#ifdef TRADITIONAL_STENCIL_COMPILATION
  OperatorDatabase opdb1;
  build_stencils        ( npts[0], npts[1], npts[2], length, length, length, opdb1 );
#endif
#ifdef CUSTOM_STENCIL_COMPILATION
  OperatorDatabase opdb2;
  build_varying_stencils( npts[0], npts[1], npts[2], length, length, length, opdb2 );
#endif
#if defined(CUSTOM_STENCIL_COMPILATION) || defined(TRADITIONAL_STENCIL_COMPILATION)
  const Grid grid( npts, DoubleVec(length, length, length) );
#endif

#ifdef COMPILE_PLUS_STATEMENT
  status( Plus(npts) );
#endif
#ifdef COMPILE_VARYING_STENCIL
  status( VaryingStencil(npts, opdb2, grid) );
#endif
#ifdef COMPILE_NINE_POINT_STENCIL
  status( NinePointStencil(npts, opdb2, grid) );
#endif
#ifdef COMPILE_FOUR_POINT_STENCIL
  status( FourPointStencil(npts, opdb2, grid) );
#endif
#ifdef COMPILE_TWO_POINT_STENCIL
  status( TwoPointStencil(npts, opdb1, grid) );
#endif
#ifdef COMPILE_REDUCTION_SUM
  status( ReductionSum(npts) );
#endif

  if( status.ok() ){
    cout << "Tests passed" << endl;
    return 0;
  }
  else {
    cout << "******************************" << endl
        << "At least one test did not pass" << endl
        << "******************************" << endl;
    return -1;
  }
}

//--------------------------------------------------------------------

