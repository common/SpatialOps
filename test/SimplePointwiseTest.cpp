#include <spatialops/structured/FVStaggeredFieldTypes.h>

#include <spatialops/Nebo.h>
#include <test/TestHelper.h>
#include <spatialops/structured/FieldHelper.h>

#include <spatialops/structured/FieldComparisons.h>

#include <iostream>

using namespace SpatialOps;
using std::cout;
using std::endl;

typedef SVolField Field;

struct FTest0 {
    double operator()(double i1, double i2, double i3, double i4) const {
        return i1 + i2 + i3 + i4;
    }
};

struct FTest1 {
    const double & val_;

    FTest1(const double & val) : val_(val) {}

    double operator()(double i1, double i2, double i3) const {
        return i1 + i2 + i3 + val_;
    }
};

int main( int iarg, char* carg[] )
{
    int nx, ny, nz;
    const IntVec hasBC( false, false, false );

    nx = 11;
    ny = 11;
    nz = 11;

    const int nghost = 1;
    const GhostData ghost(nghost);
    const BoundaryCellInfo bcinfo = BoundaryCellInfo::build<Field>(hasBC,hasBC);
    const MemoryWindow window( get_window_with_ghost(IntVec(nx,ny,nz),ghost,bcinfo) );

    Field input1( window, bcinfo, ghost, NULL );
    Field input2( window, bcinfo, ghost, NULL );
    Field input3( window, bcinfo, ghost, NULL );
    Field  test1( window, bcinfo, ghost, NULL );
    Field  test2( window, bcinfo, ghost, NULL );
    Field    ref( window, bcinfo, ghost, NULL );
    const int total = nx * ny * nz;

    initialize_field(input1, 0.0);
    initialize_field(input2, total);
    initialize_field(input3, total);

    TestHelper status(true);

    ref <<= (input1 + input2) + input3 + 5.0 + 6.0;

    /*
     * A few notes on apply_pointwise factory usage:
     *   - Arguments supplied to the factory are held by reference - not value.
     *     So be sure to not allow them to go out of scope during the lifetime
     *     of the factory
     *   - It is best to construct the factory inline with the apply_pointwise statement.
     */
    test1 <<= apply_pointwise(factory<FTest0>(), input1 + input2, input3, 5.0, 6.0);
    test2 <<= apply_pointwise(factory<FTest1>(6.0), input1 + input2, input3, 5.0);

#   ifndef NEBO_GPU_TEST
    status( field_equal(ref, test1, 0.0), "simple pointwise 1" );
    status( field_equal(ref, test2, 0.0), "simple pointwise 2" );
#   endif

    if( status.ok() ) {
      cout << "ALL TESTS PASSED :)" << endl;
      return 0;
    }
    else {
        cout << "******************************" << endl
             << " At least one test FAILED! :(" << endl
             << "******************************" << endl;
        return -1;
    };
}
