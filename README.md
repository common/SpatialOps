The SpatialOps library provides support for operations on fields primarily on structured meshes.

# Questions?
For questions, please contact [Professor Sutherland](http://www.che.utah.edu/~sutherland)

# More Information
For more details, please see the [Doxygen documentation](https://jenkins.multiscale.utah.edu/job/SpatialOps/doxygen/).